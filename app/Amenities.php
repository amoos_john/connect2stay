<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenities extends Model {
    protected $table='amenities';
    
    public  function child()
    {
        return $this->hasMany('App\Amenities', 'parent_id', 'id');
    }     
     
}
