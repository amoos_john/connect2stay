<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Attractions extends Model {
    protected $table='attractions';
    
    public static $status=[""=>"Select Status","1"=>"Active","0"=>"Inactive","2"=>"Pending Approval"];
    

    public static function search($search)
    {
        $result =  DB::table('attractions');
                
        if(isset($search['type']) && $search['type']!="") 
        {
            $result = $result->where('type','=',$search['type']);
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('status','=',$search['status']);
        }
        
        //$result = $result->orderBy("start","asc");
        $result = $result->paginate(10);
        return $result;
    }
    public static $type=[""=>"Select Type",
                                "512" => "Spring Attraction",
				"513" => "Summer Attraction",
				"514" => "Fall Attraction",
				"515" => "Winter Attraction",
				"516" => "All Seasons Attraction",
				"92" => "Airport",
				"93" => "Amusement Park",
				"94" => "Beach ",
				"494" => "Boat Rentals",
				"95" => "Camping Ground ",
				"96" => "Casino",
				"97" => "Church",
				"98" => "Cinema",
				"99" => "College/University",
				"100" => "Company",
				"101" => "Concert Hall",
				"102" => "Convention Center",
				"103" => "Court House",
				"104" => "Dentist",
				"105" => "Doctor",
				"106" => "Embassy",
				"107" => "Exhibition Center",
				"108" => "Ferry Terminal",
				"478" => "Fishing",
				"481" => "Fitness Center",
				"109" => "Gas Station",
				"110" => "Golf Course",
				"111" => "Government Office",
				"479" => "Grocery Store",
				"112" => "Hospital",
				"113" => "Hotel/Motel",
				"114" => "Ice Skating Ring",
				"115" => "Library",
				"493" => "Marina",
				"482" => "Miniature Golf",
				"116" => "Mountain Peak",
				"117" => "Museum",
				"118" => "Nightlife",
				"119" => "Open Parking",
				"467" => "Outdoor Activities",
				"120" => "Park And Recreation Area",
				"121" => "Parking Garage",
				"122" => "Pharmacy",
				"483" => "Photography",
				"123" => "Place of Worship",
				"124" => "Police Station",
				"125" => "Post Office",
				"484" => "Property Management",
				"468" => "Recreation",
				"126" => "Rental Car Facility",
				"127" => "Rental Parking Lot",
				"128" => "Rest Area",
				"129" => "Restaurant",
				"285" => "Salon And Spa",
				"487" => "Scuba And Snorkeling",
				"130" => "Shopping",
				"480" => "Shopping - Outlet Mall",
				"131" => "Shopping Center",
				"488" => "Ski And Snowboard Equipment",
				"132" => "Skiing Facility",
				"133" => "Sports Center",
				"134" => "Tennis Court",
				"135" => "Theatre",
				"411" => "Theme Park",
				"136" => "Tourist Attraction",
				"137" => "Tourist Information Office",
				"322" => "Tours",
				"138" => "Train Station",
				"139" => "Veterinarian",
				"412" => "Water Park",
				"323" => "Water Sports",
				"140" => "Winery",
				"141" => "Yacht Basin ",
				"142" => "Zoo",
				"143" => "Other"
    ];    
       
}
