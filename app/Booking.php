<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Session;

class Booking extends Model {
    protected $table='booking';
    
    public  function booking_information()
    {
        return $this->hasOne('App\BookingInformation', 'booking_id', 'id');
    }
    public  function property()
    {
        return $this->hasOne('App\Properties', 'id', 'property_id');
    }
    
  
    public static $status=[""=>"Select Status","1"=>"Pending Confirmation",
		"2"=>"Confirmed",
		"3"=>"Checked In",
		"4"=>"Checked-Out",
		"5"=>"Cancelled",
		"6"=>"Held"];

    public static $payment_status = [""=>"Select Status","success"=>"success",
		"cancel"=>"cancel"]; 
      
    public static $payment_method=[""=>"Select Payment Method","paypal"=>"Paypal",
		"stripe"=>"Stripe"];    
    
    public static function search($search,$owner_id="")
    {
        
        $result = DB::table("booking as book")
        ->join("properties as pro","book.property_id","=","pro.id")
        ->join("booking_information as info","info.booking_id","=","book.id");
        if($owner_id!="") 
        {
            $result = $result->join("units as uni","uni.property_id","=","book.property_id");
        }
        $result = $result->select("pro.internal_name","book.*","info.first_name","info.last_name");
        
                
        if(isset($search['id']) && is_numeric($search['id'])!="") 
        {
            $result = $result->where('book.id','=',$search['id']);
        }
        if(isset($search['property_id']) && $search['property_id']!="") 
        {
            $result = $result->where('book.property_id','=',$search['property_id']);
        }
        if(isset($search['first_name']) && $search['first_name']!="") 
        {
             $result = $result->where('info.first_name','LIKE',$search['first_name']."%");
        }
        if(isset($search['last_name']) && $search['last_name']!="") 
        {
             $result = $result->where('info.last_name','LIKE',$search['last_name']."%");
        } 
        if(isset($search['renter_id']) && is_numeric($search['renter_id'])) 
        {
            $result = $result->where('info.renter_id','=',$search['renter_id']);
        }
        if((isset($search['checkin_from']) && $search['checkin_from']!="") || 
        (isset($search['checkin_to']) && $search['checkin_to']!="")) 
        {
             $result = $result->whereBetween('book.checkin',array($search['checkin_from'],$search['checkin_to']));
        }
        if((isset($search['checkout_from']) && $search['checkout_from']!="") || 
        (isset($search['checkout_to']) && $search['checkout_to']!="")) 
        {
             $result = $result->whereBetween('book.checkin',array($search['checkout_from'],$search['checkout_to']));
        }
        if(isset($search['payment_method']) && $search['payment_method']!="") 
        {
            $result = $result->where('book.payment_type','=',$search['payment_method']);
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('book.status','=',$search['status']);
        }
        if($owner_id!="") 
        {
            $result = $result->where('uni.owner_id','=',$owner_id);
        }
        
        $result = $result->whereNull("pro.deleted_at");
        $result = $result->where("book.deleted","!=",1);
        $result = $result->orderBy("id","desc");
        $result = $result->paginate(10);
        return $result;
    }
    
    public static $lead_source = [""=>"Select Source","0.facebook.com",
                        "9Flats",
                        "AirBnB",
                        "AlphaHolidayLettings.com",
                        "Always on Vacation",
                        "Amazing Accom - Luxury Holiday Houses",
                        "Apartment Barcelona",
                        "Atraveo",
                        "Barcelona Point",
                        "Be Mate",
                        "bedandbreakfast.com",
                        "Bing",
                        "bookabach.co.nz","BOOKING.COM",
                        "booktmail.com","Casamundo","Cities Reference","City Getaway","Despegar.com","Divino Villas","E-Domizil","Emailed Link","Escalea.com","Flat4Day.com","Flipkey","Flyer","Friday Flats","Friend","Friendly Rentals","Furnished Rentals","Get Me An Apartment","google","google","google","google","GowithOh.com","Habitat Apartments","HitRental","Holiday Velvet","HolidayApartments.co.za","Holidu.es","homeaway.gr","homeaway.ie","homeaway.pl","homeaway.ru","homeaway.tr","homyspace.com","HouseTrip","I'm a previous customer","itravex.com","La Comunity","Locasun.fr","londonchoice.com","Loving Apartments","Magazine","Magic Event","Migoa","OK Apartment","Only Apartments ","Other Website","Packlate","Private Homes","Rated Apartments","RedAwning","Rent The Sun","RentalsCombined","Rentxpress","Roomorama","Spain Holiday","stayz.com.au","Tourico Holidays","travelmob.com","Travelopo.com","TravelStaytion.com","VacAgent","vacationrentals.com","Vive Unique","VRguest.com","Way To Stay","WikkiWi.com","Wimdu","Yahoo","yesbookit.com","Zhubaijia.com","abritel.fr","aluguetemporada.com.br","fewo-direkt.de","HomeAway (Email Leads)","homeaway.at","homeaway.co.uk","homeaway.com","homeaway.com.ar","homeaway.dk","homeaway.es","homeaway.fi","homeaway.it","homeaway.nl","homeaway.pt","homeaway.se","homelidays.com","homelidays.es","homelidays.it","indicação","outro","ownersdirect.co.uk","vrbo.com"];
                            
    
    
    public static $countries =  ["Afghanistan"=>"Afghanistan",
		"Albania"=>"Albania",
		"Algeria"=>"Algeria",
		"American Samoa"=>"American Samoa",
		"Andorra"=>"Andorra",
		"Angola"=>"Angola",
		"Anguilla"=>"Anguilla",
		"Antigua"=>"Antigua",
		"Antigua And Barbuda"=>"Antigua And Barbuda",
		"Argentina"=>"Argentina",
		"Armenia"=>"Armenia",
		"Aruba"=>"Aruba",
		"Australia"=>"Australia",
		"Austria"=>"Austria",
		"Azerbaijan"=>"Azerbaijan",
		"The Bahamas"=>"The Bahamas",
		"Bahrain"=>"Bahrain",
		"Bangladesh"=>"Bangladesh",
		"Barbados"=>"Barbados",
		"Belarus"=>"Belarus",
		"Belgium"=>"Belgium",
		"Belize"=>"Belize",
		"Benin"=>"Benin",
		"Bermuda"=>"Bermuda",
		"Bhutan"=>"Bhutan",
		"Bolivia"=>"Bolivia",
		"Bosnia And Herzegovina"=>"Bosnia And Herzegovina",
		"Botswana"=>"Botswana",
		"Brazil"=>"Brazil",
		"British Virgin Islands"=>"British Virgin Islands",
		"Brunei Darussalam"=>"Brunei Darussalam",
		"Bulgaria"=>"Bulgaria",
		"Burkina Faso"=>"Burkina Faso",
		"Burundi"=>"Burundi",
		"Cambodia"=>"Cambodia",
		"Cameroon"=>"Cameroon",
		"Canada"=>"Canada",
		"Cape Verde"=>"Cape Verde",
		"Cayman Islands"=>"Cayman Islands",
		"Central African Republic"=>"Central African Republic",
		"Chad"=>"Chad",
		"Chile"=>"Chile",
		"China"=>"China",
		"Colombia"=>"Colombia",
		"Comoros"=>"Comoros",
		"Congo Democratic Republicof"=>"Congo Democratic Republicof",
		"Congo Republicof"=>"Congo Republicof",
		"Cook Islands"=>"Cook Islands",
		"Costa Rica"=>"Costa Rica",
		"Coted Ivoire"=>"Coted Ivoire",
		"Croatia"=>"Croatia",
		"Cuba"=>"Cuba",
		"Northern Cyprus"=>"Northern Cyprus",
		"Turkish Republic Of Northern Cyprus"=>"Turkish Republic Of Northern Cyprus",
		"Cyprus"=>"Cyprus",
		"Czech Republic"=>"Czech Republic",
		"Denmark"=>"Denmark",
		"Djibouti"=>"Djibouti",
		"Dominica"=>"Dominica",
		"Dominican Republic"=>"Dominican Republic",
		"Ecuador"=>"Ecuador",
		"Egypt"=>"Egypt",
		"El Salvador"=>"El Salvador",
		"Equatorial Guinea"=>"Equatorial Guinea",
		"Eritrea"=>"Eritrea",
		"Estonia"=>"Estonia",
		"Ethiopia"=>"Ethiopia",
		"Falkland Islands Malvinas"=>"Falkland Islands Malvinas",
		"Faroe Islands"=>"Faroe Islands",
		"Fiji"=>"Fiji",
		"Finland"=>"Finland",
		"France"=>"France",
		"French Guiana"=>"French Guiana",
		"French Polynesia"=>"French Polynesia",
		"Gabon"=>"Gabon",
		"Gambia"=>"Gambia",
		"Georgia"=>"Georgia",
		"Germany"=>"Germany",
		"Ghana"=>"Ghana",
		"Gibraltar"=>"Gibraltar",
		"Greece"=>"Greece",
		"Greenland"=>"Greenland",
		"Grenada"=>"Grenada",
		"Guadeloupe"=>"Guadeloupe",
		"Guam"=>"Guam",
		"Guatemala"=>"Guatemala",
		"Guinea"=>"Guinea",
		"Guinea Bissau"=>"Guinea Bissau",
		"Guyana"=>"Guyana",
		"Haiti"=>"Haiti",
		"Honduras"=>"Honduras",
		"Hong Kong"=>"Hong Kong",
		"Hungary"=>"Hungary",
		"Iceland"=>"Iceland",
		"India"=>"India",
		"Indonesia"=>"Indonesia",
		"Iran"=>"Iran",
		"Iraq"=>"Iraq",
		"Ireland"=>"Ireland",
		"Israel"=>"Israel",
		"Italy"=>"Italy",
		"Jamaica"=>"Jamaica",
		"Japan"=>"Japan",
		"Jordan"=>"Jordan",
		"Kazakhstan"=>"Kazakhstan",
		"Kenya"=>"Kenya",
		"Kiribati"=>"Kiribati",
		"Korea North"=>"Korea North",
		"Korea South"=>"Korea South",
		"Kuwait"=>"Kuwait",
		"Kyrgyzstan"=>"Kyrgyzstan",
		"Laos"=>"Laos",
		"Latvia"=>"Latvia",
		"Lebanon"=>"Lebanon",
		"Lesotho"=>"Lesotho",
		"Liberia"=>"Liberia",
		"Libya"=>"Libya",
		"Liechtenstein"=>"Liechtenstein",
		"Lithuania"=>"Lithuania",
		"Luxembourg"=>"Luxembourg",
		"Macau"=>"Macau",
		"Macedonia"=>"Macedonia",
		"Madagascar"=>"Madagascar",
		"Malawi"=>"Malawi",
		"Malaysia"=>"Malaysia",
		"Maldives"=>"Maldives",
		"Mali"=>"Mali",
		"Malta"=>"Malta",
		"Marshall Islands"=>"Marshall Islands",
		"Martinique"=>"Martinique",
		"Mauritania"=>"Mauritania",
		"Mauritius"=>"Mauritius",
		"Mayotte"=>"Mayotte",
		"Mexico"=>"Mexico",
		"Micronesia"=>"Micronesia",
		"Moldova"=>"Moldova",
		"Monaco"=>"Monaco",
		"Mongolia"=>"Mongolia",
		"Montserrat"=>"Montserrat",
		"Morocco"=>"Morocco",
		"Mozambique"=>"Mozambique",
		"Myanmar"=>"Myanmar",
		"Namibia"=>"Namibia",
		"Nauru"=>"Nauru",
		"Nepal"=>"Nepal",
		"Netherlands"=>"Netherlands",
		"Netherlands Antilles"=>"Netherlands Antilles",
		"New Caledonia"=>"New Caledonia",
		"New Zealand"=>"New Zealand",
		"Nicaragua"=>"Nicaragua",
		"Niger"=>"Niger",
		"Nigeria"=>"Nigeria",
		"Niue"=>"Niue",
		"Norfolk Island"=>"Norfolk Island",
		"Northern Mariana Islands"=>"Northern Mariana Islands",
		"Norway"=>"Norway",
		"Oman"=>"Oman",
		"Pakistan"=>"Pakistan",
		"Palau"=>"Palau",
		"Palestinian Territory Occupied"=>"Palestinian Territory Occupied",
		"Panama"=>"Panama",
		"Papua New Guinea"=>"Papua New Guinea",
		"Paraguay"=>"Paraguay",
		"Peru"=>"Peru",
		"Philippines"=>"Philippines",
		"Poland"=>"Poland",
		"Portugal"=>"Portugal",
		"Puerto Rico"=>"Puerto Rico",
		"Qatar"=>"Qatar",
		"Reunion"=>"Reunion",
		"Romania"=>"Romania",
		"Russia"=>"Russia",
		"Rwanda"=>"Rwanda",
		"Saint Helena"=>"Saint Helena",
		"Saint Kitts And Nevis"=>"Saint Kitts And Nevis",
		"Saint Lucia"=>"Saint Lucia",
		"Saint Pierre And Miquelon"=>"Saint Pierre And Miquelon",
		"Saint Vincent Andthe Grenadines"=>"Saint Vincent Andthe Grenadines",
		"Samoa"=>"Samoa",
		"San Marino"=>"San Marino",
		"Sao Tome And Principe"=>"Sao Tome And Principe",
		"Saudia Arabia"=>"Saudia Arabia",
		"Senegal"=>"Senegal",
		"Seychelles"=>"Seychelles",
		"Sierra Leone"=>"Sierra Leone",
		"Singapore"=>"Singapore",
		"Slovakia"=>"Slovakia",
		"Slovenia"=>"Slovenia",
		"Solomon Islands"=>"Solomon Islands",
		"Somalia"=>"Somalia",
		"South Africa"=>"South Africa",
		"Spain"=>"Spain",
		"Sri Lanka"=>"Sri Lanka",
		"Sudan"=>"Sudan",
		"Suriname"=>"Suriname",
		"Svalbard And Jan Mayen Islands"=>"Svalbard And Jan Mayen Islands",
		"Swaziland"=>"Swaziland",
		"Sweden"=>"Sweden",
		"Switzerland"=>"Switzerland",
		"Syria"=>"Syria",
		"Taiwan"=>"Taiwan",
		"Tajikistan"=>"Tajikistan",
		"Tanzania"=>"Tanzania",
		"Thailand"=>"Thailand",
		"Togo"=>"Togo",
		"Tonga"=>"Tonga",
		"Trinidad And Tobago"=>"Trinidad And Tobago",
		"Tunisia"=>"Tunisia",
		"Turkey"=>"Turkey",
		"Turkmenistan"=>"Turkmenistan",
		"Turks And Caicos Islands"=>"Turks And Caicos Islands",
		"Turks And Caicos"=>"Turks And Caicos",
		"Tuvalu"=>"Tuvalu",
		"Uganda"=>"Uganda",
		"Ukraine"=>"Ukraine",
		"United Arab Emirates"=>"United Arab Emirates",
		"United Kingdom"=>"United Kingdom",
		"United States"=>"United States",
		"United States Virgin Islands"=>"United States Virgin Islands",
		"US Virgin Islands"=>"US Virgin Islands",
		"Uruguay"=>"Uruguay",
		"Uzbekistan"=>"Uzbekistan",
		"Vanuatu"=>"Vanuatu",
		"Vatican City State Holy See"=>"Vatican City State Holy See",
		"Venezuela"=>"Venezuela",
		"Vietnam"=>"Vietnam",
		"Wallis And Futuna Islands"=>"Wallis And Futuna Islands",
		"Western Sahara"=>"Western Sahara",
		"Yemen"=>"Yemen",
		"Zambia"=>"Zambia",
		"Zimbabwe"=>"Zimbabwe",
		"Saint Martin"=>"Saint Martin",
		"Sint Maarten"=>"Sint Maarten",
		"St Barts"=>"St Barts",
		"St Barthelemy"=>"St Barthelemy",
		"Saint Barthelemy"=>"Saint Barthelemy",
		"Curacao"=>"Curacao",
		"Caribbean Netherlands"=>"Caribbean Netherlands",
		"Serbia"=>"Serbia",
		"Montenegro"=>"Montenegro"];
       
}
