<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class BookingItems extends Model {
    protected $table='booking_items';
  
    public static $type=[""=>"Select Type",'1'=>'Deposit',
	'2'=>'Rent',
	'3'=>'Tax',
	'4'=>'Fee',
	'5'=>'Other',
	'6'=>'Discounts',
	'7'=>'Commission',
	'8'=>'Insurance'];
    
       
}
