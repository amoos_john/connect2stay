<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class BookingRules extends Model {
    protected $table='booking_rules';
    
    public static $status=[""=>"Select Status","1"=>"Active","0"=>"Inactive"];
    
    public static $type=[""=>"Select Type","1"=>"Min Stay","2"=>"Booking Charge Rules"];
    
    public static $applicability=["0"=>"Applies to All Properties","1"=>"Property Specific"];
    
    public static $charge_type=["0"=>"Percent","1"=>"Flat Fee","2"=>"Nights"];
    
    public static $reminder_type=["0"=>"Days After Booking","1"=>"Days Before Check-in","2"=>"Days Before Check-out"];

    public static $processor=["0"=>"[Do Not Process Credit Card]","1"=>"Credit Card Link with Spreedly"];
    
    public static function search($search)
    {
        $result =  DB::table('booking_rules');
                
        if(isset($search['type']) && $search['type']!="") 
        {
            $result = $result->where('type','=',$search['type']);
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('status','=',$search['status']);
        }
        
        $result = $result->orderBy("start","asc");
        $result = $result->paginate(20);
        return $result;
    }
    public static function getBookingRules($id)
    {
        $result =  BookingRules::join("property_booking_rules as rule","rule.rules_id","=","booking_rules.id")
       ->select("booking_rules.*","rule.property_id")
       ->where("rule.property_id","=",$id)
       ->where('status','=',1)
       ->ORwhere('booking_rules.applicability','=',0)
       ->orderby("name","asc")
       ->groupby('booking_rules.id')->get();
         
        return $result;
    } 
    
}
