<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model {
    protected $table='contacts';
    
    public static $lead_funnel=[""=>"Select Lead Funnel","Closed Sale"=>"Closed Sale",
    "Incubation Lead"=>"Incubation Lead","Lead Not Yet Qualified"=>"Lead Not Yet Qualified",
    "Lead Qualified"=>"Lead Qualified","Not a Lead"=>"Not a Lead",
    "Past Customer"=>"Past Customer"];
    
       
}
