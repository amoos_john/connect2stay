<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class FeesTaxes extends Model {
    protected $table='fees_taxes';
    
    public static $status=[""=>"Select Status","1"=>"Active","0"=>"Inactive"];
    
    public static $type=[""=>"Select Type","1"=>"Cleaning Fees","2"=>"Extra Person Fee","3"=>"Other Taxes"];
   
    public static $applicability=["0"=>"Applies to All Properties","1"=>"Property Specific"];

    public static $amount_type=["0"=>"Flat Rate","1"=>"Percent"];
    
    public static $input_type=["0"=>"Mandatory Fee","1"=>"Optional Checkbox","2"=>"Optional Textbox"];
    
    public static function search($search)
    {
        $result =  DB::table('fees_taxes');
                
        if(isset($search['type']) && $search['type']!="") 
        {
            $result = $result->where('type','=',$search['type']);
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('status','=',$search['status']);
        }
        
        $result = $result->paginate(20);
        return $result;
    }
    
    public  function property_taxes_fees()
    {
        return $this->hasOne('App\PropertyFeesTaxes', 'feestaxes_id', 'id');
    }
    
    public static function getFees($id)
    {
        $fees=FeesTaxes::join("property_fees_taxes as fees","fees.feestaxes_id","=","fees_taxes.id")
        ->select("fees_taxes.*","fees.property_id")
        ->where("status","=",1)
        ->where("fees.property_id","=",$id)
        ->ORwhere('fees_taxes.applicability','=',0)
        ->whereIn("fees_taxes.type",array(1,2))
        ->orderby("name","asc")
        ->groupby('fees_taxes.id')->get();
           
        return $fees;
    }
    public static function getTaxes($id)
    {
        $taxes=FeesTaxes::join("property_fees_taxes as tax","tax.feestaxes_id","=","fees_taxes.id")
        ->select("fees_taxes.*","tax.property_id")
        ->where("status","=",1)
        ->where("tax.property_id","=",$id)
        ->ORwhere('fees_taxes.applicability','=',0)
        ->where("fees_taxes.type","=",3)
        ->orderby("name","asc")
        ->groupby('fees_taxes.id')
        ->get();       
                
       
        return $taxes;
    }
    public static function getFeesTaxes($id)
    {
        $taxes=FeesTaxes::join("property_fees_taxes as tax","tax.feestaxes_id","=","fees_taxes.id")
        ->select("fees_taxes.*","tax.property_id")
        ->where("tax.property_id","=",$id)
        ->where("status","=",1)
        ->ORwhere('fees_taxes.applicability','=',0)
        ->whereDate('fees_taxes.effective', '<=', date('Y-m-d'))
        ->whereDate('fees_taxes.end', '>', date('Y-m-d'))
        ->orderby("name","asc")
        ->groupby('fees_taxes.id')
        ->get();       
         /* ->whereDate('fees_taxes.effective', '>=', date('Y-m-d'))
          */
        return $taxes;
    }
    

   
        
       
}
