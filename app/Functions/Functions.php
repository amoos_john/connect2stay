<?php

namespace App\Functions;

use Intervention\Image\Facades\Image as Image;
use Session;

class Functions {

    public static function prettyJson($inputArray, $statusCode) {
        return response()->json($inputArray, $statusCode, array('Content-Type' => 'application/json'), JSON_PRETTY_PRINT);
    }

    public static function saveImage($file, $destinationPath, $destinationPathThumb = '') {
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(111, 999) . time() . '.' . $extension;
        $image = $destinationPath . '/' . $fileName;
        $upload_success = $file->move($destinationPath, $fileName);
        //Functions::saveThumbImage($image,'fit',$destinationPath.$fileName);
        return $fileName;
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789';
        //abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public static function MoneyFormat($price)
    {
        $price=number_format($price,2);
        return $price;
    }
    public static function DateFormat($date)
    {
        $dates=date("d/m/Y",strtotime($date));
        return $dates;
    }
    public static function nights($start_date,$end_date)
    {
        $date1=date_create($start_date);
        $date2=date_create($end_date);
        $diff=date_diff($date1,$date2);
        $nights=$diff->format("%a");
        return $nights;
    }
    public static function setEmailTemplate($contentModel, $replaces) {
        $data['body'] = $contentModel[0]->body;
        $data['subject'] = $contentModel[0]->subject;
        $data['title'] = $contentModel[0]->title;
        foreach ($replaces as $key => $replace) {
            $data['body'] = str_replace("%%" . $key . "%%", $replace, $data['body']);
        }

        return $data;
    }
    public static function setEmailSubject($contentModel, $replaces) {
        
        $data['subject'] = $contentModel[0]->subject;
        
        foreach ($replaces as $key => $replace) {
            $data['subject'] = str_replace("%%" . $key . "%%", $replace, $data['subject']);
        }

        return $data;
    }
    public static function sendEmail($email,$subject,$body,$header='',$from,$cc="",$bcc="")
    {
        // To send HTML mail, the Content-type header must be set
        if($header=='')
        {
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: Connect2Stay <'.$from.'>' . "\r\n";
            $headers .= 'Reply-To: '.$from."\r\n";
            $headers .= 'Cc: amoos.golpik@gmail.com' . "\r\n";
            //$headers .= 'To: '.$email.'' . "\r\n";
            //$headers .= 'From: '.$from.'' . "\r\n";
            if($cc!=""){
                $headers .= 'Cc: '.$cc. "\r\n";
            }
            if($bcc!=""){
                $headers .= 'Bcc: '.$bcc. "\r\n";
            }
        }                  
        //$body = "<title></title><style></style></head><body>".$body."</body></html>";
        return mail($email,$subject,$body,$headers);     
    }
    public static function getBooking($key)
    {
        $row = Session::get($key);
        
        $data['start_date'] = $row["start_date"];
        $data['end_date'] = $row["end_date"];
        $data['guest'] = $row["guest"];
        $data['children'] = $row["children"];
        $data['id'] = $row["id"];
        
        return $data;
        
    }
    public static function setBooking($start_date,$end_date,$guest,$children,$id)
    {
        $key = Functions::generateRandomString(6);
        
        $item = [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'guest' => $guest,
            'children' => $children,
            'id' => $id,
          ];
        
        Session::put($key,$item);
        
        
        return $key;
        
        /*if($start_date!='')
        {
            Session::put('start_date',$start_date);
        }
        else
        {
            Session::forget('start_date');
        }
        if($end_date!='')
        {
            Session::put('end_date',$end_date);
        }
        else
        {
            Session::forget('end_date');
        }
        if($guest!='')
        {
            Session::put('guest',$guest);
        }
        else
        {
            Session::forget('guest');
        }
        if($children!='')
        {
            Session::put('children',$children);
        }
        else
        {
            Session::forget('children');
        }*/
    }
    public static function showDate($date)
    {
        $dates=date("d/m/Y",strtotime($date));
        return $dates;
    }
}
