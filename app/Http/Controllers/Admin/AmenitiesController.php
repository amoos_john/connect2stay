<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\Amenities;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Auth;

class AmenitiesController extends AdminController {

   
    public function __construct() 
    {
        parent::__construct();   
    }

    public function index() {
        $model = Amenities::where("parent_id","!=",0)->orderby("id","desc")->paginate(10);
        return view('admin.amenities.index', ['model' => $model]);
    }

    public function create() {
        $parents = Amenities::where("parent_id","=",0)->orderby("sort_order","desc")->get();
        return view('admin.amenities.create',compact('parents'));
    }

    public function insert(Request $request) {
        
        $validation = array(
            'name' => 'required|max:100',

            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            if($request->parent_id=='')
            {
                $parent=0;
                $sort_parent = Amenities::where("parent_id","=",0)->orderby("sort_order","desc")->first();
                $sort_order = $sort_parent->sort_order + 1;
            }
            else
            {
                $parent=$request->parent_id;
                $sort_order = '';
            }
            $amenity = new Amenities;
            $amenity->name = $request->name;
            $amenity->parent_id = $parent;
            $amenity->sort_order = $sort_order;
            $amenity->save();

            \Session::flash('success', 'Amenity Added Successfully!');
            return redirect('admin/amenities');
        }
        
       
    }

    public function edit($id) {
        $model = Amenities::findOrFail($id);
        $parents = Amenities::where("parent_id","=",0)->get();
        return view('admin.amenities.edit', compact('model','parents'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $user = Amenities::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);
      
        $validation = array(
            'name' => 'required|max:100',

            );
       
       
            $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput();

        }
        if($request->parent_id=='')
        {
                $input['parent_id']=0;
        }
       else
       {
                $input['parent_id']=$request->parent_id;
       }
        $affectedRows = Amenities::where('id', '=', $id)->update($input);
        
        
        \Session::flash('success', 'Updated Successfully!');
        return redirect('admin/amenities');
    }

    public function delete($id) {
        $row = Amenities::where('id', '=', $id)->delete();
        \Session::flash('success', 'Amenity Deleted Successfully!');
        return redirect('admin/amenities');
    }

}
