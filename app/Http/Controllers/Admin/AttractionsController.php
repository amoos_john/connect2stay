<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\Attractions;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Gallery;
use App\GalleryImage;

class AttractionsController  extends AdminController {

    public function __construct() 
    {
        parent::__construct();
    }

    public function index() 
    {
        $model = Attractions::paginate(10);
        
        $status=Attractions::$status;
                
        $type=Attractions::$type;

        return view('admin.attractions.index', compact('model','status','type'));
    }
    public function search(Request $request)
    {
        $types = $request->input('type');
        $status_id = $request->input('status');
      
        $search['type']=$types;
        $search['status']=$status_id;
        
        $model=Attractions::search($search);
        
        $status=Attractions::$status;
        
        $type=Attractions::$type;
        
        return view('admin.attractions.index', compact('model','status','type','status_id','types'));
    }

    public function create(Request $request) {
       
        $status=Attractions::$status;
        
        $type=Attractions::$type;
       
        return view('admin.attractions.create',compact('status','type'));
    }

     public function insert(Request $request) {
        
        $validation = array(
            'name' => 'required|max:255',
            'public_name' => 'required|max:255',
            'type' => 'required',
            'status' => 'required',
            'type' => 'required',
            'address_1' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',    
            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $gallery=new Gallery;
            $gallery->title=$request->name;  
            $gallery->type=3;
            $gallery->save();
            
            $attr = new Attractions;
            $attr->name = $request->name;
            $attr->public_name = $request->public_name;
            $attr->status = $request->status;
            $attr->gallery_id=$gallery->id;
            $attr->type = $request->type;
            $attr->summary = $request->summary;
            $attr->description = $request->description;
            $attr->email=$request->email;
            $attr->fax=$request->fax;
            $attr->phone=$request->phone;  
            $attr->phone2=$request->phone2; 
            $attr->toll_free=$request->toll_free;
            $attr->website=$request->website; 
            $attr->address=$request->address;
            $attr->address_1=$request->address_1;
            $attr->address_2=$request->address_2;
            $attr->city=$request->city;
            $attr->country=$request->country;
            $attr->neighborhood=$request->neighborhood;
            $attr->county=$request->county;
            $attr->metro=$request->metro;
            $attr->state=$request->state;
            $attr->postal_code=$request->postal_code;
            $attr->region=$request->region;
            $attr->longitude=$request->longitude;
            $attr->latitude=$request->latitude;
            
            $attr->save();
            
            \Session::flash('success', 'Attraction Added Successfully!');
            return redirect('admin/attractions');
        }
        
       
    }
    
    public function edit($id) {
        $model = Attractions::findOrFail($id);
        
        $type=Attractions::$type;
       
        $status=Attractions::$status;
        
        $gallery_id=$model->gallery_id;
       
        return view('admin.attractions.edit',compact('model','status','type','gallery_id'))->with('id', $id);
        
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $attrial = Attractions::findOrFail($id);
        $input = $request->all();
      
        unset($input['_token']);
        
        
       $validation = array(
            'name' => 'required|max:255',
            'public_name' => 'required|max:255',
            'type' => 'required',
            'status' => 'required',
            'type' => 'required',
            'address_1' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',    
            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
            
            $input['name'] = $request->name;
            $input['public_name'] = $request->public_name;
            $input['status'] = $request->status;
            $input['type'] = $request->type;
            $input['summary'] = $request->summary;
            $input['description'] = $request->description;
            $input['email']=$request->email;
            $input['fax']=$request->fax;
            $input['phone']=$request->phone;  
            $input['phone2']=$request->phone2; 
            $input['toll_free']=$request->toll_free;
            $input['website']=$request->website; 
            $input['address']=$request->address;
            $input['address_1']=$request->address_1;
            $input['address_2']=$request->address_2;
            $input['city']=$request->city;
            $input['country']=$request->country;
            $input['neighborhood']=$request->neighborhood;
            $input['county']=$request->county;
            $input['metro']=$request->metro;
            $input['state']=$request->state;
            $input['postal_code']=$request->postal_code;
            $input['region']=$request->region;
            $input['longitude']=$request->longitude;
            $input['latitude']=$request->latitude;
            
          
        
        if(count($request->image_id)>0)
         {
                $images=$request->image_id;
                $display_order=$request->display_order;
                $image_caption=$request->image_caption;
                $i=0;
                foreach($images as $image)
                {
                    $input_image["display_order"]=$display_order[$i];
                    $input_image["image_caption"]=$image_caption[$i];
                    
                    $gallery_image = GalleryImage::where("id",$image)->update($input_image);
                    
                    
                    $i++;
                }
          }
         unset($input['image_id']);
         unset($input['display_order']);
         unset($input['image_caption']);
         unset($input['change_id']);
         $affectedRows = Attractions::where('id', '=', $id)->update($input);

        \Session::flash('success', 'Attraction Updated Successfully!');
        return redirect('admin/attractions');
    }
    

    public function delete($id) {
        $row = Attractions::find($id)->delete();
        \Session::flash('success', 'Attraction Deleted Successfully!');
        return redirect('admin/attractions');
    }

}
