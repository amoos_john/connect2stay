<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect,
    Session,
    Config,
    Mail,DB;
use Illuminate\Http\Request;
use Auth;
use App\Booking;
use App\Properties;
use App\User;
use App\RatesandAvailability;
use App\FeesTaxes;
use App\BookingItems;
use App\BookingInformation;
use App\Functions\Functions;
use App\Settings;
use App\Content;
use Netshell\Paypal\Paypal;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
use App\Reviews;


class BookingController extends AdminController {

   
    public function __construct() 
    {
        $this->paypal= new PayPal();
	   
        $this->_apiContext = $this->paypal->ApiContext(
            config('paypal.express.client_id'),
            config('paypal.express.secret'));

        $this->_apiContext->setConfig(config('paypal.express.config'));
        
        parent::__construct(); 
        
    }

    public function index() 
    {
        $statuses = Booking::$status;
        $payment_method = Booking::$payment_method;
        
        $role_id = Auth::user()->role_id;

        if($role_id==1 || $role_id==2)
        {
            $properties = Properties::ActiveProperties();
            $page_title = "New house booking";
            
            $model = Booking::search('');
            
            $book_url = 'booking';
            $search_url = 'booking/search';
        }
        elseif($role_id==3)
        {
            $owner_id=Auth::user()->id;
       
            $date = date('Y-m-d');

            $properties = User::getOwnerProperty($owner_id,$date);
            
            $page_title = "New owner booking";
            
            $model = Booking::search('',$owner_id);
            
            $book_url = 'ownerbooking';
            $search_url = 'ownerbooking/search';
        }
        
        
        return view('admin.booking.index', compact("model","book_url","search_url","page_title","role_id","payment_method", "properties", "statuses"));
        
    }
    
    public function search(Request $request) {
        
        $id = $request->input('id');
        $property_id = $request->input('property_id');
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $renter_id = $request->input('renter_id');
        $checkin_from = $request->input('checkin_from');
        $checkin_to = $request->input('checkin_to');
        $checkout_from = $request->input('checkout_from');
        $checkout_to = $request->input('checkout_to');
        $status_id = $request->input('status');
        $method = $request->input('payment_method');
 
        $search['id'] = $id;
        $search['property_id'] = $property_id;
        $search['first_name'] = $first_name;
        $search['last_name'] = $last_name;
        $search['renter_id'] = $renter_id;
        $search['checkin_from'] = $checkin_from;
        $search['checkin_to'] = $checkin_to;
        $search['checkout_from'] = $checkout_from;
        $search['checkout_to'] = $checkout_to;
        $search['status'] = $status_id;
        $search['payment_method'] = $method;
        
        $role_id = Auth::user()->role_id;
        
        if($role_id==1 || $role_id==2)
        {
            $properties = Properties::ActiveProperties();
            $page_title = "New house booking";
            
            $model = Booking::search($search);
            
            $book_url = 'booking';
            $search_url = 'booking/search';
        }
        elseif($role_id==3)
        {
            $owner_id=Auth::user()->id;
       
            $date = date('Y-m-d');

            $properties = User::getOwnerProperty($owner_id,$date);
            
            $page_title = "New owner booking";
            
            $model = Booking::search($search,$owner_id);
            
            $book_url = 'ownerbooking';
            $search_url = 'ownerbooking/search';
        }

        
        $statuses = Booking::$status;
        
        $payment_method = Booking::$payment_method;

        
        return view('admin.booking.index', compact("model","book_url","search_url","page_title","role_id","payment_method", "properties", "statuses", "payment_method",
         "status_id", "first_name", "last_name", "renter_id", "id","property_id", "checkin_from", "checkin_to", "checkout_from","checkout_to","method"));
    
        
    }
    

    public function create() 
    {
        $user_id = Auth::user()->id;
        
        $role_id = Auth::user()->role_id;
        
        $model = User::find($user_id);
        
        $status = Booking::$status;
        
        $lead_source = Booking::$lead_source;
        
        $countries = Booking::$countries;
        
        $langauges = User::$langauges;
        
        $date = date("Y-m-d",strtotime("+1 day"));
        
        $nights = 4;
        
        $end_date = date ("Y-m-d", strtotime("+4 days", strtotime($date)));

        
        
        $setting = Settings::find(1); 
        
        if($role_id==1 || $role_id==2)
        {
            $users = User::orderby("name","asc")->where("role_id","!=",3)->lists("name", "id")->prepend('Select', '');
            
            $owners = User::orderby("name","asc")->where("role_id","=",3)->lists("name", "id")->prepend('Select', '');
           
            $properties = Properties::ActiveProperties();
            
            $book_url = 'booking';
        }
       elseif($role_id==3)
        {
            $properties = User::getOwnerProperty($user_id,$date);
            
            $book_url = 'ownerbooking';

        }
        
        return view('admin.booking.owner.create',compact('model','book_url','user_id','owners','users','role_id','nights','date','end_date','setting','properties','countries','langauges','status','lead_source'));
    }

    public function insert(Request $request) {
        
        $validation = array(
            'first_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'lead_source' => 'required|max:255',
            'country' => 'required',
            'checkin' => 'required|date|max:255',
            'checkout' => 'required|date|max:255',
            'adults' => 'required',
            'property_id' => 'required',

            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $grand_total = 0;
            $property_id = $request->property_id;
            $checkin = $request->checkin;
            $checkout = $request->checkout;
            $email = $request->email;
            $date = date("Y-m-d");
            
            $property=Properties::where("status","=",1)
            ->where("id","=",$property_id)->get();
            
            $date_diff = Functions::nights($checkin,$checkout);
            $min_stay=(count($property)>0)?$property[0]->min_stay:'';
            
            $checkdate = RatesandAvailability::checkDatesAvail($checkin,$checkout,$property_id);
            
            $error = array();
            if(count($property)==0)
            {   
                $error[] = "Property is Inactive!";

            }
            elseif($checkin <= $date)
            {
               $error[] = "Check in date must be at least 1 days from today.";
            }
            elseif($checkout<=$checkin)
            {
                $error[] = "CheckOut must be greater than CheckIn. ";

            }
            elseif($date_diff<$min_stay)
            {
                $error[] = 'This Property has a minimum stay of '.$min_stay.' nights.';

            }
            else if(count($checkdate)>0)
            {
                 $error[] = 'Unfortunately dates are not available on this property!';
            }
            
            if(count($error)>0)
            {
              return redirect()->back()->with("error",$error)->withInput();

            }
            else
            {
                $role_id = Auth::user()->role_id;
                
                $booking = new Booking;
                $booking->property_id = $property_id;
                $booking->checkin = $request->checkin;
                $booking->checkout = $request->checkout;
                $booking->checkin_time = $request->checkin_time;
                $booking->checkout_time = $request->checkout_time;
                $booking->adults = $request->adults;
                $booking->children = $request->children;
                $booking->payment_type = 'paypal';
                $booking->grand_total = $grand_total;
                $booking->status = 1;
                $booking->internal_notes = $request->internal_notes;
                if($role_id==1 || $role_id==2)
                {
                    $booking->booked_by = $request->booked_by;
                }
                $booking->save();

                $booking_id = $booking->id;

                $info = new BookingInformation;
                $info->booking_id = $booking_id;
                $info->first_name = $request->first_name;
                $info->last_name = $request->last_name;
                $info->email = $email;
                $info->phone = $request->phone;
                $info->lead_source = $request->lead_source;
                $info->special = $request->special;
                $info->country = $request->country;
                $info->address = $request->address;
                $info->address2 = $request->address2;
                $info->city = $request->city;
                $info->state = $request->state;
                $info->postal_code = $request->postal_code;
                $info->save();
                
                $update = RatesandAvailability::updateDates($checkin,$checkout,$property_id);

                \Session::flash('success', 'Booking added Successfully!');
                if($role_id==1 || $role_id==2)
                {
                    return redirect('admin/booking');
                }
                else
                {
                    return redirect('admin/ownerbooking');
                }
                
            }
        }
        
       
    }

    public function edit($id) 
    {
        $model = Booking::with("booking_information")->with("property")->findOrFail($id);
        
        $status = Booking::$status;
        
        $lead_source = Booking::$lead_source;
        
        $countries = Booking::$countries;
        
        $users = User::orderby("name","asc")->where("role_id","!=",3)->lists("name", "id")->prepend('Select', '');
        
        $properties = Properties::ActiveProperties();
        
        $nights=Functions::nights($model->checkin,$model->checkout);
        
        $items = BookingItems::where("booking_id","=",$id)->get();
                
        $type = BookingItems::$type;
        
        if($model->payment_type=='paypal')
        {
            $payment = \App\Paypal::where("booking_id","=",$id)->get();
        }
        elseif($model->payment_type=='stripe')
        {
            $payment = \App\Stripe::where("booking_id","=",$id)->get();
        }
        
        $payment_status = Booking::$payment_status;
            
        return view('admin.booking.edit', compact('model','payment_status','payment','countries','type','feestaxes','items','daily','total','rent','properties','nights','status','users','lead_source'))->with('id', $id);
    }

    public function update($id, Request $request) {
        
        $book = Booking::findOrFail($id);
        $req = $request->all();
        unset($req['_token']);
      
        $validation = array(
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'checkin' => 'required|max:255',
            'checkout' => 'required|max:255',
            'adults' => 'required',

            );
       
       $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();

        }
        else
        {
            $date = date("Y-m-d");
            $property_id = $request->property_id;
            
            $checkin = $request->checkin;
            $checkout = $request->checkout;
            
            if($book->checkin!=$checkin)
            {
                if($checkin<=$date)
                {
                   return redirect()->back()->with("error","Check in date must be at least 1 days from today.");

                }
            }
            if($book->property_id!=$property_id)
            {
                $check_date = RatesandAvailability::checkDatesAvail($checkin,$checkout,$property_id);
                
                if(count($check_date)>0)
                {
                    return redirect()->back()->with("error","Unfortunately dates are not available on this property!");
                }
            }
            
            $status = $request->status;
            
            $input['checkin'] = $request->checkin;
            $input['checkout'] = $request->checkout;
            $input['checkin_time'] = $request->checkin_time;
            $input['checkout_time'] = $request->checkout_time;
            $input['adults'] = $request->adults;
            $input['children'] = $request->children;
            $input['status'] = $request->status;
            $input['internal_notes'] = $request->internal_notes;
            $input['payment_status'] = $request->payment_status;
            
            $update_book = Booking::where("id","=",$id)->update($input);
            
            $input2['first_name'] = $request->first_name;
            $input2['last_name'] = $request->last_name;
            $input2['email'] = $request->email;
            $input2['phone'] = $request->phone;
            $input2['lead_source'] = $request->lead_source;
            $input2['special'] = $request->special;
            $input2['country'] = $request->country;
            $input2['address'] = $request->address;
            $input2['address2'] = $request->address2;
            $input2['city'] = $request->city;
            $input2['state'] = $request->state;
            $input2['postal_code'] = $request->postal_code;
            
            $update_info = BookingInformation::where("booking_id","=",$id)->update($input2);
            
            if($status==4 && $book->status!=$status)
            {
                $token = Functions::generateRandomString(12);
                $from = Config::get("params.admin_email");
                $email = $request->email;
                
                $review = new Reviews;
                $review->property_id =  $book->property_id;
                $review->booking_id =  $id;
                $review->token =  $token;
                $review->save();
                
                $content = Content::where("code","=","reviews")->get();
                
                $replaces['LOGO'] = asset('front/images/email.png');
                $replaces['NAME'] = $request->first_name." ".$request->last_name;
                $replaces['LINK'] = Config::get("params.url")."/reviews/".$token;
                $template = Functions::setEmailTemplate($content, $replaces);

                $subject = $template["subject"];
                $body = $template["body"];
                
                $send_email = Functions::sendEmail($email,$subject,$body,'',$from);
                
                
            }
            
            if($status==5)
            {
                $update = RatesandAvailability::updateDates($checkin,$checkout,$property_id,1);

            }
            else
            {
                $update = RatesandAvailability::updateDates($checkin,$checkout,$property_id);

            }

            
            \Session::flash('success', 'Booking updated Successfully!');
            return redirect('admin/booking');
            
        }
        
    }

    public function delete($id) {
        
        $book = Booking::find($id);
        
        $update_dates = RatesandAvailability::updateDates($book->checkin,$book->checkout,$book->property_id,1);
        
        $row = Booking::where('id', '=', $id)->update(["deleted"=>1]);
        
        \Session::flash('success', 'Booking Deleted Successfully!');
        return redirect('admin/booking');
    }
    public function createBooking() 
    {
        $user_id = Auth::user()->id;
        
        $role_id = Auth::user()->role_id;
	
	if($role_id==3)
        {
            
		return redirect('admin/ownerbooking/create');
        }
        
        $status = Booking::$status;
        
        $lead_source = Booking::$lead_source;
        
        $countries = Booking::$countries;
        
        $langauges = User::$langauges;
        
        $date = date("Y-m-d",strtotime("+1 day"));
        
        $nights = 4;
        
        $end_date = date ("Y-m-d", strtotime("+4 days", strtotime($date)));

        $setting = Settings::find(1); 
        
        $users = User::orderby("name","asc")->where("role_id","!=",3)->lists("name", "id")->prepend('Select', '');

        
        $properties = Properties::ActiveProperties();
        
        $payment_status = Booking::$payment_status;

        return view('admin.booking.create',compact('payment_status','user_id','users','role_id','nights','date','end_date','setting','properties','countries','langauges','status','lead_source'));
    }
    public function insertBooking(Request $request) {
        
        $validation = array(
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
            'lead_source' => 'required|max:255',
            'country' => 'required',
            'checkin' => 'required|date|max:255',
            'checkout' => 'required|date|max:255',
            'adults' => 'required',
            'property_id' => 'required',
            'payment_type' => 'required',

            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $grand_total = 0;
            $property_id = $request->property_id;
            $checkin = $request->checkin;
            $checkout = $request->checkout;
            $email = $request->email;
            $date = date("Y-m-d");
            $adults = $request->adults;
            $children = $request->children;
            
            $property=Properties::where("status","=",1)
            ->where("id","=",$property_id)->get();
            
            $date_diff = Functions::nights($checkin,$checkout);
            $min_stay=(count($property)>0)?$property[0]->min_stay:0;
            
            $checkdate = RatesandAvailability::checkDatesAvail($checkin,$checkout,$property_id);
            
            $sleeps  = (count($property)>0)?$property[0]->sleeps:0;
            $people = $adults+$children;
            
            $error = array();
            if(count($property)==0)
            {   
                $error[] = "Property is Inactive!";

            }
            elseif($checkin <= $date)
            {
               $error[] = "Check in date must be at least 1 days from today.";
            }
            elseif($checkout<=$checkin)
            {
                $error[] = "CheckOut must be greater than CheckIn. ";

            }
            elseif($date_diff<$min_stay)
            {
                $error[] = 'This Property has a minimum stay of '.$min_stay.' nights.';

            }
            else if(count($checkdate)>0)
            {
                 $error[] = 'Unfortunately dates are not available on this property!';
            }
            else if($people>$sleeps)
            {
                $error[] = 'This Property can only accommodate '.$sleeps.' people.';
            }
            if(count($error)>0)
            {
              return redirect()->back()->with("val_error",$error)->withInput();

            }
            else
            {
                $rates = RatesandAvailability::getRates($checkin,$checkout,$property_id);
        
                $rent = (count($rates)>0)?$rates[0]->rent:0;
                
                $feestaxes=FeesTaxes::getFeesTaxes($property_id);

                $otherfees=0;
                foreach($feestaxes as $feetax)
                {
                    $otherfees+=$feetax->amount;
                }

                $total = $rent+$otherfees;
                
                DB::beginTransaction();
            try
            {
           
            $payment_type = $request->payment_type;
            $property_id = $request->property_id;
            $checkin = $request->checkin;
            $checkout = $request->checkout;
            $email = $request->email;
            $grand_total=$total;
            $currency= Config::get('params.currency');
            $symbol=$currency["BRL"]["symbol"];
                
            $booking = new Booking;
            if($payment_type==1)
            {
                $booking->payment_type = 'paypal';
            }
            elseif($payment_type==2)
            {
                $booking->payment_type = 'stripe';
            }
            
            $booking->property_id = $property_id;
            $booking->checkin = $request->checkin;
            $booking->checkout = $request->checkout;
            $booking->checkin_time = $request->checkin_time;
            $booking->checkout_time = $request->checkout_time;
            $booking->adults = $request->adults;
            $booking->children = $request->children;
            $booking->grand_total = $grand_total;
            $booking->status =  $request->status;
            $booking->booked_by = $request->booked_by;
            $booking->save();
            
            $booking_id=$booking->id;
            
            $info = new BookingInformation;
            $info->booking_id = $booking_id;
            $info->first_name = $request->first_name;
            $info->last_name = $request->last_name;
            $info->email = $email;
            $info->phone = $request->phone;
            $info->lead_source = $request->lead_source;
            $info->special = $request->special;
            $info->country = $request->country;
            $info->address = $request->address;
            $info->address2 = $request->address2;
            $info->city = $request->city;
            $info->state = $request->state;
            $info->postal_code = $request->postal_code;
            $info->save();
                
            $property = Properties::find($property_id);   
            
            $quantity = Functions::nights($checkin,$checkout);

            $rate = RatesandAvailability::getDateRate($checkin,$property_id);

            $rent =(count($rate)>0)?$rate[0]->daily:0;

            $description = $quantity." Nights @".$symbol.''.Functions::MoneyFormat($rent)." ".$checkin."-".$checkout;
            
            $billing_item = new BookingItems;
            $billing_item->booking_id = $booking_id;
            $billing_item->type = 2;
            $billing_item->description = $description;
            $billing_item->taxed = 0;     
            $billing_item->quantity = $quantity; 
            $billing_item->rate = $rent; 
            $billing_item->amount = $rent*$quantity; 
            $billing_item->save();
            
            $feestaxes = FeesTaxes::getFeesTaxes($property_id);
            
            foreach($feestaxes as $feetax)
            {
                $type = '';
                if($feetax->type==3)
                {
                    $type = 3;
                }
                if($feetax->type==2 || $feetax->type==1)
                {
                    $type = 4;
                }
                $billingitem = new BookingItems;
                $billingitem->booking_id = $booking_id;
                $billingitem->type = $type;
                $billingitem->description = $feetax->public_name;
                $billingitem->taxed = 1;     
                $billingitem->quantity = 1; 
                $billingitem->rate = $feetax->amount; 
                $billingitem->amount = $feetax->amount*1;
                $billingitem->save();  

            }
            
            if($payment_type==1)
            {
                $item = new Item();
                $item->setName($property->public_headline)
                ->setQuantity($quantity)
                ->setPrice($rent)
                ->setCurrency(config('params.currency_default'));  
                $item->setDescription($description);
                $items[]=$item;
                
                $item2 = array();
                foreach($feestaxes as $feetax)
                {
                    $item2 = new Item();
                    $item2->setName($feetax->public_name)
                    ->setQuantity(1)
                    ->setPrice($feetax->amount)
                    ->setCurrency(config('params.currency_default'));     
                }
              
                if(count($item2)>0)
                {
                    $items[]=$item2;
                }
                

                $item_list = new ItemList();
                $item_list->setItems($items);

                $payer = new Payer();
                $payer->setPaymentMethod('paypal');


                $details = new Details();
                $details->setShipping($grand_total)
                    ->setTax(0)
                    ->setSubtotal($grand_total);

                $amount = new Amount();
                $amount->setCurrency(config('params.currency_default'));
              
                $amount->setTotal($grand_total);
               
                $transaction = new Transaction();
               
                $transaction->setAmount($amount)->setInvoiceNumber($booking_id)
                ->setItemList($item_list)
                ->setDescription(config('params.site_name').' Booking');
                
                //Admin\PaymentsController@success
                //config('paypal.express.success')Admin\PaymentsController@cancel
                //config('paypal.express.cancel')
                $redirectUrls = new RedirectUrls();
                $redirectUrls->setReturnUrl(action('Admin\PaymentsController@success'));
                $redirectUrls->setCancelUrl(action('Admin\PaymentsController@cancel'));

                $payment = new Payment();
                $payment->setIntent('sale');
                $payment->setPayer($payer);
                $payment->setRedirectUrls($redirectUrls);
                $payment->setTransactions(array($transaction));
                //dd($transaction);  
                try 
                {
                    $response = $payment->create($this->_apiContext);
                    $redirectUrl = $response->links[1]->href;
                    Session::put('book_id', $booking_id);
                    DB::commit();
                  
                    return Redirect::to( $redirectUrl );
                }catch (\PayPal\Exception\PayPalConnectionException $ex) {
                    $error[] = $ex->getData();
                    \Session::put('error',$error);
                    return Redirect::to('admin/booking/cancel');
                    
                }
           } 
           elseif($payment_type==2)
           {
               
               $ccExpiry = explode("/",$request->ccExpiry);
               $ccExpiryMonth = trim($ccExpiry[0]);
               $ccExpiryYear = trim($ccExpiry[1]);
               $redirectUrl = "admin/booking/success";
               $redirectUrlFail = "admin/booking/cancel";
               $stripe = Stripe::make(env("STRIPE_SECRET"));
               
               
               try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'name'    => $request->card_name,
                        'email'   => $email,
                        'number'    => $request->card_no,
                        'exp_month' => $ccExpiryMonth,
                        'exp_year'  => $ccExpiryYear,
                        'cvc'       => $request->cvcNumber,
                    ],
                ]);
                if (!isset($token['id'])) {
                    \Session::put('error','The Stripe Token was not generated correctly');
                    return redirect($redirectUrlFail);
                }
                
                $description .= "\n Booking ID: ".$booking_id;
                $description .= "\n Property ID: ".$property_id;
                
                
                $charge = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => config('params.currency_default'),
                    'amount'   => $grand_total,
                    'description' => $description,
                ]);
                Session::put('book_id', $booking_id);
                $stripeCard = [
                        'card_name'    => $request->card_name,
                        'email'   => $email,
                        'card_no'    => $request->card_no,
                        'month' => $ccExpiryMonth,
                        'year'  => $ccExpiryYear,
                        'cvc'       => $request->cvcNumber,
                    ];
                
                if($charge['status'] == 'succeeded') {
                    /**
                    * Write Here Your Database insert logic.
                    */
                    DB::commit();
                    return redirect($redirectUrl)->with("charge",$charge)
                            ->with("stripeCard",$stripeCard);
                    
                } else {
                    \Session::flash('error','Stripe Payment has been failed!');
                    return redirect($redirectUrlFail);
                }
            } catch (Exception $e) {
                \Session::flash('error',$e->getMessage());
                return redirect($redirectUrlFail);
            } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
                \Session::flash('error',$e->getMessage());
                return redirect($redirectUrlFail);
            } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                \Session::flash('error',$e->getMessage());
                return redirect($redirectUrlFail);
            }
           }
            
            //return redirect('checkout/success/'.$order_id);
            
            

            //\Session::flash('success', 'Booking submitted successfully!');
            }
            catch(Exception $e)
            {
                 DB::rollBack();
                 return redirect('admin/booking/cancel');
            }
                
                
                
                //$booking = $this->checkout($request,$total);
            }
        }
        
       
    }
    function checkout($request,$total)
    {
            
    }

}
