<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\BookingRules;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Auth;
use App\Properties;
use App\PropertyBookingRules;

class BookingRulesController extends AdminController {

   
    public function __construct() 
    {
        parent::__construct();
    }

    public function index() {
        $model = BookingRules::orderBy("start","asc")->paginate(20);
        
        $status=BookingRules::$status;
        
        $type=BookingRules::$type;
        
        return view('admin.bookingrules.index', compact('model','status','type'));
    }
    public function search(Request $request)
    {
        $types = $request->input('type');
        $status_id = $request->input('status');
      
        $search['type']=$types;
        $search['status']=$status_id;
        
        $model=BookingRules::search($search);
        
        $status=BookingRules::$status;
        
        $type=BookingRules::$type;
        
        return view('admin.bookingrules.index', compact('model','status','type','status_id','types'));
    }
    public function create(Request $request) {
        
        $type=$request->type;
        
        $status=BookingRules::$status;
        
        $applicability=BookingRules::$applicability;
        
        $charge_type=BookingRules::$charge_type;
        
        $reminder_type=BookingRules::$reminder_type;
        
        $processor=BookingRules::$processor;
        
        $start_date=date("Y-m-d");
        $end_date='';
        
        return view('admin.bookingrules.create',compact('status','type','applicability','start_date','end_date','charge_type','reminder_type','processor'));
    }

    public function insert(Request $request) {
        
        $validation = array(
            'name' => 'required|max:255',
            'status' => 'required|max:255',
            'type' => 'required|max:255',
            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $rules = new BookingRules;
            if($request->type==1)
            {
                $rules->name = $request->name;
                $rules->status = $request->status;
                $rules->type = $request->type;
                $rules->applicability = $request->applicability;
                $rules->min_stay = $request->min_stay;
                $rules->start = $request->start;
                $rules->end = $request->end;
                $rules->description = $request->description;
                $rules->notes = $request->notes;
                $rules->save();
            }
            elseif($request->type==2)
            {
                $rules->name = $request->name;
                $rules->status = $request->status;
                $rules->type = $request->type;
                $rules->applicability = $request->applicability;
                $rules->start = $request->start;
                $rules->end = $request->end;
                $rules->charge = $request->charge;
                $rules->charge_type = $request->charge_type;
                $rules->reminder = $request->reminder;
                $rules->reminder_type = $request->reminder_type;
                $rules->processor = $request->processor;
                
                $rules->auto_confirm = (isset($request->auto_confirm))?$request->auto_confirm:0;
                if(isset($request->due))
                {
                    $tages= implode(",", $request->due);
                    $rules->calculation_due=$tages;
                }
                $rules->description = $request->description;
                $rules->notes = $request->notes;
                $rules->save();
                $rule_id=$rules->id;
                
                /*if($request->applicability==0)
                {
                    $properties=Properties::allProperties();
                    foreach($property_id as $pro)
                    {
                        $data[] = array('property_id'=>$pro, 'rules_id'=> $rule,'created_at'=>date('Y-m-d H:i:s'))
                        ;
                    }
                    if(count($data)>0)
                    {
                        $delete = PropertyBookingRules::where('rules_id', '=', $rule)->delete();
                        PropertyBookingRules::insert($data);
                    }
                }*/
            }
                
            
            

            \Session::flash('success', 'Booking Rule Added Successfully!');
            return redirect('admin/bookingrules');
        }
        
       
    }
    
    public function edit($id) {
        $model = BookingRules::findOrFail($id);
        
        $status=BookingRules::$status;
        
        $applicability=BookingRules::$applicability;
        
        $charge_type=BookingRules::$charge_type;
        
        $reminder_type=BookingRules::$reminder_type;
        
        $processor=BookingRules::$processor;
        
        $type=$model->type;
        
        $start_date=($model->start!='0000-00-00')?$model->start:'';
        $end_date=($model->end!='0000-00-00')?$model->end:'';
        
        $dues=explode(",",$model->calculation_due);
        
        $auto_confirm=($model->auto_confirm==1)?'checked':'';
        
        $properties=Properties::allProperties('rules');
        
        $url='admin/bookingrules/multiple';
        
        $module='rules';
        
        return view('admin.bookingrules.edit', compact('model','module','status','type','applicability','start_date','end_date','charge_type','reminder_type','processor','dues','auto_confirm','properties','url'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $user = BookingRules::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);
      
        $validation = array(
            'name' => 'required|max:255',
            'status' => 'required|max:255',
            'type' => 'required|max:255',
            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        
        if($input['type']==2)
        {
           if(isset($input['due']))
           {
                $tages= implode(",", $input['due']);
                $input['calculation_due']=$tages;
                unset($input['due']);
           }
          $input['auto_confirm'] = (isset($input['auto_confirm']))?$input['auto_confirm']:0;

        }
      
       $affectedRows = BookingRules::where('id', '=', $id)->update($input);
        
        
        \Session::flash('success', 'Booking Rule Updated Successfully!');
        return redirect()->back();
    }
    public function multiple(Request $request) {
        
        $validation = array(
            'id' => 'required|max:255',
            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $rule=$request->id;
            $delete_pro=(isset($request->delete))?$request->delete:'';
            $bookingrules=BookingRules::findOrFail($rule);
            if($bookingrules->status==0)
            {
                return back()
    		->with('success','Booking Rule was applied to 0 Properties');
            }
            elseif(count($request->property_id)>0)
            {
                $property_id=$request->property_id;
                $data=array();
                foreach($property_id as $pro)
                {
                    $data[] = array('property_id'=>$pro, 'rules_id'=> $rule,'created_at'=>date('Y-m-d H:i:s'))
                    ;
                }
                if(count($data)>0)
                {
                    if($delete_pro!='')
                    {
                        $property_to_delete = array_map(function($delproperty){ return $delproperty['property_id']; }, $data);
                        $delete = PropertyBookingRules::whereIn('property_id', $property_to_delete)
                        ->where("rules_id","=",$rule)->delete();
                    }
                    else
                    {
                       $delete = PropertyBookingRules::rulesDelete($rule); 
                    }
                  
                   PropertyBookingRules::insert($data);
                } 
                
            }
           else
            {
                $delete = PropertyBookingRules::rulesDelete($rule); 

            }
          
             return back()
    		->with('success','Booking Rule was applied to all Properties');
            
        }
        
       
    }


    public function delete($id) {
        $row = BookingRules::where('id', '=', $id)->delete();
        \Session::flash('success', 'Booking Rule Deleted Successfully!');
        return redirect('admin/bookingrules');
    }

}
