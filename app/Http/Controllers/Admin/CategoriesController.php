<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\Categories;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;

class CategoriesController extends AdminController {

    public function __construct() 
    {
        parent::__construct();
    }

    public function index() {
        $model = Categories::paginate(10);
        return view('admin.categories.index', ['model' => $model]);
    }

    public function create() {
        return view('admin.categories.create');
    }

    public function insert(Request $request) {
        
        $validation = array(
            'category_name' => 'required|max:50',

            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $user = new Categories;
            $user->category_name = $request->category_name;
            $user->save();

            \Session::flash('success', 'Category Added Successfully!');
            return redirect('admin/categories');
        }
        
       
    }

    public function edit($id) {
        $model = Categories::findOrFail($id);
        return view('admin.categories.edit', compact('model'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $user = Categories::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);
      
        $validation = array(
            'category_name' => 'required|max:50',

            );
       
            $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput();

        }
        $affectedRows = Categories::where('id', '=', $id)->update($input);
        
        
        \Session::flash('success', 'Updated Successfully!');
        return redirect('admin/categories');
    }

    public function delete($id) {
        $row = Categories::where('id', '=', $id)->delete();
        \Session::flash('success', 'Categories Deleted Successfully!');
        return redirect('admin/categories');
    }

}
