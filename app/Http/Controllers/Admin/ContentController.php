<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Validator,
    Redirect;
use DB;
use Auth;
use App\Content;
use App\Categories;
use App\Functions\Functions;
use Session;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Input as Input;

class ContentController extends AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
       parent::__construct();
    }

    public function index() {
        $model = Content::all();
        return view('admin.content.index', [
            'model' => $model,
        ]);
    }

    /**
     * Clients Cafe.
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('admin.content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function insert(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title' => 'required|max:255|unique:content',
                    'code' => 'required|max:255|unique:content',
                    'url' => 'required|unique:content',
                    'type' => 'required',
                    'body' => 'required',
                    'image' => 'mimes:jpeg,bmp,png,gif',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        //d($request->all(),1);
        $model = new Content;
        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $destinationPath = public_path() . '/uploads/content/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';
            $fileName = Functions::saveImage($file, $destinationPath, $destinationPathThumb);
            $upload = Image::make($destinationPath . $fileName)->fit(280)->save($destinationPathThumb . $fileName);
            $model->image = $fileName;
        }
        
        $model->type = $request->type;
        $model->code = $request->code;
        $model->title = $request->title;
        $model->subject = $request->subject;
        $model->metaTitle = $request->metaTitle;
        $model->metaDescription = $request->metaDescription;
        $model->keywords = $request->keywords;
        $model->body = $request->body;
        $model->url = $request->url;

        $model->save();
        \Session::flash('success', 'Content Added Successfully!');
        return redirect('admin/content');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $model = Content::findOrFail($id);
        return view('admin.content.edit', compact('model'))->with('id', $id);
    }

    public function update($id,
            Request $request) {
        $id = $request->id;

        $category = Content::findOrFail($id);


        $input = $request->all();

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $destinationPath = public_path() . '/uploads/content/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';
            $fileName = Functions::saveImage($file, $destinationPath, $destinationPathThumb);
            $upload = Image::make($destinationPath . $fileName)->fit(280)->save($destinationPathThumb . $fileName);
            $input['image'] = $fileName;
        }



        unset($input['_wysihtml5_mode']);
        unset($input['_token']);
        $affectedRows = Content::where('id', '=', $id)->update($input);

        \Session::flash('message', 'Updated Successfully!');
        return redirect('admin/content');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id) {
        $row = Content::where('id', '=', $id)->delete();
        return redirect('admin/content');
    }

}
