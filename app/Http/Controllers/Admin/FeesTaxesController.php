<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\FeesTaxes;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Auth;
use App\Properties;
use App\PropertyFeesTaxes;

class FeesTaxesController extends AdminController {

   
    public function __construct() 
    {
        parent::__construct();
    }

    public function index() {
        $model = FeesTaxes::paginate(20);
        
        $status=FeesTaxes::$status;
        
        $type=FeesTaxes::$type;
        
        return view('admin.feestaxes.index', compact('model','status','type'));
    }
    public function search(Request $request)
    {
        $types = $request->input('type');
        $status_id = $request->input('status');
      
        $search['type']=$types;
        $search['status']=$status_id;
        
        $model=FeesTaxes::search($search);
        
        $status=FeesTaxes::$status;
        
        $type=FeesTaxes::$type;
        
        return view('admin.feestaxes.index', compact('model','status','type','status_id','types'));
    }
    public function create(Request $request) {
        
        $type=$request->type;
        
        $status=FeesTaxes::$status;
        
        $applicability=FeesTaxes::$applicability;
        
        $amount_type=FeesTaxes::$amount_type;
        
        $types=FeesTaxes::$type;
        
        $input_type=FeesTaxes::$input_type;
        
        $start_date=date("Y-m-d");
        $end_date='';
        
        return view('admin.feestaxes.create',compact('status','type','applicability','start_date','end_date','amount_type','types','input_type','owner_statements'));
    }

    public function insert(Request $request) {
        
        $validation = array(
            'name' => 'required|max:255',
            'public_name' => 'required|max:255',
            'status' => 'required|max:255',
            'type' => 'required|max:255',
            );
       
        $validator = Validator::make($request->all(), $validation);
        
        
        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
      
        else
        {
            $taxes = new FeesTaxes;
            if($request->type==1 || $request->type==2)
            {
                $taxes->name = $request->name;
                $taxes->public_name = $request->public_name;
                $taxes->status = $request->status;
                $taxes->type = $request->type;
                $taxes->applicability = $request->applicability;
                $taxes->effective = $request->effective;
                $taxes->amount = $request->amount;
                $taxes->amount_type = $request->amount_type;
                $taxes->start = $request->start;
                $taxes->end = $request->end;
                $taxes->min = $request->min;
                $taxes->max = $request->max;
                $taxes->input_type = $request->input_type;
                if(isset($request->per_person))
                {
                    $per_person= implode(",", $request->per_person);
                    $taxes->per_person=$per_person;
                }
                if(isset($request->per_taxable))
                {
                    $per_taxable= implode(",", $request->per_taxable);
                    $taxes->per_taxable=$per_taxable;
                }
                if(isset($request->owner_statements))
                {
                    $owner_statements= implode(",", $request->owner_statements);
                    $taxes->owner_statements=$owner_statements;
                }
                if($request->type==1)
                {
                    $taxes->quantity=$request->quantity;
                }
                $taxes->description = $request->description;
                $taxes->notes = $request->notes;
                $taxes->save();
            }
            if($request->type==3)
            {
                $taxes->name = $request->name;
                $taxes->public_name = $request->public_name;
                $taxes->status = $request->status;
                $taxes->type = $request->type;
                $taxes->applicability = $request->applicability;
                $taxes->amount = $request->amount;
                $taxes->amount_type = $request->amount_type;
      
                $taxes->min = $request->min;
                $taxes->max = $request->max;
                if(isset($request->per_person))
                {
                    $per_person= implode(",", $request->per_person);
                    $taxes->per_person=$per_person;
                }
                if(isset($request->per_taxable))
                {
                    $per_taxable= implode(",", $request->per_taxable);
                    $taxes->per_taxable=$per_taxable;
                }
                $taxes->owner_statements = (isset($request->owner_statements))?$request->per_night:0;

                $taxes->description = $request->description;
                $taxes->notes = $request->notes;
                $taxes->save();
            }
            $tax_id = $taxes->id;

            \Session::flash('success', 'Tax Fee Added Successfully!');
            return redirect('admin/taxes/edit/'.$tax_id);
        }
        
       
    }

    public function edit($id) {
        $model = FeesTaxes::findOrFail($id);
        
        $status=FeesTaxes::$status;
        
        $applicability=FeesTaxes::$applicability;
        
        $amount_type=FeesTaxes::$amount_type;
        
        $types=FeesTaxes::$type;
        
        $input_type=FeesTaxes::$input_type;
        
        $type=$model->type;
        
        $start_date=($model->start!='0000-00-00')?$model->start:'';
        $end_date=($model->end!='0000-00-00')?$model->end:'';
        
        $per_person=explode(",",$model->per_person);
       
        $per_taxable=explode(",",$model->per_taxable);
        if($type!=3)
        {
            $owner_statements=explode(",",$model->owner_statements);

        }
        else
        {
          
            $owner=($model->owner_statements==1)?'checked':'';
        }
        
        $properties=Properties::allProperties('fees');
        
        $url='admin/taxes/multiple';
        
        $module='fees';
                
        return view('admin.feestaxes.edit', compact('model','module','status','type','applicability','start_date','end_date','amount_type','types','input_type','per_person','per_taxable','owner_statements','properties','url'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $user = FeesTaxes::findOrFail($id);
        $input = $request->all();
        unset($input['_token']);
        
         $validation = array(
            'name' => 'required|max:255',
            'public_name' => 'required|max:255',
            'status' => 'required|max:255',
            'type' => 'required|max:255',
            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        
        if($input['type']==1 || $input['type']==2)
        {
             if(isset($input['per_person']))
             {
                    $per_person= implode(",", $input['per_person']);
                    $input['per_person']=$per_person;
             }
             if(isset($input['per_taxable']))
             {
                    $per_taxable= implode(",", $input['per_taxable']);
                    $input['per_taxable']=$per_taxable;
             }
            if(isset($input['owner_statements']))
            {
                    $owner_statements= implode(",", $input['owner_statements']);
                    $input['owner_statements']=$owner_statements;
            }
          

        }
     
       $affectedRows = FeesTaxes::where('id', '=', $id)->update($input);
        
       
        \Session::flash('success', 'Tax Fee Updated Successfully!');
        return redirect()->back();
    }
    public function multiple(Request $request) {
        
        $validation = array(
            'id' => 'required|max:255',
            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $feestaxes_id=$request->id;
            $delete_pro=(isset($request->delete))?$request->delete:'';
            $feestaxes=FeesTaxes::findOrFail($feestaxes_id);
            if($feestaxes->status==0)
            {
                return redirect()->back()
    		->with('success','Tax Fee was applied to 0 Properties');
            }
            elseif(count($request->property_id)>0)
            {
                $property_id=$request->property_id;
                $data=array();
                foreach($property_id as $pro)
                {
                    $data[] = array('property_id'=>$pro, 'feestaxes_id'=> $feestaxes_id,'created_at'=>date('Y-m-d H:i:s'))
                    ;
                }
                if(count($data)>0)
                {
                    if($delete_pro!='')
                    {
                        $property_to_delete = array_map(function($delproperty){ return $delproperty['property_id']; }, $data);
                        $delete = PropertyFeesTaxes::whereIn('property_id', $property_to_delete)
                        ->where("feestaxes_id","=",$feestaxes_id)->delete();
                    }
                    else
                    {
                       $delete = PropertyFeesTaxes::feesDelete($feestaxes_id); 
                    }
                  
                    PropertyFeesTaxes::insert($data);
                } 
            }
            else
            {
                 $delete = PropertyFeesTaxes::feesDelete($feestaxes_id); 

            }
          
             return redirect()->back()
    		->with('success','Tax Fee was applied to all Properties');
            
        }
        
       
    }
    public function multiplesearch(Request $request)
    {
        $search=$request->search;
        $module=$request->module;
        $id=$request->id;
        
        if(!empty($module) && !empty($id))
        {
            $properties=Properties::allProperties($module,$search);
            $result='<input type="hidden" name="delete" value="1"/>';
            foreach($properties as $row)
            {   
                $checked='';
                if($module=='rules')
                {
                    foreach($row->property_booking_rules as $booking_rules)
                    {   
                           if($booking_rules->rules_id==$id)
                           {
                                $checked='checked';
                           }

                    }
                }    
                else if($module=='fees')
                {
                    foreach($row->property_taxes_fees as $taxes_fees)
                    {
                           if($taxes_fees->feestaxes_id==$id)
                           {
                                $checked='checked';
                           }
                    }
                }
                else if($module=='special')
                {
                    foreach($row->property_specials as $special)
                    {
                           if($special->special_id==$id)
                            {
                                 $checked='checked';

                            }
                    }
                }
                   $result.='<tr>
                      <td><input type="checkbox" value='.$row->id.'" name="property_id[]" '.$checked.'/></td>
                      <td>'.$row->internal_name.'</td>
                   </tr>';
            }
            echo $result;
        }
        
    }

    public function delete($id) {
        $row = FeesTaxes::where('id', '=', $id)->delete();
        \Session::flash('success', 'Tax Fee Deleted Successfully!');
        return redirect('admin/taxes');
    }

}
