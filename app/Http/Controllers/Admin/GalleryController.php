<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Redirect;
use App\User;
use App\Properties;
use App\Gallery;
use App\GalleryImage;
use App\FeesTaxes;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use DB;
use Illuminate\Support\Facades\Input as Input;
use Session;

class GalleryController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function addImage(Request $request) {
        $gallery_id = $request->id;

        $code = "";
        if (isset($request->code)) {
            $code = $request->code;
        }

        return view('admin.properties.image', compact("gallery_id", "code"));
    }

    public function uploadImage(Request $request) {

        $gallery_id = $request->gallery_id;

        $rules['image'] = 'required|mimes:jpeg,bmp,png,jpg,jpeg|max:5000';

        $message = [
            'image.max' => 'File size must be less or equal to 5MB.'
        ];

        $response['error'] = 0;

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {

            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
            //return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $orders = GalleryImage::where("gallery_id", "=", $gallery_id)->orderby("display_order", "desc")->get();
            $display_order = (count($orders) > 0) ? $orders[0]->display_order + 1 : 1;

            $model = new GalleryImage;
            $file = Input::file('image');
            $size = Input::file('image')->getSize();
            if ($request->code != "") {
                $path = 'uploads/' . $request->code . '/';
            } else {
                $path = 'uploads/properties/';
            }

            $destinationPath = public_path() . '/' . $path;
            $destinationPathThumb = $destinationPath . 'thumbnail/';

            if ($request->code != "") {
                $fileName = Functions::saveImage($file, $destinationPath);
                //$upload = Image::make($destinationPath . $fileName)->fit(280)->save($destinationPathThumb . $fileName);
            } else {
                $fileName = Functions::saveImage($file, $destinationPath, $destinationPathThumb);
                $upload = Image::make($destinationPath . $fileName)->fit(280)->save($destinationPathThumb . $fileName);
            }
            $ext = pathinfo(public_path() . '/' . $path . $fileName, PATHINFO_EXTENSION);

            list($width, $height) = getimagesize($destinationPath . $fileName);

            $model->gallery_id = $gallery_id;
            $model->image_name = $fileName;
            $model->file_size = $size;
            $model->width = $width;
            $model->height = $height;
            $model->display_order = $display_order;
            $model->url = $path . $fileName;
            $model->save();

            //Session::flash('pageclose', '0');
            //return redirect('admin/property/addimage/'.$gallery_id)->with('success','Image Uploaded successfully.');
            $response['error'] = 0;
        }
        if ($response['error'] == 1) {
            foreach ($response['errors']->all() as $error) {
                echo '<li>' . $error . '</li>';
            }
        } else {
            echo '0';
        }
    }

    public function addMultipleImage(Request $request) {
        $gallery_id = $request->id;
        $code = "";
        if (isset($request->code)) {
            $code = $request->code;
        }

        return view('admin.properties.multipleimage', compact("gallery_id", "code"));
    }

    public function uploadMultipleImage(Request $request) {
        $gallery_id = $request->gallery_id;
        $images = Input::file('images');
        if (Input::hasFile('images')) {
            $image_count = count($images);
            $response['error'] = 0;
            $orders = GalleryImage::where("gallery_id", "=", $gallery_id)->orderby("display_order", "desc")->get();
            $display_order = (count($orders) > 0) ? $orders[0]->display_order + 1 : 1;
            $uploadcount = 0;
            foreach ($images as $image) {

                $rules = array('images' => 'required|mimes:jpeg,bmp,png,jpg,jpeg|max:5000');
                $message = [
                    'images.max' => 'File size must be less or equal to 5MB.'
                ];
                $validator = Validator::make(array('images' => $image), $rules, $message);
                if ($validator->passes()) {
                    $model = new GalleryImage;
                    $file = $image;
                    $size = $image->getSize();
                    if ($request->code != "") {
                        $path = 'uploads/' . $request->code . '/';
                    } else {
                        $path = 'uploads/properties/';
                    }
                    $destinationPath = public_path() . '/' . $path;
                    $destinationPathThumb = $destinationPath . 'thumbnail/';

                    if ($request->code != "") {
                        $fileName = Functions::saveImage($file, $destinationPath);
                    } else {
                        $fileName = Functions::saveImage($file, $destinationPath, $destinationPathThumb);
                        $upload = Image::make($destinationPath . $fileName)->fit(280)->save($destinationPathThumb . $fileName);
                    }

                    $ext = pathinfo(public_path() . '/' . $path . $fileName, PATHINFO_EXTENSION);

                    list($width, $height) = getimagesize($destinationPath . $fileName);

                    $model->gallery_id = $gallery_id;
                    $model->image_name = $fileName;
                    $model->file_size = $size;
                    $model->width = $width;
                    $model->height = $height;
                    $model->display_order = $display_order;
                    $model->url = $path . $fileName;
                    $model->save();
                    $uploadcount ++;
                    $display_order++;
                } elseif ($validator->fails()) {

                    $errors = $validator->errors();
                    $response['error'] = 1;
                    $response['errors'] = $errors;
                }
            }
            if ($uploadcount == $image_count) {
                //\Session::flash('pageclose', '0');
                //return redirect('admin/property/addmultiple/'.$gallery_id)->with('success','Image Uploaded successfully.');;

                echo '0';
            } else {

                if ($response['error'] == 1) {
                    foreach ($response['errors']->all() as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                }

                //return redirect()->back()->withErrors($validator->errors())->withInput();
            }
        }
    }

    public function loadGallery(Request $request) {

        $gallery_id = $request->gallery_id;

        $gallery_images = GalleryImage::where("gallery_id", "=", $gallery_id)->orderby("display_order", "asc")->get();
        $order = array();
        if ($gallery_images->count()) {

            $image = 1;
            foreach ($gallery_images as $total_image) {
                $order[$image] = $image;
                $image++;
            }
        }

        return view('admin.properties.gallery', compact("gallery_id", "order", "gallery_images"));
    }

    public function deleteImage($id) {
        $image = GalleryImage::findOrFail($id);
        $gallery_id = $image->gallery_id;
        $replace_order = $image->display_order;

        $count = GalleryImage::where("gallery_id", "=", $gallery_id)
                        ->where("id", "!=", $id)->orderby("display_order", "asc")->get();

        for ($i = $replace_order; $i <= count($count); $i++) {
            if ($replace_order == $i) {
                $update_order = $replace_order;
            } else {
                $update_order = $i;
            }

            $gallery_image = GalleryImage::where("gallery_id", "=", $gallery_id)
                    ->where("id", $count[$i - 1]->id)
                    ->update(array('display_order' => $update_order));
        }

        $file = public_path() . '/' . $image->url;
        if (file_exists($file)) {
            unlink($file);
        }
        $image->delete();



        return redirect()->back()
                        ->with('success', 'Image Deleted successfully.');
    }

    public function deleteAllImage(Request $request) {
        $gallery_id = $request->gallery_id;
        $code = $request->code;
        $gallery_images = GalleryImage::where("gallery_id", "=", $gallery_id)->get();
        
        if (count($gallery_images)>0) {
            foreach ($gallery_images as $image) {
                $image = GalleryImage::findOrFail($image->id);
                $file = public_path() . '/' . $image->url;
                if (file_exists($file)) {
                    unlink($file);
                }
                $image->delete();
            }
        }
        return redirect()->back()
                        ->with('success', 'All Images Deleted successfully.');
    }

    public function insertOrder(Request $request) {
        //$image_id=$request->image_id;
        //$display_order=$request->display_order;
        $gallery_id = $request->gallery_id;

        if (count($request->image_id) > 0 && $request->change_id != '') {

            $images = $request->image_id;
            $display_order = $request->display_order;
            $image_caption = $request->image_caption;

            $change_id = $request->change_id;

            $k = array_keys($display_order);
            $start = array_search($change_id, $images);
            $images[$start];
            $display_order[$start];
            $current = array_search($start, $k);

            $find = GalleryImage::find($change_id);
            $replace_order = $find->display_order;



            if ($replace_order > $display_order[$start]) {//$start==count($images)-1
                //d($replace_order);
                //d($display_order);
                //d($count,1);
                $count = $display_order[$start];
                for ($i = $count; $i < $replace_order; $i++) {

                    $gallery_image = GalleryImage::where("gallery_id", "=", $gallery_id)
                            ->where("id", "=", $images[$i - 1])
                            ->update(array('display_order' => $i + 1));
                }

                $gallery_image = GalleryImage::where("gallery_id", "=", $gallery_id)
                        ->where("id", "=", $change_id)
                        ->update(array('display_order' => $count));
                echo "1";
            } else {
                $update_image = GalleryImage::find($change_id);
                $old_order = $update_image->display_order + 1;


                for ($i = $old_order; $i <= $display_order[$start]; $i++) {
                    $gallery_image = GalleryImage::where("gallery_id", "=", $gallery_id)
                            ->where("display_order", "=", $i)
                            ->update(array('display_order' => $i - 1));
                }

                $gallery_image = GalleryImage::where("gallery_id", "=", $gallery_id)
                        ->where("id", "=", $change_id)
                        ->update(array('display_order' => $display_order[$start]));
                echo "1";
            }
        }



        /* $update = GalleryImage::find($image_id);
          if(count($update)>0)
          {
          $input["display_order"]=$update->display_order;
          $gallery_image = GalleryImage::where("gallery_id",$gallery_id)
          ->where("display_order",$display_order)->update($input);


          $input_image["display_order"]=$display_order;
          $update = GalleryImage::where("id",$image_id)->update($input_image);

          echo '1';
          }
         */
    }

    public function updateGallery($gallery_id) {
        $gallery_images = GalleryImage::where("gallery_id", "=", $gallery_id)
                        ->orderby("display_order", "desc")->get();
        if ($gallery_images->count()) {

            $order = 1;
            //$count = 0;
            foreach ($gallery_images as $image) {
                $gallery_image = GalleryImage::where("id", "=", $image->id)
                        ->update(array('display_order' => $order));

                //$count++;
                $order++;
            }


            echo 'success';
            die;
        }
        echo 'error';
        die;
    }

    public function upload(Request $request) {


        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');

        if (!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)) {
            if (is_uploaded_file($_FILES['file']['tmp_name'])) {

                //open uploaded csv file with read only mode
                $csvFile = fopen($_FILES['file']['tmp_name'], 'r');

                //skip first line
                fgetcsv($csvFile);

                //parse data from csv file line by line
                while (($line = fgetcsv($csvFile)) !== FALSE) {
                    //check whether member already exists in database with same email

                    $prevQuery = Properties::where('internalproperty_id', '=', $line[0])->first();

                    if (count($prevQuery) > 0) {
                        
                        //update member data
                        if ($prevQuery->gallery_id != 0 && $prevQuery->gallery_id != '') {
                            $gallery_id = $prevQuery->gallery_id;

                            $gallery_image = GalleryImage::where('gallery_id', '=', $gallery_id)->first();
                            //if (count($gallery_image) == 0) {
                                //dd($line[1]);
                                $path = 'uploads/properties/';
                                $destinationPath = public_path() . '/' . $path;
                                $destinationPathThumb = $destinationPath . 'thumbnail/';
                                $fileName = rand(111, 999) . time() . '.jpg';
                                $image = file_get_contents($line[1]);
                                file_put_contents($destinationPath.$fileName, $image); //Where to save the image on your server
                                

                                $orders = GalleryImage::where("gallery_id", "=", $gallery_id)->orderby("display_order", "desc")->get();
                                $display_order = (count($orders) > 0) ? $orders[0]->display_order + 1 : 1;


                                $file = $image;
                                

                                //Functions::saveImage($file, $destinationPath, $destinationPathThumb);
                                //$upload = Image::make($destinationPath . $fileName)->fit(280)->save($destinationPathThumb . $fileName);

                                $ext = pathinfo(public_path() . '/' . $path . $fileName, PATHINFO_EXTENSION);
                               
                                //d($size);
                                //dd($fileName);
                                
                                list($width, $height) = getimagesize($destinationPath . $fileName);
                                $size = filesize($destinationPath.$fileName);
                                $model = new GalleryImage;
                                $model->gallery_id = $gallery_id;
                                $model->image_name = $fileName;
                                $model->image_caption = $line[2];
                                $model->file_size = $size;
                                $model->width = $width;
                                $model->height = $height;
                                $model->display_order = $display_order;
                                $model->url = $path . $fileName;
                                $model->save();
                            //}
                        }
                    } else {
                        //$update = Properties::where("id","=",$prevQuery->id)
                        //->update(['internalproperty_id'=>$line[0]]);
                    }
                }
                
                //close opened csv file
                fclose($csvFile);

                $qstring = '?status=succ';
            } else {
                $qstring = '?status=err';
            }
        } else {
            $qstring = '?status=invalid_file';
        }

        return redirect('admin/property/import' . $qstring);
    }

}
