<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Validator, Input, Redirect;
use DB;
use App\User;
use Auth;
use Session;
use Illuminate\Http\Request;
use App\Properties;
use App\Booking;

class HomeController extends AdminController {

    public function __construct() 
    {
        parent::__construct();
    }

    public function index() 
    {
        $role_id = Auth::user()->role_id;
        $name = Auth::user()->name;

        
        if($role_id==1 || $role_id==2)
        {
            $property = Properties::all();
        
            $total_pro =  $property->count();

            $booking = Booking::where("deleted","!=",1)->get();

            $total_book =  $booking->count();

            $owner = User::where("role_id","=",3)->get();

            $total_owners =  $owner->count();

            $users = User::whereIn("role_id",array(1,2))->get();

            $total_users =  $users->count();

            $model = Booking::with("property")->where("deleted","!=",1)
            ->orderby("created_at","desc")->limit(7)->get();

            $statuses = Booking::$status;

            $properties = $this->latestProperties();
            

            return view('admin.home',compact("model","name","role_id","properties","statuses","total_pro","total_book","total_owners","total_users"));
        }
        
        elseif($role_id==3)
        {
            return view('admin.dashboard',compact("model","name"));

        }
        

    }
    function latestProperties()
    {
        $properties = DB::table("properties as pro")
        ->join("gallery_image as images","images.gallery_id","=","pro.gallery_id")
        ->select("pro.*","images.image_caption","images.url","images.display_order")
        ->where("images.display_order","=",1)
        ->whereNull("pro.deleted_at")
        ->whereNull("images.deleted_at")
        ->orderby("pro.id","desc")->limit(5)->get();
        
        return $properties;
    }

    
}
