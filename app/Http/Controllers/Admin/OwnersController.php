<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\User;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Contacts;
use App\Properties;
use App\Units;
use App\FeesTaxes;
use App\UserFeesTaxes;
use Auth,DB;
use App\RatesandAvailability;


class OwnersController extends AdminController {

    public function __construct() 
    {
        parent::__construct();
    }

    public function index() {
        $model = User::where("role_id","=",3)->orderby("name","asc")->paginate(10);
        $status=User::$status;
        
        return view('admin.owners.index', compact('model','status'));
    }
    public function search(Request $request)
    {
        $account_name = $request->input('account_name');
        $status_id = $request->input('status');
      
        $search['account_name']=$account_name;
        $search['status']=$status_id;
        
        $model=User::search($search);
        
        $status=User::$status;
        
        return view('admin.owners.index', compact('model','status','account_name','status_id'));
    }

    public function create() {
        $status=User::$status;
        
        $langauges=User::$langauges;
        
        return view('admin.owners.create',compact('status','properties','langauges'));
    }

    public function insert(Request $request) {
        
        $validation = array(
            'name' => 'required|max:50',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|confirmed|min:6',
            'status' => 'required',

            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $user = new User;
            $user->name = $request->name;
            $user->telephone = $request->telephone;
            $user->language = $request->language;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->status = $request->status;
            $user->role_id = 3;
            $user->save();
            $owner_id=$user->id;
          

            \Session::flash('success', 'Owner Added Successfully!');
            return redirect('admin/owners');
        }
        
       
    }
    public function contactinsert(Request $request) {
        
        
       $validation = array(
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'lead_source' => 'required|max:255',
            'email' => 'required|email|max:255|unique:contacts',
            'address_1' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',

            );
       
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $con = new Contacts;
            $con->first_name = $request->first_name;
            $con->last_name = $request->last_name;
            $con->email = $request->email;
            $con->lead_source = $request->lead_source;
            $con->lead_funnel = $request->lead_funnel;
            $con->home = $request->home;
            $con->work = $request->work;
            $con->language = $request->language;
            $con->address=$request->address;
            $con->address_1=$request->address_1;
            $con->address_2=$request->address_2;
            $con->city=$request->city;
            $con->country=$request->country;
            $con->neighborhood=$request->neighborhood;
            $con->county=$request->county;
            $con->metro=$request->metro;
            $con->state=$request->state;
            $con->postal_code=$request->postal_code;
            $con->region=$request->region;
            $con->longitude=$request->longitude;
            $con->latitude=$request->latitude;
            $con->notes=$request->notes;
            $con->user_id=$request->user_id;
            if(isset($request->tags))
            {
                $tages= implode(",", $request->tags);
                $con->tags=$tages;
            }
            $con->save();
            
            \Session::flash('success', 'Contact Added Successfully!');
            return redirect()->back()->withInput();
        }
        
       
    }


    public function edit($id) {
        $model = User::findOrFail($id);
        $status=User::$status;
        
        $langauges=User::$langauges;

        
        return view('admin.owners.edit', compact('model','status','langauges'))->with('id', $id);
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $user = User::findOrFail($id);
        $input = $request->all();
      
        unset($input['_token']);
        
        if($input["password"]=="")
        {
            $validation = array(
                    'name' => 'required|max:50',
                    'email' => 'required|max:50|email|unique:users,email,' . $user->id,
                    'status' => 'required',
            );
        }
        elseif($input["password"]!="")     
        {
            $validation = array(
                    'name' => 'required|max:50',
                    'email' => 'required|max:50|email|unique:users,email,' . $user->id,
                    'password' => 'required|confirmed|min:6',
                    'status' => 'required',
            );
        }
           
         
           
       
            $validator = Validator::make($request->all(), $validation);

            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput();

            }
        
            if(!isset($input["password"]))
            {
               
                $affectedRows = User::where("id","=",$id)->update($input);
            }
            else
            {
                unset($input['password_confirmation']);
              
                
                if($input['password']=="")
                {
                    unset($input['password']);
                }
                else
                {
                   $input['password'] = bcrypt($request->password);

                }
                
                $affectedRows = User::where("id","=",$id)->update($input);
            }
	    
      
        \Session::flash('success', 'Updated Successfully!');
        return redirect('admin/owners');
    }

    public function delete($id) {
        $row = User::find($id)->delete();
        \Session::flash('success', 'Owners Deleted Successfully!');
        return redirect('admin/owners');
    }
    
    public function calendar() {
        
        $owner_id=Auth::user()->id;
       
        $date = date('Y-m-d');
        
        $properties = User::getOwnerProperty($owner_id,$date);
        
        return view('admin.owners.calendar', compact('model','properties'));
    }
    public function getCalendar(Request $request) {
        
        $owner_id=Auth::user()->id;
        
        $start=date('Y-m').'-01';
        $end=date('Y-').'-12-01';
        $id=$request->property_id;
        $checkin_date=($request->checkin_date)?$request->checkin_date:$start;
        $checkin_time=$request->checkin_time;
        $checkout_date=($request->checkout_date)?$request->checkout_date:$end;
        $checkout_time=$request->checkout_time;
        
        $avails = RatesandAvailability::where("property_id",$id)->where("availability","!=",1)->get();
         
        $property=Properties::with("categories")->find($id);
       
        return view('admin.owners.showcalendar', compact('model','property','avails','id','checkin_date','checkout_date'));
    }
    public function calendarClose(Request $request) {
        
        $validation = array(
            'property_id' => 'required|max:50',
            'checkin_date' => 'required|max:50',
            'checkout_date' => 'required|max:50',
         
            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $property_id=$request->property_id;
            
            $date=$request->checkin_date;
            $checkin_time=$request->checkin_time;
            $checkout_date=$request->checkout_date;
            $checkout_time=$request->checkout_time;
            
            if(strtotime($date) == strtotime($checkout_date))
            {
                $rates=RatesandAvailability::where("property_id",$property_id)
                ->where("date",$date)->get();
                if(count($rates)>0)
                {
                    $input["availability"]=0;
                    $input["checkin"]=$checkin_time;
                    $input["checkout"]=$checkout_time;
                    $update=RatesandAvailability::where("id",$rates[0]->id)->update($input);
                }
                else
                {
                    $rate=new RatesandAvailability;
                    $rate->property_id=$property_id;
                    $rate->date=$date;
                    $rate->checkin=$checkin_time;
                    $rate->checkout=$checkout_time;
                    $rate->availability=0;
                    $rate->save();
                }
            }
            else
            {
                while (strtotime($date) < strtotime($checkout_date)) 
                {
                    $rates=RatesandAvailability::where("property_id",$property_id)
                    ->where("date",$date)->get();
                    if(count($rates)>0)
                    {
                        $input["availability"]=0;
                        $input["checkin"]=$checkin_time;
                        $input["checkout"]=$checkout_time;
                        $update=RatesandAvailability::where("id",$rates[0]->id)->update($input);
                    }
                    else
                    {
                        $rate=new RatesandAvailability;
                        $rate->property_id=$property_id;
                        $rate->date=$date;
                        $rate->checkin=$checkin_time;
                        $rate->checkout=$checkout_time;
                        $rate->availability=0;
                        $rate->save();
                    }
                    $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
                }
            }
         
            return redirect()->back()->with('success','Date has been closed!');
            
        }
        
    }
    

}
