<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Redirect,
    Auth;
use App\User;
use App\Properties;
use App\Categories;
use App\Amenities;
use App\Gallery;
use App\GalleryImage;
use App\PropertyAmentity;
use App\PropertyFeesTaxes;
use App\FeesTaxes;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use DB;
use Illuminate\Support\Facades\Input as Input;
use Session;
use App\RatesandAvailability;
use App\Units;
use App\BookingRules;
use App\PropertyBookingRules;
use App\Specials;
use App\PropertySpecials;

class PropertiesController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $model = DB::table("properties as pro")
                ->join("categories as cat", "pro.category_id", "=", "cat.id")
                ->select("pro.*", "cat.category_name")
                ->whereNull("pro.deleted_at")
                ->orderBy("id", "desc")
                ->paginate(10);

        $categories = Categories::orderby("category_name", "asc")->lists("category_name", "id")->prepend('Select Category', '');

        $status = Properties::$status;

        $owners = User::where("role_id", "=", 3)->orderby("name", "asc")
                        ->lists("name", "id")->prepend('Select Owner', '');

        return view('admin.properties.index', compact("model", "categories", "status", "owners"));
    }

    public function search(Request $request) {
        $headline = $request->input('headline');
        $category_id = $request->input('category_id');
        $status_id = $request->input('status');
        $id = $request->input('id');
        $owner = $request->input('owner');
        $location = $request->input('location');
        $development = $request->input('development');
        $internalproperty_id = $request->input('internalproperty_id');

        $search['headline'] = $headline;
        $search['category_id'] = $category_id;
        $search['status'] = $status_id;
        $search['id'] = $id;
        $search['owner'] = $owner;
        $search['location'] = $location;
        $search['development'] = $development;
        $search['internalproperty_id'] =  $internalproperty_id;

        $model = Properties::search($search);

        $categories = Categories::lists("category_name", "id")->prepend('Select Category', '');
        $status = Properties::$status;
        $owners = User::where("role_id", "=", 3)->orderby("name", "asc")
                        ->lists("name", "id")->prepend('Select Owner', '');
        return view('admin.properties.index', compact("model", "categories","internalproperty_id", "owners", "status", "headline", "category_id", "status_id", "id", "owner", "location", "development"));
    }

    public function create() {

        $status = Properties::$status;

        $parents = Amenities::where("parent_id", "=", 0)->orderby("sort_order","asc")->get();

        $categories = Categories::orderby("category_name", "asc")->lists("category_name", "id")->prepend('Select Category', '');

        //$gallery = Gallery::orderby("id","desc")->get()(count($gallery)>0)?$gallery[0]->id+1:'';
        $owners = User::where("role_id", "=", 3)->orderby("name", "asc")
                        ->lists("name", "id")->prepend('Select Owner', '');

        $fees = FeesTaxes::whereIn("type", array(1, 2))->where("owner", "!=", "1")->where("status", "=", 1)->orderby("name", "asc")->get();

        $taxes = FeesTaxes::where("type", "=", 3)->where("owner", "!=", "1")->where("status", "=", 1)->orderby("name", "asc")->get();

        $specials = Specials::allSpecials();

        $gallery_id = session_id();

        $bookingrules = BookingRules::where("status", "=", 1)->orderby("name", "asc")->get();


        $gallery_images = GalleryImage::where("gallery_id", "=", $gallery_id)->orderby("display_order", "ASC")->get();
        $order = array();
        if ($gallery_images->count()) {

            $image = 1;
            foreach ($gallery_images as $total_image) {
                $order[$image] = $image;
                $image++;
            }
        }

        return view('admin.properties.create', compact("status", "specials", "categories", "parents", "gallery_id", "order", "gallery_images", "fees", "taxes", "owners", "bookingrules"));
    }

    public function mappinpointpopup(Request $request) {
        $lon = $request->lon;
        $lat = $request->lat;

        return view('admin.properties.maps', compact("lon", "lat"));
    }

    public function insert(Request $request) {

        $validation = array(
            'internal_name' => 'required|max:255',
            'public_headline' => 'required|max:255',
            'summary' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'status' => 'required',
            'address' => 'required|max:255',
            'address_1' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $user_id = Auth::user()->id;

            $gallery = new Gallery;
            $gallery->title = $request->internal_name;
            $gallery->type = 1;
            $gallery->save();

            $property = new Properties;
            $property->category_id = $request->category_id;
            $property->internal_name = $request->internal_name;
            $property->public_headline = $request->public_headline;
            $property->summary = $request->summary;
            $property->description = $request->description;
            $property->status = $request->status;
            $property->address = $request->address;
            $property->address_1 = $request->address_1;
            $property->address_2 = $request->address_2;
            $property->city = $request->city;
            $property->country = $request->country;
            $property->development = $request->development;
            $property->neighborhood = $request->neighborhood;
            $property->county = $request->county;
            $property->metro = $request->metro;
            $property->state = $request->state;
            $property->postal_code = $request->postal_code;
            $property->region = $request->region;
            $property->longitude = $request->longitude;
            $property->latitude = $request->latitude;
            $property->bedrooms = $request->bedrooms;
            $property->garage_spaces = $request->garage_spaces;
            $property->bathrooms = $request->bathrooms;
            $property->lot_size = $request->lot_size;
            $property->lot_meter = $request->lot_meter;
            $property->sleeps = $request->sleeps;
            $property->stories = $request->stories;
            $property->parking_spaces = $request->parking_spaces;
            $property->floor = $request->floor;
            $property->year_built = $request->year_built;
            $property->unit_size = $request->unit_size;
            $property->unit_meter = $request->unit_meter;
            $property->min_stay = $request->min_stay;
            $property->policies = $request->policies;
            if (isset($request->tags)) {
                $tages = implode(",", $request->tags);
                $property->tags = $tages;
            }
            $property->gallery_id = $gallery->id;
            $property->disable_booking = (isset($request->disable_booking)) ? $request->disable_booking : 0;
            $property->show_price = (isset($request->show_price)) ? $request->show_price : 0;
            $property->hide_on_site = (isset($request->hide_on_site)) ? $request->hide_on_site : 0;
            $property->unshare_property = (isset($request->unshare_property)) ? $request->unshare_property : 0;
            $property->internal_note = $request->internal_note;
            $property->created_by = $user_id;
            $property->save();
            $property_id = $property->id;

            if (count($request->image_id) > 0 && $request->change_id == '') {
                $gallery_temp = $request->gallery_id;
                $input_gall["gallery_id"] = $gallery->id;
                $gallery_image = GalleryImage::where("gallery_id", "=", $gallery_temp)->update($input_gall);

                $images = $request->image_id;
                //$display_order = $request->display_order;
                $image_caption = $request->image_caption;
                $i = 0;

                foreach ($images as $image) {
                    //$input_image["display_order"] = $display_order[$i];
                    $input_image["image_caption"] = $image_caption[$i];

                    $gallery_image = GalleryImage::where("id", "=", $image)->update($input_image);

                    $i++;
                }
            }

            if (isset($request->amentity)) {
                $amentities = $request->amentity;
                $data = array();
                foreach ($amentities as $amentity) {
                    $data[] = array('property_id' => $property_id, 'amentity_id' => $amentity, 'created_at' => date('Y-m-d H:i:s'))
                    ;
                }
                if (count($data) > 0) {
                    PropertyAmentity::insert($data);
                }
            }

            /* if(isset($request->fees_taxes))
              {
              $fees_taxes=$request->fees_taxes;
              $data1=array();
              foreach($fees_taxes as $fees)
              {
              $data1[] = array('property_id'=>$property_id, 'feestaxes_id'=> $fees,'created_at'=>date('Y-m-d H:i:s'))
              ;
              }
              if(count($data1)>0)
              {
              PropertyFeesTaxes::insert($data1);
              }
              }
              if(isset($request->rules))
              {
              $rules=$request->rules;
              $data4=array();
              foreach($rules as $rule)
              {
              $data4[] = array('property_id'=>$property_id, 'rules_id'=> $rule,'created_at'=>date('Y-m-d H:i:s'))
              ;
              }
              if(count($data4)>0)
              {
              PropertyBookingRules::insert($data4);
              }
              }
              if(isset($request->specials))
              {
              $specials=$request->specials;
              $data5=array();
              foreach($specials as $special)
              {
              $data5[] = array('property_id'=>$property_id, 'special_id'=> $special,'created_at'=>date('Y-m-d H:i:s'))
              ;
              }
              if(count($data5)>0)
              {
              PropertySpecials::insert($data5);
              }
              } */
            if (count($request->unit) > 0) {
                $units = $request->unit;
                $data2 = array();
                $owner_id = $request->owner_id;
                $unit_name = $request->unit_name;

                $i = 0;
                foreach ($units as $unit) {
                    $input['owner_id'] = $owner_id[$i];

                    if ($input['owner_id'] != "") {
                        $input['unit_name'] = $unit_name[$i];

                        $data2[] = array('property_id' => $property_id, 'owner_id' => $input['owner_id'], 'unit_id' => $unit,
                            'unit_name' => $input['unit_name'], 'created_at' => date('Y-m-d H:i:s'));
                    }

                    $i++;
                }
                if (count($data2) > 0) {
                    Units::insert($data2);
                }
            }


            \Session::flash('success', 'Property Added Successfully!');
            return redirect('admin/property');
        }
    }

    public function edit($id) {

        $model = Properties::with("property_amentity")
                ->with("property_taxes_fees")
                ->with("property_booking_rules")
                ->with("property_specials")
                ->findOrFail($id);
        $status = Properties::$status;

        $parents = Amenities::where("parent_id", "=", 0)->orderby("sort_order","asc")->get();

        $categories = Categories::orderby("category_name", "asc")->lists("category_name", "id")->prepend('Select Category', '');

        $fees = FeesTaxes::getFees($id);

        $taxes = FeesTaxes::getTaxes($id);
        $gallery_id = $model->gallery_id;

        $owners = User::where("role_id", "=", 3)->orderby("name", "asc")
                        ->lists("name", "id")->prepend('Select Owner', '');

        $units = Units::where("property_id", "=", $id)->get();

        $gallery_images = GalleryImage::where("gallery_id", "=", $gallery_id)->orderby("display_order", "asc")->get();
        $order = array();
        if ($gallery_images->count()) {

            $image = 1;
            foreach ($gallery_images as $total_image) {
                $order[$image] = $image;
                $image++;
            }
        }

        $tags = explode(",", $model->tags);

        $bookingrules = BookingRules::getBookingRules($id);

        $specials = Specials::getSpecials($id);

        return view('admin.properties.edit', compact("model", "status", "specials", "categories", "parents", "gallery_id", "tags", "gallery_images", "order", "fees", "taxes", "owners", "units", "bookingrules"))->with('id', $id);
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $property = Properties::findOrFail($id);
        $input = $request->all();

        unset($input['_token']);

        $validation = array(
            'internal_name' => 'required|max:255',
            'public_headline' => 'required|max:255',
            'summary' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'status' => 'required',
            'address' => 'required|max:255',
            'address_1' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $user_id = Auth::user()->id;

            $input['category_id'] = $request->category_id;
            $input['internal_name'] = $request->internal_name;
            $input['public_headline'] = $request->public_headline;
            $input['summary'] = $request->summary;
            $input['description'] = $request->description;
            $input['status'] = $request->status;
            $input['address'] = $request->address;
            $input['address_1'] = $request->address_1;
            $input['address_2'] = $request->address_2;
            $input['city'] = $request->city;
            $input['country'] = $request->country;
            $input['development'] = $request->development;
            $input['neighborhood'] = $request->neighborhood;
            $input['county'] = $request->county;
            $input['metro'] = $request->metro;
            $input['state'] = $request->state;
            $input['postal_code'] = $request->postal_code;
            $input['region'] = $request->region;
            $input['longitude'] = $request->longitude;
            $input['latitude'] = $request->latitude;
            $input['bedrooms'] = $request->bedrooms;
            $input['garage_spaces'] = $request->garage_spaces;
            $input['bathrooms'] = $request->bathrooms;
            $input['lot_size'] = $request->lot_size;
            $input['lot_meter'] = $request->lot_meter;
            $input['sleeps'] = $request->sleeps;
            $input['stories'] = $request->stories;
            $input['parking_spaces'] = $request->parking_spaces;
            $input['floor'] = $request->floor;
            $input['year_built'] = $request->year_built;
            $input['unit_size'] = $request->unit_size;
            $input['unit_meter'] = $request->unit_meter;
            $input['min_stay'] = $request->min_stay;
            $input['policies'] = $request->policies;
            $input['disable_booking'] = $request->disable_booking;
            $input['show_price'] = $request->show_price;
            $input['hide_on_site'] = $request->hide_on_site;
            $input['unshare_property'] = $request->unshare_property;
            $input['internal_note'] = $request->internal_note;
            $input['modified_by'] = $user_id;

            if ($request->tags != '') {
                $tages = implode(",", $request->tags);
                $input['tags'] = $tages;
            }
            $property_id = $id;
            if (count($request->image_id) > 0 && $request->change_id == '') {

                $images = $request->image_id;
                //$display_order = $request->display_order;
                $image_caption = $request->image_caption;
                $i = 0;

                foreach ($images as $image) {
                    //$input_image["display_order"] = $display_order[$i];
                    $input_image["image_caption"] = $image_caption[$i];

                    $gallery_image = GalleryImage::where("id", $image)->update($input_image);

                    $i++;
                }
            }

            /* if (count($request->image_id) > 0 && $request->change_id!='') { 
              $gallery_id = $property->gallery_id;
              $images = $request->image_id;
              $display_order = $request->display_order;
              $image_caption = $request->image_caption;

              $change_id = $request->change_id;


              //echo "<br/>";
              // $gallery_images=GalleryImage::where("gallery_id",$gallery_id)->orderby("display_order","asc")->get();
              //echo "display order<br/>";
              //print_r($display_order);
              //print_r($images);
              // die;
              $k = array_keys($display_order);
              $start = array_search($change_id, $images);
              $images[$start];
              $display_order[$start];
              $current = array_search($start, $k);


              //$model = GalleryImage::find($images[$current]);
              // echo $images[$start];
              //echo $start;
              //echo count($images)-1;
              //$gallery_image = GalleryImage::where("id",$images[$start])->update(array('display_order'=>$display_order[$start]));
              //die;
              //$gallery_image = GalleryImage::where("id",$images[$start])->update(array('display_order'=>6));
              if($start==count($images)-1)
              {
              $count=$display_order[$start];
              //print_r($display_order);
              //echo $display_order[$start];
              //die;
              for($i=$display_order[$start];$i<=count($images);$i++)
              {
              echo $i.'-'.$images[$i-1];

              $gallery_image = GalleryImage::
              where("id",$images[$i-1])
              ->update(array('display_order'=>$i+1));

              }

              $gallery_image = GalleryImage::
              where("id",$images[$start])
              ->update(array('display_order'=>$display_order[$start]));

              }
              else{

              $update_image = GalleryImage::find($change_id);

              if(count($update_image)>0)
              {
              //$input["display_order"] = $update->display_order;
              $gallery_image = GalleryImage::where("gallery_id",$gallery_id)
              ->where("display_order",$display_order[$start])->update(array('display_order'=> $update_image->display_order));


              $input_image["display_order"] = $display_order[$start];
              $update = GalleryImage::where("id",$change_id)->update($input_image);


              }
              }

              } */

            if (count($request->amentity) > 0) {
                $delete = PropertyAmentity::where('property_id', '=', $property_id)->delete();
                $amentities = $request->amentity;
                $data = array();
                foreach ($amentities as $amentity) {
                    $data[] = array('property_id' => $property_id, 'amentity_id' => $amentity, 'created_at' => date('Y-m-d H:i:s'))
                    ;
                }
                if (count($data) > 0) {
                    PropertyAmentity::insert($data);
                }
            }

            if (count($request->fees_taxes) > 0) {
                $delete = PropertyFeesTaxes::where('property_id', '=', $property_id)->delete();
                $fees_taxes = $request->fees_taxes;
                $data1 = array();
                foreach ($fees_taxes as $fees) {
                    $data1[] = array('property_id' => $property_id, 'feestaxes_id' => $fees, 'created_at' => date('Y-m-d H:i:s'))
                    ;
                }
                if (count($data1) > 0) {
                    PropertyFeesTaxes::insert($data1);
                }
            }
            if (count($request->rules) > 0) {
                $delete = PropertyBookingRules::where('property_id', '=', $property_id)->delete();

                $rules = $request->rules;
                $data4 = array();
                foreach ($rules as $rule) {
                    $data4[] = array('property_id' => $property_id, 'rules_id' => $rule, 'created_at' => date('Y-m-d H:i:s'))
                    ;
                }
                if (count($data4) > 0) {
                    PropertyBookingRules::insert($data4);
                }
            }
            if (isset($request->specials)) {
                $delete = PropertySpecials::where('property_id', '=', $property_id)->delete();

                $specials = $request->specials;
                $data5 = array();
                foreach ($specials as $special) {
                    $data5[] = array('property_id' => $property_id, 'special_id' => $special, 'created_at' => date('Y-m-d H:i:s'))
                    ;
                }
                if (count($data5) > 0) {
                    PropertySpecials::insert($data5);
                }
            }
            if (count($request->unit) > 0) {
                $units = $input['unit'];
                $data2 = array();
                $owner_id = $input['owner_id'];
                $unit_name = $input['unit_name'];
                $unit_id = $input['units_id'];

                $i = 0;
                foreach ($units as $unit) {
                    $input_unit['owner_id'] = $owner_id[$i];
                    $input_unit['unit_name'] = $unit_name[$i];
                    $input_unit['unit_id'] = $unit;
                    $inputs['units_id'] = $unit_id[$i];

                    if ($input_unit['owner_id'] != "") {
                        if ($inputs['units_id'] != 0) {
                            $units_update = Units::where('id', '=', $inputs['units_id'])->update($input_unit);
                        } else {
                            $data2[] = array('property_id' => $property_id, 'owner_id' => $input_unit['owner_id'], 'unit_id' => $unit,
                                'unit_name' => $input_unit['unit_name'], 'created_at' => date('Y-m-d H:i:s'));
                        }
                    }
                    $i++;
                }
                if (count($data2) > 0) {

                    Units::insert($data2);
                }
            }
            unset($input['units_id']);
            unset($input['unit']);
            unset($input['owner_id']);
            unset($input['unit_name']);
            unset($input['sunrise_date']);
            unset($input['sunset_date']);
            if (isset($input['fees_taxes'])) {
                unset($input['fees_taxes']);
            }
            unset($input['amentity']);
            unset($input['image_id']);
            unset($input['display_order']);
            unset($input['image_caption']);
            if (isset($input['rules'])) {
                unset($input['rules']);
            }
            if (isset($input['specials'])) {
                unset($input['specials']);
            }
            unset($input['change_id']);

            $affectedRows = Properties::where('id', '=', $id)->update($input);

            \Session::flash('success', 'Property Updated Successfully!');
            //return redirect('admin/property');
            return redirect()->back();
        }
    }

    public function UnitDelete($id) {
        $row = Units::find($id)->delete();
        return back()
                        ->with('success', 'Unit Deleted successfully.');
    }

    public function delete($id) {
        $row = Properties::destroy($id);
        \Session::flash('success', 'Property Deleted Successfully!');
        return redirect('admin/property');
    }

    public function import(Request $request) {

        return view('admin.properties.upload', compact("model"));
    }

    public function upload(Request $request) {

        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        $type = $request->type;
        if (!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)) {
            if (is_uploaded_file($_FILES['file']['tmp_name'])) {

                //open uploaded csv file with read only mode
                $csvFile = fopen($_FILES['file']['tmp_name'], 'r');

                //skip first line
                fgetcsv($csvFile);
                if ($type == 'amenities') {
                    while (($line = fgetcsv($csvFile)) !== FALSE) {
                       
                        $prevQuery = Properties::where('internalproperty_id', '=', $line[0])->first();
                        if (count($prevQuery) > 0) {
                            $property_id = $prevQuery->id;
                            $prop = PropertyAmentity::where('property_id','=',$property_id)->get();
                            if (count($prop)==0) {
                                $amentities = explode(",",stripslashes($line[1]));
                                $data = array();
                                foreach ($amentities as $amentity) {
                                    $name = stripslashes($amentity); 
                                    $getAmenity = Amenities::where('name', 'LIKE', "%" . $amentity . "%")
                                                ->where("parent_id",'!=',0)->first();
                                    if(count($getAmenity)>0)
                                    {
                                        $data[] = array('property_id' => $property_id, 'amentity_id' => $getAmenity->id, 'created_at' => date('Y-m-d H:i:s'));
                                    }
                                          
                                    //d(stripslashes($amentity));
                                }
                                if (count($data) > 0) {
                                    PropertyAmentity::insert($data);
                                }
                            }
                        }
                    }
                }

                //parse data from csv file line by line
                elseif ($type == 'property') {
                    while (($line = fgetcsv($csvFile)) !== FALSE) {
                        //check whether member already exists in database with same email

                        $prevQuery = Properties::where('internal_name','=',$line[2])->first();
                        //$prevQuery = "SELECT internal_name FROM properties WHERE internal_name = '" . $line[3] . "'";

                        if (count($prevQuery) > 0) {
                            //update member data
                            $update = Properties::where("id", "=", $prevQuery->id)
                                    ->update(['internalproperty_id' => $line[0]]);
                        } else {
                            //insert member data into database
                            $user_id = Auth::user()->id;

                            $gallery = new Gallery;
                            $gallery->title = $line[2];
                            $gallery->type = 1;
                            $gallery->save();

                            $category = Categories::where('category_name', '=', $line[1])->first();
                            if ($line[6] == 'Active For Rent') {
                                $status = 1;
                            } elseif ($line[6] == 'Entered - Pending Approval') {
                                $status = 2;
                            } elseif ($line[6] == 'Inactive') {
                                $status = 0;
                            }

                            $property = new Properties;
                            $property->internalproperty_id = $line[0];
                            $property->gallery_id = $gallery->id;
                            $property->category_id = (count($category) > 0) ? $category->id : 0;
                            $property->internal_name = $line[2];
                            $property->public_headline = $line[3];
                            //$property->development = $request->development;
                            $property->summary = $line[4];
                            $property->description = $line[5];
                            $property->status = $status;
                            $property->address = $line[8];
                            $property->address_1 = $line[9];
                            $property->address_2 = $line[10];
                            $property->neighborhood = $line[11];

                            $property->city = $line[12];
                            $property->county = $line[13];
                            $property->metro = $line[14];
                            $property->state = $line[15];
                            $property->postal_code = $line[16];
                            $property->region = $line[17];
                            $property->country = $line[18];
                            $property->longitude = $line[19];
                            $property->latitude = $line[20];
                            $property->bedrooms = $line[21];
                            $property->garage_spaces = $line[22];
                            $property->bathrooms = $line[23];
                            $property->lot_size = $line[24];
                            $property->lot_meter = $line[25];
                            $property->sleeps = $line[26];
                            $property->stories = $line[27];
                            $property->parking_spaces = $line[28];
                            $property->floor = $line[29];
                            $property->year_built = $line[30];
                            $property->unit_size = $line[31];
                            $property->unit_meter = $line[32];
                            $property->min_stay = $line[33];
                            $property->policies = $line[35];
                            /* if (isset($request->tags)) {
                              $tages = implode(",", $request->tags);
                              $property->tags = $tages;
                              } */

                            $property->disable_booking = 0;
                            $property->show_price = 0;
                            $property->hide_on_site = 0;
                            $property->unshare_property = 0;
                            //$property->internal_note = $request->internal_note;
                            $property->created_by = $user_id;
                            $property->save();
                            $property_id = $property->id;
                        }
                    }
                }
                //close opened csv file
                fclose($csvFile);

                $qstring = '?status=succ';
            } else {
                $qstring = '?status=err';
            }
        } else {
            $qstring = '?status=invalid_file';
        }

        return redirect('admin/property/import' . $qstring);
    }

}
