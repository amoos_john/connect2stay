<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\PropertyFinder;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Gallery;
use App\GalleryImage;
use App\Properties;
use App\Amenities;
use App\Categories;

class PropertyFinderController  extends AdminController {

    public function __construct() 
    {
        parent::__construct();
    }

    public function index() 
    {
        $model = PropertyFinder::paginate(10);
        
        $status=PropertyFinder::$status;
                
        return view('admin.propertyfinder.index', compact('model','status'));
    }
    public function search(Request $request)
    {
        $name = $request->input('name');
        $status_id = $request->input('status');
      
        $search['name']=$name;
        $search['status']=$status_id;
        
        $model=PropertyFinder::search($search);
        
        $status=PropertyFinder::$status;
                
        return view('admin.propertyfinder.index', compact('model','status','name','status_id'));
    }

    public function create(Request $request) {
       
        $status=PropertyFinder::$status;
        
        $categories=Categories::orderby("category_name","asc")->lists("category_name", "id")->prepend('Select Category', '');

        $properties=Properties::allProperties();
        
        $parents = Amenities::where("parent_id","=",0)->orderby("sort_order","asc")->get();
        
        return view('admin.propertyfinder.create',compact('status','properties','parents','categories','properties'));
    }

     public function insert(Request $request) {
        
        $validation = array(
            'name' => 'required|max:255',
            'status' => 'required',
        );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $gallery=new Gallery;
            $gallery->title=$request->name;  
            $gallery->type=4;
            $gallery->save();
            
            
            
            $propfin = new PropertyFinder;
            $propfin->name = $request->name;
            $propfin->status = $request->status;
            $propfin->gallery_id=$gallery->id;
            $propfin->description = $request->description;
            $propfin->notes=$request->notes; 
            $propfin->category_id=$request->category_id;
            $propfin->min_stay_from=$request->min_stay_from; 
            $propfin->min_stay_to=$request->min_stay_to;
            $propfin->avg_day_to=$request->avg_day_to;
            $propfin->avg_day_from=$request->avg_day_from;
            $propfin->sleeps_to=$request->sleeps_to;
            $propfin->sleeps_from=$request->sleeps_from;
            $propfin->bedrooms_to=$request->bedrooms_to;
            $propfin->bedrooms_from=$request->bedrooms_from;
            $propfin->bathrooms_to=$request->bathrooms_to;
            $propfin->bathrooms_from=$request->bathrooms_from;
            $propfin->honour_loc=(isset($request->honour_loc))?$request->honour_loc:'';
            $propfin->grouping_key=$request->grouping_key;
            $propfin->sort_index=$request->sort_index;
            if(isset($request->amentity))
            {
                $amentites= implode(",", $request->amentity);
                $propfin->amentites=$amentites;
            }
            if(isset($request->property_id))
            {
                $properties= implode(",", $request->property_id);
                $propfin->properties=$properties;
            }
            
            $propfin->save();
            
            \Session::flash('success', 'Property Finder Added Successfully!');
            return redirect('admin/propertyfinder');
        }
        
       
    }
    
    public function edit($id) {
        $model = PropertyFinder::findOrFail($id);
        
       
        $status=PropertyFinder::$status;
        
        $categories=Categories::orderby("category_name","asc")->lists("category_name", "id")->prepend('Select Category', '');

        $properties=Properties::allProperties();
        
        $parents = Amenities::where("parent_id","=",0)->orderby("sort_order","asc")->get();
        
        $gallery_id=$model->gallery_id;
       
        return view('admin.propertyfinder.edit',compact('model','status','categories','parents','properties','gallery_id'))->with('id', $id);
        
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $propfinial = PropertyFinder::findOrFail($id);
        $input = $request->all();
      
        unset($input['_token']);
        
        
        $validation = array(
            'name' => 'required|max:255',
            'status' => 'required',
        );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
            
            $input['name'] = $request->name;
            $input['status'] = $request->status;
            $input['description'] = $request->description;
            $input['notes'] =$request->notes; 
            $input['category_id'] =$request->category_id;
            $input['min_stay_from'] = $request->min_stay_from; 
            $input['min_stay_to'] = $request->min_stay_to;
            $input['avg_day_to'] = $request->avg_day_to;
            $input['avg_day_from'] = $request->avg_day_from;
            $input['sleeps_to'] = $request->sleeps_to;
            $input['sleeps_from'] = $request->sleeps_from;
            $input['bedrooms_to'] = $request->bedrooms_to;
            $input['bedrooms_from'] = $request->bedrooms_from;
            $input['bathrooms_to'] = $request->bathrooms_to;
            $input['bathrooms_from'] = $request->bathrooms_from;
            $input['honour_loc'] = (isset($request->honour_loc))?$request->honour_loc:'';
            $input['grouping_key'] = $request->grouping_key;
            $input['sort_index'] = $request->sort_index;
            if(isset($request->amentity))
            {
                $amentites= implode(",", $request->amentity);
                $input['amentites'] = $amentites;
                unset($input['amentity']);
            }
            else
            {
		$input['amentites'] = '';
            }
            if(isset($request->property_id))
            {
                $properties= implode(",", $request->property_id);
                $input['properties'] = $properties;
                unset($input['property_id']);
            }
            else
            {
                $input['properties'] = '';
            }
            
          
        
        if(count($request->image_id)>0)
         {
                $images=$request->image_id;
                $display_order=$request->display_order;
                $image_caption=$request->image_caption;
                $i=0;
                foreach($images as $image)
                {
                    $input_image["display_order"]=$display_order[$i];
                    $input_image["image_caption"]=$image_caption[$i];
                    
                    $gallery_image = GalleryImage::where("id",$image)->update($input_image);
                    
                    
                    $i++;
                }
          }
          
         unset($input['image_id']);
         unset($input['display_order']);
         unset($input['image_caption']);
         unset($input['change_id']);
         $affectedRows = PropertyFinder::where('id', '=', $id)->update($input);

        \Session::flash('success', 'Property Finder Updated Successfully!');
        return redirect('admin/propertyfinder');
    }
    
    public function delete($id) {
        $row = PropertyFinder::find($id)->delete();
        \Session::flash('success', 'Property Finder Deleted Successfully!');
        return redirect('admin/propertyfinder');
    }
    public function testSearch(Request $request) {
            
            if($request->category_id!=0)
            {
                $search["category_id"] = $request->category_id;
            }
            if($request->amentity!='')
            {
                $ids = array_map('intval', $request->amentity); 
                $search["amentites"] = $ids;
            }
            if($request->min_stay_from!='' || $request->min_stay_to!='')
            {
                $search["min_stay_from"] = $request->min_stay_from;
                $search["min_stay_to"] = $request->min_stay_to;
            }
            if($request->sleeps_to!='' || $request->sleeps_from!='')
            {
                $search["sleeps_to"] = $request->sleeps_to;
                $search["sleeps_from"] = $request->sleeps_from;
            }
            if($request->bedrooms_to!='' || $request->bedrooms_from!='')
            {
                $search["bedrooms_to"] = $request->bedrooms_to;
                $search["bedrooms_from"] = $request->bedrooms_from;
            }
            if($request->bathrooms_to!='' || $request->bathrooms_from!='')
            {
                $search["bathrooms_to"] = $request->bathrooms_to;
                $search["bathrooms_from"] = $request->bathrooms_from;
            }
            if($request->property_id!='')
            {
                $ids = array_map('intval', $request->property_id); 
                $search["ids"] = $ids;
                
            }
            if(isset($search))
            {
                $properties = PropertyFinder::getProperties($search);
                return view('admin.propertyfinder.result',compact("properties"));  
               
                
            }
            else{
                $title = "Result not found!";
                return view('admin.propertyfinder.result',compact("title"));  

            }
        
    }

}
