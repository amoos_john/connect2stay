<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Redirect;
use App\User;
use App\Properties;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use DB;
use Illuminate\Support\Facades\Input as Input;
use Session;
use App\RatesandAvailability;
use App\Booking;

class RatesandAvailabilityController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request) {
        $id = $request->id;

        $rates = RatesandAvailability::where("property_id", $id)->get();

        $date = date("Y-m-d");

        $start_date = date("Y-m-d");

        $end_date = date("Y-m-t", strtotime("+1 years"));

        $month = date("F");
        $year = date("Y");

        $currmonth = date("m", strtotime($start_date));

        $properties = Properties::orderby('id', 'desc')->get();

        return view('admin.properties.rates', compact("id", "currmonth", "rates", "date", "end_date", "month", "year", "properties", "start_date"));
    }

    public function insert($id, Request $request) {
        $input = $request->all();

        $curr_date = date("Y-m-d");

        if (count($input['min']) > 0) {
            $daily = $input['daily'];
            $weekly = $input['weekly'];
            $avail = $input['avail'];

            $date = $input['min'];
            $end_date = $input['max'];

            $data = array();
            $i = 0;

            while (strtotime($date) <= strtotime($end_date)) {

                $input['daily'] = $daily[$i];
                $input['weekly'] = $weekly[$i];
                $input['avail'] = $avail[$i];

                if ($input['daily'] != "" || $input['weekly'] != "" || $input['avail'] == 0) {
                    $data[] = array('property_id' => $id, 'date' => $date,
                        'daily' => $input['daily'], 'weekly' => $input['weekly'],
                        'availability' => $input['avail'], 'created_at' => date('Y-m-d H:i:s'));
                }

                $i++;

                $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
            }
            if (count($data) > 0) {

                $date_to_delete = array_map(function($deldate) {
                    return $deldate['date'];
                }, $data);
                $delete = RatesandAvailability::whereIn('date', $date_to_delete)
                                ->where("property_id", "=", $id)->delete();

                RatesandAvailability::insert($data);
            }
        }

        return redirect()->back()
                        ->with('success', 'Rates and Availability Update successfully.');
    }

    public function search(Request $request) {
        $id = $request->id;

        $rates = RatesandAvailability::where("property_id", $id)->get();

        $date = $request->min;

        $end_date = $request->max;

        $currmonth = date("m", strtotime($request->min));

        return view('admin.properties.dates', compact("id", "rates", "date", "end_date", "currmonth"));
    }

    public function ownercalendar(Request $request) {
        $id = $request->id;
        $avails = RatesandAvailability::where("property_id", $id)->where("availability", 0)->get();

        $property = Properties::with("categories")->orderby('id', 'desc')->find($id);

        return view('admin.properties.calendar', compact("id", "avails", "property"));
    }

    public function upload(Request $request) {


        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');

        if (!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)) {
            if (is_uploaded_file($_FILES['file']['tmp_name'])) {

                //open uploaded csv file with read only mode
                $csvFile = fopen($_FILES['file']['tmp_name'], 'r');

                //skip first line
                fgetcsv($csvFile);

                //parse data from csv file line by line
                $prop_id=0;
                while (($line = fgetcsv($csvFile)) !== FALSE) {


                    $prevQuery = Properties::where('internalproperty_id', '=', $line[0])->first();
                    if (count($prevQuery) > 0) {

                        $id = $prevQuery->id;
                        
                        $date = $line[1];
                        $end_date = $line[2];
                        //whereBetween('date', array($date, $end_date))
                        $result = RatesandAvailability::whereBetween('date', array($date, $end_date))
                                ->where("property_id", "=", $id)
                                ->get();
                        
                        if (count($result) == 0) {
                            $daily = $line[3];
                            $data = array();
                            while (strtotime($date) <= strtotime($end_date)) {
                                if ($daily != "" || $date != '') {
                                    $data[] = array('property_id' => $id, 'date' => $date,
                                        'daily' => $daily, 'availability' => 1,
                                        'created_at' => date('Y-m-d H:i:s'));
                                }


                                $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                            }
                            if (count($data) > 0) {
                                /*if($prop_id!=$id)
                                {
                                    //$date_to_delete = array_map(function($deldate){ return $deldate['date']; }, $data);
                                $delete = RatesandAvailability::where("property_id","=",$id)->delete();
                                //->whereIn('date', $date_to_delete)
                                
                                }*/
                                

                                RatesandAvailability::insert($data);
                                //$prop_id=$id;
                            }
                        }
                    }
                }

                //close opened csv file
                fclose($csvFile);

                $qstring = '?status=succ';
            } else {
                $qstring = '?status=err';
            }
        } else {
            $qstring = '?status=invalid_file';
        }

        return redirect('admin/property/import' . $qstring);
    }

}
