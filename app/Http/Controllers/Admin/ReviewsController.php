<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\Reviews;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Properties;

class ReviewsController extends AdminController {

    public function __construct() 
    {
        parent::__construct();
    }

    public function index() 
    {
        $model = Reviews::with('properties')->orderby("id","desc")
                ->where("token","=","")->paginate(10);
        
        $status=Reviews::$status;
        
        $source=Reviews::$source;
        
        $properties=Reviews::ReviewProperties();
        
        $responded=Reviews::$responded;
                
        return view('admin.reviews.index', compact('model','responded','status','source','properties'));
    }
    public function search(Request $request)
    {
        $title = $request->input('title');
        $status_id = $request->input('status');
        $property_id = $request->input('property_id');
        $responded_id = $request->input('responded');
        $source_id = $request->input('source');
      
        $search['responded']=$responded_id;
        $search['property_id']=$property_id;
        $search['source']=$source_id;
        $search['title']=$title;
        $search['status']=$status_id;
        
        $model=Reviews::search($search);
        
        $status=Reviews::$status;
        
        $source=Reviews::$source;
        
        $properties=Reviews::ReviewProperties();
        
        $responded=Reviews::$responded;
                
        return view('admin.reviews.index', compact('model','responded','title','property_id','responded_id','source_id','status','source','properties','status_id'));
    }

    public function create(Request $request) {
       
        $status=Reviews::$status;
        
        $source=Reviews::$source;
        
        $related_to=Reviews::$related_to;
        
        $properties=Reviews::ReviewProperties();
        
        return view('admin.reviews.create',compact('status','related_to','source','properties'));
    }

     public function insert(Request $request) {
        
        $validation = array(
            'title' => 'required|max:255',
            'ratings' => 'required',
            'related_to' => 'required|max:255',
            'source' => 'required|max:255',
            'property_id' => 'required|max:255',
            'status' => 'required',
        );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
         
            
            $review = new Reviews;
            $review->title = $request->title;
            $review->property_id=$request->property_id;
            $review->reviewed_by = $request->reviewed_by;
            $review->ratings = $request->ratings;
            $review->source = $request->source;
            $review->related_to = $request->related_to;
            $review->status = $request->status;
            $review->comment = $request->comment;
            $review->response = $request->response;
            $review->notes=$request->notes; 
            $review->save();
            
            \Session::flash('success', 'Review Added Successfully!');
            return redirect('admin/reviews');
        }
        
       
    }
    
    public function edit($id) {
        $model = Reviews::findOrFail($id);
        
       
        $status=Reviews::$status;
        
        $source=Reviews::$source;
        
        $related_to=Reviews::$related_to;
        
        $properties=Reviews::ReviewProperties();
       
        return view('admin.reviews.edit',compact('model','status','related_to','source','properties'))->with('id', $id);
        
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $reviewial = Reviews::findOrFail($id);
        $input = $request->all();
      
        unset($input['_token']);
        
        
        $validation = array(
            'title' => 'required|max:255',
            'ratings' => 'required',
            'related_to' => 'required|max:255',
            'source' => 'required|max:255',
            'property_id' => 'required|max:255',
            'status' => 'required',
        );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
            
            $input['property_id'] = $request->property_id;
            $input['reviewed_by'] = $request->reviewed_by;
            $input['ratings'] = $request->ratings;
            $input['source'] = $request->source;
            $input['related_to'] = $request->related_to;
            $input['status'] = $request->status;
            $input['comment'] = $request->comment;
            $input['response'] = $request->response;
            $input['notes'] = $request->notes;
         
         $affectedRows = Reviews::where('id', '=', $id)->update($input);

        \Session::flash('success', 'Review Updated Successfully!');
        return redirect('admin/reviews');
    }
    
    public function delete($id) {
        $row = Reviews::find($id)->delete();
        \Session::flash('success', 'Review Deleted Successfully!');
        return redirect('admin/reviews');
    }

}
