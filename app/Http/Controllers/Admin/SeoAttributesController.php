<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\SeoAttributes;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Properties;
use App\PropertyFinder;

class SeoAttributesController extends AdminController {

    public function __construct() 
    {
        parent::__construct();
    }

    public function index() 
    {
        $model = SeoAttributes::orderby("id","desc")->paginate(10);
        
        $status=SeoAttributes::$status;
        
        $type=SeoAttributes::$type;
        
        $language=SeoAttributes::$language;
           
        return view('admin.seo.index', compact('model','language','status','type'));
    }
    public function search(Request $request)
    {
        $name = $request->input('name');
        $status_id = $request->input('status');
        $types = $request->input('type');
       
        $search['type']=$types;
        $search['name']=$name;
        $search['status']=$status_id;
        
        $model=SeoAttributes::search($search);
        
        $status=SeoAttributes::$status;
        
        $type=SeoAttributes::$type;
        
        $language=SeoAttributes::$language;
                
        return view('admin.seo.index', compact('model','name','types','language','status','type','status_id'));
    }

    public function create(Request $request) {
        
        $type=$request->type;
       
        $status=SeoAttributes::$status;
        
        $language=SeoAttributes::$language;
        
        $types=SeoAttributes::$type;
        
        if(array_key_exists($type, $types)) 
        {
           $type_name = $types[$type];
        }
        $applies_to='';
        if($type==3)
        {
            $applies_to=PropertyFinder::orderby("name","asc")->where("status","=",1)->lists("name", "id")->prepend('Select', '');
        }
        elseif($type==4)
        {
            $applies_to=Properties::ActiveProperties();
        }
		elseif($type==9)
        {
            $applies_to=SeoAttributes::ActiveSpecials();
        }
        
        return view('admin.seo.create',compact('status','related_to','source','properties','language','type','type_name','applies_to'));
    }

     public function insert(Request $request) {
        
       if($request->type==3 || $request->type==4 || $request->type==9)
       {
           $validation = array(
            'title' => 'required|max:255',
            'applies_to' => 'required|max:255',
            'type' => 'required',
            'friendly_url' => 'required|max:255',
            'status' => 'required',
            );
       }
       else
       {
           $validation = array(
            'title' => 'required|max:255',
            'applies_to' => 'required|max:255',
            'type' => 'required',
            'friendly_url' => 'required|max:255',
            'status' => 'required',
            );
       }
         
        
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        { 
            $seo = new SeoAttributes;
            $seo->title = $request->title;
            $seo->language=$request->language;
            $seo->applies_to = $request->applies_to;
            $seo->meta_keyword = $request->meta_keyword;
            $seo->meta_description = $request->meta_description;
            $seo->primary_name = $request->primary_name;
            $seo->type = $request->type;
            $seo->status = $request->status;
            $seo->friendly_url = $request->friendly_url;
            $seo->path=$request->path; 
            $seo->save();
            
            \Session::flash('success', 'Seo Attribute Added Successfully!');
            return redirect('admin/seo');
        }
        
       
    }
    
    public function edit($id) {
        $model = SeoAttributes::findOrFail($id);
        
        $type=$model->type;
       
        $status=SeoAttributes::$status;
        
        $language=SeoAttributes::$language;
        
        $types=SeoAttributes::$type;
        
        if(array_key_exists($type, $types)) 
        {
           $type_name = $types[$type];
        }
        $applies_to='';
        if($type==3)
        {
            $applies_to=PropertyFinder::orderby("name","asc")->where("status","=",1)->lists("name", "id")->prepend('Select', '');
        }
        elseif($type==4)
        {
            $applies_to=Properties::ActiveProperties();
        }
		elseif($type==9)
        {
            $applies_to=SeoAttributes::ActiveSpecials();
        }
        return view('admin.seo.edit',compact('model','status','related_to','source','properties','language','type','type_name','applies_to'))->with('id', $id);
        
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $seoial = SeoAttributes::findOrFail($id);
        $input = $request->all();
      
        unset($input['_token']);
        
        
        if($request->type==3 || $request->type==4 || $request->type==9)
       {
           $validation = array(
            'title' => 'required|max:255',
            'applies_to' => 'required|max:255',
            'type' => 'required',
            'friendly_url' => 'required|max:255',
            'status' => 'required',
            );
       }
       else
       {
           $validation = array(
            'title' => 'required|max:255',
            'applies_to' => 'required|max:255',
            'type' => 'required',
            'friendly_url' => 'required|max:255',
            'status' => 'required',
            );
       }
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
            
            $input['title'] = $request->title;
            $input['language'] = $request->language;
            $input['applies_to'] = $request->applies_to;
            $input['meta_keyword'] = $request->meta_keyword;
            $input['meta_description'] = $request->meta_description;
            $input['primary_name'] = $request->primary_name;
            $input['type'] = $request->type;
            $input['status'] = $request->status;
            $input['friendly_url'] = $request->friendly_url;
            $input['path'] = $request->path; 
         
         $affectedRows = SeoAttributes::where('id', '=', $id)->update($input);

        \Session::flash('success', 'Seo Attribute Updated Successfully!');
        return redirect('admin/seo');
    }
    
    public function delete($id) {
        $row = SeoAttributes::find($id)->delete();
        \Session::flash('success', 'Seo Attribute Deleted Successfully!');
        return redirect('admin/seo');
    }

}
