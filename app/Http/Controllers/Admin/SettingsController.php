<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\Settings;
use App\Policy;
use App\RateSettings;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Auth;

class SettingsController extends AdminController {

   
    public function __construct() 
    {
        parent::__construct();
    }

    public function index() {
        $model = Settings::findOrFail(1);
        
        $booking_mode=Settings::$booking_mode;
        
        $rate_mode=Settings::$rate_mode;
        
        $payment_method=Settings::$payment_method;
        
        $legend_view=Settings::$legend_view;
        
        $contents=Policy::all();
        
        $ratesettings=RateSettings::all();
        
        foreach($contents as $content)
        {
            if($content->id==1)
            {
                $id1=$content->id;
                $title=$content->title;
                $description=$content->description;
            }
            elseif($content->id==2)
            {
                $id2=$content->id;
                $title2=$content->title;
                $description2=$content->description;
            }
        }
        
        return view('admin.settings.index', compact('model','booking_mode','rate_mode','payment_method','legend_view','title','title2','description','description2','id1','id2','ratesettings'));
    }
    
     public function update($id, Request $request) {
        $id = $request->id;
        $user = Settings::findOrFail($id);
        $input = $request->all();
        
        unset($input['_token']);
      
       /*$validation = array(
            'id' => 'required|max:100',

            );
       
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors())->withInput();

        }*/
//      / dd($input);   
       $input['advance_days']=$request->advance_days;
       $input['from_days']=$request->from_days;
       $input['booking_mode']=$request->booking_mode;
       $input['checkin_time']=$request->checkin_time;
       $input['checkout_time']=$request->checkout_time;
       $input['hold_time']=$request->hold_time;
       $input['min_stay']=$request->min_stay;
       $input['cancel_payment']=$request->cancel_payment;
       if(isset($request->payment_method))
       {
            $method=implode(",", $request->payment_method);
            $input['payment_method']=$method;
       }
       else
       {
           $input['payment_method']='';
       }
       $input['date_supplied']=$request->date_supplied;
       $input['nodate_supplied']=$request->nodate_supplied;
       $input['no_date_supplied']=$request->no_date_supplied;
       $input['with_date_supplied']=$request->with_date_supplied;
       $input['no_rate_avail']=$request->no_rate_avail;
       if(isset($request->consolidates_service_total))
       {
            $service=implode(",", $request->consolidates_service_total);
            $input['consolidates_service_total']=$service;
       }
       else
       {
           $input['consolidates_service_total']='';
       }
       $input['lock_rate']=$request->lock_rate;
       $input['length_of_stay']=$request->length_of_stay;
       $input['rate_mode']=$request->rate_mode;
       if(isset($request->party_owner))
       {
            $party_owner=implode(",", $request->party_owner);
            $input['party_owner']=$party_owner;
       }
       else
       {
           $input['party_owner']='';
       }
       $input['day_ahead']=$request->day_ahead;
       $input['day_behind']=$request->day_behind;
       $input['num_pros']=$request->num_pros;
       $input['legend_view']=$request->legend_view;
       
       if(count($request->ids)>0)
       {
           $ids=$request->ids;
           $title= $input['title'];
           $description=$input['description'];
           $input_pol=array();
           
           $i=0;
           foreach($ids as $p_id)
           {
               $input_pol["title"]=$title[$i];
               $input_pol["description"]=$description[$i];
               
               $policy = Policy::where("id",$p_id)->update($input_pol);

                $i++;
           }
                   
       }
       unset($input['ids']);
       unset($input['title']);
       unset($input['description']);
       $affectedRows = Settings::where('id', '=', $id)->update($input);
        
        
        \Session::flash('success', 'Updated Successfully!');
        return redirect('admin/settings');
    }


 
    public function insert(Request $request) {
       
        /*$validation = array(
            'name' => 'required|max:100',

            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
         * 
        {*/
            
            $input=$request->all();
            unset($input['_token']);
            $rate_id=$input["rate_id"];
            $length_stay=$input["length_stay"];
            $description=$input["description"];
            $s_prompt=$input["s_prompt"];
            $p_prompt=$input["p_prompt"];
            $avg_night=(isset($input["avg_night"]))?$input["avg_night"]:0;
            $status=(isset($input["status"]))?$input["status"]:0;
            //dd($input);
            $i=0;
            foreach($description as $desc)
            {
                $input["rate_id"]=$rate_id[$i];
                $input["length_stay"]=$length_stay[$i];
                $input["s_prompt"]=$s_prompt[$i];
                $input["p_prompt"]=$p_prompt[$i];
                $input["avg_night"]=(isset($avg_night[$i]))?$avg_night[$i]:0;
                $input["status"]=(isset($status[$i]))?$status[$i]:0;
                $input["description"]=$desc;
              
                if($desc!="" && $input["length_stay"]!="")
                {
                    if($input["rate_id"]!=0)
                    {
                        $id=$input["rate_id"];
                        unset($input["rate_id"]);
                        RateSettings::where("id","=",$id)->update($input);
                        //dd($input);
                    }
                    elseif($input["rate_id"]==0) 
                    {
                        $rate=new RateSettings;
                        $rate->length_stay=$input["length_stay"];  
                        $rate->description=$input["description"]; 
                        $rate->s_prompt=$input["s_prompt"]; 
                        $rate->p_prompt=$input["p_prompt"]; 
                        $rate->avg_night=$input["avg_night"]; 
                        $rate->status=$input["status"]; 
                        $rate->save();
                       
                    }   
                }
                
                $i++;
                
            }
            

            \Session::flash('success', 'Rate Setting Updated Successfully!');
            return redirect('admin/settings');
        //}
        
       
    }

   
   

}
