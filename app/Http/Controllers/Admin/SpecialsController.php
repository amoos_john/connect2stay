<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator,
    Input,
    Redirect;
use App\Specials;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Gallery;
use App\GalleryImage;
use App\Properties;
use App\PropertySpecials;

class SpecialsController extends AdminController {

    public function __construct() 
    {
        parent::__construct();
    }

    public function index() {
        $model = Specials::paginate(10);
        
        $status=Specials::$status;
                
        $type=Specials::$type;

        return view('admin.specials.index', compact('model','status','type'));
    }
    public function search(Request $request)
    {
        $types = $request->input('type');
        $status_id = $request->input('status');
      
        $search['type']=$types;
        $search['status']=$status_id;
        
        $model=Specials::search($search);
        
        $status=Specials::$status;
        
        $type=Specials::$type;
        
        return view('admin.specials.index', compact('model','status','type','status_id','types'));
    }

    public function create(Request $request) {
        $type=$request->type;
       
        $status=Specials::$status;
        
        $applicability=Specials::$applicability;
        
        $cal_base=Specials::$cal_base;
        
        $gallery_id=session_id();
        
        return view('admin.specials.create',compact('status','type','applicability','gallery_id','cal_base'));
    }

     public function insert(Request $request) {
        
        $validation = array(
            'name' => 'required|max:255',
            'public_name' => 'required|max:255',
            'coupon_code' => 'required|unique:specials|max:255',
            'status' => 'required',
            'type' => 'required',
            'effective' => 'required|date',
            'expiration' => 'required|date',
            'cutoff_date' => 'required|date',
            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $gallery=new Gallery;
            $gallery->title=$request->name;  
            $gallery->type=2;
            $gallery->save();
            
            $spec = new Specials;
            $spec->name = $request->name;
            $spec->public_name = $request->public_name;
            $spec->status = $request->status;
            $spec->gallery_id=$gallery->id;
            $spec->type = $request->type;
            $spec->applicability = $request->applicability;
            $spec->coupon_code = $request->coupon_code;
            $spec->effective = $request->effective;
            $spec->expiration = $request->expiration;
            $spec->cutoff_date = $request->cutoff_date;
            $spec->min_stay = $request->min_stay;
            $spec->amount = $request->amount;
            $spec->show_site = (isset($request->show_site))?$request->show_site:0;
            $spec->auto_apply = (isset($request->auto_apply))?$request->auto_apply:0;
            $spec->notes = $request->notes;
            $spec->summary = $request->summary;
            $spec->description = $request->description;
            $spec->terms = $request->terms;
            $spec->lower_taxes = (isset($request->lower_taxes))?$request->lower_taxes:0;
            
            if($request->type==3)
            {
                $spec->cal_base = $request->cal_base;
            }
            
            $spec->save();
            
            if(count($request->image_id)>0)
            {
                $gallery_temp=$request->gallery_id;
                $input_gall["gallery_id"]=$gallery->id;
                $gallery_image = GalleryImage::where("gallery_id",$gallery_temp)->update($input_gall);
                
                $images=$request->image_id;
                $display_order=$request->display_order;
                $image_caption=$request->image_caption;
                $i=0;
               
                foreach($images as $image)
                {
                    $input_image["display_order"]=$display_order[$i];
                    $input_image["image_caption"]=$image_caption[$i];
                   
                    $gallery_image = GalleryImage::where("id",$image)->update($input_image);
                    
                    $i++;
                }
            }
            
            \Session::flash('success', 'Special Added Successfully!');
            return redirect('admin/specials');
        }
        
       
    }
    
    public function edit($id) {
        $model = Specials::findOrFail($id);
        
        $type=$model->type;
       
        $status=Specials::$status;
        
        $applicability=Specials::$applicability;
        
        $cal_base=Specials::$cal_base;
        
        $gallery_id=$model->gallery_id;
        
        $properties=Properties::allProperties('special');
        
        $url='admin/specials/multiple';
        
        $module='special';

        return view('admin.specials.edit',compact('model','module','status','type','applicability','gallery_id','cal_base','properties','url'))->with('id', $id);
        
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $special = Specials::findOrFail($id);
        $input = $request->all();
      
        unset($input['_token']);
        
        
       $validation = array(
            'name' => 'required|max:255',
            'public_name' => 'required|max:255',
            'coupon_code' => 'required|max:255|unique:specials,coupon_code,'. $special->id,
            'status' => 'required',
            'type' => 'required',
            'effective' => 'required|date',
            'expiration' => 'required|date',
            'cutoff_date' => 'required|date',
            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
            
            $input['name'] = $request->name;
            $input['public_name'] = $request->public_name;
            $input['status'] = $request->status;
            $input['gallery_id']=$request->gallery_id;
            $input['type'] = $request->type;
            $input['applicability'] = $request->applicability;
            $input['coupon_code'] = $request->coupon_code;
            $input['effective'] = $request->effective;
            $input['expiration'] = $request->expiration;
            $input['cutoff_date'] = $request->cutoff_date;
            $input['min_stay'] = $request->min_stay;
            $input['amount']= $request->amount;
            $input['show_site'] = (isset($request->show_site))?$request->show_site:0;
            $input['auto_apply'] = (isset($request->auto_apply))?$request->auto_apply:0;
            $input['notes'] = $request->notes;
            $input['summary'] = $request->summary;
            $input['description'] = $request->description;
            $input['terms'] = $request->terms;
            $input['lower_taxes'] = (isset($request->lower_taxes))?$request->lower_taxes:0;
            
            if($request->type==3)
            {
                $input['cal_base']= $request->cal_base;
            }
        
        if(count($request->image_id)>0)
         {
                $images=$request->image_id;
                $display_order=$request->display_order;
                $image_caption=$request->image_caption;
                $i=0;
                foreach($images as $image)
                {
                    $input_image["display_order"]=$display_order[$i];
                    $input_image["image_caption"]=$image_caption[$i];
                    
                    $gallery_image = GalleryImage::where("id",$image)->update($input_image);
                    
                    
                    $i++;
                }
          }
         unset($input['image_id']);
         unset($input['display_order']);
         unset($input['image_caption']);
         unset($input['change_id']);
         $affectedRows = Specials::where('id', '=', $id)->update($input);

        \Session::flash('success', 'Special Updated Successfully!');
        return redirect('admin/specials');
    }
    public function multiple(Request $request) {
        
        $validation = array(
            'id' => 'required|max:255',
            );
       
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            $special_id=$request->id;
            $delete_pro=(isset($request->delete))?$request->delete:'';
            $specials=Specials::findOrFail($special_id);
            if($specials->status==0)
            {
                return redirect()->back()
    		->with('success','Special was applied to 0 Properties');
            }
            elseif(count($request->property_id)>0)
            {
                $property_id=$request->property_id;
                $data=array();
                foreach($property_id as $pro)
                {
                    $data[] = array('property_id'=>$pro, 'special_id'=> $special_id,'created_at'=>date('Y-m-d H:i:s'))
                    ;
                }
                if(count($data)>0)
                {
                    if($delete_pro!='')
                    {
                        $property_to_delete = array_map(function($delproperty){ return $delproperty['property_id']; }, $data);
                        $delete = PropertySpecials::whereIn('property_id', $property_to_delete)
                        ->where("special_id","=",$special_id)->delete();
                    }
                    else
                    {
                       $delete = PropertySpecials::specialDelete($special_id); 
                    }
                  
                    PropertySpecials::insert($data);
                } 
            }
            else
            {
                 $delete = PropertySpecials::specialDelete($special_id); 

            }
          
             return redirect()->back()
    		->with('success','Special was applied to all Properties');
            
        }
        
       
    }

    public function delete($id) {
        $row = Specials::find($id)->delete();
        \Session::flash('success', 'Special Deleted Successfully!');
        return redirect('admin/specials');
    }

}
