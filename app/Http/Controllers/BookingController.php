<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator,
    Input,
    Redirect,
    Session,
    Config,
    Mail;
use App\Functions\Functions;
use App\Properties;
use App\RatesandAvailability;
use App\FeesTaxes;
use App\Booking;
use App\BookingInformation;
use App\BookingItems;
use App\Content;
use Netshell\Paypal\Paypal;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Error\Card;
use App\Settings;
use App\GalleryImage;

class BookingController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Booking Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    public function __construct()
    {
        session_start();
	$this->paypal= new PayPal();
	   
        $this->_apiContext = $this->paypal->ApiContext(
            config('paypal.express.client_id'),
            config('paypal.express.secret'));

        $this->_apiContext->setConfig(config('paypal.express.config'));
             
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function makeBooking(Request $request)  
    {
        $id=$request->id;
        $start_date=$request->start_date;
        $end_date=$request->end_date;
        $guest=$request->guest;
        $children=$request->children;
        if($start_date!='')
        {
            Session::put('start_date',$start_date);
        }
        else
        {
            Session::forget('start_date');
        }
        if($end_date!='')
        {
            Session::put('end_date',$end_date);
        }
        else
        {
            Session::forget('end_date');
        }
        if($guest!='')
        {
            Session::put('guest',$guest);
        }
        else
        {
            Session::forget('guest');
        }
        //$key = Functions::setBooking($start_date,$end_date,$guest,$children,$id);
   
        $keys = Functions::generateRandomString(6);
        
        $item = [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'guest' => $guest,
            'children' => $children,
            'id' => $id,
          ];
        
        
        $key = "key".$keys;
        Session::put($key,$item);
        Session::save();
        
        return redirect("booking/".$keys);
        
    }

    public function booking(Request $request)  
    {
        //Session::flush();
        $keys = "key".$request->key;
        
        $data = Session::get($keys);
        
        $id=$data["id"];
        $start_date=$data["start_date"];
        $end_date= $data["end_date"];
        $guest=$data["guest"];
        $children= $data["children"];
        
        $property=Properties::getPropertiesId($id);
        $gallery_id=(count($property)>0)?$property[0]->gallery_id:0;
        $gallery_images=GalleryImage::where("gallery_id","=",$gallery_id)->orderby("display_order","ASC")->get();
        
        $rates = RatesandAvailability::getRates($start_date,$end_date,$id);
        
        $checkin = Functions::DateFormat($start_date);
        $checkout = Functions::DateFormat($end_date);
        
        $rent = (count($rates)>0)?$rates[0]->rent:'';
        
        $nights=Functions::nights($start_date,$end_date);
        
        $min_stay=(count($property)>0)?$property[0]->min_stay:'';
         
        $feestaxes=FeesTaxes::getFeesTaxes($id);
        
        $otherfees=0;
        foreach($feestaxes as $feetax)
        {
            $otherfees+=$feetax->amount;
        }
        
        $total=$rent+$otherfees;
       
        $rate=RatesandAvailability::getDateRate($start_date,$id);
        
        $daily=(count($rate)>0)?$rate[0]->daily:'0';
        
        $countries = Booking::$countries;
        
        $lead_source = Booking::$lead_source;
        
        $dates_not_avail = RatesandAvailability::datesNotAvail(date("Y-m-d"),$id);
        
        $checkdate = RatesandAvailability::checkDatesAvail($start_date,$end_date,$id);
        
        $page_title = "Book Vacation Rentals in the ".(count($property)>0)?$property[0]->public_headline:'';
       
        return view('front.booking',compact("id","gallery_images","page_title","keys","checkin","","checkout","lead_source","checkdate","min_stay","dates_not_avail","countries","feestaxes","daily","checkin","checkout","nights","guest","children","rent","property","otherfees","total"))->with(["start_date"=>$start_date,"end_date"=>$end_date]);
    }
    public function bookinginfo(Request $request)  
    {
        
        $key = $request->key; 
        $id=$request->id;
        $start_date=($request->start_date!="")?$request->start_date:'0000-00-00';
        $end_date=($request->end_date!="")?$request->end_date:'0000-00-00';
        $guest=$request->guest;
        $children=$request->children;
        if($start_date!='')
        {
            Session::put('start_date',$start_date);
        }
        else
        {
            Session::forget('start_date');
        }
        if($end_date!='')
        {
            Session::put('end_date',$end_date);
        }
        else
        {
            Session::forget('end_date');
        }
        if($guest!='')
        {
            Session::put('guest',$guest);
        }
        else
        {
            Session::forget('guest');
        }
        
        $property=Properties::getPropertiesId($id);
        
        $item = [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'guest' => $guest,
            'children' => $children,
            'id' => $id,
          ];
        
        //$_SESSION[$key] = $item;
        $keys = "key".$key;
        Session::put($keys,$item);
        Session::save();
        
        $rates = RatesandAvailability::getRates($start_date,$end_date,$id);
        
        $rent = (count($rates)>0)?$rates[0]->rent:0;
        
        $nights = Functions::nights($start_date,$end_date);
        
        $min_stay=(count($property)>0)?$property[0]->min_stay:'';
        
        $checkin = Functions::DateFormat($request->start_date);
        $checkout = Functions::DateFormat($request->end_date);
        
        $property=Properties::getPropertiesId($id);
        
        $feestaxes=FeesTaxes::getFeesTaxes($id);
        
        $rate=RatesandAvailability::getDateRate($start_date,$id);
        
        $daily=(count($rate)>0)?$rate[0]->daily:'0';
        
        $otherfees=0;
        foreach($feestaxes as $feetax)
        {
            $otherfees+=$feetax->amount;
        }
        
        $total = $rent+$otherfees;
        
        $dates_not_avail = RatesandAvailability::datesNotAvail(date("Y-m-d"),$id);
        
        $checkdate = RatesandAvailability::checkDatesAvail($start_date,$end_date,$id);

        return view('front.bookinginfo',compact("id","keys","checkdate","min_stay","daily","dates_not_avail","feestaxes","checkin","checkout","nights","guest","children","rent","property","otherfees","total"))->with(["start_date"=>$start_date,"end_date"=>$end_date]);
    }
    public function insert(Request $request) {
        
        $validation = array(
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
            'payment_type' => 'required',

            );
       
        if($request->payment_type==2)
        {
            $validation = array(
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
            'payment_type' => 'required',   
            'card_no' => 'required',
            'ccExpiry' => 'required',
            'cvcNumber' => 'required',

            );
        }
        
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else
        {
            DB::beginTransaction();
            try
            {
            $setting = Settings::find(1);    
            $payment_type = $request->payment_type;
            $property_id = $request->property_id;
            $checkin = $request->checkin;
            $checkout = $request->checkout;
            $email = $request->email;
            $grand_total=$request->grand_total;
            $currency= Config::get('params.currency');
            $symbol=$currency["BRL"]["symbol"];
                
            $booking = new Booking;
            if($payment_type==1)
            {
                $booking->payment_type = 'paypal';
            }
            elseif($payment_type==2)
            {
                $booking->payment_type = 'stripe';
            }
            
            $booking->property_id = $property_id;
            $booking->checkin = $request->checkin;
            $booking->checkout = $request->checkout;
            $booking->checkin_time = $setting->checkin_time;
            $booking->checkout_time = $setting->checkout_time;
            $booking->adults = $request->guest;
            $booking->children = $request->children;
            $booking->grand_total = $grand_total;
            $booking->status = 1;
            $booking->save();
            
            $booking_id=$booking->id;
            
            $info = new BookingInformation;
            $info->booking_id = $booking_id;
            $info->first_name = $request->first_name;
            $info->last_name = $request->last_name;
            $info->email = $email;
            $info->phone = $request->phone;
            $info->lead_source = $request->lead_source;
            $info->special = $request->special;
            $info->country = $request->country;
            $info->address = $request->address;
            $info->address2 = $request->address2;
            $info->city = $request->city;
            $info->state = $request->state;
            $info->postal_code = $request->postal_code;
            $info->save();
                
            $property = Properties::find($property_id);   
            
            $quantity = Functions::nights($checkin,$checkout);

            $rate = RatesandAvailability::getDateRate($checkin,$property_id);

            $rent =(count($rate)>0)?$rate[0]->daily:0;

            $description = $quantity." Nights @".$symbol.''.Functions::MoneyFormat($rent)." ".$checkin."-".$checkout;
            
            $billing_item = new BookingItems;
            $billing_item->booking_id = $booking_id;
            $billing_item->type = 2;
            $billing_item->description = $description;
            $billing_item->taxed = 0;     
            $billing_item->quantity = $quantity; 
            $billing_item->rate = $rent; 
            $billing_item->amount = $rent*$quantity; 
            $billing_item->save();
            
            $feestaxes = FeesTaxes::getFeesTaxes($property_id);
            
            foreach($feestaxes as $feetax)
            {
                $type = '';
                if($feetax->type==3)
                {
                    $type = 3;
                }
                if($feetax->type==2 || $feetax->type==1)
                {
                    $type = 4;
                }
                $billingitem = new BookingItems;
                $billingitem->booking_id = $booking_id;
                $billingitem->type = $type;
                $billingitem->description = $feetax->public_name;
                $billingitem->taxed = 1;     
                $billingitem->quantity = 1; 
                $billingitem->rate = $feetax->amount; 
                $billingitem->amount = $feetax->amount*1;
                $billingitem->save();  

            }
            
            if($payment_type==1)
            {
                $item = new Item();
                $item->setName($property->public_headline)
                ->setQuantity($quantity)
                ->setPrice($rent)
                ->setCurrency(config('params.currency_default'));  
                $item->setDescription($description);
                $items[]=$item;
                
                $item2 = array();
                foreach($feestaxes as $feetax)
                {
                    $item2 = new Item();
                    $item2->setName($feetax->public_name)
                    ->setQuantity(1)
                    ->setPrice($feetax->amount)
                    ->setCurrency(config('params.currency_default'));     
                }
              
                if(count($item2)>0)
                {
                    $items[]=$item2;
                }
                

                $item_list = new ItemList();
                $item_list->setItems($items);

                $payer = new Payer();
                $payer->setPaymentMethod('paypal');


                $details = new Details();
                $details->setShipping($grand_total)
                    ->setTax(0)
                    ->setSubtotal($grand_total);

                $amount = new Amount();
                $amount->setCurrency(config('params.currency_default'));
              
                $amount->setTotal($grand_total);
               
                $transaction = new Transaction();
               
                $transaction->setAmount($amount)->setInvoiceNumber($booking_id)
                ->setItemList($item_list)
                ->setDescription(config('params.site_name').' Order');
               
                $redirectUrls = new RedirectUrls();
                $redirectUrls->setReturnUrl(action(config('paypal.express.success')));
                $redirectUrls->setCancelUrl(action(config('paypal.express.cancel')));

                $payment = new Payment();
                $payment->setIntent('sale');
                $payment->setPayer($payer);
                $payment->setRedirectUrls($redirectUrls);
                $payment->setTransactions(array($transaction));
                //dd($payment);   
                try 
                {
                    $response = $payment->create($this->_apiContext);
                    $redirectUrl = $response->links[1]->href;
                    Session::put('book_id', $booking_id);
                    DB::commit();
                  
                    return Redirect::to( $redirectUrl );
                }catch (\PayPal\Exception\PayPalConnectionException $ex) {
                    echo $ex->getData();
                    die('here');
                }
           } 
           elseif($payment_type==2)
           {
               
               $ccExpiry = explode("/",$request->ccExpiry);
               $ccExpiryMonth = trim($ccExpiry[0]);
               $ccExpiryYear = trim($ccExpiry[1]);
               $redirectUrl = "stripe/success";
               $redirectUrlFail = "stripe/cancel";
               $stripe = Stripe::make(env("STRIPE_SECRET"));
               
               
               try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'name'    => $request->card_name,
                        'email'   => $email,
                        'number'    => $request->card_no,
                        'exp_month' => $ccExpiryMonth,
                        'exp_year'  => $ccExpiryYear,
                        'cvc'       => $request->cvcNumber,
                    ],
                ]);
                if (!isset($token['id'])) {
                    \Session::put('error','The Stripe Token was not generated correctly');
                    return redirect($redirectUrlFail);
                }
                
                $description .= "\n Booking ID: ".$booking_id;
                $description .= "\n Property ID: ".$property_id;
                
                
                $charge = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => config('params.currency_default'),
                    'amount'   => $grand_total,
                    'description' => $description,
                ]);
                Session::put('book_id', $booking_id);
                $stripeCard = [
                        'card_name'    => $request->card_name,
                        'email'   => $email,
                        'card_no'    => $request->card_no,
                        'month' => $ccExpiryMonth,
                        'year'  => $ccExpiryYear,
                        'cvc'       => $request->cvcNumber,
                    ];
                
                if($charge['status'] == 'succeeded') {
                    /**
                    * Write Here Your Database insert logic.
                    */
                    DB::commit();
                    return redirect($redirectUrl)->with("charge",$charge)
                            ->with("stripeCard",$stripeCard);
                    
                } else {
                    \Session::flash('error','Stripe Payment has been failed!');
                    return redirect($redirectUrlFail);
                }
            } catch (Exception $e) {
                \Session::flash('error',$e->getMessage());
                return redirect($redirectUrlFail);
            } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
                \Session::flash('error',$e->getMessage());
                return redirect($redirectUrlFail);
            } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                \Session::flash('error',$e->getMessage());
                return redirect($redirectUrlFail);
            }
           }
            
            //return redirect('checkout/success/'.$order_id);
            
            

            //\Session::flash('success', 'Booking submitted successfully!');
            }
            catch(Exception $e)
            {
                 DB::rollBack();
                 return redirect('booking/fail');
            }
        }
        
       
    }
 
    
}
