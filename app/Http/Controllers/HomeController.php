<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator,
    Input,
    Redirect,
    Config,
    Mail;
use App\Functions\Functions;
use App\Content;
use App\Booking;
use App\Policy;
use App\Reviews;

class HomeController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    protected $categories = array();
    protected $layout = 'layouts.search';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function index() {

        $page_title = 'Connect2Stay - Vacation Rentals in the Alpes do Bom Jesus, Atibaia, Brasil Area';
        $meta_description = 'Connect2Stay, the Alpes do Bom Jesus, Atibaia, Brasil, Brazil, Cabore, Caraguatatuba, Caraguatatuba Area, Vacation Rentals';
        return view('front.index',compact("content","page_title","meta_description"));
    }
    public function home() {
        
        if(isset(Auth::user()->id))
        {
            return redirect('admin');
        }
        else
        {
            $page_title = 'Connect2Stay - Vacation Rentals in the Alpes do Bom Jesus, Atibaia, Brasil Area';
            $meta_description = 'Connect2Stay, the Alpes do Bom Jesus, Atibaia, Brasil, Brazil, Cabore, Caraguatatuba, Caraguatatuba Area, Vacation Rentals';
            return view('front.index',compact("content","page_title","meta_description"));
        }
    }
    
    public function about() {
        
        $content = Content::where("code","=","about_us")->get();
        
        $page_title = $content[0]->metaTitle;
        
        return view('front.aboutus',compact("content","page_title"));
    }
    public function advertise() 
    {
        $page_title = "Advertise";   
        
        return view('front.advertise',compact("page_title"));
    }
    public function faq() 
    {
        $page_title = " >> FAQ";   
        
        return view('front.faq',compact("page_title"));
    }
    public function works() 
    {
        $page_title = " >> how it works";   
        
        return view('front.works',compact("page_title"));
    }
    public function contact() 
    {
        $page_title = " >> Contact";   
        
        return view('front.contact',compact("page_title"));
    }
    public function send(Request $request)
    {
        $validation = array(
            'full_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
            'message' => 'required|max:255',
           
            );
        
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else 
        {
            
            $replaces['LOGO'] = asset('front/images/email.png');
            $replaces['URL'] = Config::get("params.url");
            
            $replaces['NAME'] = $request->full_name;
            $replaces['EMAIL'] =  $request->email;
            $replaces['PHONE'] =  $request->phone; 
            $replaces['MESSAGE'] = $request->message;
          
            
            $content = Content::where("code","=","contact")->get();
            
            $template = Functions::setEmailTemplate($content, $replaces);
           
            $subject = $template["subject"];
            $body = $template["body"];
            
            $email = Config::get("params.admin_email");
            $from = $request->email;

            $send_email = Functions::sendEmail($email,$subject,$body,'',$from);
            
            \Session::flash('success', 'Thank you for your message. It has been sent.!');
            return redirect('contact');
            
        }
    }
    public function policy() 
    {
        $page_title = " >> Policy";   
        
        return view('front.policy',compact("page_title"));
    }
    
    public function registerProperty() 
    {
        $page_title = "Sign up to advertise";
        
        $countries = Booking::$countries;
        
        return view('front.register',compact("page_title","countries"));
    }
    public function sendRegisterProperty(Request $request)
    {
        $validation = array(
            'full_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
            'address' => 'required|max:255',
            'city' => 'required|max:255',
            'state' => 'required|max:255',
            'zipcode' => 'required|max:255',
            'category' => 'required|max:255',
            'stay' => 'required|max:255',
            'rooms' => 'required|max:255',
            'baths' => 'required|max:255',
            );
        
        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            return redirect()->back()->withErrors($validator->errors())->withInput();
          
        }
        else 
        {
            
            $replaces['LOGO'] = asset('front/images/email.png');
            $replaces['URL'] = Config::get("params.url");
            
            $replaces['NAME'] = $request->full_name;
            $replaces['EMAIL'] =  $request->email;
            $replaces['PHONE'] =  $request->phone;
            
            $replaces['ADDRESS'] = $request->address;
            $replaces['APT'] =  $request->apt;
            $replaces['CITY'] =  $request->city;
            $replaces['STATE'] =  $request->state;
            $replaces['ZIPCODE'] =  $request->zipcode;
            $replaces['COUNTRY'] =  $request->country;
            
            $replaces['CATEGORY'] = $request->category;
            $replaces['STAY'] = $request->stay;
            $replaces['ROOMS'] = $request->rooms;
            $replaces['BATHS'] = $request->baths;
            $area="";
            if(isset($request->areas))
            {
                $area = implode(",", $request->areas);
               
            }
            $replaces['AREAS'] = $area;
            $living_room="";
            if(isset($request->living_rooms))
            {
                $living_room = implode(",", $request->living_rooms);
               
            }
            $replaces['LIVINGROOMS'] = $living_room;
            
            $kitchen="";
            if(isset($request->kitchen))
            {
                $kitchen = implode(",", $request->kitchen);     
            }
            $replaces['KITCHEN'] = $kitchen;
            
            $bedroom="";
            if(isset($request->bedroom))
            {
                $bedroom = implode(",", $request->bedroom);   
            }
            $replaces['BEDROOM'] = $bedroom;
            
            $wc="";
            if(isset($request->wc))
            {
                $wc = implode(",", $request->wc);   
            }
            $replaces['WC'] = $wc;
            $others="";
            if(isset($request->others))
            {
                $others = implode(",", $request->others);   
            }
            $replaces['OTHERS'] = $others;
            
            
            
            $content = Content::where("code","=","property_register")->get();
            
            $template = Functions::setEmailTemplate($content, $replaces);
           
            $subject = $template["subject"];
            $body = $template["body"];
            
            $email = Config::get("params.admin_email");
            $from = $request->email;

            $send_email = Functions::sendEmail($email,$subject,$body,'',$from);
            
            \Session::flash('success', 'Thank you for your message. It has been sent.!');
            return redirect('register-your-property');
            
        }
    }
    public function rentalpolicy() 
    {
        $page_title = "Rental Policy for Connect2Stay - Specializing in Vacation Rentals";  
        
        $content = Policy::find(1);
        
        return view('front.rentalpolicy',compact("content","page_title"));
    }
    public function reviews(Request $request) 
    {
        $page_title = "Write a review";
        $token = $request->token;
        if($token!='')
        {
            $model = Reviews::where("token","=",$token)->first();
            return view('front.reviews',compact("page_title","token","model","id"));

        }
        else
        {
            return redirect('/');
        }
    }
    public function reviewInsert(Request $request)
    {
         $validation = array(
            'title' => 'required|max:255',
            'reviewed_by' => 'required|max:255',
            'ratings' => 'required|min:1|max:5|size:1',
            'comment' => 'required|max:255',
        );
        
        $response['error'] = 0;
        $response['error_ip'] = '';

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
          
        }
        else
        {
            //$ip =  $request->ip();
            $token = $request->id;
            $result = Reviews::where("token","=",$token)->first();
            
            if(count($result)>0)
            {
                $id = $result->id;
                //$review = new Reviews;
                $input['title'] = $request->title;
                //$review->property_id=$request->id;
                $input['reviewed_by'] = $request->reviewed_by;
                $input['ratings'] = $request->ratings;
                $input['source']= 1;
                $input['related_to']  = 1;
                $input['comment']  = $request->comment;
                $input['status'] = 0;
                $input['token'] = '';
                $review = Reviews::where("id","=",$id)->update($input);
            }
            else
            {
                $response['error_ip'] = '<p>Your Reviews already exist for this property!</p>';

            }
           
            
        }
        if($response['error']==1)
        {
            foreach($response['errors']->all() as $error)
            {
                echo '<p>'.$error.'</p>';
                
            }     
        }
        else
        {
            if($response['error_ip']!="")
            {
                 echo $response['error_ip'];
            }
            else
            {
                echo '0';
            }
            
        }
    }
    
   
}
