<?php 
namespace App\Http\Controllers\Payments;
use Session,Config;
use DB;
use App\Stripe;
use App\Content;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator, Input, Redirect; 
use App\Booking;
use App\BookingInformation;
use App\RatesandAvailability;
use App\Functions\Functions;
use App\Properties;
use Illuminate\Http\Request;

class StripeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    private $sessionId;
    
    public function __construct()
    {
	  //$this->middleware('auth');
	  session_start();
	  $this->sessionId=session_id();
    }
    
    public function success()
    {
      $booking_id = Session::get('book_id');
      $check = Stripe::where('booking_id','=',$booking_id)->count();
      
      if($check==0)
      {
        $stripeCard = Session::get("stripeCard");
        $charge = Session::get("charge");
          
        $model=new Stripe();
        $model->booking_id=$booking_id;
        $model->charge_id=$charge['id'];
        $model->card_no=$stripeCard['card_no'];
        $model->card_name=$stripeCard['card_name'];
        $model->month=$stripeCard['month'];
        $model->year=$stripeCard['year'];
        $model->cvc=$stripeCard['cvc'];
        $model->save();
        $affectedRows = Booking::where('id','=',$booking_id)->update(array('payment_status' => 'success'));
        
        
        $booked = Booking::find($booking_id);
        
        $statuses = Booking::$status;
        
        $info = BookingInformation::where("booking_id","=",$booking_id)->get();
        
        $name = $info[0]->first_name." ".$info[0]->last_name;
        $email = $info[0]->email;
        $phone = $info[0]->phone;
        $address = $info[0]->address." ".$info[0]->city." ".$info[0]->country;
        $source = $info[0]->lead_source;
        $special = $info[0]->special;
        $checkin = $booked->checkin;
        $checkout = $booked->checkout;
        $property_id = $booked->property_id;
        $guests = $booked->adults;
        $childrens = $booked->children;
        $grand_total = $booked->grand_total;
        
        $update = RatesandAvailability::updateDates($checkin,$checkout,$property_id);
        
        $content = Content::where('code','=','booking_confirmation')->get();
        
        $currency= Config::get('params.currency');
        $symbol=$currency["BRL"]["symbol"];
        
        $result = Properties::with("categories")->find($property_id);
        
        
        $replaces['NAME'] = $name;
        $replaces['ID'] = $booking_id;
        $replaces['EMAIL'] = $email;
        $replaces['CHECKIN'] = Functions::DateFormat($checkin);
        $replaces['CHECKOUT'] = Functions::DateFormat($checkout);
        $replaces['URL'] = Config::get("params.url");
        $replaces['LINK'] = Config::get("params.url")."/details/".$property_id;
        $replaces['LINK2'] = Config::get("params.url")."/details/".$property_id;
        $replaces['TITLE'] = (count($result)>0)?$result->public_headline:'';
        $replaces['LOGO'] = asset('front/images/email.png');
        $replaces['GUESTS'] = $guests;
        $replaces['CHILDRENS'] = $childrens;
        $replaces['SOURCE'] = $source;
        $replaces['PHONE'] = $phone;
        $replaces['ADDRESS'] = $address;
        $replaces['SYMBOL'] = $symbol;
        $replaces['RATE'] =  Functions::MoneyFormat($grand_total);
        $replaces['PAID'] = Functions::MoneyFormat($grand_total);
        $replaces['DUE'] = 0.00;
        $replaces['SPECIAL'] = $special;
        $replaces['TYPE'] = (count($result)>0)?$result->categories->category_name:'';
        $replaces['PAYMENT'] = $booked->payment_type;
        $replaces['EXP'] = "";
        
        if (array_key_exists( $booked->status, $statuses)) {
           $status = $statuses[$booked->status];
        }
        
        $replaces['STATUS'] = $status;
        
        $from = Config::get("params.admin_email");
          
        $template = Functions::setEmailTemplate($content,$replaces);
        $mail = Functions::sendEmail($email,$template['subject'],$template['body'],'',$from);
      }
      
      $content= Content::where('code','=','paypal_success')->firstOrFail();
      
      return view('front.payments.stripe.success',compact('content'));
    }
    
    public function cancel()
    {
        $error = Session::get('error');
        if($error=="")
        {
            $booking_id=Session::get('book_id');
            $check = Stripe::where('booking_id','=',$booking_id)->count();
            $booked = Booking::find($booking_id);

            if($check==0)
            {
                $stripeCard = Session::get("stripeCard");
                if($stripeCard!="")
                {
                    $model=new Stripe();
                    $model->charge_id="";
                    $model->booking_id=$booking_id;
                    $model->card_no=$stripeCard['card_no'];
                    $model->card_name=$stripeCard['card_name'];
                    $model->month=$stripeCard['month'];
                    $model->year=$stripeCard['year'];
                    $model->cvc=$stripeCard['cvc'];
                    $model->save();
                }
                $affectedRows = Booking::where('id','=',$booking_id)->update(array('payment_status' => 'cancel'));

                $content = Content::where('code','=','booking_cancel')->get();

                $info = BookingInformation::where("booking_id","=",$booking_id)->get();

                $name = $info[0]->first_name." ".$info[0]->last_name;
                $email = $info[0]->email;
                $phone = $info[0]->phone;
                $checkin = $booked->checkin;
                $checkout = $booked->checkout;
                $property_id = $booked->property_id;
                $guests = $booked->adults;
                $childrens = $booked->children;
                $grand_total = $booked->grand_total;

                $result = Properties::find($property_id);

                $replaces['NAME'] = $name;
                $replaces['EMAIL'] = $email;
                $replaces['CHECKIN'] = Functions::DateFormat($checkin);
                $replaces['CHECKOUT'] = Functions::DateFormat($checkout);
                $replaces['URL'] = Config::get("params.url");
                $replaces['TITLE'] = (count($result)>0)?$result->public_headline:'';
                $replaces['LOGO'] = asset('front/images/email.png');

                $from = Config::get("params.admin_email");

                $template = Functions::setEmailTemplate($content,$replaces);
                $mail = Functions::sendEmail($email,$template['subject'],$template['body'],'',$from);
                $stripeCard = Session::forget("stripeCard");
                Session::forget('book_id');

            }
        }
        
        $content = Content::where('code','=','paypal_cancel')->firstOrFail();
        return view('front.payments.stripe.cancel',compact('content'));
    }
}
