<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator,
    Input,
    Redirect;
use App\Functions\Functions;
use App\Content;
use App\PropertyFinder;
use App\GalleryImage;

class PropertyFinderController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | PropertyFinder Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function saoPaulo(Request $request) 
    {
        $content = Content::where("code","=","sao_paulo")->get();
        
        $page_title = $content[0]->metaTitle;
        
        return view('front.propertyfinder.index',compact("content","page_title"));
    }
    public function rioDeJaneiro(Request $request) 
    {
        $content = Content::where("code","=","rio_de_janeiro")->get();
        
        $page_title = $content[0]->metaTitle;
        
        return view('front.propertyfinder.index',compact("content","page_title"));
    }
    public function fortaleza(Request $request) 
    {
        $content = Content::where("code","=","fortaleza")->get();
        
        $page_title = $content[0]->metaTitle;
        
        return view('front.propertyfinder.index',compact("content","page_title"));
    }
    
    
    public function details(Request $request) 
    {
        $url = $request->url;
        
        if($url=="sao-paulo")//-properties
        {
            $model = PropertyFinder::getPropertyFinder(1);   
            
        }
        elseif($url=="rio-de-janeiro")
        {
            $model = PropertyFinder::getPropertyFinder(2);
                 
        }
        elseif($url=="fortaleza")
        {
            $model = PropertyFinder::getPropertyFinder(3);
                
        }
        if(count($model)>0)
        {
            $model = $model[0];
            
            
            if($model->category_id!=0)
            {
                $search["category_id"] = $model->category_id;
            }
            if($model->amentites!='')
            {
                $get_amentites = explode(",",$model->amentites);
                $ids = array_map('intval', $get_amentites); 
                $search["amentites"] = $ids;
            }
            if($model->min_stay_from!='' || $model->min_stay_to!='')
            {
                $search["min_stay_from"] = $model->min_stay_from;
                $search["min_stay_to"] = $model->min_stay_to;
            }
            if($model->sleeps_to!='' || $model->sleeps_from!='')
            {
                $search["sleeps_to"] = $model->sleeps_to;
                $search["sleeps_from"] = $model->sleeps_from;
            }
            if($model->bedrooms_to!='' || $model->bedrooms_from!='')
            {
                $search["bedrooms_to"] = $model->bedrooms_to;
                $search["bedrooms_from"] = $model->bedrooms_from;
            }
            if($model->bathrooms_to!='' || $model->bathrooms_from!='')
            {
                $search["bathrooms_to"] = $model->bathrooms_to;
                $search["bathrooms_from"] = $model->bathrooms_from;
            }
            if($model->properties!='')
            {
                $get_properties = explode(",",$model->properties);
                $ids = array_map('intval', $get_properties); 
                $search["ids"] = $ids;
            }
            if(isset($search))
            {
                $properties = PropertyFinder::getProperties($search);
                $page_title = $model->name;
            }
            else
            {
                 $page_title = 'Result not found!';
            }
            $image = GalleryImage::where("gallery_id","=",$model->gallery_id)
                    ->where("display_order","=",1)
                    ->whereNull("deleted_at")->first();
            
        }
        return view('front.propertyfinder.properties',compact("model","properties","page_title","image"));
    }
    
   
    
    
}
