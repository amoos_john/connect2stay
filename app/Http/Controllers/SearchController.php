<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Validator,
    Input,
    Redirect,
    Session,
    Config,
    Mail;
use App\Functions\Functions;
use App\Properties;
use App\GalleryImage;
use App\Amenities;
use App\RatesandAvailability;
use App\Content;
use App\SeoAttributes;
use App\Reviews;

class SearchController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */
    protected $layout = 'layouts.search';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function index(Request $request) {
        
        
        $city = $request->input('city');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $guest = $request->input('guest');
        $price_max = $request->input('price_max');
        $price_min = $request->input('price_min');
        $results_per_page=$request->input('results_per_page');
        $sort = $request->input('sort');
        $tags = $request->input('tags');
       
        $search['city']=$city;
        $search['start_date']=$start_date;
        $search['end_date']=$end_date;
        $search['guest']=$guest;
        $search['results_per_page']=$results_per_page;
        $search['price_min']=$price_min;
        $search['price_max']=$price_max;
        $search['sort']=$sort;
        $search['tags']=$tags;
        
        $properties=Properties::getProperties($search);
        $page_title='Search Vacation Rentals';
        
        if($start_date!='')
        {
            Session::put('start_date',$start_date);
        }
        else
        {
            Session::forget('start_date');
        }
        if($end_date!='')
        {
            Session::put('end_date',$end_date);
        }
        else
        {
            Session::forget('end_date');
        }
        if($guest!='')
        {
            Session::put('guest',$guest);
        }
        else
        {
            Session::forget('guest');
        }
        
        $parents = Amenities::with("child")->get();
        
        if($request->getQueryString())
        {
             $query = '?city='.$city.'&end_date='.$end_date.'&guest='.$guest.'&start_date='.$start_date;
        }
        
        return view('front.rentals',compact("properties","query","page_title","city","start_date","end_date","guest","results_per_page","parents"));
        
    }
    public function result(Request $request) {
        
        $city = $request->input('city');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $guest = $request->input('guest');
        $price_max = $request->input('price_max');
        $price_min = $request->input('price_min');
        $results_per_page=$request->input('results_per_page');
        $sort = $request->input('sort');
        $tags = $request->input('tags');
       
        $search['city']=$city;
        $search['start_date']=$start_date;
        $search['end_date']=$end_date;
        $search['guest']=$guest;
        $search['results_per_page']=$results_per_page;
        $search['price_min']=$price_min;
        $search['price_max']=$price_max;
        $search['sort']=$sort;
        $search['tags']=$tags;
        
        if($start_date!='')
        {
            Session::put('start_date',$start_date);
        }
        else
        {
            Session::forget('start_date');
        }
        if($end_date!='')
        {
            Session::put('end_date',$end_date);
        }
        else
        {
            Session::forget('end_date');
        }
        if($guest!='')
        {
            Session::put('guest',$guest);
        }
        else
        {
            Session::forget('guest');
        }
        
        $properties=Properties::getProperties($search);
        
        if($request->getQueryString())
        {
             $query = '?city='.$city.'&end_date='.$end_date.'&guest='.$guest.'&start_date='.$start_date;
        }
        
        
        if ($request->ajax()) {
            return view('front.result',compact("properties","query","page_title","city","start_date","end_date","guest","results_per_page"))->render();  
        }
        
        return view('front.result',compact("properties","query","page_title","city","start_date","end_date","guest","results_per_page"));
        
    }
    public function detail(Request $request) {
        
        $id=$request->id;
        $property=Properties::with("categories")->with("property_amentity")->where("status","=",1)
        ->where("id","=",$id)->get();
        
        if(count($property)>0)
        {
        $gallery_id=(count($property)>0)?$property[0]->gallery_id:'';
        
        $gallery_images=GalleryImage::where("gallery_id","=",$gallery_id)->orderby("display_order","ASC")->get();
        
        $parents = Amenities::with("child")->orderby("sort_order","asc")->get();
        
        $seo = SeoAttributes::where("applies_to","=",$id)->get();
        
        $page_title=(count($seo)>0)?$seo[0]->title:'Details ';
        
        $meta_keyword = (count($seo)>0)?$seo[0]->meta_keyword:'';
        
        $meta_description = (count($seo)>0)?$seo[0]->meta_description:'';
        
        $avails=RatesandAvailability::where("property_id",$id)->where("availability",0)->get();
        
        $start_date=Session::get('start_date');
        $end_date=Session::get('end_date');
        $guest=(Session::get('guest'))?Session::get('guest'):'2';
        
        $date_diff = Functions::nights($start_date,$end_date);
        $min_stay=(count($property)>0)?$property[0]->min_stay:'';
        $dates_not_avail = RatesandAvailability::datesNotAvail(date("Y-m-d"),$id);
        $checkdate = RatesandAvailability::checkDatesAvail($start_date,$end_date,$id);
        
        $reviews = Reviews::where('status','=',1)
                   ->where('property_id','=',$id)
                   ->where('related_to','=',1)
                   ->orderby('id','desc')
                   ->limit(5)->get();
        //dd($reviews);
        return view('front.details',compact("property","meta_keyword","meta_description","reviews","checkdate","start_date","end_date","dates_not_avail","date_diff","min_stay","guest","gallery_images","id","parents","page_title","avails"));
        }
        else {
            $page_title = 'Property not found!';
            return view('front.details',compact("id","property","page_title"));

        }
        
        
    }
    public function bookingval(Request $request) 
    {
        $id=$request->id;
        $start_date=$request->start_date;
        $end_date=$request->end_date;
        $guest=($request->guest)?$request->guest:0;
        $children=($request->children)?$request->children:0;
        
        $property=Properties::where("status","=",1)
        ->where("id","=",$id)->get();
        
        $date_diff = Functions::nights($start_date,$end_date);
        $min_stay=(count($property)>0)?$property[0]->min_stay:0;
        
        $checkdate = RatesandAvailability::checkDatesAvail($start_date,$end_date,$id);
        
        $sleeps  = (count($property)>0)?$property[0]->sleeps:0;
        $people = $guest+$children;
        
        if($start_date=='' &&  $end_date=='')
        {
            echo 'Please specify your dates of stay.';
        }
        elseif($end_date<=$start_date)
        {
            echo 'CheckOut must be greater than CheckIn. ';
        }
        elseif($date_diff<$min_stay)
        {
            echo 'This Property has a minimum stay of '.$min_stay.' nights.';
        }
        else if(count($checkdate)>0)
        {
            echo 'Unfortunately dates are not available on<br/> this property!';
        }
        else if($people>$sleeps)
        {
            echo 'This Property can only accommodate '.$sleeps.' people.';
        }
        else
        {
            echo '1';
        }
        
    }
    public function inquiry(Request $request)
    {
        $validation = array(
            'full_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
            'checkin' => 'required|max:255',
            'checkout' => 'required|max:255',
            'guests' => 'required|max:255',
            'id' => 'required',
            );
        
        $response['error'] = 0;

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            
            $errors = $validator->errors();
            $response['error'] = 1;
            $response['errors'] = $errors;
          
        }
        else
        {
            $from = Config::get("params.admin_email");
            
            
            $name = $request->full_name;
            $email = $request->email;
            $phone = $request->phone;
            $checkin = $request->checkin;
            $checkout = $request->checkout;
            $guests = $request->guests;
            $childrens = $request->childrens;
            $source = $request->source;
            $comments = $request->comments;
            $id = $request->id;
            
            $replaces['NAME'] = $name;
            $replaces['EMAIL'] = $email;
            $replaces['CHECKIN'] = Functions::DateFormat($checkin);
            $replaces['CHECKOUT'] = Functions::DateFormat($checkout);
            $replaces['URL'] = Config::get("params.url");
            $currency= Config::get('params.currency');
            $symbol=$currency["BRL"]["symbol"];
            
            $result = Properties::getPropertiesId($id);
            $property = $result[0];
            $replaces['TITLE'] = $property->public_headline;
           
            $replaces['LOGO'] = asset('front/images/email.png');
            
            $check_dates = RatesandAvailability::checkDatesAvail($checkin,$checkout,$id);
            
            if(count($check_dates)>0)
            {
                $rates = RatesandAvailability::getRatesRent($checkin,$checkout,$id);
                $rent = (count($rates))?$rates[0]->rent:0;
                $replaces['LINK'] = Config::get("params.url")."/details/".$id;
                $replaces['SYMBOL'] = $symbol;
                $replaces['RATE'] = Functions::MoneyFormat($rent);
                $replaces['BEDS'] = $property->bedrooms;
                $replaces['BATHS'] = $property->bathrooms;
                $replaces['SLEEPS'] = $property->sleeps;
                $replaces['DESCRIPTION'] = $property->summary;
                $replaces['IMAGE'] = asset('').'/'.$property->url;
                
                $content = Content::where("code","=","inquiry_fail")->get();
                
            }
            else
            {
                $replaces['GUESTS'] = $guests;
                $replaces['CHILDRENS'] = $childrens;
                $replaces['COMMENTS'] = $comments;
                $replaces['SOURCE'] = $source;
                $replaces['DATE'] = date("d/m/Y");
                $replaces['STATUS'] = "In Progress";
                $replaces['LINK'] = Config::get("params.url")."/details/".$id;
                $content = Content::where("code","=","inquiry")->get();
            }
            
            $template = Functions::setEmailTemplate($content, $replaces);
            
            $replace['NAME'] = $name;
            $subtemplate = Functions::setEmailSubject($content, $replace);
            
            $subject = $subtemplate["subject"];
            $body = $template["body"];
            
            /*$data= [
            'from' =>$from,
            'body' => $body,
            'subject' => $subject,
            'email_send' =>$email
                   
            ];

            Mail::send('email.inquiry',['data' => $data], function($message) use ($data) {

             $message->from($data['from']);
             $message->to($data['email_send'])->subject($data['subject']);
            }); */
            $send_email = Functions::sendEmail($email,$subject,$body,'',$from);
            
        }
        if($response['error']==1)
        {
            foreach($response['errors']->all() as $error)
            {
                echo '<li>'.$error.'</li>';
                
            }     
        }
        else
        {
            echo '0';
        }
    }
    
    
}
