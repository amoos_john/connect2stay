<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@home');
Route::get('about-us', 'HomeController@about');
Route::get('advertise', 'HomeController@advertise');
Route::get('register-your-property', 'HomeController@registerProperty');
Route::post('register/send', 'HomeController@sendRegisterProperty');
Route::get('rentalpolicy', 'HomeController@rentalpolicy');
Route::get('rentals', 'SearchController@index');
Route::get('rentals/allrentals/{id}', 'SearchController@detail');
Route::get('result', 'SearchController@result');
Route::get('detail/bookval', 'SearchController@bookingval');
Route::post('booking/inquiry', 'SearchController@inquiry');
Route::get('reviews/{token}', 'HomeController@reviews');
Route::post('property/reviews', 'HomeController@reviewInsert');

Route::get('sao-paulo', 'PropertyFinderController@saoPaulo');
Route::get('rio-de-janeiro', 'PropertyFinderController@rioDeJaneiro');
Route::get('fortaleza', 'PropertyFinderController@fortaleza');

Route::get('rentals/{url}', 'PropertyFinderController@details');

Route::get('faq', 'HomeController@faq');
Route::get('how-it-works', 'HomeController@works');
Route::get('contact', 'HomeController@contact');
Route::get('privacy-policy', 'HomeController@policy');
Route::post('contact/send', 'HomeController@send');
//Booking

Route::group(
        ['middleware' => 'prevent-back'], 
      function() {  
    Route::get('makebooking', 'BookingController@makeBooking');
    Route::get('booking/{key}', 'BookingController@booking');
    Route::get('bookinginfo', 'BookingController@bookinginfo');  
        
    }
);

Route::post('booking/insert', 'BookingController@insert');

Route::group(
    array('prefix' => 'paypal'), 
    function() {
        $folder= "Payments\\";
        Route::get('success', $folder.'PaypalController@success');
        Route::get('cancel', $folder.'PaypalController@cancel');
    }
);
Route::group(
    array('prefix' => 'stripe'), 
    function() {
        $folder= "Payments\\";
        Route::get('success', $folder.'StripeController@success');
        Route::get('cancel', $folder.'StripeController@cancel');
    }
);
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::controllers(['auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController']);

//Route::get('admin/login','Auth\AuthController@showLoginForm');  
//Route::get('admin/login', function() {
//    return 'Success';
//});  




Route::group(
    array('prefix' => 'admin'), 
    function() {
        $admin= "Admin\\";
        
        Route::get('/', $admin . 'HomeController@index');

        Route::get('/login', $admin . 'UserController@getLogin');
        
        Route::post('/login', $admin . 'UserController@postLogin');
        
        Route::get('forgot',  $admin . 'UserController@forgot_password');
        Route::post('reset',  $admin . 'UserController@reset_password');
        
        Route::get('users', $admin . 'UsersController@index');
        Route::get('users/create', $admin . 'UsersController@create');
        Route::post('users/insert', $admin . 'UsersController@insert');
        Route::get('users/edit/{id}', $admin . 'UsersController@edit');
        Route::post('users/update/{id}', $admin . 'UsersController@update');
        Route::get('users/delete/{id}', $admin . 'UsersController@delete');
        
        Route::get('property', $admin . 'PropertiesController@index');
        Route::get('property/create', $admin . 'PropertiesController@create');
        Route::post('property/insert', $admin . 'PropertiesController@insert');
        Route::get('property/edit/{id}', $admin . 'PropertiesController@edit');
        Route::post('property/update/{id}', $admin . 'PropertiesController@update');
        Route::get('property/delete/{id}', $admin . 'PropertiesController@delete');
        Route::get('property/mappinpointpopup', $admin . 'PropertiesController@mappinpointpopup');
        Route::get('property/search', $admin . 'PropertiesController@search');
        Route::post('property/search', $admin . 'PropertiesController@search');

        Route::get('property/addimage/{id}', $admin . 'GalleryController@addImage');
        Route::post('property/uploadimage', $admin . 'GalleryController@uploadImage');
        Route::get('property/addmultiple/{id}', $admin . 'GalleryController@addMultipleImage');
        Route::post('property/uploadmultiple', $admin . 'GalleryController@uploadMultipleImage');
        Route::get('property/deleteimage/{id}', $admin . 'GalleryController@deleteImage');
        Route::get('property/deleteallimages/{gallery_id}', $admin . 'GalleryController@deleteAllImage');
        Route::get('property/loadgallery', $admin . 'GalleryController@loadGallery');
        Route::post('property/insertorder', $admin . 'GalleryController@insertOrder');

        
        Route::get('property/ratesandavailability', $admin . 'RatesandAvailabilityController@index');
        Route::get('property/ratesandavailability/{id}', $admin . 'RatesandAvailabilityController@index');
        Route::post('property/ratesandavailability/search', $admin . 'RatesandAvailabilityController@search');
        Route::post('property/ratesandavailability/insert/{id}', $admin . 'RatesandAvailabilityController@insert');
        Route::get('ownercalendar/{id}', $admin . 'RatesandAvailabilityController@ownercalendar');

        
        Route::get('property/units/delete/{id}', $admin . 'PropertiesController@UnitDelete');
        
        Route::get('categories', $admin . 'CategoriesController@index');
        Route::get('categories/create', $admin . 'CategoriesController@create');
        Route::post('categories/insert', $admin . 'CategoriesController@insert');
        Route::get('categories/edit/{id}', $admin . 'CategoriesController@edit');
        Route::post('categories/update/{id}', $admin . 'CategoriesController@update');
        Route::get('categories/delete/{id}', $admin . 'CategoriesController@delete');
        
        Route::get('amenities', $admin . 'AmenitiesController@index');
        Route::get('amenities/create', $admin . 'AmenitiesController@create');
        Route::post('amenities/insert', $admin . 'AmenitiesController@insert');
        Route::get('amenities/edit/{id}', $admin . 'AmenitiesController@edit');
        Route::post('amenities/update/{id}', $admin . 'AmenitiesController@update');
        Route::get('amenities/delete/{id}', $admin . 'AmenitiesController@delete');
        
        Route::get('owners', $admin . 'OwnersController@index');
        Route::get('owners/create', $admin . 'OwnersController@create');
        Route::post('owners/insert', $admin . 'OwnersController@insert');
        Route::get('owners/edit/{id}', $admin . 'OwnersController@edit');
        Route::post('owners/update/{id}', $admin . 'OwnersController@update');
        Route::get('owners/delete/{id}', $admin . 'OwnersController@delete');
        Route::get('owners/search', $admin . 'OwnersController@search');
        Route::post('contacts/insert', $admin . 'OwnersController@contactinsert');
        Route::get('owners/calendar', $admin . 'OwnersController@calendar');
        Route::get('owners/getcalendar', $admin . 'OwnersController@getCalendar');
        Route::post('owners/calendarclose', $admin . 'OwnersController@calendarClose');

        Route::get('bookingrules', $admin . 'BookingRulesController@index');
        Route::get('bookingrules/create/{type}', $admin . 'BookingRulesController@create');
        Route::post('bookingrules/insert', $admin . 'BookingRulesController@insert');
        Route::get('bookingrules/edit/{id}', $admin . 'BookingRulesController@edit');
        Route::post('bookingrules/update/{id}', $admin . 'BookingRulesController@update');
        Route::get('bookingrules/delete/{id}', $admin . 'BookingRulesController@delete');
        Route::get('bookingrules/search', $admin . 'BookingRulesController@search');
        Route::post('bookingrules/multiple', $admin . 'BookingRulesController@multiple');

        
        Route::get('taxes', $admin . 'FeesTaxesController@index');
        Route::get('taxes/create/{type}', $admin . 'FeesTaxesController@create');
        Route::post('taxes/insert', $admin . 'FeesTaxesController@insert');
        Route::get('taxes/edit/{id}', $admin . 'FeesTaxesController@edit');
        Route::post('taxes/update/{id}', $admin . 'FeesTaxesController@update');
        Route::get('taxes/delete/{id}', $admin . 'FeesTaxesController@delete');
        Route::get('taxes/search', $admin . 'FeesTaxesController@search');
        Route::post('taxes/multiple', $admin . 'FeesTaxesController@multiple');

        Route::get('settings', $admin . 'SettingsController@index');
        Route::post('settings/update/{id}', $admin . 'SettingsController@update');
        Route::post('settings/insert', $admin . 'SettingsController@insert');
        
        Route::get('specials', $admin . 'SpecialsController@index');
        Route::get('specials/create/{type}', $admin . 'SpecialsController@create');
        Route::post('specials/insert', $admin . 'SpecialsController@insert');
        Route::get('specials/edit/{id}', $admin . 'SpecialsController@edit');
        Route::post('specials/update/{id}', $admin . 'SpecialsController@update');
        Route::get('specials/delete/{id}', $admin . 'SpecialsController@delete');
        Route::get('specials/search', $admin . 'SpecialsController@search');
        Route::post('specials/multiple', $admin . 'SpecialsController@multiple');
        
      
        Route::get('property/multiple', $admin . 'FeesTaxesController@multiplesearch');
        
        Route::get('attractions', $admin . 'AttractionsController@index');
        Route::get('attractions/create', $admin . 'AttractionsController@create');
        Route::post('attractions/insert', $admin . 'AttractionsController@insert');
        Route::get('attractions/edit/{id}', $admin . 'AttractionsController@edit');
        Route::post('attractions/update/{id}', $admin . 'AttractionsController@update');
        Route::get('attractions/delete/{id}', $admin . 'AttractionsController@delete');
        Route::get('attractions/search', $admin . 'AttractionsController@search');
        
        Route::get('propertyfinder', $admin . 'PropertyFinderController@index');
        Route::get('propertyfinder/create', $admin . 'PropertyFinderController@create');
        Route::post('propertyfinder/insert', $admin . 'PropertyFinderController@insert');
        Route::get('propertyfinder/edit/{id}', $admin . 'PropertyFinderController@edit');
        Route::post('propertyfinder/update/{id}', $admin . 'PropertyFinderController@update');
        Route::get('propertyfinder/delete/{id}', $admin . 'PropertyFinderController@delete');
        Route::get('propertyfinder/search', $admin . 'PropertyFinderController@search');
        Route::get('propertyfinder/test-search', $admin . 'PropertyFinderController@testSearch');
        
        Route::get('reviews', $admin . 'ReviewsController@index');
        Route::get('reviews/create', $admin . 'ReviewsController@create');
        Route::post('reviews/insert', $admin . 'ReviewsController@insert');
        Route::get('reviews/edit/{id}', $admin . 'ReviewsController@edit');
        Route::post('reviews/update/{id}', $admin . 'ReviewsController@update');
        Route::get('reviews/delete/{id}', $admin . 'ReviewsController@delete');
        Route::get('reviews/search', $admin . 'ReviewsController@search');
        
        Route::get('seo', $admin . 'SeoAttributesController@index');
        Route::get('seo/create/{type}', $admin . 'SeoAttributesController@create');
        Route::post('seo/insert', $admin . 'SeoAttributesController@insert');
        Route::get('seo/edit/{id}', $admin . 'SeoAttributesController@edit');
        Route::post('seo/update/{id}', $admin . 'SeoAttributesController@update');
        Route::get('seo/delete/{id}', $admin . 'SeoAttributesController@delete');
        Route::get('seo/search', $admin . 'SeoAttributesController@search');
        
        Route::get('booking', $admin . 'BookingController@index');
        Route::get('booking/create', $admin . 'BookingController@createBooking');
        Route::post('booking/insert', $admin . 'BookingController@insertBooking');
        Route::get('booking/search', $admin . 'BookingController@search');
        Route::get('booking/edit/{id}', $admin . 'BookingController@edit');
        Route::post('booking/update/{id}', $admin . 'BookingController@update');
        Route::get('booking/delete/{id}', $admin . 'BookingController@delete');

        Route::get('content', $admin . 'ContentController@index');
        Route::get('content/create', $admin . 'ContentController@create');
        Route::post('content/insert', $admin . 'ContentController@insert');
        Route::get('content/edit/{id}', $admin . 'ContentController@edit');
        Route::post('content/update/{id}', $admin . 'ContentController@update');
        Route::get('content/delete/{id}', $admin . 'ContentController@delete');
        
        Route::get('addimage/{code}/{id}', $admin . 'GalleryController@addImage');
        Route::get('addmultiple/{code}/{id}', $admin . 'GalleryController@addMultipleImage');
        Route::get('property/loadgallery', $admin . 'GalleryController@loadGallery');
        Route::get('deleteallimages/{code}/{gallery_id}', $admin . 'GalleryController@deleteAllImage');
        
        
        Route::get('ownerbooking', $admin . 'BookingController@index');
        Route::get('ownerbooking/create', $admin . 'BookingController@create');
        Route::post('ownerbooking/insert', $admin . 'BookingController@insert');
        Route::get('ownerbooking/search', $admin . 'BookingController@search');
        
        Route::get('booking/success', $admin.'PaymentsController@success');
        Route::get('booking/cancel', $admin.'PaymentsController@cancel');
        
        Route::get('gallery/update/{gallery_id}', $admin.'GalleryController@updateGallery');
        Route::get('property/import',  $admin . 'PropertiesController@import');
        Route::post('property/upload',  $admin . 'PropertiesController@upload');
        Route::post('property/upload/image',  $admin . 'GalleryController@upload');
        Route::post('property/upload/rates',  $admin . 'RatesandAvailabilityController@upload');

    }
);


