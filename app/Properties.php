<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\RatesandAvailability;


class Properties extends Model {
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    protected $table='properties';
    

    public static $status=[""=>"Select Status","1"=>"Active For Rent","0"=>"Inactive","2"=>"Entered - Pending Approval"];
        
    public  function categories()
    {
        return $this->hasOne('App\Categories', 'id', 'category_id');
    }
    public  function units()
    {
        return $this->hasMany('App\Units', 'property_id', 'id');
    }
    public  function property_amentity()
    {
        return $this->hasMany('App\PropertyAmentity', 'property_id', 'id');
    }
    public  function property_taxes_fees()
    {
        return $this->hasMany('App\PropertyFeesTaxes', 'property_id', 'id');
    }
    public  function property_booking_rules()
    {
        return $this->hasMany('App\PropertyBookingRules', 'property_id', 'id');
    }
    public  function property_specials()
    {
        return $this->hasMany('App\PropertySpecials', 'property_id', 'id');
    }
    public static function getProperties($search)
    {
        $result = DB::table("properties as pro")
        ->join("categories as cat","pro.category_id","=","cat.id")
        ->join("gallery_image as images","images.gallery_id","=","pro.gallery_id")
        ->join("rates","rates.property_id","=","pro.id");
        
        if(isset($search['tags']) && $search['tags']!="") 
        {
            $result = $result->join("property_amentity as pro_ame","pro_ame.property_id","=","pro.id");
        }
        
        $result = $result->select("pro.*","cat.category_name","images.image_caption","images.url",
        "images.display_order");
        //,"pro_ame.amentity_id"
             
        if(isset($search['city']) && $search['city']!="") 
        {
             $result = $result->where('pro.city','=',$search['city']);
        }
        if((isset($search['start_date']) && $search['start_date']!="") || 
        (isset($search['end_date']) && $search['end_date']!="")) 
        {
             $result = $result->whereBetween('rates.date',array($search['start_date'],$search['end_date']));
        }
        if(isset($search['guest']) && $search['guest']!="") 
        {
             $result = $result->where('pro.sleeps','>=',$search['guest']);
        }
        if((isset($search['price_max']) && $search['price_max']!="") || 
        (isset($search['price_min']) && $search['price_min']!="")) 
        {
             $result = $result->whereBetween('rates.daily',array($search['price_min'],$search['price_max']));
        }
        if(isset($search['tags']) && $search['tags']!="") 
        {
            $get_tags = implode(",", $search['tags']);
            $get_tag = explode(",", $get_tags);
            
            $tags = array_map('intval', $get_tag);
            
            $result = $result->whereIn('pro_ame.amentity_id',$tags);
        }
        if(isset($search['results_per_page']) && $search['results_per_page']!="") 
        {
             $results_per_page = $search['results_per_page'];
        }
        else
        {
            $results_per_page = 10;
        }
              
        $result = $result->where("pro.status","=",1);
        $result = $result->where("images.display_order","=",1);
        if((isset($search['start_date']) && $search['start_date']!="") || 
        (isset($search['end_date']) && $search['end_date']!="")) 
        {
            $result = $result->where("rates.availability","=",1);
            $result = $result->where("rates.date","=",$search['start_date']);
        }
        else
        {
            $result = $result->where("rates.date","=",date('Y-m-d'));

        }
        $result = $result->whereNull("pro.deleted_at");
        //$result = $result->whereNull("images.deleted_at");
        $result = $result->groupby("pro.id");
        
        if(isset($search['sort']) && $search['sort']!="") 
        {
            $result = $result->orderby("pro.id",$search['sort']);
        }
        else
        {
            //$result = $result->orderby("pro.id",'desc');
            $result = $result->inRandomOrder();
        }
        
        
        $result = $result->paginate($results_per_page);
        
        return $result;
    }
    public static function getPropertiesId($id)
    {
        $result = DB::table("properties as pro")
        ->join("categories as cat","pro.category_id","=","cat.id")
        ->join("gallery_image as images","images.gallery_id","=","pro.gallery_id")      
        ->select("pro.*","cat.category_name","images.image_caption","images.url",
        "images.display_order")
        ->where("pro.id","=",$id)        
        ->where("pro.status","=","1")
        ->where("images.display_order","=",1)
        ->whereNull("pro.deleted_at")
        ->whereNull("images.deleted_at")   
        ->get();
        
        return $result;
    }
    public static function getRates($property_id,$date='')
    {
        $date=($date!='')?$date:date("Y-m-d");
        $result=RatesandAvailability::whereDate("date","=",$date)
        //->where("availability","=",1)
        ->where("property_id","=",$property_id)->get();
        
        return $result;
    }
    public static function allProperties($name='',$search='')
    {
        if($name=='rules')
        {
            $result=Properties::with("property_booking_rules");
            
        }
        else if($name=='fees')
        {
            $result=Properties::with("property_taxes_fees");
        }
        else if($name=='special')
        {
            $result=Properties::with("property_specials");
        }
        else
        {
            $result=Properties::select("*");
        }
         
        if(isset($search) && $search!="") 
        {
            $result = $result->where('internal_name','LIKE',"%".$search."%");
        }
       
        
       $result = $result->where("status","=",1);
       $result = $result->orderby("internal_name","asc")->get();
   
       return $result;
    }
    
    public static function search($search)
    {
        
        $result = DB::table("properties as pro")
        ->join("categories as cat","pro.category_id","=","cat.id");
        if(isset($search['owner']) && $search['owner']!="") 
        {
            $result = $result->join("units as uni","uni.property_id","=","pro.id") ;
        }       
        $result = $result->select("pro.*","cat.category_name")
        ->whereNull("pro.deleted_at");
                
        if(isset($search['headline']) && $search['headline']!="") 
        {
            $result = $result->where('pro.internal_name','LIKE',"%".$search['headline']."%");
        }
        if(isset($search['category_id']) && $search['category_id']!="") 
        {
            $result = $result->where('pro.category_id','=',$search['category_id']);
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('pro.status','=',$search['status']);
        }
        if(isset($search['id']) && is_numeric($search['id'])) 
        {
            $result = $result->where('pro.id','=',$search['id']);
        }
        if(isset($search['owner']) && $search['owner']!="") 
        {
            $result = $result->where('uni.owner_id','=',$search['owner']);
        }
        if(isset($search['location']) && $search['location']!="") 
        {
             $result = $result->where('pro.city','LIKE','%'.$search['location'].'%');
        }
        if(isset($search['development']) && $search['development']!="") 
        {
             $result = $result->where('pro.development','=',$search['development']);
        }
        if(isset($search['internalproperty_id']) && $search['internalproperty_id']!="") 
        {
             $result = $result->where('pro.internalproperty_id','=',$search['internalproperty_id']);
        }
        
        $result = $result->orderBy("pro.id","desc");
        $result = $result->groupby("pro.id");
        $result = $result->paginate(10);
        return $result;
    }
    public static function ActiveProperties()
    {
         $properties=Properties::orderby("internal_name","ASC")->where("status","=",1)->lists("internal_name", "id")->prepend('Select', '');
         
         return $properties;
    }
    
}
