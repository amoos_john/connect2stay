<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Properties;

class PropertyAmentity extends Model {
    protected $table='property_amentity';
    
    public static function countAmenities($id,$date='')
    {
        $date=($date!='')?$date:date("Y-m-d");
        $result = Properties::
                join("property_amentity","property_amentity.property_id","=","properties.id")
                ->join("rates","rates.property_id","=","properties.id")
                ->where("properties.status","=",1)
                ->where("property_amentity.amentity_id","=",$id)
                ->where("rates.date","=",$date)
                ->count();
       //->groupby("property_amentity.amentity_id") 
        return $result;
    }     
       
}
