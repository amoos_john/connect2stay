<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyBookingRules extends Model {
    protected $table='property_booking_rules';
    
    public static function rulesDelete($id)
    {
        $delete = PropertyBookingRules::where('rules_id', '=', $id)->delete(); 
    }     
       
}
