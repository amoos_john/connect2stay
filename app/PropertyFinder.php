<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PropertyFinder extends Model {
    protected $table='property_finder';
    
    public static $status=[""=>"Select Status","1"=>"Active","0"=>"Inactive"];
    

    public static function search($search)
    {
        $result =  DB::table('property_finder');
                
        if(isset($search['name']) && $search['name']!="") 
        {
            $result = $result->where('name','LIKE',"%".$search['name']."%");
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('status','=',$search['status']);
        }
        
        //$result = $result->orderBy("start","asc");
        $result = $result->paginate(10);
        return $result;
    }
    /*Front End Functions*/
    public static function getPropertyFinder($id)
    {
        $result = PropertyFinder::where("id","=",$id)        
        ->where("status","=",1)
        ->get();
        
        return $result;
    }
    
    public static function getProperties($search)
    {
        $result = DB::table("properties as pro")
        ->join("categories as cat","pro.category_id","=","cat.id")
        ->join("gallery_image as images","images.gallery_id","=","pro.gallery_id")
        ->join("rates","rates.property_id","=","pro.id");
        if(isset($search['amentites']) && $search['amentites']!="") 
        {
            $result = $result->join("property_amentity as pro_ame","pro_ame.property_id","=","pro.id");
        }
        $result=$result->select("pro.*","cat.category_name","images.image_caption","images.url",
        "images.display_order");
        if(isset($search['category_id']) && $search['category_id']!="") 
        {
            $result=$result->where("pro.category_id","=",$search['category_id']); 
        }
        if(isset($search['ids']) && $search['ids']!="") 
        {
            $result=$result->whereIn("pro.id",$search['ids']); 
        }
        if(isset($search['amentites']) && $search['amentites']!="") 
        {
            $result = $result->whereIn('pro_ame.amentity_id',$search['amentites']);
        }
        if((isset($search['min_stay_from']) && $search['min_stay_from']!="") && 
        (isset($search['min_stay_to']) && $search['min_stay_to']!="")) 
        {
             $result = $result->whereBetween('pro.min_stay',array($search['min_stay_from'],$search['min_stay_to']));
        }
        if((isset($search['sleeps_to']) && $search['sleeps_to']!="") && 
        (isset($search['sleeps_from']) && $search['sleeps_from']!="")) 
        {
             $result = $result->whereBetween('pro.sleeps',array($search['sleeps_from'],$search['sleeps_to']));
        }
        if((isset($search['bedrooms_to']) && $search['bedrooms_to']!="") && 
        (isset($search['bedrooms_from']) && $search['bedrooms_from']!="")) 
        {
             $result = $result->whereBetween('pro.bedrooms',array($search['bedrooms_from'],$search['bedrooms_to']));
        }
        if((isset($search['bathrooms_to']) && $search['bathrooms_to']!="") && 
        (isset($search['bathrooms_from']) && $search['bathrooms_from']!="")) 
        {
             $result = $result->whereBetween('pro.bathrooms',array($search['bathrooms_from'],$search['bathrooms_to']));
        }
        
        $result=$result->where("pro.status","=",1)
        ->where("images.display_order","=",1)
        ->whereNull("pro.deleted_at")
        ->whereNull("images.deleted_at") 
        ->groupby('pro.id')
        ->orderby('pro.id','desc')
        ->get();
        
        return $result;
    }
      
       
}
