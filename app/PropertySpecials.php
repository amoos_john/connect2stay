<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertySpecials extends Model {
    protected $table='property_specials';
    
    public static function specialDelete($id)
    {
        $delete = PropertySpecials::where('special_id', '=', $id)->delete(); 
    }     
       
}
