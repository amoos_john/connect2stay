<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RatesandAvailability extends Model {
    protected $table='rates';
    
    public static function getRates($start,$end,$id)
    {
        $start = date('Y-m-d',strtotime("+1 day", strtotime($start)));
        $result=RatesandAvailability::select('*',DB::raw('sum(daily) as rent'))
        ->whereBetween('date',array($start,$end))
        ->where("availability","=",1)
        ->where("property_id","=",$id)->get();
        
        return $result;
    } 
    public static function getRatesRent($start,$end,$id)
    {
        $start = date('Y-m-d',strtotime("+1 day", strtotime($start)));
        $result=RatesandAvailability::select('*',DB::raw('sum(daily) as rent'))
        ->whereBetween('date',array($start,$end))
        ->where("property_id","=",$id)->get();
        
        return $result;
    }
    public static function getDateRate($date,$id)
    {
       $result=RatesandAvailability::whereDate("date","=",$date)
        ->where("availability","=",1)
        ->where("property_id","=",$id)->get();
        
        return $result;
    } 
    public static function checkDatesAvail($start,$end,$id)
    {
       $end = date ("Y-m-d", strtotime("-1 day", strtotime($end))); 
       $result = RatesandAvailability::whereBetween('date',array($start,$end))
        ->where("availability","!=",1)
        ->where("property_id","=",$id)
        ->get();
        
        return $result;
    }
    public static function datesNotAvail($date,$id)
    {
       $result = RatesandAvailability::where("availability","=",0)
        ->where("availability","=",2)       
        ->whereDate("date",">=",$date)       
        ->where("property_id","=",$id)->get();
       
        $dates_not_avail =  '';
        foreach($result as $dates_avail)
        {
            $dates_not_avail .= "'".$dates_avail->date."',";
        }
        
        return $dates_not_avail;
    }
    
    public static function updateDates($date,$end_date,$id,$value = '')
    {
        if($value=='')
        {
            $value = 2;
        }
        
        $data=array();
        $i=0;
        $end_date = date ("Y-m-d", strtotime("-1 day", strtotime($end_date)));
           
            while(strtotime($date) <= strtotime($end_date)) 
            {
                
               $data[] = array('date'=> $date);
               $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));

            }
            if(count($data)>0)
            {
                $date_to_update = array_map(function($updatedate){ return $updatedate['date']; }, $data);
                $delete = RatesandAvailability::whereIn('date', $date_to_update)
                ->where("property_id","=",$id)->update(['availability'=> $value]);
            } 
    }
       
}
