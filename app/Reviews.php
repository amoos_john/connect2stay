<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Properties;

class Reviews extends Model {
    protected $table='reviews';
    
    public static $status=[""=>"Select Status","1"=>"Active","0"=>"Inactive"];
    
    public static $source=[""=>"Select Source","5"=>"Control Panel","2"=>"FlipKey",
    "3"=>"HomeAway", "1"=>"Solution Site", "4"=>"Trip Advisor"];
    
    public static $related_to=["1"=>"Properties","2"=>"Developement"];
    
    public static $responded=[""=>"All Reviews","1"=>"No","2"=>"Yes"];

    public static function search($search)
    {
        $result = Reviews::with('properties');
                
        if(isset($search['title']) && $search['title']!="") 
        {
            $result = $result->where('title','LIKE',"%".$search['title']);
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('status','=',$search['status']);
        }
        if(isset($search['source']) && $search['source']!="") 
        {
            $result = $result->where('source','=',$search['source']);
        }
        if(isset($search['property_id']) && $search['property_id']!="") 
        {
            $result = $result->where('property_id','=',$search['property_id']);
        }
        if(isset($search['responded']) && $search['responded']!="") 
        {
            if($search['responded']==1)
            {
                $result = $result->where('response','=','');
            }
            else if($search['responded']==2)
            {
                $result = $result->where('response','!=','');
            }
            
        }
        
        $result = $result->where("token","=","")->paginate(10);
        return $result;
    }
    public function properties()
    {
        return $this->hasOne('App\Properties', 'id', 'property_id');
    }
    public static function ReviewProperties()
    {
         $properties=Properties::orderby("internal_name","asc")->where("status","=",1)->lists("internal_name", "id")->prepend('Select', '');
         
         return $properties;
    }
      
       
}
