<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Specials;

class SeoAttributes extends Model {
    protected $table='seo_attributes';
    
    public static $status=[""=>"Select Status","1"=>"Active","0"=>"Inactive"];
    //"2" => "POI URL","5" => "Redir Map URL","7" => "Blog URL","8" => "Development URL","11" => "Virtual Page URL",
    public static $type=[""=>"Select Type",
                //"1" => "Ad Src Code",
                "3" => "Property Finder Search URL",
				"4" => "Property URL",
				//"6" => "System Defined Page",
				"9" => "Special URL",
				//"10" => "Location Landing Page URL",
				//"12" => "User Created Page"
				];
    
    public static $language=["1" => "English (United Kingdom)",
                "2" => "Portuguese (Brazil)"];
    
       



    public static function search($search)
    {
        $result = SeoAttributes::select('*');
                
        if(isset($search['name']) && $search['name']!="") 
        {
            $result = $result->where('primary_name','LIKE',$search['name']."%");
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('status','=',$search['status']);
        }
        if(isset($search['type']) && $search['type']!="") 
        {
            $result = $result->where('type','=',$search['type']);
        }
      
        $result = $result->paginate(10);
        return $result;
    }
   
    public static function ActiveSpecials()
    {
         $properties=Specials::orderby("name","asc")->where("status","=",1)->lists("name", "id")->prepend('Select', '');
         
         return $properties;
    }  
       
}
