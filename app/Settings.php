<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model {
    protected $table='settings';
    
   public static $booking_mode=["0"=>"Inquiry Only","1"=>"Make Booking"];
   public static $rate_mode=["0"=>"Standard","1"=>"Per Person","2"=>"Per Room"]; 
   public static $payment_method=["0"=>" Visa","1"=>"American Express","2"=>"Discover"
       ,"3"=>" Master Card","4"=>"Maestro","5"=>"Diners Club"];   
   public static $legend_view=["0"=>"Type","1"=>"Booked By","2"=>"Status","3"=>"Payment"]; 

       
}
