<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Specials extends Model {
    protected $table='specials';
    
    public static $status=[""=>"Select Status","1"=>"Active","0"=>"Inactive"];
    
    public static $type=[""=>"Select Type","1"=>"Percentage Of Rate","2"=>"Fixed Discount",
    "3"=>"Free Night","4"=>"Replacement Nightly Rate"];
    
    public static $applicability=["0"=>"Applies to All Properties","1"=>"Property Specific"];
    
    public static $cal_base=["1"=>"Avg. Nightly Cost","2"=>"Least Expensive Night","3"=>"Most Expensive Night"];

    public static function getSpecials($id)
    {
        $result =  Specials::join("property_specials as spec","spec.special_id","=","specials.id")
       ->select("specials.*","spec.property_id")
       ->where("spec.property_id","=",$id)
       ->where('status','=',1)
       ->ORwhere('specials.applicability','=',0)
       ->groupby('specials.id')
       ->orderby('name','asc')
       ->get();
         
        return $result;
    } 
    public static function allSpecials()
    {
        $result =  Specials::where('status','=',1)->get();
         
        return $result;
    } 
    
    public static function search($search)
    {
        $result =  DB::table('specials');
                
        if(isset($search['type']) && $search['type']!="") 
        {
            $result = $result->where('type','=',$search['type']);
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('status','=',$search['status']);
        }
        
        //$result = $result->orderBy("start","asc");
        $result = $result->paginate(10);
        return $result;
    }    
       
}
