<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Units extends Model {
    protected $table='units';
    
    public function properties()
    {
        return $this->hasMany('App\Properties', 'id', 'property_id');
    } 
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'owner_id');
    } 
       
}
