<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class User extends Authenticatable
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public static $status=[""=>"Select Status","1"=>"Active","0"=>"Inactive"];

     public function role()
    {
        return $this->hasOne('App\Role', 'id', 'role_id');
    }
    
    
    public  function contacts()
    {
        return $this->hasOne('App\Contacts', 'id', 'contact_id');
    }
    public  function users_taxes_fees()
    {
        return $this->hasMany('App\UserFeesTaxes', 'user_id', 'id');
    }
    public static function getOwnerProperty($owner_id,$date='')
    {
        $properties = DB::table("properties as pro")
        ->join("units as uni","uni.property_id","=","pro.id")        
        ->select("pro.*","uni.owner_id")
        ->where("pro.status","=",1)
        ->where('uni.owner_id','=',$owner_id)
        ->whereNull("pro.deleted_at")
        ->get();
        
        return $properties;
    }
    public static function search($search)
    {
        $result =  User::where("role_id","=",3);
                
        if(isset($search['account_name']) && $search['account_name']!="") 
        {
            $result = $result->where('name','LIKE',"%".$search['account_name']."%");
        }
        if(isset($search['status']) && $search['status']!="") 
        {
            $result = $result->where('status','=',$search['status']);
        }
        
        $result = $result->orderBy("name","asc");
        $result = $result->paginate(10);
        return $result;
    }
    					
    public static $langauges =[""=>"Select Language",
    '54' => 'Afrikaans',
    '1078' => 'Afrikaans (South Africa)',
    '28' => 'Albanian',
    '1052' => 'Albanian (Albania)',
    '1' => 'Arabic',
    '5121' => 'Arabic (Algeria)',
    '15361' => 'Arabic (Bahrain)',
    '3073' => 'Arabic (Egypt)',
    '2049' => 'Arabic (Iraq)',
    '11265' => 'Arabic (Jordan)',
    '13313' => 'Arabic (Kuwait)',
    '12289' => 'Arabic (Lebanon)',
    '4097' => 'Arabic (Libya)',
    '6145' => 'Arabic (Morocco)',
    '8193' => 'Arabic (Oman)',
    '16385' => 'Arabic (Qatar)',
    '1025' => 'Arabic (Saudi Arabia)',
    '10241' => 'Arabic (Syria)',
    '7169' => 'Arabic (Tunisia)',
    '14337' => 'Arabic (U.A.E.)',
    '9217' => 'Arabic (Yemen)',
    '43' => 'Armenian',
    '1067' => 'Armenian (Armenia)',
    '44' => 'Azeri',
    '2092' => 'Azeri (Azerbaijan Cyrillic)',
    '1068' => 'Azeri (Azerbaijan Latin)',
    '45' => 'Basque',
    '1069' => 'Basque (Basque)',
    '35' => 'Belarusian',
    '1059' => 'Belarusian (Belarus)',
    '2' => 'Bulgarian',
    '1026' => 'Bulgarian (Bulgaria)',
    '3' => 'Catalan',
    '1027' => 'Catalan (Catalan)',
    '3076' => 'Chinese (Hong Kong SAR PRC)',
    '5124' => 'Chinese (Macao SAR)',
    '2052' => 'Chinese (PRC)',
    '4' => 'Chinese (Simplified)',
    '4100' => 'Chinese (Singapore)',
    '1028' => 'Chinese (Taiwan)',
    '31748' => 'Chinese (Traditional)',
    '26' => 'Croatian',
    '1050' => 'Croatian (Croatia)',
    '1029' => 'Czech',
    '1029' => 'Czech (Czech Republic)',
    '6' => 'Danish',
    '1030' => 'Danish (Denmark)',
    '101' => 'Divehi',
    '1125' => 'Divehi (Maldives)',
    '19' => 'Dutch',
    '2067' => 'Dutch (Belgium)',
    '1043' => 'Dutch (Netherlands)',
    '9' => 'English',
    '3081' => 'English (Australia)',
    '10249' => 'English (Belize)',
    '4105' => 'English (Canada)',
    '9225' => 'English (Caribbean)',
    '6153' => 'English (Ireland)',
    '8201' => 'English (Jamaica)',
    '5129' => 'English (New Zealand)',
    '13321' => 'English (Philippines)',
    '7177' => 'English (South Africa',
    '11273' => 'English (Trinidad and Tobago)',
    '2057' => 'English (United Kingdom)',
    '1033' => 'English (United States)',
    '12297' => 'English (Zimbabwe)',
    '37' => 'Estonian',
    '1061' => 'Estonian (Estonia)',
    '56' => 'Faroese',
    '1080' => 'Faroese (Faroe Islands)',
    '41' => 'Farsi',
    '1065' => 'Farsi (Iran)',
    '11' => 'Finnish',
    '1035' => 'Finnish (Finland)',
    '12' => 'French',
    '2060' => 'French (Belgium)',
    '3084' => 'French (Canada)',
    '1036' => 'French (France)',
    '5132' => 'French (Luxembourg)',
    '6156' => 'French (Monaco)',
    '4108' => 'French (Switzerland)',
    '86' => 'Galician',
    '1110' => 'Galician (Spain)',
    '55' => 'Georgian',
    '1079' => 'Georgian (Georgia)',
    '7' => 'German',
    '3079' => 'German (Austria)',
    '1031' => 'German (Germany)',
    '5127' => 'German (Liechtenstein)',
    '4103' => 'German (Luxembourg)',
    '2055' => 'German (Switzerland)',
    '8' => 'Greek',
    '1032' => 'Greek (Greece)',
    '71' => 'Gujarati',
    '1095' => 'Gujarati (India)',
    '13' => 'Hebrew',
    '1037' => 'Hebrew (Israel)',
    '57' => 'Hindi',
    '1081' => 'Hindi (India)',
    '14' => 'Hungarian',
    '1038' => 'Hungarian (Hungary)',
    '15' => 'Icelandic',
    '1039' => 'Icelandic (Iceland)',
    '33' => 'Indonesian',
    '1057' => 'Indonesian (Indonesia)',
    '127' => 'Invariant culture',
    '16' => 'Italian',
    '1040' => 'Italian (Italy)',
    '2064' => 'Italian (Switzerland)',
    '17' => 'Japanese',
    '1041' => 'Japanese (Japan)',
    '75' => 'Kannada',
    '1099' => 'Kannada (India)',
    '63' => 'Kazakh',
    '1087' => 'Kazakh (Kazakhstan)',
    '87' => 'Konkani',
    '1111' => 'Konkani (India)',
    '18' => 'Korean',
    '1042' => 'Korean (Korea)',
    '64' => 'Kyrgyz',
    '1088' => 'Kyrgyz (Kyrgyzstan)',
    '38' => 'Latvian',
    '1062' => 'Latvian (Latvia)',
    '39' => 'Lithuanian',
    '1063' => 'Lithuanian (Lithuania)',
    '47' => 'Macedonian',
    '1071' => 'Macedonian (Macedonia FYROM)',
    '62' => 'Malay',
    '2110' => 'Malay (Brunei Darussalam)',
    '1086' => 'Malay (Malaysia)',
    '78' => 'Marathi',
    '1102' => 'Marathi (India)',
    '80' => 'Mongolian',
    '1104' => 'Mongolian (Mongolia)',
    '20' => 'Norwegian',
    '1044' => 'Norwegian (Bokmål Norway)',
    '2068' => 'Norwegian (Nynorsk Norway)',
    '21' => 'Polish',
    '1045' => 'Polish (Poland)',
    '22' => 'Portuguese',
    '1046' => 'Portuguese (Brazil)',
    '2070' => 'Portuguese (Portugal)',
    '70' => 'Punjabi',
    '1094' => 'Punjabi (India)',
    '24' => 'Romanian',
    '1048' => 'Romanian (Romania)',
    '25' => 'Russian',
    '1049' => 'Russian (Russia)',
    '79' => 'Sanskrit',
    '1103' => 'Sanskrit (India)',
    '3098' => 'Serbian (Serbia Cyrillic)',
    '2074' => 'Serbian (Serbia Latin)',
    '27' => 'Slovak',
    '1051' => 'Slovak (Slovakia)',
    '36' => 'Slovenian',
    '1060' => 'Slovenian (Slovenia)',
    '10' => 'Spanish',
    '11274' => 'Spanish (Argentina)',
    '16394' => 'Spanish (Bolivia)',
    '13322' => 'Spanish (Chile)',
    '9226' => 'Spanish (Colombia)',
    '5130' => 'Spanish (Costa Rica)',
    '7178' => 'Spanish (Dominican Republic)',
    '12298' => 'Spanish (Ecuador)',
    '17418' => 'Spanish (El Salvador)',
    '4106' => 'Spanish (Guatemala)',
    '18442' => 'Spanish (Honduras)',
    '2058' => 'Spanish (Mexico)',
    '19466' => 'Spanish (Nicaragua)',
    '6154' => 'Spanish (Panama)',
    '15370' => 'Spanish (Paraguay)',
    '10250' => 'Spanish (Peru)',
    '20490' => 'Spanish (Puerto Rico)',
    '3082' => 'Spanish (Spain)',
    '14346' => 'Spanish (Uruguay)',
    '8202' => 'Spanish (Venezuela)',
    '1115' => 'Sri Lankan (Sri Lanka)',
    '65' => 'Swahili',
    '1089' => 'Swahili (Kenya)',
    '29' => 'Swedish',
    '2077' => 'Swedish (Finland)',
    '1053' => 'Swedish (Sweden)',
    '90' => 'Syriac',
    '1114' => 'Syriac (Syria)',
    '73' => 'Tamil',
    '1097' => 'Tamil (India)',
    '68' => 'Tatar',
    '1092' => 'Tatar (Russia)',
    '74' => 'Telugu',
    '1098' => 'Telugu (India)',
    '30' => 'Thai',
    '1054' => 'Thai (Thailand)',
    '31' => 'Turkish',
    '1055' => 'Turkish (Turkey)',
    '34' => 'Ukrainian',
    '1058' => 'Ukrainian (Ukraine)',
    '32' => 'Urdu',
    '1056' => 'Urdu (Pakistan)',
    '67' => 'Uzbek',
    '2115' => 'Uzbek (Uzbekistan Cyrillic)',
    '1091' => 'Uzbek (Uzbekistan Latin)',
    '42' => 'Vietnamese',
    '1066' => 'Vietnamese (Vietnam)'];	

}
