<?php
return [
    'site_name' => 'Connect2Stay',
    'url' => 'http://ecommercehouse.com/projects/connect2stay/connect2stay/public',//https://www.connect2stay.com
    'image_path' => 'uploads/properties/',
     'symbol'=>'R$',
	'currency'=>[
            'BRL'=>['symbol'=>'R$','name'=>'Brazil Real'],
    ],
    'currency_default'=>'BRL',
    'languages'=>[
            'en_uk'=>'English (UK)',
            'en_us'=>'English (Us)',
    ],
    'language_default'=>'en_uk',
    'contentTypes'=>[
            'page'=>'Page',
            'email'=>'Email',
            'block'=>'Block',
    ],
   'admin_email' => 'amoos.golpik@gmail.com'
    
    
];