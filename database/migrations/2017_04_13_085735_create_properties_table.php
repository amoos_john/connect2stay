<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('properties');
         Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('category_id');
            $table->bigInteger('gallery_id');
            $table->string('internal_name');
            $table->string('public_headline');
            $table->longText('summary');
            $table->longText('description');
            $table->string('status');
            $table->string('development');
            $table->mediumText('address');
            $table->string('address_1');
            $table->string('address_2');
            $table->string('neighborhood');
            $table->string('city');
            $table->string('county');
            $table->string('metro');
            $table->string('state');
            $table->string('postal_code');
            $table->string('region');
            $table->string('country');
            $table->string('longitude');
            $table->string('latitude');
            $table->string('bedrooms');
            $table->string('garage_spaces');
            $table->string('bathrooms');
            $table->string('lot_size');
            $table->string('lot_meter');
            $table->string('sleeps');
            $table->string('stories');
            $table->string('parking_spaces');
            $table->string('floor');
            $table->string('year_built');
            $table->string('unit_size');
            $table->string('unit_meter');
            $table->string('min_stay');
            $table->string('tags');
            $table->longText('policies');
            $table->timestamps();
            $table->softDeletes();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
