<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('lead_source');
            $table->string('lead_funnel');
            $table->string('email');
            $table->string('home');
            $table->string('mobile');
            $table->string('work');
            $table->string('fax');
            $table->string('language');
            $table->mediumText('address');
            $table->string('address_1');
            $table->string('address_2');
            $table->string('neighborhood');
            $table->string('city');
            $table->string('county');
            $table->string('metro');
            $table->string('state');
            $table->string('postal_code');
            $table->string('region');
            $table->string('country');
            $table->string('longitude');
            $table->string('latitude');
            $table->mediumText('notes');
            $table->string('tags');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}
