<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingrulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('name');
            $table->Integer('status');
            $table->Integer('applicability');
            $table->date('start');
            $table->date('end');
            $table->Integer('min_stay');
            $table->float('charge');
            $table->string('charge_type');
            $table->Integer('reminder');
            $table->string('reminder_type');
            $table->string('calculation_due');
            $table->string('processor');
            $table->Integer('auto_confirm');
            $table->longText('description');
            $table->mediumText('notes');

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_rules');
    }
}
