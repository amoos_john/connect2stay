<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('advance_days');
            $table->Integer('from_days');
            $table->string('booking_mode');
            $table->string('checkin_time');
            $table->string('checkout_time');
            $table->Integer('hold_time');
            $table->Integer('min_stay');
            $table->Integer('children_include');
            $table->Integer('cancel_payment');
            $table->string('payment_method');
            $table->Integer('date_supplied');
            $table->Integer('nodate_supplied');
            $table->string('no_date_supplied');
            $table->string('with_date_supplied');
            $table->string('no_rate_avail');
            $table->string('consolidates_service_total');
            $table->Integer('lock_rate');
            $table->Integer('length_of_stay');
            $table->string('rate_mode');
            $table->string('3_party_owner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
