<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('length_stay');
            $table->string('description');
            $table->string('s_prompt');
            $table->string('p_prompt');
            $table->string('avg_night');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rate_settings');
    }
}
