<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specials', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('type');
            $table->Integer('gallery_id');
            $table->string('name');
            $table->string('public_name');
            $table->Integer('status');
            $table->Integer('applicability');
            $table->string('coupon_code');
            $table->date('effective');
            $table->date('expiration');
            $table->date('cutoff_date');
            $table->Integer('min_stay');
            $table->double('amount');
            $table->Integer('show_site');
            $table->Integer('auto_apply');
            $table->mediumText('notes');
            $table->mediumText('summary');
            $table->mediumText('description');
            $table->mediumText('terms');
            $table->Integer('lower_taxes');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('specials');
    }
}
