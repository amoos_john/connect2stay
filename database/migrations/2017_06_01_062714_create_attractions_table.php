<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attractions', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('gallery_id');
            $table->string('name');
            $table->string('public_name');
            $table->string('type');
            $table->Integer('status');
            $table->longText('summary');
            $table->longText('description');
            $table->string('email');
            $table->string('fax');
            $table->string('phone');
            $table->string('phone2');
            $table->string('toll_free');
            $table->string('website');
            $table->mediumText('address');
            $table->string('address_1');
            $table->string('address_2');
            $table->string('neighborhood');
            $table->string('city');
            $table->string('county');
            $table->string('metro');
            $table->string('state');
            $table->string('postal_code');
            $table->string('region');
            $table->string('country');
            $table->string('longitude');
            $table->string('latitude');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attractions');
    }
}
