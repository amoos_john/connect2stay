<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyfinderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_finder', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('gallery_id');
            $table->string('name');
            $table->Integer('status');
            $table->longText('description');
            $table->mediumText('notes');
            $table->mediumText('query');
            $table->Integer('category_id');
            $table->string('min_stay_from');
            $table->string('min_stay_to');
            $table->string('avg_day_to');
            $table->string('avg_day_from');
            $table->string('sleeps_to');
            $table->string('sleeps_from');
            $table->string('bedrooms_to');
            $table->string('bedrooms_from');
            $table->string('bathrooms_to');
            $table->string('bathrooms_from'); 
            $table->string('honour_loc');
            $table->string('grouping_key');
            $table->string('sort_index');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_finder');
    }
}
