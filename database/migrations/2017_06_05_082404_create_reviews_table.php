<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('property_id');
            $table->string('title');
            $table->string('reviewed_by');
            $table->Integer('ratings');
            $table->string('related_to');
            $table->string('source');
            $table->Integer('status');
            $table->longText('comment');
            $table->longText('response');
            $table->mediumText('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}
