<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoattributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('language');
            $table->string('applies_to');
            $table->string('meta_keyword');
            $table->longText('meta_description');
            $table->string('primary_name');
            $table->Integer('type');
            $table->Integer('status');
            $table->string('friendly_url');
            $table->string('path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seo_attributes');
    }
}
