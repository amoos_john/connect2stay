<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->increments('id');
            $table->string('booking_id');
            $table->bigInteger('property_id');
            $table->Integer('status');
            $table->string('payment_type',5);
            $table->date('checkin');
            $table->date('checkout');
            $table->Integer('adults');
            $table->Integer('children');
            $table->double('grand_total');
            $table->mediumText('internal_notes');
            $table->string('booked_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking');
    }
}
