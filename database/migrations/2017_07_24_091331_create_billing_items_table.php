<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_items', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('booking_id');
            $table->Integer('type');
            $table->string('description');
            $table->string('taxed');
            $table->Integer('quantity');
            $table->double('rate');
            $table->double('amount');
            $table->double('commission_statement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billing_items');
    }
}
