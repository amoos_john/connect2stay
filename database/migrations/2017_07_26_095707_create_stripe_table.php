<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('booking_id');
            $table->string('charge_id');
            $table->string('card_no');
            $table->string('card_name');
            $table->Integer('month');
            $table->Integer('year');
            $table->string('cvc');
            $table->string('fee');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stripe');
    }
}
