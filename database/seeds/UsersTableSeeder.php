<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users=new User;
        $users->name='Admin';
        $users->email='admin@admin.com';
        $users->password= bcrypt('admin123');
        $users->save();
    }
}
