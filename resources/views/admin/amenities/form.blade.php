<?php
$required="required";
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::label('Parent Amenity') !!}
    <em>(If you want to Parent Amenity don't select this input)</em>
    <select name="parent_id" class="form-control">
        <option value="">Select Parent</option>
        @if(count($parents)>0)
        @foreach($parents as $row)
            <?php
                $selected='';
                if(isset($id))
                {
                    $selected=($row->id==$model->parent_id)?'selected':'';
                }
                
            ?>
            <option value="{{ $row->id }}" <?php echo  $selected; ?> >{{ $row->name }}</option>
        @endforeach
        @endif
    </select>
    
</div>

<div class="form-group">
    {!! Form::label('Amenity Name*') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="{{ url('admin/amenities') }}" class="btn btn-danger">Cancel</a>
</div>