@extends('admin/admin_template')
@section('content')
<?php
use App\Amenities;
?>
<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
    <!-- PRODUCT LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">( Total : {{ count($model) }} ) </h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-success" href="{{url('admin/amenities/create')}}">Add New Amentity</a>
                </div>
            </div>
            @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success !</h4>
                {!! session('success') !!}
            </div>
            @endif
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Parent Amentity</th>
                            <th>Amentity Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach ($model as $row)
                        <?php
                            $parent=Amenities::where("id","=",$row->parent_id)->where("parent_id","=",0)->get();
                        ?>
                        <tr>
                            <td><?php echo $row->id; ?></td>
                            <td><?php echo (count($parent)>0)?$parent[0]->name:''; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td>
                                <a class="btn btn-primary" href="<?php echo url('admin/amenities/edit/'.$row->id); ?>" ><i class="fa fa-pencil"></i> </a>                       
                                <a data-href="<?php echo url('admin/amenities/delete/'.$row->id); ?>" data-target="#confirm-delete"  class="btn btn-danger"  data-toggle="modal"><i class="fa fa-trash"></i> </a>
                            </td>     
                          
                        </tr>
                        <?php $i++; ?>
                        @endforeach       
                    </tbody>
                </table>
                <?php echo $model->render(); ?>
            </div>
        </div>
    
     <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this Amenity?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->	

@endsection