@extends('admin/admin_template')

@section('content')

<style>
    .list_property {
    height: 300px;
    overflow-y: scroll;
}
.textarea{
    resize: none;
    height: 100px !important;
}
</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
     @include('admin/commons/errors')     
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
	@endif
        <div class="box-header with-border">
          <h3 class="box-title">Add New Attraction</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          {!! Form::open(array( 'class' => 'form','url' => 'admin/attractions/insert', 'files' => true)) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ url('admin/attractions') }}" class="btn btn-danger">Cancel</a>

            </div>
                     <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
              <li><a href="#tab_2" data-toggle="tab">Photos</a></li>
            </ul>
            <div class="tab-content">
            
            <?php $page='admin.attractions.form'?>
            @include($page)
            <div class="tab-pane" id="tab_2">
            </div>
            </div>
            <!-- /.tab-content -->
          </div>
          {!! Form::close() !!}
      
        </div>
        <!-- /.box-body -->
      </div>
      <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this image?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
      <!-- /.box -->
    </div>
</div>

<script>   
    initSample();
    initSample1();
    
</script>
@include('admin/commons/javascript')
@endsection