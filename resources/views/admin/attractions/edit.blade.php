@extends('admin/admin_template')

@section('content')
 <style>
.text-light-blue{
  background: #2c3b41;
padding: 8px;
margin-top: 20px;
  text-align: center;
  border-radius: 3px; 
}
.text-light-blue a,.text-light-blue i{
  color:#8aa4af;
}            
.image-box{
  padding-top: 20px;
  text-align: center;
}
#gallery{
  background: #fff;
}
.image-style
{margin-top: -22px;  border: 1px solid #ddd;
  border-radius: 4px;
  padding: 8px;
height: 212px;
}
.image-style img{
  width: 100%;
    max-height: 100%;
  border-radius: 4px;
}
.box-body .form-control{border-radius: 4px;}
.panel{
  border-radius: 0px;
  margin-bottom: 0px;
      border-bottom: 1px dashed #ddd;
}
.view-btn{

  background: #ddd;
    height: 40px;
  line-height: 40px;
  width: 100px;
  text-align: center;
  margin: 0 auto;
  display: block;
      z-index: 8;
    position: relative;
    border-radius: 4px;
      transition-timing-function: ease-in;

  /* Quick on the way out */
  transition: 0.2s;
}
.view-btn a{
  color:#000;
  display: block;
  transition-timing-function: ease-in;

  /* Quick on the way out */
  transition: 0.2s;
}
input[type=checkbox], input[type=radio]{
  width: 17px;
  height: 17px;
}
.view-btn:hover{background: #26ABE2;}
.view-btn a:hover{color: #fff;}
.label-style{
 padding-right: 0px;
  text-align: center;
  color:#555;

}
label{
 color:#26ABE2;
}
.label-style1{
  padding-right: 0px;

}
.label-style label{
  color:#26ABE2;
  font-size: 16px !important;
      display: block;
    margin-top: 14px;
}

        </style> 
<div class="row">
    <div class="col-md-12">
          
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
     @include('admin/commons/errors')
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        <div class="box-header with-border">
          <h3 class="box-title">Edit Attraction</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['admin/attractions/update', $model->id], 'method' => 'post']) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="{{ url('admin/attractions') }}" class="btn btn-danger">Cancel</a>
           
            </div>
            <!-- text input -->
                   <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
              <li><a href="#tab_2" data-toggle="tab">Photos</a></li>
            </ul>
            <div class="tab-content">
            <?php $page='admin.attractions.form'?>
            @include($page)
           <div class="tab-pane" id="tab_2">
               <div class="col-sm-12">
                <h4 class="text-light-blue"><i class="fa fa-image"></i> <a  onclick="window.open('<?php echo url('admin/addimage/attractions')."/".$gallery_id; ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1000,height=600');">Add an Image</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-camera" aria-hidden="true"></i> <a onclick="window.open('<?php echo url('admin/addmultiple/attractions')."/".$gallery_id; ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1000,height=600');">Add Multiple Images</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-times" aria-hidden="true"></i> <a data-href="{{ url('admin/deleteallimages/attractions').'/'.$gallery_id }}" data-target="#confirm-delete"   data-toggle="modal">Delete All Images</a> 
                </h4>
                 <div id="gallery"></div>
              </div> 
            </div>
                
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        
      </div>
       <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this image?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
      <!-- /.box -->
    </div>
</div>
<script>
     $('.datepicker').datepicker({
      autoclose: true
    });
    initSample();
    initSample1();
loadGallery();
function loadGallery()
{
var gallery_id='<?php echo ($gallery_id)?$gallery_id:''; ?>';
 $.ajax({
    type:'get',
    url:'<?php echo url('admin/property/loadgallery'); ?>',
    data:{
      gallery_id:gallery_id,
      loadgllery:1
    },
  success:function(html){

         $('#gallery').html(html);

        },
        error: function(errormessage) {
              //you would not show the real error to the user - this is just to see if everything is working

            alert("Error ");
        }
});
}

function display_order(image_id)
{
    
    var images = $('input[name="image_id[]"]').map(function(){ 
                    return this.value; 
     }).get();
     
    var order = $('select[name="display_order[]"]').map(function(){ 
                    return this.value; 
     }).get();
  
   var gallery_id='<?php echo ($gallery_id)?$gallery_id:''; ?>';
    
    
    $.ajax({
            type:'post',
            url:'<?php echo url('admin/property/insertorder'); ?>',
            data:{
              _token:'<?php echo csrf_token(); ?>',
              'image_id[]': images,
              'display_order[]':order,
               change_id:image_id,
               gallery_id:gallery_id
               
            },
          success:function(html){
                
                if(html==1)
                {
                    loadGallery();
                }
                 
                        
                },
                error: function(errormessage) {
                      //you would not show the real error to the user - this is just to see if everything is working
                    
                    alert("Error ");
                }
      });
    
    //document.getElementById("image_id").value = image_id;
    //document.getElementById("myform").submit();
}
</script>
@include('admin/commons/javascript')
@endsection