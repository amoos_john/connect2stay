<?php
$required="required";
?>

<div class="tab-pane active" id="tab_1">
    
<div class="form-group col-md-7">
    {!! Form::label('Name:*') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="clearfix"></div>
<div class="form-group col-md-7">

    {!! Form::label('Public Name:*') !!}
    {!! Form::text('public_name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="clearfix"></div>

<div class="form-group col-md-5">
    {!! Form::label('Type:*') !!}
    {!! Form::select('type', $type,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="form-group col-md-5">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}
   
</div>

<div class="clearfix"></div>

    <div class="col-md-10">
    <div class="form-group">
       {!! Form::label('Summary:') !!}
       {!! Form::textarea('summary', null , array('class' => 'form-control','id'=>'editor') ) !!}
   </div>  
   <div class="form-group">
        {!! Form::label('Description:') !!}
        {!! Form::textarea('description', null , array('class' => 'form-control','id'=>'editor2') ) !!}
    </div> 
   
    </div>
                <div class="form-group col-md-5">
                  <label>Email</label>    
                    {!! Form::email('email', null , array('class' => 'form-control') ) !!}
                 </div>
                <div class="form-group col-md-5">
                   <label>Fax</label>  
                  {!! Form::text('fax', null , array('class' => 'form-control') ) !!}
                </div>
                <div class="form-group col-md-5">
                   <label>Phone</label>  
                  {!! Form::text('phone', null , array('class' => 'form-control') ) !!}
                </div> 
                <div class="form-group col-md-5">
                   <label>Toll-free</label>  
                  {!! Form::text('toll_free', null , array('class' => 'form-control') ) !!}
                </div>
                <div class="form-group col-md-5">
                   <label>Phone2</label>  
                  {!! Form::text('phone2', null , array('class' => 'form-control') ) !!}
                </div>
                <div class="form-group col-md-5">
                   <label>Website</label>  
                  {!! Form::text('website', null , array('class' => 'form-control') ) !!}
                </div>
                
                
                <div class="form-group col-sm-8">
                    {!! Form::label('Address') !!}
                    {!! Form::text('address', null , array('class' => 'form-control','id' => 'autocomplete','onFocus' => 'geolocate()') ) !!}
                     <img id="imgVerfied" src="{{ url('images/ico-status-green.png') }}" alt="Verified" title="Verified" style="display:none;"/>
                    <img id="imgNotYetVerified" src="{{ url('images/ico-status-red.png') }}" alt="Not yet verified" title="Not yet verified" style="">
                    <a href="javascript:void(0)" id="aMore" onclick="showAddressDetails(this);" title="Display address details">Less</a> | 
                    <a href="javascript:void(0)" id="aPinPoint" onclick="showPinPoint();" title="Show map and manually set location">Pin Point</a> 
                </div>
             
                  <div class="form-group col-md-5">
                    {!! Form::label('Address1*') !!}
                   {!! Form::text('address_1', null , array('class' => 'form-control','id' => 'address1',$required) ) !!}
   
                  </div>
                
                  <div class="form-group col-md-5">
                    {!! Form::label('Address2') !!}
                   {!! Form::text('address_2', null , array('class' => 'form-control') ) !!}
   
                  </div>
                   <div class="form-group col-md-5">
                   {!! Form::label('Neighborhood') !!}
                   {!! Form::text('neighborhood', null , array('class' => 'form-control','id' => 'administrative_area_level_3') ) !!}
   
                  </div>
               
                  <div class="form-group col-md-5">
                    {!! Form::label('City*') !!}
                   {!! Form::text('city', null , array('class' => 'form-control','id' => 'locality',$required) ) !!}
   
                  </div>
                      
                   <div class="form-group col-md-5">
                    {!! Form::label('County') !!}
                   {!! Form::text('county', null , array('class' => 'form-control','id' => 'administrative_area_level_2') ) !!}
   
                  </div>
               
                  <div class="form-group col-md-5">
                    {!! Form::label('Metro') !!}
                   {!! Form::text('metro', null , array('class' => 'form-control') ) !!}
   
                  </div>
                      
                   <div class="form-group col-md-5">
                    {!! Form::label('State') !!} 
                   {!! Form::text('state', null , array('class' => 'form-control','id' => 'administrative_area_level_1') ) !!}
   
                  </div>
               
                  <div class="form-group col-md-5">
                    {!! Form::label('Postal Code') !!}
                   {!! Form::text('postal_code', null , array('class' => 'form-control','id' => 'postal_code') ) !!}
   
                  </div>
                  <div class="form-group col-md-5">
                    {!! Form::label('Region') !!}
                   {!! Form::text('region', null , array('class' => 'form-control') ) !!}
   
                  </div>
               
                  <div class="form-group col-md-5">
                    {!! Form::label('Country *') !!}
                   {!! Form::text('country', null , array('class' => 'form-control','id' => 'country',$required) ) !!}
   
                  </div>
                  <div class="form-group col-md-5">
                    {!! Form::label('Longitude') !!}
                    {!! Form::text('longitude', null , array('class' => 'form-control','id' => 'longitude') ) !!}
                   
                  </div>        
                  <div class="form-group col-md-5">
                    {!! Form::label('Latitude') !!}
                   {!! Form::text('latitude', null , array('class' => 'form-control','id' => 'latitude') ) !!}
   
                  </div>  
 </div> 
              
<!-- /.tab-pane -->
         

