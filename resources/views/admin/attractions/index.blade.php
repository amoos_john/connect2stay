@extends('admin/admin_template')
@section('content')
<!-- Main row -->
<?php
if(isset($types) || isset($status_id))
{
    $querystringArray = [ 'type'=>$types, 'status'=>$status_id];
    $link=str_replace("search/?","search?",$model->appends($querystringArray)->render()); 
}
else
{
     $link=$model->render(); 
}

                  
?>
<style>
    .time{
        color: #999;
    }
    td.text__in p {
    white-space: nowrap;
    width: 300px;
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>
<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Attractions</h3>
                   
                    <div class="box-tools pull-right">
                    <a class="btn btn-success" href="{{url('admin/attractions/create')}}">Add New Attraction</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   

                    </div>
                
                </div>
               
               {!! Form::open(array( 'class' => 'form','url' => 'admin/attractions/search', 'method' => 'get')) !!}

            
                <div class="box-body">
                    
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Type') !!}
                                {!! Form::select('type',$type,(isset($types))?$types:'' ,['class' => 'form-control']) !!} 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Status') !!}
                                {!! Form::select('status',$status,(isset($status_id))?$status_id:'' ,['class' => 'form-control']) !!} 
                            </div>
                        </div>
                     
                        <div class='clearfix'></div>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Search</button>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <a href="{{ url('admin/attractions') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
    <!-- PRODUCT LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>
            @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-check"></i> {!! session('success') !!}
            </div>
            @endif
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Summary</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($model as $row)
                        <?php
                           
                            $color = '';
                            $status = '';
                            if($row->status=='1')
                            {
                                $color = 'text-green text-bold';
                                $status = 'Active';
                            }
                            elseif($row->status=='2')
                            {
                                $color = 'text-orange text-bold';
                                $status = 'Pending Approval';
                            }
                            elseif($row->status=='0')
                            {
                                $color = 'text-red text-bold';
                                $status = 'Inactive';
                            }
                           
                            if (array_key_exists($row->type, $type)) {
                               $type_name = $type[$row->type];
                            }
                            
                        ?>
                        <tr>
                            <td><?php echo $row->id; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td><?php echo $type_name.'<br/><em>'.$row->city.','.$row->state.'</em>'; ?></td>
                            <td class="text__in"><?php echo $row->summary; ?></td>
                            <td class="{{ $color }}"><i class="fa fa-circle"></i>  <?php echo $status; ?></td>

                            <td>
  
                                <a class="btn btn-primary btn-sm" href="<?php echo url('admin/attractions/edit/'.$row->id); ?>" title="Edit"><i class="fa fa-pencil"></i> </a>                       
                                <a data-href="<?php echo url('admin/attractions/delete/'.$row->id); ?>" data-target="#confirm-delete"  class="btn btn-danger btn-sm"  data-toggle="modal" title="Delete"><i class="fa fa-trash"></i> </a>

                            
                        </tr>
                        
                        @endforeach       
                    </tbody>
                </table>
                <?php echo $link; ?>
                 <div><?php echo "Showing ".$model->count()." of ".$model->total(); ?></div>
            </div>
        </div>
    
     <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this Attraction?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->	

@endsection