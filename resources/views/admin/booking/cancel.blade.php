@extends('admin/admin_template')

@section('content') 

<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
      
        <div class="box-header with-border">
          <h3 class="box-title">Booking Cancellation</h3>
        </div>
        
        
          
        <!-- /.box-header -->
        <div class="box-body">
         
       <div class="alert alert-danger alert-dismissible">
            <h4><i class="icon fa fa-check"></i> Your Payment was unsuccessful !</h4>
            Your payment cannot made. You can track you booking from this ID {{ $booking_id }}
        </div>
            
        <div class="form-group">
            <a href="{{ url('admin/booking') }}" class="btn btn-default btn-flat">Go back to Booking</a>
            
        </div>
	
    
            
            
            
        </div>
        <!-- /.box-body -->
        
      </div>
     
      <!-- /.box -->
    </div>
</div>

@endsection