@extends('admin/admin_template')

@section('content')
<?php
$required = "required";
if($role_id==1 || $role_id==2)
{
    $readonly = "";
}
else
{
    $readonly = "readonly";
}
?>
<link rel="stylesheet" href="{{ asset('plugins/timepicker/jquery.timepicker.min.css') }}">
<script src="{{ asset('plugins/timepicker/jquery.timepicker.min.js') }}"></script>
<style>
.list_property {
    height: 300px;
    overflow-y: scroll;
}
.textarea{
    resize: none;
    height: 100px !important;
}
 .pac-container {
        background-color: #FFF;
        z-index: 20;
        position: fixed;
        display: inline-block;
        float: left;
    }
input.form-control.col-md-3 {
    width: 40%;
    margin-right: 20px;
}
.padding0 {
    padding: 0 !important;
}
</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      @include('admin/commons/errors')
      <div class="box box-warning">
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
	@endif
        @if (Session::has('val_error'))
       <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <ul>
             @foreach (session('val_error') as $error)
                <li>{{ $error }}</li>
             @endforeach  
              
           </ul>

       </div>
       @endif
        <div class="box-header with-border">
          <h3 class="box-title">Booking Detail</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          {!! Form::open(array( 'class' => 'form','url' => 'admin/booking/insert', 'files' => true)) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ url('admin/booking') }}" class="btn btn-danger">Cancel</a>

            </div>
          
            <!-- text input -->
            <?php $page='admin.booking.form2'?>
            @include($page)
            
            <!-- Modal -->
<div class="modal fade strip_modal" id="myModalHorizontal_stripe" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" style="border-bottom:0px solid #fff;">
                <button type="button" class="close strip_modal_close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
    

  <div class="container-fluid grid strip">
    
    <div class="row pull-center">
      <div class="col-md-12">

        <div class="well">

          <div class="row card">
          </div>

          <br/>

          <div class="row-fluid">
            <div class="col-md-8">
              <div class="form-group">
                <label>Credit Card Number </label>
                <input type="text" name="card_no" value="{{ old('card_no') }}"  class="form-control" />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Expiration</label>

                <input type="text" placeholder="MM/YY" name="ccExpiry"  value="{{ old('ccExpiry') }}" class="form-control" />

              </div>
            </div>
          </div>

          <div class="row-fluid">
            <div class="col-md-8">
              <div class="form-group">
                <label>Name</label>
                <input type="text" name="card_name" value="{{ old('card_name') }}" class="form-control" />
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">

                <label>CVC </label>

                <input type="text" name="cvcNumber" value="{{ old('cvcNumber') }}" class="form-control" />
              </div>
            </div>
          </div>

          <div class="row ">

            <div class="col-md-12 text-right">
              <button type="button" data-dismiss="modal" class="btn btn-success">Submit</button>
              
            </div>

          </div>

        </div>
      </div>
    </div>

        
            </div>
            
              </div>  
      
        </div>
    </div>
</div>
           
          {!! Form::close() !!}
         

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>

<script>
$('.datepicker').datepicker({
      autoclose: true
});

$("#datepicker").keydown(function(event) { 
    return false;
});  
$("#datepicker2").keydown(function(event) { 
    return false;
});
$("input.timepicker").keydown(function(event) { 
    return false;
});
(function($) {
        $(function() {
            $('input.timepicker').timepicker();
        });
    })(jQuery); 

$("input[name$='payment_type']").click(function() {
        var test = $(this).val();

        $("div.desc-img").hide();
        $("#Payment" + test).show();
    });
    
    
    

function initLOSOnChange() {
        $("#content_ccBookingDetails2_txtLos").change(function () {
            var dateFormat = $('#' + 'content_ccBookingDetails2_txtCheckIn').datepicker('option', 'dateFormat');
            var cinDate = new Date();
            var strCheckIn = $get('content_ccBookingDetails2_txtCheckIn').value;
            cinDate = $.datepicker.parseDate(dateFormat, strCheckIn);
            if (cinDate == null)
                return false;
            var cout = new Date();
            //make the up down string value numeric 
            var days = +($("#content_ccBookingDetails2_txtLos").val());
            cout = new Date(cinDate.getFullYear(), cinDate.getMonth(), cinDate.getDate() + days);
            $("#content_ccBookingDetails2_txtCheckOut").val($.datepicker.formatDate(dateFormat, cout));

        }); 
        $("#content_ccBookingDetails2_txtCheckOut").bind('change',function () {
            $("#content_ccBookingDetails2_txtLos").val(getLOS());
        });
    }    
    
</script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/card/1.3.1/js/card.min.js"> </script>

<script>
new Card({
  form: '.form',
  container: '.card',
  formSelectors: {
    numberInput: 'input[name=card_no]',
    expiryInput: 'input[name=ccExpiry]',
    cvcInput: 'input[name=cvvNumber]',
    nameInput: 'input[name=card_name]'
  },

  width: 390, // optional — default 350px
  formatting: true,

  placeholders: {
    card_no: '•••• •••• •••• ••••',
    card_name: 'Full Name',
    ccExpiry: '••/••',
    cvvNumber: '•••'
  }
})
  </script>
@endsection