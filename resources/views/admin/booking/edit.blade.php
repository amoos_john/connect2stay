@extends('admin/admin_template')

@section('content') 
<?php
use App\Functions\Functions;

$currency= Config::get('params.currency');
$symbol=$currency["BRL"]["symbol"];
?>
<link rel="stylesheet" href="{{ asset('plugins/timepicker/jquery.timepicker.min.css') }}">
<script src="{{ asset('plugins/timepicker/jquery.timepicker.min.js') }}"></script>
<style>
.list_property {
    height: 300px;
    overflow-y: scroll;
}
.textarea{
    resize: none;
    height: 100px !important;
}
 .pac-container {
        background-color: #FFF;
        z-index: 20;
        position: fixed;
        display: inline-block;
        float: left;
    }
input.form-control.col-md-3 {
    width: 40%;
    margin-right: 20px;
}
.padding0 {
    padding: 0 !important;
}
</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        @if (Session::has('error'))
       <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
             @foreach (Session::get('error') as $error)
                <li>{{ $error }}</li>
             @endforeach 
          <?php Session::forget('error'); ?>  
       </div>
       @endif
        <div class="box-header with-border">
          <h3 class="box-title">Booking Detail</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            
            {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['admin/booking/update', $model->id], 'method' => 'post']) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="{{ url('admin/booking') }}" class="btn btn-danger">Cancel</a>
            
            </div>
            <p>Booking ID: {{ $id }}</p>
            <p>Property Name: {{ $model->property->internal_name }} </p>
            <p>Stay Date: {{ Functions::DateFormat($model->checkin) }} to {{ Functions::DateFormat($model->checkout) }}</p>
            <p>Booking Date: {{ Functions::DateFormat($model->created_at) }}</p>
            <!-- text input -->
            <?php $page='admin.booking.form'?>
            @include($page)
       
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        
      </div>
     
      <!-- /.box -->
    </div>
</div>

<script>

function apply()
{
    alert('This action is not applicable because this booking rule applies to all Properties'); 
    return false;  
}

 $('.datepicker').datepicker({
      autoclose: true
 });
</script>
<script>
$('.datepicker').datepicker({
      autoclose: true
});

$("#datepicker").keydown(function(event) { 
    return false;
});  
$("#datepicker2").keydown(function(event) { 
    return false;
});
$("input.timepicker").keydown(function(event) { 
    return false;
});
(function($) {
        $(function() {
            $('input.timepicker').timepicker();
        });
    })(jQuery); 
</script>   
@endsection