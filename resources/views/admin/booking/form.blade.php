<?php
use App\Functions\Functions;
$required="required";
?>
@include('admin/commons/errors')
<h3 class="text-light-blue">Guest Information</h3>
<div class="form-group col-md-6">
    {!! Form::label('First Name:*') !!}
    {!! Form::text('first_name', (isset($model->booking_information))?$model->booking_information->first_name:null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-md-6">

    {!! Form::label('Last Name:*') !!}
    {!! Form::text('last_name', (isset($model->booking_information))?$model->booking_information->last_name:null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Email:*') !!}
    {!! Form::email('email', (isset($model->booking_information))?$model->booking_information->email:null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Lead Source:') !!}
    {!! Form::select('lead_source',$lead_source,(isset($model->booking_information))?$model->booking_information->lead_source:null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="form-group col-md-6">
    {!! Form::label('Phone:*') !!}
    {!! Form::text('phone', (isset($model->booking_information))?$model->booking_information->phone:null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control') ) !!}
   
</div>
<div class="form-group col-md-6">
    {!! Form::label('Booked by:') !!}
    {!! Form::select('booked_by',$users,null , array('class' => 'form-control') ) !!}
   
</div>
<div class="clearfix"></div>
<h3 class="text-light-blue">Billing Address</h3>
<div class="form-group col-md-6">
    {!! Form::label('Address') !!}
    {!! Form::text('address', (isset($model->booking_information))?$model->booking_information->address:null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">

    {!! Form::label('Additional Address') !!}
    {!! Form::text('address2', (isset($model->booking_information))?$model->booking_information->address2:null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('City') !!}
    {!! Form::text('city', (isset($model->booking_information))?$model->booking_information->city:null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">

    {!! Form::label('State/Province') !!}
    {!! Form::text('state', (isset($model->booking_information))?$model->booking_information->state:null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Postal Code') !!}
    {!! Form::text('postal_code', (isset($model->booking_information))?$model->booking_information->postal_code:null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">

    {!! Form::label('Country') !!}
    {!! Form::select('country',$countries,(isset($model->booking_information))?$model->booking_information->country:null , array('class' => 'form-control') ) !!}

</div>
<div class="clearfix"></div>
<h3 class="text-light-blue">Property Information</h3>
<div class="form-group col-md-6">
        {!! Form::label('Property') !!}
        {!! Form::select('property_id',$properties,null,['class' => 'form-control','id' => 'property_id']) !!} 
   
</div>
<div class="form-group col-md-2">
    <label>&nbsp;</label>
    <a href="{{ url('rentals/allrentals/'.$model->property_id) }}" target="_blank" class="btn btn-primary">View on website</a>
</div>
<div class="clearfix"></div>
<h3 class="text-light-blue">Stay Information</h3>
<div class="col-md-5 margin-0 padding0 form-group">

     <div class="col-xs-6 margin-0">
        {!! Form::label('Check-in*') !!}
        {!! Form::text('checkin', null , array('class' => 'datepicker form-control','id' => 'datepicker', $required) ) !!}
      </div>
       <div class="col-xs-6 margin-0">
           <label>&nbsp;</label>
       {!! Form::text('checkin_time', null , array('class' => 'form-control timepicker','id' => 'checkin_time') ) !!}
       </div>
 
</div> 
<div class="col-md-5 margin-0 padding0 form-group">

     <div class="col-xs-6 margin-0">
         <label>Check-out</label>
    {!! Form::text('checkout', null  , array('class' => 'datepicker form-control','id' => 'datepicker2', $required) ) !!}
      </div>
       <div class="col-xs-6 margin-0">
           <label>&nbsp;</label>
       {!! Form::text('checkout_time', null , array('class' => 'form-control timepicker','id' => 'checkout_time') ) !!}
       </div>

</div>

<div class="clearfix"></div>
<div class="form-group col-md-4">
    {!! Form::label('#Adults/#Children') !!}
    <div class="clearfix"></div>
    {!! Form::number('adults', null , array('class' => 'col-md-3 form-control','min' => '2') ) !!}
    {!! Form::number('children', null , array('class' => 'col-md-3 form-control','min' => '0') ) !!}
</div>

<div class="form-group col-md-4">
    {!! Form::label('#Nights') !!}
    <p>{{ $nights }}</p>
</div>
<div class="clearfix"></div>
<h3 class="text-light-blue">Payment Information</h3>
@if(count($payment)>0)

@if($model->payment_type=='paypal')
    @include('admin.booking.paypal')
    
@elseif($model->payment_type=='stripe')
    @include('admin.booking.stripe')
@endif

@endif
<div class="form-group col-md-4">
    {!! Form::label('Payment Status') !!}
    {!! Form::select('payment_status',$payment_status,null,['class' => 'form-control']) !!} 
 
</div> 

<div class="clearfix"></div>
@if(count($items)>0)
<h3 class="text-light-blue">Statement</h3>
<div class="col-md-10">
    <table class="table table-responsive">
        <tbody>
            <tr>
               <td>{!! Form::label('Taxed') !!}</td>
               <td>{!! Form::label('Type') !!}</td>
               <td>{!! Form::label('Qty') !!}</td>
               <td>{!! Form::label('Description') !!}</td>
               <td>{!! Form::label('Rate') !!}</td>
               <td>{!! Form::label('Amount') !!}</td>
            </tr>
            @foreach($items as $item)
            <?php
            
            if (array_key_exists($item->type, $type)) {
               $type_name = $type[$item->type];
            }
           ?>
            <tr>
               <td>{{ ($item->taxed==1)?'Y':'N' }}</td>
               <td>{{ $type_name }}</td>
               <td>{{ $item->quantity }}</td>
               <td><?php echo $item->description; ?></td>
               <td><?php echo $symbol.''.Functions::MoneyFormat($item->rate); ?> </td>
               <td><?php echo $symbol.''.Functions::MoneyFormat($item->amount); ?></td>
            </tr>
           @endforeach
        </tbody>
    </table>
</div>
<div class="col-md-4">
<div  class="form-group">
<span>Total:</span>
<p class="form-control-static"><?php echo $symbol.''.Functions::MoneyFormat($model->grand_total); ?></p>
</div>
<div class="clearfix"></div>
<div  class="form-group">
<span>Amount Paid:</span>
<p class="form-control-static">
    <?php echo ($model->payment_status=='success')?
    $symbol.''.Functions::MoneyFormat($model->grand_total):
    $symbol.''.Functions::MoneyFormat(0) ?></p>
</div>
</div>  
<div class="col-md-4">
<div class="form-group">
<span>Total Due:</span>
<p class="form-control-static">
    <?php echo ($model->payment_status=='cancel')?
    $symbol.''.Functions::MoneyFormat($model->grand_total): 
    $symbol.''.Functions::MoneyFormat(0) ?></p>
</div>
<div class="clearfix"></div>
<div class="form-group" style="display:none;">
<span>Amount Due Now:</span>
<p id="ddDueNow" class="form-control-static"></p>
</div>
</div>
<!--
<div class="col-md-4">

<div class="clearfix"></div>
<div class="form-group">
    <span>Due On:</span>
    <input type="hidden" name="" id="content_ccBookingDetails2_ccStatement_hfDueOnEdited">
    <input name="ctl00$content$ccBookingDetails2$ccStatement$txtDueOn" type="text" value="01/06/2017" id="content_ccBookingDetails2_ccStatement_txtDueOn" class="dp hasDatepicker" onchange="document.getElementById('content_ccBookingDetails2_ccStatement_hfDueOnEdited').value='true';" style="width:105px;">
    <input type="hidden" name="ctl00$content$ccBookingDetails2$ccStatement$hfPropIDcontent_ccBookingDetails2_ccStatement_ceDueOn" id="content_ccBookingDetails2_ccStatement_hfPropIDcontent_ccBookingDetails2_ccStatement_ceDueOn">
</div>
</div>
-->
@endif
<div class="clearfix"></div>
<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Special Requests</h3>
    {!! Form::textarea('special', (isset($model->booking_information))?$model->booking_information->special:null , array('class' => 'form-control textarea') ) !!}
</div>  
<div class="clearfix"></div>
<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Internal Notes</h3>
   {!! Form::textarea('internal_notes', null , array('class' => 'form-control textarea','row' => '20') ) !!}
</div>


