
<h3 class="text-light-blue">Guest Information</h3>
<div class="form-group col-md-6">
    {!! Form::label('First Name:*') !!}
    {!! Form::text('first_name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-md-6">

    {!! Form::label('Last Name:*') !!}
    {!! Form::text('last_name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('Email:*') !!}
    {!! Form::email('email', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Lead Source:') !!}
    {!! Form::select('lead_source',$lead_source,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="form-group col-md-6">
    {!! Form::label('Phone:*') !!}
    {!! Form::text('phone', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="form-group col-md-6">
    {!! Form::label('Booked by:') !!}
    {!! Form::select('booked_by',$users,$user_id , array('class' => 'form-control') ) !!}

 </div>
<div class="clearfix"></div>
<h3 class="text-light-blue">Billing Address</h3>
<div class="form-group col-md-6">
    {!! Form::label('Address') !!}
    {!! Form::text('address', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">

    {!! Form::label('Additional Address') !!}
    {!! Form::text('address2', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('City') !!}
    {!! Form::text('city', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">

    {!! Form::label('State/Province') !!}
    {!! Form::text('state', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">
    {!! Form::label('Postal Code') !!}
    {!! Form::text('postal_code', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-6">

    {!! Form::label('Country') !!}
    {!! Form::select('country',$countries,null , array('class' => 'form-control') ) !!}

</div>
<div class="clearfix"></div>

<h3 class="text-light-blue">Property Information</h3>
<div class="form-group col-md-6">
        {!! Form::label('Property') !!}
        {!! Form::select('property_id',$properties,null,['class' => 'form-control','id' => 'property_id']) !!} 
   
</div>
<div class="clearfix"></div>
<h3 class="text-light-blue">Stay Information</h3>
<div class="col-md-5 margin-0 padding0 form-group">

     <div class="col-xs-6 margin-0">
        {!! Form::label('Check-in*') !!}
        {!! Form::text('checkin', $date , array('class' => 'datepicker form-control','id' => 'datepicker', $required) ) !!}
      </div>
       <div class="col-xs-6 margin-0">
           <label>&nbsp;</label>
       {!! Form::text('checkin_time', $setting->checkin_time , array('class' => 'form-control timepicker','id' => 'checkin_time') ) !!}
       </div>

</div> 
<div class="col-md-5 margin-0 padding0 form-group">

     <div class="col-xs-6 margin-0">
         <label>Check-out</label>
    {!! Form::text('checkout', $end_date  , array('class' => 'datepicker form-control','id' => 'datepicker2', $required) ) !!}
      </div>
       <div class="col-xs-6 margin-0">
           <label>&nbsp;</label>
       {!! Form::text('checkout_time', $setting->checkout_time , array('class' => 'form-control timepicker','id' => 'checkout_time') ) !!}
       </div>

</div>
<div class="clearfix"></div>
<div class="form-group col-md-4">
    {!! Form::label('#Adults/#Children') !!}
    <div class="clearfix"></div>
    {!! Form::number('adults', 2 , array('class' => 'col-md-3 form-control','min' => '2') ) !!}
    {!! Form::number('children', null , array('class' => 'col-md-3 form-control','min' => '2') ) !!}
</div>

<div class="form-group col-md-4">
    {!! Form::label('#Nights') !!}
    <div class="clearfix"></div>
    {!! Form::number('nights', $nights , array('class' => 'col-md-3 form-control','min' => '2') ) !!}

</div>
<div class="clearfix"></div>
<h3 class="text-light-blue">Payment Information</h3>

<div class="form-group col-md-2">
<input type="radio" name="payment_type" id="input1" value="1" required/>
<label for="input1">Paypal</label>
</div>
<div class="form-group col-md-2">
<input type="radio" name="payment_type" id="input2" value="2" data-toggle="modal" data-target="#myModalHorizontal_stripe"/>
<label for="input2">Stripe</label>
</div>



<div class="clearfix"></div>

<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Special Requests</h3>
    {!! Form::textarea('special', null , array('class' => 'form-control textarea') ) !!}
</div>  
<div class="clearfix"></div>
<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Internal Notes</h3>
   {!! Form::textarea('internal_notes', null , array('class' => 'form-control textarea','row' => '20') ) !!}
</div>



