﻿@extends('admin/admin_template')

@section('content')
<!-- Main row -->
<?php
use App\Functions\Functions;
$currency= Config::get('params.symbol');

if(isset($id))
{   //dd(Request::getQueryString());
    $querystringArray = [ "id"=>$id,
        "property_id"=>$property_id,
        "first_name"=>$first_name,
        "last_name"=>$last_name,
        "renter_id"=>$renter_id,
        "checkin_from"=>$checkin_from,
        "checkin_to"=>$checkin_to,
        "checkout_from"=>$checkout_from,
        "checkout_to"=>$checkout_to,
        "status"=>$status_id,
        "payment_method"=>$method,];
    $link=str_replace("search/?","search?",$model->appends($querystringArray)->render()); 
    
    
  //    dd($querystringArray);
}
else
{
     $link=$model->render();
}
                
?>
<style>
    .box-body .row > div:nth-child(6) {
    width:25%;
}
</style>
 <section class="content-header">
             <h1>{{ $page_title }}</h1>
</section>

<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                
                    <h3 class="box-title">Search</h3>
                    <div class="box-tools pull-right">
		    @if($role_id!=3)
                    <a class="btn btn-default" href="{{url('admin/booking/create')}}">New House Booking</a>
		    @endif	
                    <a class="btn btn-default" href="{{url('admin/ownerbooking/create')}}">New Owner Booking</a>
                   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   

                    </div>
                </div>
                
                {!! Form::model('', ['class' => 'form','url' => ['admin/'.$search_url], 'method' => 'get']) !!}
            
                <div class="box-body">
                    
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('ID:') !!}
                                {!! Form::text('id', (isset($id))?$id:'' , array('class' => 'form-control') ) !!}
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Property (Unit):') !!}
                                @if($role_id==1 || $role_id==2)
                                {!! Form::select('property_id',$properties,(isset($property_id))?$property_id:'',['class' => 'form-control']) !!} 
                                @elseif($role_id==3)
                                <select name="property_id" id="property_id" class="form-control">
                                    <option value="">Select Property</option>
                                    @foreach($properties as $row)
                                    <option value="{{ $row->id }}">{{ $row->internal_name }}</option>
                                    @endforeach
                                </select>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="form-group">
                             {!! Form::label('Renter First Name:') !!}
                                {!! Form::text('first_name',(isset($first_name))?$first_name:''  , array('class' => 'form-control') ) !!}
                            </div>
                        </div>
                       <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Renter Last Name:') !!}
                                {!! Form::text('last_name',(isset($last_name))?$last_name:''  , array('class' => 'form-control') ) !!}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Renter ID:') !!}
                                {!! Form::number('renter_id',(isset($renter_id))?$renter_id:''  , array('class' => 'form-control') ) !!}
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Check In From:') !!}
                                {!! Form::text('checkin_from', (isset($checkin_from))?$checkin_from:''  , array('class' => 'datepicker form-control','id' => 'datepicker') ) !!}
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Check In To:') !!}
                                {!! Form::text('checkin_to', (isset($checkin_to))?$checkin_to:''  , array('class' => 'datepicker form-control','id' => 'datepicker') ) !!}
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Check Out From:') !!}
                                {!! Form::text('checkout_from', (isset($checkout_from))?$checkout_from:''  , array('class' => 'datepicker form-control','id' => 'datepicker') ) !!}
                            </div>
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Check Out To:') !!}
                                {!! Form::text('checkout_to', (isset($checkout_to))?$checkout_to:''  , array('class' => 'datepicker form-control','id' => 'datepicker') ) !!}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Status') !!}
                                {!! Form::select('status',$statuses,(isset($status_id))?$status_id:'',['class' => 'form-control']) !!} 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Payment Source') !!}
                                {!! Form::select('payment_method',$payment_method,(isset($method))?$method:'',['class' => 'form-control']) !!} 
                            </div>
                        </div>
                        <div class='clearfix'></div>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Search</button>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <a href="{{ url('admin/'.$book_url) }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
    
    <!-- PRODUCT LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">( Total : {{ count($model) }} ) </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>
            @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            
               <i class="icon fa fa-check"></i> {!! session('success') !!}
            </div>
            @endif
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Status / Type</th>
                            <th>Property / Renter</th>
                            <th>Check In / Out</th>
                            <th>Balance</th> 
                            @if($role_id==1 || $role_id==2)
                            <th>Action</th>  
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($model)>0)   
                        @foreach ($model as $row)
                        <?php
                            $color = '';
                            $status = '';
                            
                            if (array_key_exists($row->status, $statuses)) {
                               $status = $statuses[$row->status];
                            }
                            
                        ?>
                        <tr>
                            <td>{{ $row->id }}<br/>({{ $row->id }})<br/>{{ Functions::DateFormat($row->created_at) }}</td>
                            <td><i class="fa fa-envelope" aria-hidden="true"></i> {{ $status }}</td>
                            <td>{{ $row->internal_name }}<br/>Rented by: {{ $row->first_name }},{{ $row->last_name }}</td>
                            <td style="color: green">{{ Functions::DateFormat($row->checkin) }}(IN)
                                <br/>{{ Functions::DateFormat($row->checkout) }}(OUT)</td>
                            <td>{{ ($row->grand_total!=0)?$currency."".Functions::MoneyFormat($row->grand_total):'N/A' }}</td>
                            @if($role_id==1 || $role_id==2)
                            <td>
                                
                                <a class="btn btn-primary btn-sm" href="<?php echo url('admin/booking/edit/'.$row->id); ?>" title="Details"><i class="fa fa-table"></i> </a>
                                <a data-href="<?php echo url('admin/booking/delete/'.$row->id); ?>" data-target="#confirm-delete"  class="btn btn-danger btn-sm"  data-toggle="modal" title="Delete"><i class="fa fa-trash"></i> </a>
                                
                            </td>   
                            @endif
                        </tr>
                       
                        @endforeach  
                    @else
                    <tr>
                        <td colspan='7' class='text-center'>Result not found!</td>
                    </tr>
                    @endif
                    </tbody>
                </table>
                <?php echo $link; ?>
                
             
                       
                <div><?php echo "Showing ".$model->count()." of ".$model->total(); ?></div>
                     
                     
                 </div>
            </div>
        </div>
    
     <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this booking?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->	
<script>
$('.datepicker').datepicker({
      autoclose: true
});
$(document).ready( function() {
        $("#datepicker").keypress( function(e) {
        if (e.which==32){
        return false;
        }
    });
});
</script>
@endsection