@extends('admin/admin_template')

@section('content')
<?php
$required = "required";
if($role_id==1 || $role_id==2)
{
    $readonly = "";
}
else
{
    $readonly = "readonly";
}
?>
<link rel="stylesheet" href="{{ asset('plugins/timepicker/jquery.timepicker.min.css') }}">
<script src="{{ asset('plugins/timepicker/jquery.timepicker.min.js') }}"></script>
<style>
.list_property {
    height: 300px;
    overflow-y: scroll;
}
.textarea{
    resize: none;
    height: 100px !important;
}
 .pac-container {
        background-color: #FFF;
        z-index: 20;
        position: fixed;
        display: inline-block;
        float: left;
    }
input.form-control.col-md-3 {
    width: 40%;
    margin-right: 20px;
}
.padding0 {
    padding: 0 !important;
}
</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      @include('admin/commons/errors')
      <div class="box box-warning">
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
	@endif
        @if (Session::has('error'))
       <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <ul>
               @foreach (session('error') as $error)
                <li>{{ $error }}</li>
                @endforeach  
              
           </ul>

       </div>
       @endif
        <div class="box-header with-border">
          <h3 class="box-title">Booking Detail</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          {!! Form::open(array( 'class' => 'form','url' => 'admin/ownerbooking/insert', 'files' => true)) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ url('admin/'.$book_url) }}" class="btn btn-danger">Cancel</a>

            </div>
          
          <h3 class="text-light-blue">Guest Information</h3>
            <div class="form-group col-md-6">
                {!! Form::label('First Name:*') !!}
                {!! Form::text('first_name', $model->name , array('class' => 'form-control',$required,$readonly) ) !!}
            </div>
            <div class="form-group col-md-6">

                {!! Form::label('Last Name:') !!}
                {!! Form::text('last_name', null , array('class' => 'form-control',$readonly) ) !!}
            </div>
            <div class="form-group col-sm-6">
                {!! Form::label('Email:*') !!}
                {!! Form::email('email', $model->email , array('class' => 'form-control',$required,$readonly) ) !!}
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('Lead Source:*') !!}
                {!! Form::select('lead_source',$lead_source,null , array('class' => 'form-control',$required,) ) !!}

            </div>
            <div class="form-group col-md-6">
                {!! Form::label('Phone:') !!}
                {!! Form::text('phone', $model->phone , array('class' => 'form-control',$required,$readonly) ) !!}
            </div>
          @if($role_id==1 || $role_id==2)

          <div class="form-group col-md-6">
            {!! Form::label('Booked by:') !!}
            {!! Form::select('booked_by',$users,$user_id , array('class' => 'form-control') ) !!}

           </div>
          @endif
            
            <div class="clearfix"></div>
            <h3 class="text-light-blue">Billing Address</h3>
            <div class="form-group col-md-6">
                {!! Form::label('Address') !!}
                {!! Form::text('address', null , array('class' => 'form-control') ) !!}
            </div>
            <div class="form-group col-md-6">

                {!! Form::label('Additional Address') !!}
                {!! Form::text('address2', null , array('class' => 'form-control') ) !!}
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('City') !!}
                {!! Form::text('city', null , array('class' => 'form-control') ) !!}
            </div>
            <div class="form-group col-md-6">

                {!! Form::label('State/Province') !!}
                {!! Form::text('state', null , array('class' => 'form-control') ) !!}
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('Postal Code') !!}
                {!! Form::text('postal_code',null , array('class' => 'form-control') ) !!}
            </div>
            <div class="form-group col-md-6">

                {!! Form::label('Country*') !!}
                {!! Form::select('country',$countries,null , array('class' => 'form-control',$required) ) !!}

            </div>
            <div class="clearfix"></div>
            <h3 class="text-light-blue">Property Information</h3>
            <div class="form-group col-md-6">
                    {!! Form::label('Property*') !!}
                     @if($role_id==1 || $role_id==2)
                    {!! Form::select('property_id',$properties,null,['class' => 'form-control',$required]) !!} 
                    @elseif($role_id==3)
                    <select name="property_id" id="property_id" class="form-control" <?php echo $required;?>>
                        <option value="">Select Property</option>
                        @foreach($properties as $row)
                        <option value="{{ $row->id }}">{{ $row->internal_name }}</option>
                        @endforeach
                    </select>
                    @endif
                 
            </div>
           
            <div class="clearfix"></div>
            <h3 class="text-light-blue">Stay Information</h3>
            <div class="col-md-5 margin-0 padding0 form-group">

                 <div class="col-xs-6 margin-0">
                    {!! Form::label('Check-in*') !!}
                    {!! Form::text('checkin', $date , array('class' => 'datepicker form-control','id' => 'datepicker', $required) ) !!}
                  </div>
                   <div class="col-xs-6 margin-0">
                       <label>&nbsp;</label>
                   {!! Form::text('checkin_time', $setting->checkin_time , array('class' => 'form-control timepicker','id' => 'checkin_time') ) !!}
                   </div>

            </div> 
            <div class="col-md-5 margin-0 padding0 form-group">

                 <div class="col-xs-6 margin-0">
                     <label>Check-out</label>
                {!! Form::text('checkout', $end_date  , array('class' => 'datepicker form-control','id' => 'datepicker2', $required) ) !!}
                  </div>
                   <div class="col-xs-6 margin-0">
                       <label>&nbsp;</label>
                   {!! Form::text('checkout_time', $setting->checkout_time , array('class' => 'form-control timepicker','id' => 'checkout_time') ) !!}
                   </div>

            </div>
            <div class="clearfix"></div>
            <div class="form-group col-md-4">
                {!! Form::label('#Adults/#Children') !!}
                <div class="clearfix"></div>
                {!! Form::number('adults', 2 , array('class' => 'col-md-3 form-control','min' => '2') ) !!}
                {!! Form::number('children', null , array('class' => 'col-md-3 form-control','min' => '2') ) !!}
            </div>

            <div class="form-group col-md-4">
                {!! Form::label('#Nights') !!}
                <div class="clearfix"></div>
                {!! Form::number('nights', $nights , array('class' => 'col-md-3 form-control','min' => '2') ) !!}

            </div>
            <div class="clearfix"></div>
            <div class="form-group col-sm-8">
                <h3 class="text-light-blue">Special Requests</h3>
                {!! Form::textarea('special', (isset($model->booking_information))?$model->booking_information->special:null , array('class' => 'form-control textarea') ) !!}
            </div>  
            <div class="clearfix"></div>
            <div class="form-group col-sm-8">
                <h3 class="text-light-blue">Internal Notes</h3>
               {!! Form::textarea('internal_notes', null , array('class' => 'form-control textarea','row' => '20') ) !!}
            </div>
           
    
          {!! Form::close() !!}
         

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>

<script>
$('.datepicker').datepicker({
      autoclose: true
});

$("#datepicker").keydown(function(event) { 
    return false;
});  
$("#datepicker2").keydown(function(event) { 
    return false;
});
$("input.timepicker").keydown(function(event) { 
    return false;
});
(function($) {
        $(function() {
            $('input.timepicker').timepicker();
        });
    })(jQuery); 
    
function initLOSOnChange() {
        $("#content_ccBookingDetails2_txtLos").change(function () {
            var dateFormat = $('#' + 'content_ccBookingDetails2_txtCheckIn').datepicker('option', 'dateFormat');
            var cinDate = new Date();
            var strCheckIn = $get('content_ccBookingDetails2_txtCheckIn').value;
            cinDate = $.datepicker.parseDate(dateFormat, strCheckIn);
            if (cinDate == null)
                return false;
            var cout = new Date();
            //make the up down string value numeric 
            var days = +($("#content_ccBookingDetails2_txtLos").val());
            cout = new Date(cinDate.getFullYear(), cinDate.getMonth(), cinDate.getDate() + days);
            $("#content_ccBookingDetails2_txtCheckOut").val($.datepicker.formatDate(dateFormat, cout));

        }); 
        $("#content_ccBookingDetails2_txtCheckOut").bind('change',function () {
            $("#content_ccBookingDetails2_txtLos").val(getLOS());
        });
    }    
    
</script> 
@endsection