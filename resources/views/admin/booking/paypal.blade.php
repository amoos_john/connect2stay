<div class="form-group col-md-6">
    {!! Form::label('Payment Method') !!}
    <p>{{ $model->payment_type }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Payment ID') !!}
    <p>{{ $payment[0]->paymentId }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Payer ID') !!}
    <p>{{ $payment[0]->payerID }}</p>
</div>
