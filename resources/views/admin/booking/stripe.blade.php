<div class="form-group col-md-6">
    {!! Form::label('Payment Method') !!}
    <p>{{ $model->payment_type }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Charge ID') !!}
    <p>{{ $payment[0]->charge_id }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Card No.') !!}
    <p>{{ $payment[0]->card_no }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Card Name') !!}
    <p>{{ $payment[0]->card_name }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('Month/Year') !!}
    <p>{{ $payment[0]->month }}/{{ $payment[0]->year }}</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('CVC No.') !!}
    <p>{{ $payment[0]->cvc }}</p>
</div>