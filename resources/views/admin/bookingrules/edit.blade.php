@extends('admin/admin_template')

@section('content') 
<style>
.list_property {
    height: 300px;
    overflow-y: scroll;
}
.textarea{
    resize: none;
    height: 100px !important;
}
 .pac-container {
        background-color: #FFF;
        z-index: 20;
        position: fixed;
        display: inline-block;
        float: left;
    }

</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        <div class="box-header with-border">
          <h3 class="box-title">Edit Booking Rule</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['admin/bookingrules/update', $model->id], 'method' => 'post']) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="{{ url('admin/bookingrules') }}" class="btn btn-danger">Cancel</a>
            @if($model->applicability==0)
            <a onclick="apply();" class="btn btn-default pull-right" title="Apply to Multiple Properties">Apply to Multiple Properties</a>
            @else
               <a class="btn btn-default pull-right" data-target="#multiple" data-toggle="modal" title="Apply to Multiple Properties">Apply to Multiple Properties</a>
            @endif
          
            </div>
            <!-- text input -->
            <?php $page='admin.bookingrules.form'?>
            @include($page)
       
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        
      </div>
     
      <!-- /.box -->
    </div>
</div>
@include('admin/commons/multiple')
<script>
    initSample();

function apply()
{
    alert('This action is not applicable because this booking rule applies to all Properties'); 
    return false;  
}

 $('.datepicker').datepicker({
      autoclose: true
 });
</script>
@endsection