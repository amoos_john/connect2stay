<?php
$required="required";
?>
@include('admin/commons/errors')
<div class="form-group col-md-8">
    <h3 class="text-light-blue">General</h3>

    {!! Form::label('Name:*') !!}
    {!! Form::text('name', null , array('class' => 'form-control col-sm-6',$required) ) !!}
</div>
<div class="form-group col-md-5">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="form-group col-md-5">
    {!! Form::label('Applicability:') !!}
    {!! Form::select('applicability', $applicability,null , array('class' => 'form-control',$required) ) !!}
   
</div>
{!! Form::hidden('type', $type) !!}
@if($type==1)
 <div class="form-group  col-md-3">
    {!! Form::label('Min Stay:*') !!}
    {!! Form::number('min_stay', null , array('class' => 'form-control','min' => '1',$required) ) !!}
</div>
<div class="col-md-12">
   
 <h3 class="text-light-blue">Apply this rule when dates of stay are in this date range
</h3>  
</div>
@endif
<div class="form-group col-md-5">
 
    {!! Form::label('Beginning:') !!}
    {!! Form::text('start', $start_date , array('class' => 'datepicker form-control') ) !!}
   
</div>
<div class="form-group col-md-5">
    {!! Form::label('End:') !!}
    {!! Form::text('end', $end_date , array('class' => 'datepicker form-control') ) !!}
   
</div>

    
@if($type==2)
 <div class="col-md-5">   
   
         {!! Form::label('Charge:') !!}
    
     <div class="row">
         <div class="col-md-6"> 
            {!! Form::number('charge', null , array('class' => 'form-control','min' => '1',$required) ) !!}

         </div>
         <div class="col-md-6"> 
           {!! Form::select('charge_type', $charge_type,null , array('class' => 'form-control') ) !!}

         </div>
     </div>
</div>
<div class="col-md-5">   
   
         {!! Form::label('Remainder Due:') !!}
    
     <div class="row">
         <div class="col-md-6"> 
            {!! Form::number('reminder', null , array('class' => 'form-control','min' => '1') ) !!}

         </div>
         <div class="col-md-6"> 
           {!! Form::select('reminder_type', $reminder_type,null , array('class' => 'form-control') ) !!}

         </div>
     </div>
</div>
<div class="col-md-12"> 
   <h3  class="text-light-blue">Total Due Calculation</h3>
   <?php
   $tax='';
   $fax='';
   $dep='';
   $spe='';
   if(isset($dues))
   {
        
       foreach($dues as $due)
        {
           if($due==1)
            {
               $tax='checked';
            }
            elseif($due==2)
            {
               $fax='checked';
            }
            elseif($due==3)
            {
               $dep='checked';
            }
            elseif($due==4)
            {
               $spe='checked';
            }
                            
       }
   }
   ?> 
    <div class="form-group">
         <div class="col-md-2">
            <label><input type="checkbox" value="1" name="due[]" <?php echo $tax; ?>/> Exclude Taxes</label>
         </div>   
                      
         <div class="col-md-4">
             <label><input type="checkbox" value="2"  name="due[]" <?php echo $fax; ?>/> Exclude Fees (not marked treat as rent)</label>
         </div>
                      
          <div class="col-md-2">
                <label><input type="checkbox" value="3" name="due[]" <?php echo $dep; ?>/>Exclude Deposits</label>
          </div>
        
           <div class="col-md-2">
                <label><input type="checkbox" value="4" name="due[]" <?php echo $spe; ?>/> Exclude Specials</label>
          </div>
  </div>     
    
</div>
<div class="col-md-12"> 
   <h3  class="text-light-blue">Advance</h3>
   <div class="form-group col-md-6">
    {!! Form::label('Processor:') !!} 
    {!! Form::select('processor', $processor,null , array('class' => 'form-control') ) !!}

  </div>
   
    <div class="form-group">
           <div class="col-md-7">
                <label><input type="checkbox" value="1" name="auto_confirm"  <?php echo (isset($auto_confirm))?$auto_confirm:'';?>/> Do not auto-confirm charged bookings</label>
         </div>
  </div>     
    
</div>
@endif

<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Public Description</h3>
    {!! Form::textarea('description', null , array('class' => 'form-control','id'=>'editor') ) !!}
</div>  
<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Notes</h3>
   {!! Form::textarea('notes', null , array('class' => 'form-control textarea','row' => '20') ) !!}
</div>


