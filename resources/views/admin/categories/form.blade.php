<?php
$required="required";
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::label('Category Name*') !!}
    {!! Form::text('category_name', null , array('class' => 'form-control',$required) ) !!}
</div>


<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="{{ url('admin/categories') }}" class="btn btn-danger">Cancel</a>
</div>