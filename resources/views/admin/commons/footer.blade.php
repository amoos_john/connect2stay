<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Powered by <a href="http://www.golpik.com/" target="_blank" >Golpik</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="" target="_blank" >{{ Config::get('params.site_name') }}</a>.</strong> All rights reserved.
  </footer>
