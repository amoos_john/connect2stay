<aside class="main-sidebar">
    <section class="sidebar">
         <div class="user-panel">
        <!--<div class="pull-left image">
          <img src="{{ url('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
       
        </div>-->
      </div>
       
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
             <li>
                 <a href="{{ url('admin') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                </a>
               
            </li>
            @if(Auth::user()->role_id==1 || Auth::user()->role_id==2)
             <li class="treeview">
                 <a href="#">
                    <i class="fa fa-users"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{ url('admin/users') }}"><i class="fa fa-circle-o"></i> List Users</a></li>
                    <li><a href="{{ url('admin/users/create') }}"><i class="fa fa-circle-o"></i> Add New User</a></li>
                </ul>
            </li>
            
             <li class="treeview">
                 <a href="#">
                    <i class="fa fa-home"></i> <span>All Properties</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{ url('admin/property') }}"><i class="fa fa-circle-o"></i> List Properties</a></li>
                    <li><a href="{{ url('admin/property/create') }}"><i class="fa fa-circle-o"></i> Add New Property</a></li>
                    <li><a href="{{ url('admin/property/ratesandavailability') }}"><i class="fa fa-circle-o"></i> Rates & Availability</a></li>
                </ul>
            </li>
             <li class="treeview">
                 <a href="#">
                    <i class="fa fa-tags"></i> <span>Categories</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{ url('admin/categories') }}"><i class="fa fa-circle-o"></i> List Categories</a></li>
                    <li><a href="{{ url('admin/categories/create') }}"><i class="fa fa-circle-o"></i> Add New Category</a></li>
                </ul>
            </li>
             <li class="treeview">
                 <a href="#">
                    <i class="fa fa-check-square"></i> <span>Amenities</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{ url('admin/amenities') }}"><i class="fa fa-circle-o"></i> List Amenities</a></li>
                    <li><a href="{{ url('admin/amenities/create') }}"><i class="fa fa-circle-o"></i> Add New Amenity</a></li>
                </ul>
            </li>
            <li class="treeview">
                 <a href="#">
                    <i class="fa fa-user"></i> <span>Owner Account</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{ url('admin/owners') }}"><i class="fa fa-circle-o"></i> List Owner Accounts</a></li>
                    <li><a href="{{ url('admin/owners/create') }}"><i class="fa fa-circle-o"></i> Add Owner Account</a></li>
                </ul>
            </li>
            
           <li class="treeview">
                 <a href="#">
                    <i class="fa fa-calendar-check-o"></i> <span>All Bookings</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                   <li><a href="{{ url('admin/booking') }}"><i class="fa fa-circle-o"></i> New house booking</a></li>
                  
                </ul>
            </li>
            <li class="treeview">
                 <a href="#">
                    <i class="fa fa-cogs"></i> <span>Booking Engine</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{ url('admin/settings') }}"><i class="fa fa-circle-o"></i> Settings</a></li>
                    <li><a href="{{ url('admin/bookingrules') }}"><i class="fa fa-circle-o"></i> Bookings Rules</a></li>
                    <li><a href="{{ url('admin/taxes') }}"><i class="fa fa-circle-o"></i> Taxes Fees</a></li>
                    <li><a href="{{ url('admin/specials') }}"><i class="fa fa-circle-o"></i> Specials</a></li>

                </ul>
            </li>
            <li class="treeview">
                 <a href="#">
                    <i class="fa fa-globe"></i> <span>Website</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{ url('admin/attractions') }}"><i class="fa fa-circle-o"></i> Attractions</a></li>
                    <li><a href="{{ url('admin/propertyfinder') }}"><i class="fa fa-circle-o"></i> Property Finders</a></li>
                    <li><a href="{{ url('admin/seo') }}"><i class="fa fa-circle-o"></i> SEO attributes</a></li>
                    <li><a href="{{ url('admin/reviews') }}"><i class="fa fa-circle-o"></i> Reviews</a></li>

                </ul>
            </li>
            @endif
            @if(Auth::user()->role_id==3)
              <li class="treeview">
                 <a href="#">
                    <i class="fa fa-calendar-check-o"></i> <span>Owner Account</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="{{ url('admin/owners/calendar') }}"><i class="fa fa-circle-o"></i> Property Calendar </a></li>
                    <li><a href="{{ url('admin/ownerbooking/create') }}"><i class="fa fa-circle-o"></i> New owner booking</a></li>

                </ul>
              </li>
            @endif
        </ul>
    </section>
</aside>