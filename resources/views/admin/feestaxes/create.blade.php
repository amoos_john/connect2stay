@extends('admin/admin_template')

@section('content')
<style>
.list_property {
    height: 300px;
    overflow-y: scroll;
}
.textarea{
    resize: none;
    height: 80px !important;
}

</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
      @if (Session::has('success'))
	<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        <div class="box-header with-border">
          <h3 class="box-title">Add New Taxes Fees</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          {!! Form::open(array( 'class' => 'form','url' => 'admin/taxes/insert', 'files' => true)) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ url('admin/taxes') }}" class="btn btn-danger">Cancel</a>
            <a onclick="save_first();" class="btn btn-default pull-right" title="Apply to Multiple Properties">Apply to Multiple Properties</a>

            </div>
           <?php $page='admin.feestaxes.form'?>
            @include($page)
    
          {!! Form::close() !!}

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
<script>
    initSample();
    
function save_first()
{
    alert('You must save first before selecting this action.');
    return false;
}

 $('.datepicker').datepicker({
      autoclose: true
    });
</script>
@endsection