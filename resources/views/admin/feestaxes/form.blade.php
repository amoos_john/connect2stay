<?php
$required="required";
?>
@include('admin/commons/errors')
<div class="form-group col-md-8">
    <h3 class="text-light-blue">General</h3>

    {!! Form::label('Name:*') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-md-8">

    {!! Form::label('Public Name:*') !!}
    {!! Form::text('public_name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="clearfix"></div>
<div class="form-group col-md-4">
    {!! Form::label('Type:') !!}
    {!! Form::select('', $types, $type, array('class' => 'form-control','disabled' => 'true') ) !!}

    {!! Form::hidden('type', $type) !!} 
</div>
<div class="form-group col-md-4">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="form-group col-md-8">
    {!! Form::label('Applicability:') !!}
    {!! Form::select('applicability', $applicability,null , array('class' => 'form-control',$required) ) !!}
   
</div>


<div class="col-md-12">
   
 <h3 class="text-light-blue">Values</h3>
  
</div>

<div class="form-group col-md-5">   
   
      {!! Form::label('Amount:') !!}
    
     <div class="row">
         <div class="col-md-6"> 
            {!! Form::number('amount', null , array('class' => 'form-control','min' => '1',$required) ) !!}

         </div>
         <div class="col-md-6"> 
           {!! Form::select('amount_type', $amount_type,null , array('class' => 'form-control') ) !!}

         </div>
     </div>
</div>
@if($type==1)
<div class="form-group col-md-8">
 
    {!! Form::label('Default Qty:') !!}
    {!! Form::number('quantity', 1 , array('class' => 'form-control') ) !!}
   
</div>
@endif
@if($type!=3)
<div class="form-group col-md-8">
 
    {!! Form::label('Effective:') !!}
    {!! Form::text('effective', null , array('class' => 'datepicker form-control') ) !!}
   
</div>
<div class="clearfix"></div>
<div class="form-group col-md-4">
 
    {!! Form::label('Start:') !!}
    {!! Form::text('start', $start_date , array('class' => 'datepicker form-control') ) !!}
   
</div>
<div class="form-group col-md-4">
    {!! Form::label('Stop:') !!}
    {!! Form::text('end', $end_date , array('class' => 'datepicker form-control') ) !!}
   
</div>
@endif
<div class="col-md-12">
 @if($type==2)  
 <h3 class="text-light-blue">Number of Persons</h3>
 @else
 <h3 class="text-light-blue">Number of Nights</h3>
 @endif
  
</div>
 <div class="form-group  col-md-4">
    {!! Form::label('Min:') !!}
    {!! Form::number('min', null , array('class' => 'form-control','min' => '1') ) !!}
</div>
 <div class="form-group  col-md-4">
    {!! Form::label('Max:') !!}
    {!! Form::number('max', null , array('class' => 'form-control','min' => '1') ) !!}
</div>

<div class="col-md-12">
   
 <h3 class="text-light-blue">Options</h3>
  
</div>
@if($type!=3)
<div class="form-group col-md-8">
    {!! Form::label('Input Type:') !!}
    {!! Form::select('input_type', $input_type,null , array('class' => 'form-control',$required) ) !!}
   
</div>
@endif
<div class="form-group col-md-8">
    {!! Form::label('Calculation:') !!}
    <div class="clearfix"></div>
    <?php
   $nig='';
   $per='';
   $adu='';
   $chi='';
   if(isset($per_person))
   {
        
       foreach($per_person as $person)
        {
           if($person==1)
            {
               $nig='checked';
            }
            elseif($person==2)
            {
               $per='checked';
            }
            elseif($person==3)
            {
               $adu='checked';
            }
            elseif($person==4)
            {
               $chi='checked';
            }
                            
       }
   }
   $tax1='';
   $tax2='';
   $tax3='';
   $tax4='';
   if(isset($per_taxable))
   {
        
       foreach($per_taxable as $tax)
        {
           if($tax==1)
            {
               $tax1='checked';
            }
            elseif($tax==2)
            {
               $tax2='checked';
            }
            elseif($tax==3)
            {
               $tax3='checked';
            }
            elseif($tax==4)
            {
               $tax4='checked';
            }
                            
       }
   }
   ?> 
      <div class="form-group">
        <div class="col-md-12">  
        <label><input type="checkbox" value="1" name="per_person[]" <?php echo $nig; ?>/> Per Night</label>
        </div>
         <div class="col-md-3">
            <label><input type="checkbox" value="2" name="per_person[]" <?php echo $nig; ?>/> Per Person </label>
         </div>   
                      
         <div class="col-md-3">
             <label><input type="checkbox" value="3"  name="per_person[]" <?php echo $adu; ?>/> Per Adult</label>
         </div>
                      
       <div class="col-md-3">
                <label><input type="checkbox" value="4" name="per_person[]" <?php echo $chi; ?>/> Per Child</label>
       </div>
       @if($type!=3)   
       <div class="col-md-12">  
        <label><input type="checkbox" value="1" name="per_taxable[]" <?php echo $tax1; ?>/> Taxable</label>
        </div>
        <div class="col-md-12">  
        <label><input type="checkbox" value="2" name="per_taxable[]" <?php echo $tax2; ?>/> Calculate on Statement Total</label>
        </div>
         <div class="col-md-12">  
        <label><input type="checkbox" value="3" name="per_taxable[]" <?php echo $tax3; ?>/> Is Group Fee?</label>
        </div>
         <div class="col-md-12">  
        <label><input type="checkbox" value="4" name="per_taxable[]" <?php echo $tax4; ?>/> Lock fee in on date of check in?</label>
        </div>
       @else
        <div class="col-md-12">  
        <label><input type="checkbox" value="1" name="per_taxable[]" <?php echo $tax1; ?>/> Include Fees When Calculating this Tax</label>
        </div>
        <div class="col-md-12">  
        <label><input type="checkbox" value="2" name="per_taxable[]" <?php echo $tax2; ?> /> Include Services When Calculating this Tax</label>
        </div>
       @endif
          
    </div>
</div>
<div class="form-group col-md-8">
    {!! Form::label('Owner Statements:') !!}
    <div class="clearfix"></div>

      <div class="form-group">
     @if($type!=3)  
     <?php
   $own1='';
   $own2='';
   $own3='';
 
   if(isset($owner_statements))
   {
        
       foreach($owner_statements as $owner)
        {
           if($owner==1)
            {
               $own1='checked';
            }
            elseif($owner==2)
            {
               $own2='checked';
            }
            elseif($owner==3)
            {
               $own3='checked';
            }
                        
       }
   }
   ?> 

         <div class="col-md-4">
            <label><input type="checkbox" value="1" name="owner_statements[]" <?php echo $own1; ?>/> Amount Due To Owner </label>
         </div>   
                      
         <div class="col-md-5">
             <label><input type="checkbox" value="2"  name="owner_statements[]" <?php echo $own2; ?>/> Net of Cost Due To Owner</label>
         </div>
                      
          <div class="col-md-3">
                <label><input type="checkbox" value="3" name="owner_statements[]" <?php echo $own3; ?>/> Treat as Rent</label>
          </div>
       @else
       <?php  $owner_statements=(isset($owner))?$owner:''; ?>
           <div class="col-md-12">
                <label><input type="checkbox" value="1" name="owner_statements" <?php echo $owner_statements;?>/> Include in Owner's Commissions (for VATs - does not show on booking statements)</label>
          </div>
       @endif
    </div>
</div>
<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Public Description</h3>
    {!! Form::textarea('description', null , array('class' => 'form-control','id'=>'editor') ) !!}
</div>  
<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Notes</h3>
   {!! Form::textarea('notes', null , array('class' => 'form-control textarea','row' => '20') ) !!}
</div>


