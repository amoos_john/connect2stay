<?php
$required="required";
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::label('title') !!}
    {!! Form::text('title', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group">
    {!! Form::label('description') !!}
    {!! Form::textarea('description', null, ['size' => '7x7','class' => 'form-control']) !!} 
</div>
<div class="form-group">
    {!! Form::label('image') !!}
    {!! Form::file('image', null,array('class'=>'form-control')) !!}
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="{{ url('admin/games') }}" class="btn btn-danger">Cancel</a>
</div>