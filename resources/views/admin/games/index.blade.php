@extends('admin/admin_template')
@section('content')
<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
    <!-- PRODUCT LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">( Total : {{ count($model) }} ) </h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-danger" href="{{url('admin/games/create')}}">Add New Game</a>
                </div>
            </div>
            @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success !</h4>
                {!! session('success') !!}
            </div>
            @endif
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>User</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach ($model as $row)

                        <?php
                        $color = ($i % 2 == 0 ? 'success' : 'info');
                        ?>
                        <tr>
                            <td><?php echo $row->id; ?></td>
                            <td><?php echo $row->title; ?></td>
                            <td><?php echo $row->creatred_at; ?></td>
                            <td><?php echo $row->description; ?></td>
                            <td>
                                <a class="btn btn-info" href="games/logs/<?php echo $row->id ?>">Logs</a>
                                <a class="btn btn-warning" href="games/edit/<?php echo $row->id ?>" >Edit</a>                       <a class="btn btn-danger" href="games/delete/<?php echo $row->id ?>">Delete</a></td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->	

@endsection