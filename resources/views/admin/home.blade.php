@extends('admin.admin_template')

@section('content')
 <section class="content-header">
         <h3 class="main-title">Welcome, {{ $name }}.</h3>
 </section>
<section class="content">
    
    <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $total_pro }}</h3>
              <p><small>Total Properties</small></p>
            </div>
            <div class="icon">
              <i class="fa fa-home"></i>
            </div>
            <a href="{{ url('admin/property') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $total_book }}</h3>

              <p><small>All Bookings</small></p>
            </div>
            <div class="icon">
              <i class="fa fa-calendar-check-o"></i>
            </div>
            <a href="{{ url('admin/booking') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ $total_owners }}</h3>

              <p><small>Total Owners</small></p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="{{ url('admin/owners') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ $total_users }}</h3>

              <p>Total Users</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="{{ url('admin/users') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      
        <!-- TABLE: LATEST Booking -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-8">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Booking</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Property</th>
                    <th>Status</th>
                    <th>Date</th>
                  </tr>
                  </thead>
                  <tbody>
                  @if(count($model)>0)   
                        @foreach ($model as $row)
                        <?php
                            $color = '';
                            $status = '';
                            
                            if (array_key_exists($row->status, $statuses)) {
                               $status = $statuses[$row->status];
                            }
                            
                        ?>
                        <tr>
                            <td><a href="<?php echo url('admin/booking/edit/'.$row->id); ?>">{{ $row->id }}</a></td>
                            <td>{{ $row->property["internal_name"] }}</td>
                            <td><span class="label label-success"> {{ $status }}</span></td>
                            <td>{{ date('d/m/Y',strtotime($row->created_at)) }}</td> 
                            
                        </tr>
                       
                        @endforeach  
                    @else
                    <tr>
                        <td colspan='4' class='text-center'>Result not found!</td>
                    </tr>
                    @endif    
                 
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="{{ url('admin/booking') }}" class="btn btn-sm btn-default btn-flat pull-right">View All Bookings</a>
            </div>
            <!-- /.box-footer -->
          </div>
            
          </div>
        <div class="col-md-4">
           <!-- PRODUCT LIST -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Properties</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                 @if(count($properties)>0)   
                 @foreach($properties as $row)  
                   <li class="item">
                      <div class="product-img">
                        <img src="{{ $row->url }}" alt="{{ $row->image_caption }}">
                      </div>
                      <div class="product-info">
                        <a href="{{ url("admin/property/edit/".$row->id) }}" class="product-title">{{ $row->internal_name }}
                          </a>
                            <span class="product-description">
                             {{ $row->public_headline }}
                            </span>
                      </div>
                    </li>    
                  
                 @endforeach   
                 @endif
               
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="{{ url('admin/property') }}" class="uppercase">View All Properties</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->  
         </div>  
        <!-- ./col -->
      </div>
      <!-- /.row -->

        
</section>      
<script>
$(document).ready(function(){
    localStorage.removeItem('selectedTab');  
});
</script>
@endsection