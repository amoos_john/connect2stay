    @extends('admin/admin_template')

    @section('content')
   
<link rel="stylesheet" href="{{ asset('plugins/timepicker/jquery.timepicker.min.css') }}">
<script src="{{ asset('plugins/timepicker/jquery.timepicker.min.js') }}"></script>
    <link href='{{ asset('plugins/calendar/fullcalendar.css') }}' rel='stylesheet' />
    <link href='{{ asset('plugins/calendar/fullcalendar.print.css') }}' rel='stylesheet' media='print' />
    <script src='{{ asset('plugins/calendar/lib/moment.min.js') }}'></script>
    <script src='{{ asset('plugins/calendar/fullcalendar.min.js') }}'></script>
 
    <style>
        .form-group.col-sm-6 {
        padding-left: 0;
    }
    .datepicker {
         border-radius: 0;
     }
    .btn-default {

        margin-right: 5px;
    }
     .list_property {
        height: 300px;
        overflow-y: scroll;
    }

    td.fc-past {
        opacity: 0.3;
        background: #fff !important;
    }
    .fc-day-number.fc-other-month {
        opacity: 0.3 !important;
    }
         td.fc-other-month {
          opacity: 1 !important;
    }
    .margin-0
    {
            margin: 0 !important;
            padding-right: 10px;
            padding-left: 0;
    } 
    .ui-timepicker-standard
    {
        z-index: 1 !important;
        width: 140px !important;
    }
    .ui-timepicker-viewport {
            padding-right: 0px !important;
    }
    td.fc-past {
    opacity: .35 !important;
    filter: Alpha(Opacity=35);
    }
    .opacity {
    opacity: .35 !important;
    filter: Alpha(Opacity=35);
    }
    .mt20{
        margin-top:10px; 
    }
    </style>
    <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->

          <!-- /.box -->
          <!-- general form elements disabled -->
         <?php
          $required="required";
          ?>
          @include('admin/commons/errors')
          <div class="box box-warning">
          @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success !</h4>
                {!! session('success') !!}
            </div>
                    @endif
            <div class="box-header with-border">
              <h3 class="box-title">Property Calendar</h3>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-header -->
        {!! Form::open(array( 'class' => 'form','url' => 'admin/owners/calendarclose', 'files' => true)) !!}
            <div class="form-group mt20">
                <button type="button" data-target="#calendarModal" data-toggle="modal" class="btn btn-primary">Save</button>
                <a href="{{ url('admin') }}" class="btn btn-danger">Cancel</a>
           </div>
            <div id="calendarModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                        <h4 id="modalTitle" class="modal-title">Close Date</h4>
                    </div>
                    <div id="modalBody" class="modal-body">
                         <p>Are you sure to close this dates?</p>   
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >Yes</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
            </div>
            <div class="box-body">
             <div class="col-md-7">
                 <h3 class="text-light-blue">Property Information</h3>
           
             <div class="form-group">
                   {!! Form::label('Property*') !!}
                    
                   <select name="property_id" id="property_id" class="form-control" required>
                       <option value="">Select Property</option>
                       @foreach($properties as $row)
                       <option value="{{ $row->id }}">{{ $row->internal_name }}</option>
                       @endforeach
                   </select>
             </div>
             
            <div class="col-md-6 margin-0">
             <div class="form-group">
                 <div class="col-xs-6 margin-0">
                     <label>Check-in*</label>
                   {!! Form::text('checkin_date', null , array('class' => 'form-control datepicker','id' => 'checkin_date', $required) ) !!}
                  </div>
                   <div class="col-xs-6 margin-0">
                       <label>&nbsp;</label>
                   {!! Form::text('checkin_time', null , array('class' => 'form-control timepicker','id' => 'checkin_time') ) !!}
                   </div>
            </div> 
            </div> 
           <div class="col-md-6 margin-0">
             <div class="form-group">
                 <div class="col-xs-6 margin-0">
                     <label>Check-out*</label>
                   {!! Form::text('checkout_date', null , array('class' => 'datepicker form-control','id' => 'checkout_date', $required) ) !!}
                  </div>
                   <div class="col-xs-6 margin-0">
                       <label>&nbsp;</label>
                   {!! Form::text('checkout_time', null , array('class' => 'form-control timepicker','id' => 'checkout_time') ) !!}
                   </div>
            </div> 
            </div>      
                  
            </div>
            <div class="clearfix"></div>
               <div class="calendar"></div>
                
            </div>
            <!-- /.box-body -->

          </div>
          {!! Form::close() !!}
          <!-- /.box -->
        </div>
    </div>
    <script>
    function getCal()
    {
      var datasend = $("form").serialize();
      
        $.ajax({
            type: "GET",
            url: "<?php echo url('admin/owners/getcalendar'); ?>",
            data: datasend,
            cache:false,
            success: function(success){
               $(".calendar").html(success);
            }
        });
    }    
   $(document).on("change","#property_id",function(){
     var id=$(this).val();
     
     if(id!='')
     {
         getCal();
     }
     else
     {
          $(".calendar").html('');
     }
     
    });    
    
    (function($) {
        $(function() {
            $('input.timepicker').timepicker();
        });
    })(jQuery); 
    
    $('.datepicker').datepicker({
      autoclose: true
    });
    </script>    
@endsection