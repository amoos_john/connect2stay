@extends('admin/admin_template')

@section('content')
<?php
use App\Units;
?>

<style>
    .list_property {
    height: 300px;
    overflow-y: scroll;
}
.textarea{
    resize: none;
    height: 100px !important;
}
 .pac-container {
        background-color: #FFF;
        z-index: 20;
        position: fixed;
        display: inline-block;
        float: left;
    }
    .modal{
        z-index: 20;   
    }
    .modal-backdrop{
        z-index: 10;        
    }
</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        <div class="box-header with-border">
          <h3 class="box-title">Add New Owner Account</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          {!! Form::open(array( 'class' => 'form','url' => 'admin/owners/insert', 'files' => true)) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ url('admin/owners') }}" class="btn btn-danger">Cancel</a>
            </div>
      
            <?php $page='admin.owners.form'?>
            @include($page)
          
        
      
          {!! Form::close() !!}
   
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
<script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        administrative_area_level_2: 'long_name',
        administrative_area_level_3: 'long_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
        
        
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

		document.getElementById("latitude").value = place.geometry.location.lat();
		document.getElementById("longitude").value = place.geometry.location.lng();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        var street_number='',route='';
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          
           if (addressType == 'street_number')
           street_number = place.address_components[i].long_name;
           else if (addressType  == 'route')
           route = place.address_components[i].long_name;
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
            document.getElementById("address1").value = street_number + ' ' + route ;
            setLocationStatusIndicator('9');
          }
          
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            
             var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            document.getElementById("latitude").value = latitude;
            document.getElementById("longitude").value = longitude;

            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
      
	
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvDVcbivo-q_sIh891hIpXJoLFyHZs3mE&libraries=places&callback=initAutocomplete"
        async defer></script>
    <script>
    function showPinPoint() {
        //var url = '/compositecontrols/misc/mappinpointpopup.aspx?lon=' + $('#content_tabActions_tabGeneral_ucLocation_txtLongitude').val().replace(',', '.') + '&lat=' + $('#content_tabActions_tabGeneral_ucLocation_txtLatitude').val().replace(',', '.');
        var lon=$('#longitude').val();
        var lat=$('#latitude').val();
        var url = '<?php echo url('admin/property/mappinpointpopup'); ?>?lon='+lon+'&lat='+lat;
        
        var ret;
        if (window.showModalDialog) {
            ret = window.showModalDialog(url, window, 'dialogWidth:420px;dialogHeight:480px');
            if (ret != null) {
                sloc = ret.split("|");
                $('#content_tabActions_tabGeneral_ucLocation_txtLongitude').val(sloc[0]);
                $('#content_tabActions_tabGeneral_ucLocation_txtLatitude').val(sloc[1]);
                    setLocationStatusIndicator('117'); // status is now pinpointed
                }
            }
            else {
                ret = window.open(url, 'Pin Point', 'height=480,width=420,scrollbars=no,resizable=no,toolbars=no,status=no,menubar=no,location=no');
                ret.onload = function () {
                    ret.onMapPinPointOK(function (point) {
                        $('#content_tabActions_tabGeneral_ucLocation_txtLongitude').val(point.lng().toString().replace(',', '.'));
                    $('#content_tabActions_tabGeneral_ucLocation_txtLatitude').val(point.lat().toString().replace(',', '.'));
                    setLocationStatusIndicator('117');
                });
            }
        }
    }
        function showAddressDetails(a, c) {
        if ($(a).text() == "Less") $(a).text("More");
        else $(a).text("Less");
        $('#showAddress').toggle();
    }
    
    function setLocationStatusIndicator(s) {
        if (s == null || s == '')
        {
            s = '111';
        }
          
        $('#imgVerfied').hide();
        $('#imgNotYetVerified').hide();
      

         if (s == '111') // not yet verified
            $('#imgNotYetVerified').show();
        else // if (s == '9') // verified
            $('#imgVerfied').show();
    }
$("#enable_login").change(function () {
    
   $("#email").prop("disabled", !$(this).is(':checked'));
   $("#pass").prop("disabled", !$(this).is(':checked'));
   $("#con_pass").prop("disabled", !$(this).is(':checked'));
   
});
</script>
@endsection