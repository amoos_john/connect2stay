<?php
$required="required";
?>
@include('admin/commons/errors')
<div class="form-group col-sm-8">
    {!! Form::label('Name:*') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-sm-8">
    {!! Form::label('Email:*') !!}
    {!! Form::email('email', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-md-8">
    {!! Form::label('Telephone:') !!}  
    {!! Form::text('telephone', null , array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-md-8">
    {!! Form::label('Language:') !!} 
    {!! Form::select('language',$langauges,(isset($model))?$model->language:'2057', array('class' => 'form-control') ) !!}
</div>
<div class="form-group col-sm-8">
    {!! Form::label('Password*') !!}
    {!! Form::password('password' , array('class' => 'form-control','id' => 'pass') ) !!}
</div>
<div class="form-group col-sm-8">
    {!! Form::label('Confirm Password*') !!}
    {!! Form::password('password_confirmation' , array('class' => 'form-control','id' => 'con_pass') ) !!}
</div>
<div class="form-group col-sm-8">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<!--<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Notes</h3>
   {!! Form::textarea('notes', null , array('class' => 'form-control textarea','row' => '20') ) !!}
</div>-->


