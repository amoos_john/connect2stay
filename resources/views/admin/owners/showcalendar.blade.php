    <script>

            $(document).ready(function() {

                    $('#calendar').fullCalendar({
                           
                            header: {
                                    left: 'prev,next today',
                                    center: 'title',
                                    right: 'year,month'
                            },
                            defaultView: 'year',
                            yearColumns: 3,
                            selectable: true,
                            selectHelper: true,
                            select: function(start, end,cell) {
                                    var selectionStart = moment(start).format('YYYY-MM-DD'); 
                                    var today = moment().format('YYYY-MM-DD'); // passing moment nothing defaults to today

                                    if (selectionStart < today) { 
                                        $(this).attr("tooltip","Unavailable");
                                        $('#calendar').fullCalendar('unselect');
                                    }
                                    else
                                    {
                                        //var dataToFind = moment(start).format('YYYY-MM-DD');
                                        //closedate(selectionStart);    
                                    }
                            },
                            dayRender: function(date, cell) {
                                   var selectionStart = moment(date).format('YYYY-MM-DD'); 
                                    var today = moment().format('YYYY-MM-DD'); // passing moment nothing defaults to today

                                  if (selectionStart < today) { 
                                      $(cell).attr("title","Unavailable");
                                      $(cell).attr("data-toggle","tooltip");
    
                                    }  
                                  else
                                  {
                                      //$(cell).attr("title","Available");
                                      //$(cell).attr("data-toggle","tooltip");
                                      var dataToFind = moment(date).format('YYYY-MM-DD');
                                      $("td[data-date='"+dataToFind+"']").attr("title","Available");
                                      $("td[data-date='"+dataToFind+"']").attr("data-toggle","tooltip");
         
                                  }
                                 
                            },
                            
                            eventLimit: true, // allow "more" link when too many events
                             eventRender: function(event, element, view) {
                                $(element).attr("title","Unavailable");
                                $(element).attr("data-toggle","tooltip");
                                var dataToFind = moment(event.start).format('YYYY-MM-DD');
                                $("td[data-date='"+dataToFind+"']").addClass('opacity');
                                $("td[data-date='"+dataToFind+"']").attr("title","Unavailable");
        
                                $(view).fullCalendar('unselect');
                                //console.log(parentc);
                                //$(parentc).addClass('opacity');             '  
                             
                            },
                            eventClick: function(calEvent, jsEvent, view) {
                                
                                $(calEvent).fullCalendar('unselect');
                                
                            },

                            events: [

                                @foreach($avails as $avail)
                    
                                {
                                    title : 'Unavailable',
                                    start : '{{ $avail->date }}',
                                    end : '{{ $avail->date }}',
                                },
                                
                                @endforeach
                                
                            ], 
                            visibleRange: {
                                start: '<?php echo $checkin_date; ?>',
                                end: '<?php echo $checkout_date; ?>'
                            }

                    });

            });

        function closedate(date)
        {
            $('#close_date').val(date);
            $('#calendarModal').modal();
        }
    </script>
<div id="calendar"></div>
<!--<div id="calendarModal" class="modal fade">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
            <h4 id="modalTitle" class="modal-title">Close Date</h4>
        </div>
        <div id="modalBody" class="modal-body">
             <p>Are you sure to Close this date?</p>
             <input type="hidden" value="" id="close_date" name="date"/>
             <input type="hidden" value="" id="avail" name="avail"/>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        </div>
    </div>
</div>
</div>-->