    @extends('admin/admin_template')

    @section('content')
    <?php
    use App\Functions\Functions;
    $currency= Config::get('params.currency');
    ?>
    <link href='{{ asset('plugins/calendar/fullcalendar.css') }}' rel='stylesheet' />
    <link href='{{ asset('plugins/calendar/fullcalendar.print.css') }}' rel='stylesheet' media='print' />
    <script src='{{ asset('plugins/calendar/lib/moment.min.js') }}'></script>
    <script src='{{ asset('plugins/calendar/fullcalendar.min.js') }}'></script>
    <script>

          $(document).ready(function() {

                    $('#calendar').fullCalendar({
                            header: {
                                    left: 'prev,next today',
                                    center: 'title',
                                    right: 'year,month'
                            },
                            defaultView: 'year',
                            yearColumns: 3,
                            selectable: true,
                            selectHelper: true,
                            select: function(start, end) {
                                    var selectionStart = moment(start).format('YYYY-MM-DD'); 
                                    var today = moment().format('YYYY-MM-DD'); // passing moment nothing defaults to today

                                    if (selectionStart < today) { 
                                        $(this).attr("tooltip","Unavailable");
                                        $('#calendar').fullCalendar('unselect');
                                    }
                                    else
                                    {
                                       
                                        
                                    }
                            },
                            dayRender: function(date, cell) {
                                   var selectionStart = moment(date).format('YYYY-MM-DD'); 
                                    var today = moment().format('YYYY-MM-DD'); // passing moment nothing defaults to today

                                  if (selectionStart < today) { 
                                      $(cell).attr("title","Unavailable");
                                      $(cell).attr("data-toggle","tooltip");
    
                                    }  
                                  else
                                  {
                                      //$(cell).attr("title","Available");
                                      //$(cell).attr("data-toggle","tooltip");
                                      var dataToFind = moment(date).format('YYYY-MM-DD');
                                      $("td[data-date='"+dataToFind+"']").attr("title","Available");
                                      $("td[data-date='"+dataToFind+"']").attr("data-toggle","tooltip");
         
                                  }
                                 
                            },
                            
                            eventLimit: true, // allow "more" link when too many events
                             eventRender: function(event, element, view) {
                                $(element).attr("title","Unavailable");
                                $(element).attr("data-toggle","tooltip");
                                var dataToFind = moment(event.start).format('YYYY-MM-DD');
                                $("td[data-date='"+dataToFind+"']").addClass('opacity');
                                $("td[data-date='"+dataToFind+"']").attr("title","Unavailable");
        
                                $(view).fullCalendar('unselect');
                                //console.log(parentc);
                                //$(parentc).addClass('opacity');             '  
                             
                            },
                            eventClick: function(calEvent, jsEvent, view) {
                                
                                $(calEvent).fullCalendar('unselect');
                                
                            },

                            events: [

                                @foreach($avails as $avail)
                    
                                {
                                    title : 'Unavailable',
                                    start : '{{ $avail->date }}',
                                    end : '{{ $avail->date }}',
                                },
                                
                                @endforeach
                                
                            ]

                    });

            });

    </script>
    <style>
   .form-group.col-sm-6 {
        padding-left: 0;
    }
    .btn-default {

        margin-right: 5px;
    }
     .list_property {
        height: 300px;
        overflow-y: scroll;
    }
    td.fc-past {
        opacity: 0.3;
        background: #fff !important;
    }
    .fc-day-number.fc-other-month {
        opacity: 0.3 !important;
    }
    td.fc-other-month {
          opacity: 1 !important;
    }
    .margin-0
    {
            margin: 0 !important;
            padding-right: 10px;
            padding-left: 0;
    } 
    .ui-timepicker-standard
    {
        z-index: 1 !important;
        width: 140px !important;
    }
    .ui-timepicker-viewport {
            padding-right: 0px !important;
    }
    td.fc-past {
    opacity: .35 !important;
    filter: Alpha(Opacity=35);
    }
    </style>
    <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->

          <!-- /.box -->
          <!-- general form elements disabled -->
         <?php
          $required="required";
          ?>
          @include('admin/commons/errors')
          <div class="box box-warning">
          @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success !</h4>
                {!! session('success') !!}
            </div>
                    @endif
            <div class="box-header with-border">
              <h3 class="box-title">Availability Calendar</h3>
              <div class="clearfix"></div>
              <h4>{{ $property->categories->category_name." ".$property->public_headline }}</h4>
            </div>
            <!-- /.box-header -->

            <div class="box-body">

                <div id="calendar"></div>

            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>
    </div>
    <script>
    function getDates(token,min,max,id)
    {
        var datasend = 'id='+id+'&min='+min+'&max='+max+'&_token='+token;

            $.ajax({
                type: "POST",
                url: "<?php echo url('admin/property/ratesandavailability/search'); ?>",
                data: datasend,
                cache:false,
                success: function(success){
                   $("#rates").html(success);
                }
            });
    }

     $("body").on("click","#apply",function(){

          var token='<?php echo csrf_token(); ?>';
          var min= $('#datepicker').val();
          var max= $('#datepicker2').val();
          var id= <?php echo $id;?>;
          if(min!='' && max!='')
          {
              getDates(token,min,max,id);
          }
          else
          {
             $("#message").html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Start or end date is required</p></div>');

          }
    }); 
    $("#submit").click(function () {
        $('input:checkbox[name^=avail]').each(function () {
            $(this).prop("checked") ? $(this).attr('value','1') : $(this).html("<input type='hidden' name='avail[]' value='0' />");
        });
    });

    $("#bulk").click(function () {

        var daily=$("#daily").val();
        var weekly=$("#weekly").val();


          var token='<?php echo csrf_token(); ?>';
          var min= $('#datepicker3').val();
          var max= $('#datepicker4').val();
          var id= <?php echo $id;?>;


          if(min!='' && max!='')
          {
             var datasend = 'id='+id+'&min='+min+'&max='+max+'&_token='+token;

            $.ajax({
                type: "POST",
                url: "<?php echo url('admin/property/ratesandavailability/search'); ?>",
                data: datasend,
                cache:false,
                success: function(success){
                   $("#rates").html(success);
                    $("#datepicker").val(min);
                    $("#datepicker2").val(max);
                    $(".daily").val(daily);
                    $(".weekly").val(weekly);
                }
            });


          }
          else
          {
             $("#message").html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Start or end date is required</p></div>');

          }

    });

    </script>
 
    @endsection