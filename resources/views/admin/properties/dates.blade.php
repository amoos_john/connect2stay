<?php
use App\Functions\Functions;
?>
          <div class="col-md-10">   

            <table class="table">
              
              
              <?php
           $i=0;
            while (strtotime($date) <= strtotime($end_date)) 
            {
                $currdate=date("d",strtotime($date));
                $mon=date("m",strtotime($date));
                
                
                $month=date("F",strtotime($date));
                $shormonth=date("M",strtotime($date));
                $year=date("Y",strtotime($date));
                $day=date("D", strtotime($date));
       
                $daily='';
                $weekly='';
                $avail=1;
                
                if($currmonth==$mon)
                {
            ?>
              <tr>
                  <td colspan="5">
                      <h3 class="text-light-blue">{{ $month." ".$year }}</h3>
                  </td>
              </tr>
   
              <tr>
                  <th>Date</th>
                  <th>Daily</th>
                  <th>Weekly</th>
                  <th>Availability <a id="allcheck<?php echo $mon.$year;?>">All</a> | <a  id="alluncheck<?php echo $mon.$year;?>">None</a></th>
              </tr>
              <?php
                }
              ?>
              @foreach($rates as $rate)
                @if($rate->date==$date)
                <?php
                $daily=($rate->daily!=0)?$rate->daily:'';
                $weekly=($rate->weekly!=0)?$rate->weekly:'';
                $avail=$rate->availability;
                ?>
                @endif
              @endforeach
               <tr>
                  <td>{{ $shormonth }} {{ $currdate }}</td>
                  <td>{!! Form::number('daily[]', $daily , array('class' => 'form-control daily'.$day.$date,'min' => '1', 'step'=>'0.01') ) !!}</td>
                  <td>{!! Form::number('weekly[]', $weekly , array('class' => 'form-control weekly'.$day.$date,'min' => '1', 'step'=>'0.01') ) !!}</td>
                  <td><input type="checkbox" value="1" name="avail[]" class="avail_chk<?php echo $mon.$year;?>" <?php echo ($avail==1)?'checked':''; ?> <?php echo ($avail==2)?'disabled':''; ?> /></td>

               </tr>
                <?php
                $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
                $i++;
              if($currmonth==$mon)
              {  
             ?>   
<script>
 $("#allcheck<?php echo $mon.$year;?>").click(function(){

      $('.avail_chk<?php echo $mon.$year;?>').prop('checked', true);
   
 });
  $("#alluncheck<?php echo $mon.$year;?>").click(function(){
  
 $('.avail_chk<?php echo $mon.$year;?>').prop('checked', false);
     
 });
 </script>
                
        <?php    
         $currmonth=date("m",strtotime("+1 month",strtotime($date)));
              }
           }
           
?>
</table>
          
</div>
       
        