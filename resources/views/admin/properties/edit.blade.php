@extends('admin/admin_template')

@section('content')
<?php
use App\Amenities;
$currency= Config::get('params.currency');
?>
<style>
.form-group.col-sm-6 {
padding-left: 0;
}
.nav-tabs-custom>.tab-content {
padding: 0;
}
.margin-0
{
margin:0 !important;
}
.textarea{
width: 60%;
resize: none;
}
.text-light-blue{
background: #2c3b41;
padding: 8px;
margin-top: 20px;
text-align: center;
border-radius: 3px; 
}
.text-light-blue a,.text-light-blue i{
color:#8aa4af;
}
.image-box{
padding-top: 20px;
text-align: center;
}
#gallery{
background: #fff;
}
.image-style
{margin-top: -22px;  border: 1px solid #ddd;
border-radius: 4px;
padding: 8px;
height: 212px;
}
.image-style img{
width: 100%;
max-height: 100%;
border-radius: 4px;
}
.box-body .form-control{border-radius: 4px;}
.panel{
border-radius: 0px;
margin-bottom: 0px;
border-bottom: 1px dashed #ddd;
}
.view-btn{

background: #ddd;
height: 40px;
line-height: 40px;
width: 100px;
text-align: center;
margin: 0 auto;
display: block;
z-index: 8;
position: relative;
border-radius: 4px;
transition-timing-function: ease-in;

/* Quick on the way out */
transition: 0.2s;
}
.view-btn a{
color:#000;
display: block;
transition-timing-function: ease-in;

/* Quick on the way out */
transition: 0.2s;
}
input[type=checkbox], input[type=radio]{
width: 17px;
height: 17px;
}
.view-btn:hover{background: #26ABE2;}
.view-btn a:hover{color: #fff;}
.label-style{
/*padding-right: 0px;*/
text-align: center;
color:#555;

}
label{
color:#26ABE2;
}
.label-style1{
padding-right: 0px;

}
.label-style label{
color:#26ABE2;
font-size: 16px !important;
display: block;
margin-top: 14px;
}
.col-sm-7.padding-0 {
padding: 0;
}
.padding-0 {
padding: 0;
}
.col-xs-6.padding0 {
padding-left: 0;
}
</style>
<div class="row">
<div class="col-md-12">
<!-- Horizontal Form -->

<!-- /.box -->
<!-- general form elements disabled -->
<?php
$required="required";
?>
@include('admin/commons/errors')
<div class="box box-warning">
@if (Session::has('success'))
        <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Success !</h4>
    {!! session('success') !!}
</div>
        @endif
<div class="box-header with-border">
  <h3 class="box-title">Edit Property</h3>
</div>
<!-- /.box-header -->
<div class="box-body">
    {!! Form::model($model, ['files' => true,'class' => 'form','id' => 'myform','url' => ['admin/property/update', $model->id], 'method' => 'post']) !!}
   <div class="form-group">
        <button type="submit" class="btn btn-primary">Update</button>
        <a href="{{ url('admin/property') }}" class="btn btn-danger">Cancel</a>
        <a href="{{ url('admin/property/ratesandavailability/'.$id) }}" class="btn btn-default pull-right" title="Rates & Availability">Setup Rates & Availability</a>

   </div>

    <!-- Custom Tabs -->
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
      <li><a href="#tab_2" data-toggle="tab">Photos</a></li>
      <li><a href="#tab_3" data-toggle="tab">Amenities</a></li>
      <li><a href="#tab_4" data-toggle="tab">Units</a></li>
      <li><a href="#tab_5" data-toggle="tab">Policies</a></li>
      <li><a href="#tab_6" data-toggle="tab">Advanced</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
          <div class="col-sm-7 padding-0">
              <h3 class="text-light-blue">Description</h3>

          <div class="form-group">
           {!! Form::label('Internal Name:*') !!}
           {!! Form::text('internal_name', null , array('class' => 'form-control',$required) ) !!}
          </div>
          <div class="form-group">
           {!! Form::label('Public Headline:*') !!}
           {!! Form::text('public_headline', null , array('class' => 'form-control',$required) ) !!}
          </div>
          <div class="form-group">
           {!! Form::label('Summary:*') !!}
           {!! Form::textarea('summary', null , array('class' => 'form-control','id'=>'editor',$required) ) !!}
          </div>  
          <div class="form-group">
           {!! Form::label('Description:*') !!}
           {!! Form::textarea('description', null , array('class' => 'form-control','id'=>'editor2',$required) ) !!}
          </div>      

          <div class="form-group col-sm-6">
            {!! Form::label('Category:*') !!}
            {!! Form::select('category_id', $categories ,null, array('class' => 'form-control',$required) ) !!}

          </div>

          <div class="form-group col-sm-6">
            {!! Form::label('Status:*') !!}
            {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}

          </div>
          <div class="form-group">
            {!! Form::label('Development:') !!}
            {!! Form::select('development', array(''=>'Select'),null , array('class' => 'form-control') ) !!}

          </div>

          <div class="form-group">
            {!! Form::label('Address:*') !!}
            {!! Form::text('address', null , array('class' => 'form-control','id' => 'autocomplete','onFocus' => 'geolocate()',$required) ) !!}
            <img id="imgVerfied" src="{{ url('images/ico-status-green.png') }}" alt="Verified" title="Verified" style="display:none;"/>
            <img id="imgNotYetVerified" src="{{ url('images/ico-status-red.png') }}" alt="Not yet verified" title="Not yet verified" style="display:none;"/>
            <a href="javascript:void(0)" id="aMore" onclick="showAddressDetails(this);" title="Display address details">Less</a> | 
            <a href="javascript:void(0)" id="aPinPoint" onclick="showPinPoint();" title="Show map and manually set location">Pin Point</a>
          </div>

          <div id="showAddress"> 
          <div class="form-group col-sm-6">
            {!! Form::label('Address1*') !!}
           {!! Form::text('address_1', null , array('class' => 'form-control','id' => 'address1',$required) ) !!}

          </div>

          <div class="form-group col-sm-6">
            {!! Form::label('Address2') !!}
           {!! Form::text('address_2', null , array('class' => 'form-control') ) !!}

          </div>

           <div class="form-group col-sm-6">
            {!! Form::label('Neighborhood') !!}
           {!! Form::text('neighborhood', null , array('class' => 'form-control','id' => 'administrative_area_level_3') ) !!}

          </div>

          <div class="form-group col-sm-6">
            {!! Form::label('City*') !!}
           {!! Form::text('city', null , array('class' => 'form-control','id' => 'locality',$required) ) !!}

          </div>

           <div class="form-group col-sm-6">
            {!! Form::label('County') !!}
           {!! Form::text('county', null , array('class' => 'form-control','id' => 'administrative_area_level_2') ) !!}

          </div>

          <div class="form-group col-sm-6">
            {!! Form::label('Metro') !!}
           {!! Form::text('metro', null , array('class' => 'form-control') ) !!}

          </div>

           <div class="form-group col-sm-6">
            {!! Form::label('State') !!} 
           {!! Form::text('state', null , array('class' => 'form-control','id' => 'administrative_area_level_1') ) !!}

          </div>

          <div class="form-group col-sm-6">
            {!! Form::label('Postal Code') !!}
           {!! Form::text('postal_code', null , array('class' => 'form-control','id' => 'postal_code') ) !!}

          </div>
          <div class="form-group col-sm-6">
            {!! Form::label('Region') !!}
           {!! Form::text('region', null , array('class' => 'form-control') ) !!}

          </div>


          <div class="form-group col-sm-6">
            {!! Form::label('Country *') !!}
           {!! Form::text('country', null , array('class' => 'form-control','id' => 'country',$required) ) !!}

          </div>
          <div class="form-group col-sm-6">
            {!! Form::label('Longitude') !!}
            {!! Form::text('longitude', null , array('class' => 'form-control','id' => 'longitude') ) !!}

          </div>        
          <div class="form-group col-sm-6">
            {!! Form::label('Latitude') !!}
           {!! Form::text('latitude', null , array('class' => 'form-control','id' => 'latitude') ) !!}

          </div>     

          </div>    

          </div>
          <div class="col-sm-5">
              <h3 class="text-light-blue">Details</h3>

           <div class="form-group col-sm-6">
            {!! Form::label('Bedrooms') !!}
           {!! Form::text('bedrooms', null , array('class' => 'form-control') ) !!}

          </div>        
          <div class="form-group col-sm-6">
            {!! Form::label('Garage Spaces') !!}
           {!! Form::text('garage_spaces', null , array('class' => 'form-control') ) !!}

          </div>
           <div class="form-group col-sm-6">
            {!! Form::label('Bathrooms') !!}
           {!! Form::text('bathrooms', null , array('class' => 'form-control') ) !!}

          </div>
           <div class="form-group col-sm-6">
            <div class="col-xs-12">   
            {!! Form::label('Lot Size') !!}
            </div>
            <div class="col-xs-6 padding0">
            {!! Form::text('lot_size', null , array('class' => 'form-control') ) !!}
            </div>
            <div class="col-xs-6 padding0">
            {!! Form::select('lot_meter', array('0'=>'Sq Feet','1'=>'Sq Meters'),null , array('class' => 'form-control') ) !!}
            </div>
          </div>

           <div class="form-group col-sm-6">
            {!! Form::label('Sleeps') !!}
           {!! Form::text('sleeps', null , array('class' => 'form-control') ) !!}

          </div>
           <div class="form-group col-sm-6">
            {!! Form::label('Stories') !!}
           {!! Form::text('stories', null , array('class' => 'form-control') ) !!}

          </div>
          <div class="form-group col-sm-6">
            {!! Form::label('Parking Spaces:') !!}
           {!! Form::text('parking_spaces', null , array('class' => 'form-control') ) !!}

          </div>   
           <div class="form-group col-sm-6">
            {!! Form::label('Floor') !!}
           {!! Form::text('floor', null , array('class' => 'form-control') ) !!}

          </div>

         <div class="form-group col-sm-6">
            {!! Form::label('Year Built:') !!}
           {!! Form::number('year_built', null , array('class' => 'form-control','min' => '0') ) !!}

          </div>   
           <div class="form-group col-sm-6">
           <div class="col-xs-12">   
            {!! Form::label('Unit Size:') !!}
            </div>
            <div class="col-xs-6 padding0">
            {!! Form::text('unit_size', null , array('class' => 'form-control') ) !!}
            </div>
            <div class="col-xs-6 padding0">
            {!! Form::select('unit_meter', array('0'=>'Sq Feet','1'=>'Sq Meters'),null , array('class' => 'form-control') ) !!}
            </div>

          </div>   
           <div class="form-group col-sm-6">
            {!! Form::label('Min Stay:') !!}
           {!! Form::number('min_stay', null , array('class' => 'form-control','min' => '1') ) !!}

          </div>    
          <div class="col-sm-12">
          <h3  class="text-light-blue">Tags</h3>
          </div>
          <div class="form-group">
              <?php
              $lux='';
              $mid='';
              $bud='';
              foreach($tags as $tag)
              {
                  if($tag=="Luxury")
                  {
                       $lux='checked';
                  }
                  elseif($tag=="Mid-Level")
                  {
                      $mid='checked';
                  }
                  elseif($tag=="Budget")
                  {
                      $bud='checked';
                  }

              }
              ?>  
              <div class="col-xs-4">
              <input type="checkbox" value="Luxury" name="tags[]" <?php echo $lux; ?>/>
              <label>Luxury</label>
               </div>   

              <div class="col-xs-4">
              <input type="checkbox" value="Mid-Level"  name="tags[]" <?php echo $mid; ?>/>
              <label>Mid-Level</label>
               </div>

              <div class="col-xs-4">
              <input type="checkbox" value="Budget" name="tags[]" <?php echo $bud; ?>/>
              <label>Budget</label>
               </div>
          </div>     



          </div>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_2">
           <div class="col-sm-12">
               <h4 class="text-light-blue"><i class="fa fa-image"></i> <a  onclick="window.open('<?php echo url('admin/property/addimage')."/".$gallery_id; ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1000,height=600');">Add an Image</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <i class="fa fa-camera" aria-hidden="true"></i> <a onclick="window.open('<?php echo url('admin/property/addmultiple')."/".$gallery_id; ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1000,height=600');">Add Multiple Images</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <i class="fa fa-times" aria-hidden="true"></i> <a data-href="{{ url('admin/property/deleteallimages').'/'.$model->gallery_id }}" data-target="#confirm-delete"   data-toggle="modal">Delete All Images</a> 
               </h4>
               <div id="gallery">
                @include('admin/properties/gallery')
               </div>
           </div>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_3">
          @if(count($parents)>0)
           <?php $i=0; ?>
            @foreach($parents as $row)
            <div class="col-md-3">
            <h3 class="text-light-blue">{{ $row->name }}</h3>
            <?php
            $amentities=Amenities::where("parent_id","=",$row->id)->get();  
            ?>
            @if(count($amentities)>0)
                @foreach($amentities as $child)
                    <?php $checked=''; ?>
                    @foreach($model->property_amentity as $property_amentity)
                        <?php
                            if($property_amentity->amentity_id==$child->id)
                            {
                                $checked='checked';
                            }
                        ?>
                    @endforeach
                 <div class="form-group">
                    <label>
                        <input type="checkbox" value="{{ $child->id }}" name="amentity[]" class="flat-red" <?php echo $checked; ?>/>
                      {{ $child->name }}
                    </label>
                </div>

            @endforeach
            @endif
            </div>
            <?php $i++; ?>
             @if($i==4)
             <div class="clearfix"></div>
             @endif
            @endforeach
          @endif
      </div>
      <!-- /.tab-pane -->
       <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_4">
        <div class="col-md-10">
        <table class="table table-hover">
        <thead>
        <tr>
          <th>Unit ID</th>
          <th>Name</th>
          <th>Owner Account</th>
          <th></th>
        </tr>
         </thead>
        <tbody id="content">
        @if(count($units)>0)
            @foreach($units as $unit)    

        <tr>
          <td>{!! Form::text('unit[]', $unit->unit_id , array('class' => 'form-control') ) !!}
          {!! Form::hidden('units_id[]', $unit->id  ) !!}</td>

          <td>
          {!! Form::text('unit_name[]', $unit->unit_name , array('class' => 'form-control') ) !!}
          </td>
          
          <td>{!! Form::select('owner_id[]', $owners,$unit->owner_id , array('class' => 'form-control') ) !!}</td>

        <td><a data-href="<?php echo url('admin/property/units/delete/'.$unit->id); ?>" data-target="#confirm-delete"  class="btn btn-danger btn-sm"  data-toggle="modal" title="Delete"><i class="fa fa-minus"></i></a></td>
        </tr>
        @endforeach
        @else
          <tr>
          <td>{!! Form::text('unit[]', null , array('class' => 'form-control') ) !!}
          {!! Form::hidden('units_id[]', 0 ) !!}</td>

          <td>
          {!! Form::text('unit_name[]', null , array('class' => 'form-control') ) !!}
             </td>
         
          <td>{!! Form::select('owner_id[]', $owners,null , array('class' => 'form-control') ) !!}</td>

          <td>&nbsp;</td>                
          </tr>
        @endif

      </tbody>
       </table>
          <a id="addunit" class="btn btn-success">Add new unit</a>  
        </div>    
      </div>
      <!-- /.tab-pane -->
         <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_5">
        <h3 class="text-light-blue">Check-In Instructions</h3>
        {!! Form::textarea('policies', null , array('class' => 'form-control','id'=>'editor3') ) !!}
      </div>
      <!-- /.tab-pane -->
         <div class="tab-pane" id="tab_6">
            @if(count($fees)>0) 
            <div class="col-sm-12">
            <h3 class="text-light-blue">Fees</h3>
            @foreach($fees as $row)
            @if($row->type!=3)
               <?php $checked='checked'; 
                      $disabled='';
                      $applies='';
                ?>
                @if($row->applicability==0)
                        <?php 
                            $checked='checked';
                            $disabled='disabled="disabled"';
                            $applies='(Applies to All)';
                        ?>
                    @else

               @endif
               <div class="form-group margin-0">
                    <label>
                       <input type="checkbox" value="{{ $row->id }}" name="fees_taxes[]" class="flat-red" <?php echo $checked.' '.$disabled; ?>/>
                        {{ $row->name }} {{ $applies }}
                    </label>
                </div>
            @endif
            @endforeach

            </div>
            @endif
         @if(count($taxes)>0)
          <div class="col-sm-12">
            <h3 class="text-light-blue">Taxes</h3>
            @foreach($taxes as $row)
            @if($row->type==3)
               <?php $checked='checked'; 
                      $disabled='';
                      $applies='';
                ?>
                    @if($row->applicability==0)
                        <?php 
                            $checked='checked';
                            $disabled='disabled="disabled"';
                            $applies='(Applies to All)';
                        ?>

                      @endif
               <div class="form-group margin-0">
                    <label>
                         <input type="checkbox" value="{{ $row->id }}" name="fees_taxes[]" class="flat-red" <?php echo $checked.' '.$disabled; ?>/>
                        {{ $row->name }} {{ $applies }}
                    </label>
                </div>
            @endif      
            @endforeach
          </div>
         @endif
         @if(count($specials)>0)
          <div class="col-sm-12">
            <h3 class="text-light-blue">Specials</h3>
            @foreach($specials as $special)
               <?php $checked=''; 
                      $disabled='';
                      $applies='';
                ?>
                    @if($special->applicability==0)
                        <?php 
                            $checked='checked';
                            $disabled='disabled="disabled"';
                            $applies='(Applies to All)';
                        ?>

                     @endif

               <div class="form-group margin-0">
                    <label>
                         <input type="checkbox" value="{{ $special->id }}" name="specials[]" class="flat-red" <?php echo $checked.' '.$disabled; ?>/>
                      {{ $special->name }} {{ $applies }}
                    </label>
                </div>

                @endforeach
           </div>
          @endif
         @if(count($bookingrules)>0)
          <div class="col-sm-12">
          <h3 class="text-light-blue">Booking rules</h3>

                @foreach($bookingrules as $rules)
                <?php $checked='checked'; 
                      $disabled='';
                      $applies='';
                ?>
                    @if($rules->applicability==0)
                        <?php 
                            $checked='checked';
                            $disabled='disabled="disabled"';
                            $applies='(Applies to All)';
                        ?>
                    @endif
               <div class="form-group margin-0">
                    <label>
                     <input type="checkbox" value="{{ $rules->id }}" name="rules[]" class="flat-red" <?php echo $checked.' '.$disabled; ?>/>

                      {{ $rules->name }} {{ $applies }}
                    </label>
                </div>
                @endforeach   
         </div>
         @endif
          <div class="col-sm-12">
          <h3 class="text-light-blue">Website settings</h3>
          <div class="form-group">
              <label>
               <input type="checkbox" value="1"  name="disable_booking" <?php echo ($model->disable_booking==1)?'checked':'';?> class="flat-red"/>
                  Disable Online Booking    
              </label>
          </div>
          <div class="form-group">
              <label>
               <input type="checkbox" value="1" name="show_price" <?php echo ($model->show_price==1)?'checked':'';?> class="flat-red"/>
                  Do not show prices on site    
              </label>
          </div>
          <div class="form-group">
              <label>
               <input type="checkbox" value="1" name="hide_on_site" <?php echo ($model->hide_on_site==1)?'checked':'';?> class="flat-red"/>
                  Hide availability on site    
              </label>
          </div>
          <div class="form-group">
              <label>
               <input type="checkbox" value="1" name="unshare_property" <?php echo ($model->unshare_property==1)?'checked':'';?> class="flat-red"/>
                  Unshare Property
              </label>
          </div>

          </div>
          <div class="col-sm-12">
          <h3 class="text-light-blue">Internal Notes</h3>

          {!! Form::textarea('internal_note', null , array('class' => 'form-control textarea','row' => '20') ) !!}

          </div>
      </div>
      <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
  </div>
  <!-- nav-tabs-custom --> 
  {!! Form::close() !!}

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
        </div>

        <div class="modal-body">
            <p>Are you sure to delete this Item?</p>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
</div>
</div>
<script>
initSample();
initSample1();
initSample2();   
$(document).ready(function(){
$('a[data-toggle="tab"]').click(function (e) {
e.preventDefault();
$(this).tab('show');
});

$('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
var id = $(e.target).attr("href");
localStorage.setItem('selectedTab', id)
});

var selectedTab = localStorage.getItem('selectedTab');
if (selectedTab != null) {
$('a[data-toggle="tab"][href="' + selectedTab + '"]').tab('show');
}
});
function loadGallery()
{
var gallery_id='<?php echo ($gallery_id)?$gallery_id:''; ?>';
 $.ajax({
    type:'get',
    url:'<?php echo url('admin/property/loadgallery'); ?>',
    data:{
      gallery_id:gallery_id,
      loadgllery:1
    },
  success:function(html){

         $('#gallery').html(html);

        },
        error: function(errormessage) {
              //you would not show the real error to the user - this is just to see if everything is working

            alert("Error ");
        }
});
}

function display_order(image_id)
{
    
    var images = $('input[name="image_id[]"]').map(function(){ 
                    return this.value; 
     }).get();
     
    var order = $('select[name="display_order[]"]').map(function(){ 
                    return this.value; 
     }).get();
  
   var gallery_id='<?php echo ($gallery_id)?$gallery_id:''; ?>';
    
    
    $.ajax({
            type:'post',
            url:'<?php echo url('admin/property/insertorder'); ?>',
            data:{
              _token:'<?php echo csrf_token(); ?>',
              'image_id[]': images,
              'display_order[]':order,
               change_id:image_id,
               gallery_id:gallery_id
               
            },
          success:function(html){
                
                if(html==1)
                {
                    loadGallery();
                }
                 
                        
                },
                error: function(errormessage) {
                      //you would not show the real error to the user - this is just to see if everything is working
                    
                    alert("Error ");
                }
      });
    
    //document.getElementById("image_id").value = image_id;
    //document.getElementById("myform").submit();
}
$('#datepicker').datepicker({
autoclose: true
});
$('#datepicker2').datepicker({
autoclose: true
});
$('.datepicker').datepicker({
autoclose: true
});  

$('#addunit').click(function(){

$('#content').append('<tr><td>{!! Form::text('unit[]', null , array('class' => 'form-control') ) !!}{!! Form::hidden('units_id[]', 0 ) !!}</td><td>{!! Form::text('unit_name[]', null , array('class' => 'form-control') ) !!}</td><td>{!! Form::select('owner_id[]', $owners,null , array('class' => 'form-control') ) !!}</td><td><button type="button" class="btn btn-danger btn-sm removebutton"><i class="fa fa-minus"></i></button></td></tr>');
$('.datepicker').datepicker({
autoclose: true
});
});

$(document).on('click', 'button.removebutton', function () {

$(this).closest('tr').remove();
return false;
});
</script>    
@include('admin/commons/javascript')
@endsection
