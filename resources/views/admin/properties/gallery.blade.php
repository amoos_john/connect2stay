@if(count($gallery_images)>0)
{!! Form::hidden('change_id',null,array('id' => 'image_id') ) !!}
@foreach($gallery_images as $image) 
{!! Form::hidden('image_id[]',$image->id,array('class'=>'images')) !!}
<div class="panel col-sm-4 image-box">

<div class="view-btn">
    <a href="{{ url('').'/'.$image->url }}" target="_blank">
        {{ ($image->display_order==1)?'Main Photo':'View' }}
    </a>
</div>  
<div class="col-sm-12 image-style"> 
    <img src="{{ asset('/'.$image->url) }}"  class="responsive" />
    </div>
<!--display_order(this.value,'.$image->id.');-->
<div class="col-sm-12" style="padding:0px;">  
    <div class="form-group col-sm-6 label-style">
      {!! Form::label('Display Order:') !!}  
      {!! Form::select('display_order[]', $order,old('',$image->display_order) , array('onchange' => 'display_order('.$image->id.');','class' => 'form-control orders') ) !!}
    </div>
    <div class="form-group col-sm-6 label-style label-style1">
      {!! Form::label('Caption:') !!}  
      {!! Form::text('image_caption[]', $image->image_caption , array('class' => 'form-control') ) !!}
    </div>

     <div class="form-group  col-sm-6 label-style">
      {!! Form::label('Dimensions:') !!}  
      {{ $image->width }} x {{ $image->height }}
    </div>
     <div class="form-group col-sm-6 label-style">
      {!! Form::label('File Size:') !!}  
      {{ $image->file_size }} bytes
    </div>
    <div class="form-group">
      <a onClick="confirmDelete('{{ $image->id }}');"  class="btn btn-danger btn-sm">Remove</a>
    </div>

</div>

</div>
@endforeach
@endif
<script>
    function confirmDelete(id)
    {
        var r = confirm("Are you sure to delete this Item?");
        if (r == true) {
            
            $.ajax({
            type:'get',
            url:'<?php echo url('admin/property/deleteimage'); ?>/'+id,
            success:function(html){

                 loadGallery();

                },
                error: function(errormessage) {
                      //you would not show the real error to the user - this is just to see if everything is working

                    alert("Error ");
                }
        });
            
        } else {
            
            return false;
        }
    }
    
</script>
