@extends('admin/admin_template')

@section('content')
<!-- Main row -->
<?php
if(isset($headline))
{
    $querystringArray = [ 'headline'=>$headline, 'status'=>$status_id,
             'category_id'=>$category_id, 'id'=>$id,
             'owner'=>$owner, 'location'=>$location, 'internalproperty_id'=>$internalproperty_id];
    $link=str_replace("search/?","search?",$model->appends($querystringArray)->render()); 
}
else
{
     $link=$model->render(); 
}
                
?>
<style>
    .box-body .row > div:nth-child(6) {
    width: 25%;
}
</style>
 <section class="content-header">
             <h1>All Properties</h1>
</section>

<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                
                    <h3 class="box-title">Search</h3>
                    <div class="box-tools pull-right">
                    <a class="btn btn-success" href="{{url('admin/property/create')}}">Add New Property</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   

                    </div>
                </div>
                
                {!! Form::model('', ['class' => 'form','url' => ['admin/property/search'], 'method' => 'get']) !!}
            
                <div class="box-body">
                    
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Headline') !!}
                                {!! Form::text('headline', (isset($headline))?$headline:'' , array('class' => 'form-control') ) !!}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Category') !!}
                                {!! Form::select('category_id',$categories,(isset($category_id))?$category_id:'',['class' => 'form-control']) !!} 
                            </div>
                        </div>
               
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Status') !!}
                                {!! Form::select('status',$status,(isset($status_id))?$status_id:'',['class' => 'form-control']) !!} 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                             {!! Form::label('Internal Property ID') !!}
                                {!! Form::text('internalproperty_id',(isset($internalproperty_id))?$internalproperty_id:''  , array('class' => 'form-control') ) !!}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                             {!! Form::label('ID') !!}
                                {!! Form::text('id',(isset($id))?$id:''  , array('class' => 'form-control') ) !!}
                            </div>
                        </div>
                       <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Owner') !!}
                                {!! Form::select('owner',$owners,(isset($owner))?$owner:'',['class' => 'form-control']) !!} 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Location') !!}
                                {!! Form::text('location',(isset($location))?$location:''  , array('class' => 'form-control') ) !!}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Development') !!}
                               {!! Form::select('development', array(''=>'Select'),(isset($development))?$development:'' , array('class' => 'form-control') ) !!}
                            </div>
                        </div>
                      

                        <div class='clearfix'></div>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Search</button>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <a href="{{ url('admin/property') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
    
    <!-- PRODUCT LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">( Total : {{ $model->total() }} ) </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>
            @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            
               <i class="icon fa fa-check"></i> {!! session('success') !!}
            </div>
            @endif
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><a href="#">ID/Internal ID</a></th>
                            <th><a href="#">Headline</a></th>
                            <th>Location</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Details</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($model)>0)   
                        @foreach ($model as $row)
                        <?php
                            $color = '';
                            $status = '';
                            if($row->status=='1')
                            {
                                $color = 'text-green text-bold';
                                $status = 'Active For Rent';
                            }
                            elseif($row->status=='2')
                            {
                                $color = 'text-orange text-bold';
                                $status = 'Entered - Pending Approval';
                            }
                            elseif($row->status=='0')
                            {
                                $color = 'text-red text-bold';
                                $status = 'Inactive';
                            }
                            ?>
                        <tr>
                            <td>{{ ($row->internalproperty_id!=0)?$row->id.'/'.$row->internalproperty_id:$row->id }}</td>
                            <td>{{ $row->internal_name }}</td>
                            <td>{{ $row->city }}</td>
                            <td>{{ ($row->category_name)?$row->category_name:'' }}<br/>
                            {{ $row->bedrooms }} Beds / {{ $row->bathrooms }} Baths / {{ $row->sleeps }} Sleeps</td>
                            <td class="{{ $color }}"><i class="fa fa-circle"></i>  <?php echo $status; ?></td>
                            <td>
                                <a href="{{ url('rentals/allrentals/'.$row->id) }}" target="_blank">View on public website</a><br/>
                                <a href="{{ url('admin/ownercalendar/'.$row->id) }}" target="_blank">View availability calendar</a>
                            </td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="<?php echo url('admin/property/edit/'.$row->id); ?>" title="Edit"><i class="fa fa-pencil"></i> </a>                       
                                <a data-href="<?php echo url('admin/property/delete/'.$row->id); ?>" data-target="#confirm-delete"  class="btn btn-danger btn-sm"  data-toggle="modal" title="Delete"><i class="fa fa-trash"></i> </a>
                            </td>  
                        </tr>
                       
                        @endforeach  
                    @else
                    <tr>
                        <td colspan='7' class='text-center'>Search result not found!</td>
                    </tr>
                    @endif
                    </tbody>
                </table>
                <?php echo $link; ?>
                
             
                       
                <div><?php echo "Showing ".$model->count()." of ".$model->total(); ?></div>
                     
                     
                 </div>
            </div>
        </div>
    
     <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this properties?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->	
<script>
$(document).ready(function(){
    localStorage.removeItem('selectedTab');  
});
</script> 
@endsection