@extends('admin/commons/popup')

@section('content')

<div class="row">
    <div class="col-md-12 mapping">
        
        <div id="map" class="map__" style="height: 500px;"></div>
        
        <div class="col-md-6 map__style">
            <div class="form-group">
              <input id="lat" name="lat" type="hidden" value="{{ $lat }}" class="form-control">
              <input id="long" name="long" type="hidden" value="{{ $lon }}"  class="form-control">
              <input type="text" name="address" id="address"  class="form-control" placeholder="TYPE ADDRESS">
              <input type="submit" id="submit"  value="Find Address" class="btn submit__style"/>
            </div>
        </div>
        <div class="col-md-12">
            <input type="button" value="OK" onclick="getReturnValue();" class="btn btn-success submit__style1"/>
            <input type="button" value="CANCEL" onclick="self.close();" class="btn btn-danger submit__style1"/>    
        </div>
        
    </div>    
</div>

<script type="text/javascript" >

    var markers = [];
    var a = document.getElementById("lat");

    
    function initMap() {
        var haightAshbury = {lat: <?php echo ($lat!='')?$lat:'-14.235004'; ?>, lng: <?php echo ($lon!='')?$lon:'-51.92527999999999'; ?>};


        map = new google.maps.Map(document.getElementById('map'), {
            zoom: <?php echo ($lat!='')?'15':'8'; ?>,
            center: haightAshbury,
            mapTypeId: 'terrain'
        });

        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function(event) {
            addMarker(event.latLng,event.latLng.lat(),event.latLng.lng());
        });
        
        // Adds a marker at the center of the map.
        addMarker(haightAshbury,'45.4311700','-75.6911200');
        
        var geocoder = new google.maps.Geocoder();

        document.getElementById('submit').addEventListener('click', function() {
          geocodeAddress(geocoder, map);
      });
    }

    // Adds a marker to the map and push to the array.
    function addMarker(location,lat,long) {
        clearMarkers();
        markers = [];

        var marker = new google.maps.Marker({
            position: location,
            draggable:true,
            map: map
        });
        markers.push(marker);

        var latitude_text_field = document.getElementById("lat");
        latitude_text_field.value = lat;

        var longitude_text_field = document.getElementById("long");
        longitude_text_field.value = long;
        
         google.maps.event.addListener(marker,'position_changed',function(){
            
            var lat=marker.getPosition().lat();
            var lng=marker.getPosition().lng();
            
            var latitude_text_field = document.getElementById("lat");
            latitude_text_field.value = lat;

            var longitude_text_field = document.getElementById("long");
            longitude_text_field.value = lng;

        }); 

    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
        setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

    function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
            
            var lat=marker.getPosition().lat();
            var lng=marker.getPosition().lng();
            
            var latitude_text_field = document.getElementById("lat");
            latitude_text_field.value = lat;

            var longitude_text_field = document.getElementById("long");
            longitude_text_field.value = lng;
            
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
      
      function getReturnValue()
      {
           var latitude_text_field = document.getElementById("lat").value;

           var longitude_text_field = document.getElementById("long").value;
           
           window.opener.document.getElementById("latitude").value = latitude_text_field;
	   window.opener.document.getElementById("longitude").value = longitude_text_field;
           
           self.close();
      }
</script> 
 <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpxQvcYyGxCSZWDof-C1qF6XT_YETCfJQ&callback=initMap"
      ></script>
      <style>
.map__{
  border:1px dashed #2c3b41;
  color:#8aa4af;
  
}
.map__style{
  margin-top: 20px;
}

.submit__style{
   margin: 0 auto;
  background: #428bca !important;
  margin-top: 10px;
  width:160px;
  display: block;
  border:none;
  border-radius: 0px;
  font-size: 18px;
  text-transform: uppercase;
  font-family: sans-serif;
  color:#fff;
  font-size: 18px;
  color:#fff;
  height: 40px;

  border:1px dashed transparent;
  transition:.3s;
}
.submit__style:hover{
    background: #fff !important;
    color:#8aa4af;
  border:1px dashed #8aa4af;
  outline: 0;

}
.submit__style1{
    width: 100px;
    display: inline-block;
    vertical-align: top;
    margin-top: 10px;
  border-radius: 0px;
   color:#fff;
  font-size: 18px;

}
.mapping{
    text-align: center;
}
      </style>

@endsection