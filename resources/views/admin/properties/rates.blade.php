@extends('admin/admin_template')

@section('content')
<?php
use App\Functions\Functions;
$currency= Config::get('params.currency');
?>
<style>
    .form-group.col-sm-6 {
    padding-left: 0;
}
.nav-tabs-custom>.tab-content {
    padding: 0;
}
.margin-0
{
    margin:0 !important;
}

.textarea{
    width: 60%;
    resize: none;
}
.btn-default {
  
    margin-right: 5px;
}
 .list_property {
    height: 300px;
    overflow-y: scroll;
}
.modal-header {
    border-bottom-color: #ddd;
}
.check_{
  width: 15px;
  height: 15px;
margin-right: 7px;
margin-top: 6px;
vertical-align: sub;
}
.mr-10{
  margin-right: 8px;
}
.modal-body{
  text-align: center;
}
.buttn-style{
  background: #3c8dbc;
  color:#fff !important;
  padding: 4px 0px;
}
.table-condensed thead tr:first-child{
   background: #2EACDF;
  color:#fff;
}
.table-condensed thead tr th{
  border-radius: 0px;
}
.datepicker table tr td.day:hover, .datepicker table tr td.day.focused {
    background: none;
    cursor: pointer;
}
.datepicker table tr td{
  color:#3AB2E4;
}
.datepicker table tr td.old, .datepicker table tr td.new {
    color: #fff;
}
.table-condensed thead tr:nth-child(2) th{
 color: #8d8c88;
}
.datepicker table tr td.active, .datepicker table tr td.active:hover, .datepicker table tr td.active.disabled, .datepicker table tr td.active.disabled:hover{
  background:#3AB2E4 !important;
  color:#fff !important;
}
.datepicker thead tr:first-child th:hover, .datepicker tfoot tr th:hover {
    background: none;
}
.datepicker.dropdown-menu th, .datepicker.dropdown-menu td {
    padding: 6px 5px;
}
.datepicker table tr td, .datepicker table tr th {
    width: 35px;
    height: 35px;
}
.datepicker.dropdown-menu {
     font-size: 16px;
}
</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
     <?php
      $required="required";
      ?>
      @include('admin/commons/errors')
      <div class="box box-warning">
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        <div class="box-header with-border">
          <h3 class="box-title">Rates & Availability</h3>
        </div>
        <!-- /.box-header -->
      
        <div class="box-body">
          @if($id!="")
          {!! Form::open(array( 'class' => 'form','url' => 'admin/property/ratesandavailability/insert/'.$id, 'files' => true)) !!}
          <div class="form-group">
                <button type="submit" id="submit" class="btn btn-primary">Save</button>
                <a href="{{ url('admin/property') }}" class="btn btn-danger">Cancel</a>
                <a  class="btn btn-default pull-right"  data-target="#bulk-update" data-toggle="modal" title="Bulk Update">Bulk Update</a>
                 <a href="{{ url('admin/property/edit/'.$id) }}"  class="btn btn-default pull-right" title="Edit Main Info">Edit Main Info</a>

          </div>
          <div id="message"></div>
                <div class="col-md-3">
                <label>Working Date Range:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="min"  value="{{ $date }}" class="datepicker form-control" id="datepicker" />
                </div>
                
                </div>
               
                <div class="col-md-3">
                    <label>&nbsp;</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="max" value="{{ $end_date }}" class="datepicker form-control pull-right" id="datepicker2" />
                </div>
                </div>
                <div class="col-md-1">
                    <label>&nbsp;</label>
                    <div class="clearfix"></div>
                    <button type="button" class="btn btn-success btn-sm" id="apply">Apply</button>
                </div>
                <!-- /.input group -->
          <div id="rates">   
          @include('admin/properties/dates')
           
          </div>
          {!! Form::close() !!}
          
          @else
          <h3 class="box-title">Pick List</h3>
          
          <div class="box-body table-responsive no-padding">
                <div class="list_property">
                 @if(count($properties)>0)   
                   @foreach ($properties as $row)
               
                    <p><a href="{{ url('admin/property/ratesandavailability/'.$row->id) }}" title="{{ $row->internal_name }}">{{ $row->internal_name }}</a></p>
                
                @endforeach
                @endif
              </div>
            </div>
          
          @endif
          
          
        </div>
        <!-- /.box-body -->
       <div class="modal fade " id="bulk-update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Bulk Update</h4>
                </div>
            
                <div class="modal-body">
                  <h3  class="text-light-blue buttn-style">Date Range to Apply Rate</h3>
                  <div class="col-md-6">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="bulk_min"  value="{{ $start_date }}" class="datepicker form-control" id="datepicker3"/>
                </div>
                
                </div>
               
                <div class="col-md-6">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="bulk_max" value="{{ $end_date }}" class="datepicker form-control pull-right" id="datepicker4"/>
                </div>
                </div>
                <div class="col-md-12" style="padding:0px;">
                 <h3  class="text-light-blue buttn-style" >Options</h3>
                 <label>Days of Week:</label>
                 <div class="form-group">
                    
                    <label class="mr-10"><input class="check_" type="checkbox" id="sun" checked="" value="1"/>Sun</label>
                    <label class="mr-10"><input class="check_" type="checkbox" id="mon" checked="" value="1"/>Mon</label>
                    <label class="mr-10"><input class="check_" type="checkbox" id="tue" checked="" value="1"/>Tue</label>
                    <label class="mr-10"><input class="check_" type="checkbox" id="wed" checked="" value="1"/>Wed</label>
                    <label class="mr-10"><input class="check_" type="checkbox" id="thu" checked="" value="1"/>Thu</label>
                    <label class="mr-10"><input class="check_" type="checkbox" id="fri" checked="" value="1"/>Fri</label>
                    <label class="mr-10"><input class="check_" type="checkbox" id="sat" checked="" value="1"/>Sat</label>

                 </div>   
                </div>
                 
                <div class="col-md-12" style="padding:0px;"> 
                 <h3  class="text-light-blue buttn-style">Rate</h3>
                 <div class="form-group col-md-6" style="padding:0px;">
                  <label>Daily</label>    
                  {!! Form::text('daily', null , array('class' => 'form-control','id' => 'daily',$required) ) !!}
                 </div>
                <div class="form-group col-md-6">
                   <label>Weekly</label>  
                  {!! Form::text('weekly', null , array('class' => 'form-control','id' => 'weekly',$required) ) !!}
                 </div>
                 
                </div>
                  
                 </div>
                
                <div class="modal-footer">
                    <div class="col-md-12"> 
                    <button type="button" class="btn btn-success" id="bulk" data-dismiss="modal">Update</button>

                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
                    </div>
            </div>
        </div>
    </div>
      </div>
      <!-- /.box -->
    </div>
</div>
<script>
// startDate: new Date(),
 $('.datepicker').datepicker({
     format: "yyyy-mm-dd",
    
     autoclose: true
    });

function getDates(token,min,max,id)
{
    var datasend = 'id='+id+'&min='+min+'&max='+max+'&_token='+token;
        
        $.ajax({
            type: "POST",
            url: "<?php echo url('admin/property/ratesandavailability/search'); ?>",
            data: datasend,
            cache:false,
            success: function(success){
               $("#rates").html(success);
            }
        });
}

 $("body").on("click","#apply",function(){
  
      var token='<?php echo csrf_token(); ?>';
      var min= $('#datepicker').val();
      var max= $('#datepicker2').val();
      var id= <?php echo $id;?>;

      
      if(min!='' && max!='')
      {
          getDates(token,min,max,id);
      }
      else
      {
         $("#message").html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Start or end date is required</p></div>');

      }
}); 
$("#submit").click(function () {
    $('input:checkbox[name^=avail]').each(function () {
        $(this).prop("checked") ? $(this).attr('value','1') : $(this).html("<input type='hidden' name='avail[]' value='0' />");
    });
});
$("#bulk").click(function () {

    var daily=$("#daily").val();
    var weekly=$("#weekly").val();
                  
    var sun=$("#sun");
    var mon=$("#mon");
    var tue=$("#tue");
    var wed=$("#wed");
    var thu=$("#thu");
    var fri=$("#fri");
    var sat=$("#sat");
   
    
      var token='<?php echo csrf_token(); ?>';
      var min= $('#datepicker3').val();
      var max= $('#datepicker4').val();
      var id= <?php echo $id;?>;

      
      if(min!='' && max!='')
      {
         var datasend = 'id='+id+'&min='+min+'&max='+max+'&_token='+token;
        
        /*$.ajax({
            type: "POST",
            url: "<?php echo url('admin/property/ratesandavailability/search'); ?>",
            data: datasend,
            cache:true,
            success: function(success){*/
               //$("#rates").html(success);
               
               //$("#datepicker").val(min);
               //$("#datepicker2").val(max);
                var start = new Date(min); //yyyy-mm-dd
                var end = new Date(max); //yyyy-mm-dd
                while(start <= end){
                var mm = ((start.getMonth()+1)>=10)?(start.getMonth()+1):'0'+(start.getMonth()+1);
                var dd = ((start.getDate())>=10)? (start.getDate()) : '0' + (start.getDate());
                var yyyy = start.getFullYear();
                var date = yyyy+"-"+mm+"-"+dd; //yyyy-mm-dd    
                console.log(date);
               if(sun.is(':checked'))
               {
                  $(".dailySun"+date).val(daily);
                  $(".weeklySun"+date).val(weekly);
               }
               if(mon.is(':checked'))
               {
                  $(".dailyMon"+date).val(daily);
                  $(".weeklyMon"+date).val(weekly);
               }
                if(tue.is(':checked'))
               {
                  $(".dailyTue"+date).val(daily);
                  $(".weeklyTue"+date).val(weekly);
               }
               if(wed.is(':checked'))
               {
                  $(".dailyWed"+date).val(daily);
                  $(".weeklyWed"+date).val(weekly);
               }
               if(thu.is(':checked'))
               {
                  $(".dailyThu"+date).val(daily);
                  $(".weeklyThu"+date).val(weekly);
               }
               if(fri.is(':checked'))
               {
                  $(".dailyFri"+date).val(daily);
                  $(".weeklyFri"+date).val(weekly);
               }
               if(sat.is(':checked'))
               {
                  $(".dailySat"+date).val(daily);
                  $(".weeklySat"+date).val(weekly);
               }
               
                start = new Date(start.setDate(start.getDate() + 1));//date increase by 1
                //console.log(start);
               }
              
           
        //});
           
         
      }
      else
      {
         $("#message").html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><p>Start or end date is required</p></div>');

      }
  
});


$(document).ready(function(){
    localStorage.removeItem('selectedTab');  
});
</script>
@endsection