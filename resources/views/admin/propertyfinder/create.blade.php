@extends('admin/admin_template')

@section('content')
<?php
use App\Amenities;
?>

<style>
    .nav-tabs-custom>.tab-content {
    padding: 0;
}
    .list_property {
   height: 400px;
    overflow-y: scroll;
    display: block;
}
.textarea{
    resize: none;
    height: 100px !important;
}
.box-body .nav-tabs-custom {
    padding: 0;
}
input.checkbox {
    display: inline-block !important;
}
</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
     @include('admin/commons/errors')     
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
	@endif
        <div class="box-header with-border">
          <h3 class="box-title">Add New Property Finder</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          {!! Form::open(array( 'class' => 'form','id' => 'frm_search','url' => 'admin/propertyfinder/insert', 'files' => true)) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ url('admin/propertyfinder') }}" class="btn btn-danger">Cancel</a>

            </div>
                     <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
              <li><a href="#tab_2" data-toggle="tab">Criteria</a></li>
              <li><a href="#tab_3" data-toggle="tab">Photos</a></li>
              <li><a href="#tab_4" data-toggle="tab">Advanced</a></li>
              <li><a href="#tab_5" data-toggle="tab">Test This Search</a></li>
            </ul>
            <div class="tab-content">
            
            <?php $page='admin.propertyfinder.form'?>
            @include($page)
            <div class="tab-pane" id="tab_2">
                 <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_11" data-toggle="tab" aria-expanded="true">General</a></li>
                <li class=""><a href="#tab_12" data-toggle="tab" aria-expanded="false">Amenities</a></li>
                <li class=""><a href="#tab_13" data-toggle="tab" aria-expanded="false">Specific Properties</a></li>

              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_11">
                    <div class="form-group col-sm-10">
                    {!! Form::label('Category:') !!}
                    {!! Form::select('category_id', $categories ,null, array('class' => 'form-control') ) !!}
   
                   </div>
                    
                    <div class="clearfix"></div>
                   <div class="form-group col-md-6">
                    <div class="col-xs-4">
                          {!! Form::label('Min Stay') !!}
                         {!! Form::number('min_stay_from', null , array('class' => 'form-control','min' => '1') ) !!}
                         
                    </div>
                     <div class="col-xs-1">  
                      <label>&nbsp;</label>  
                       to
                     </div> 
                     <div class="col-xs-4">
                         <label>&nbsp;</label>  
                          {!! Form::number('min_stay_to', null , array('class' => 'form-control','min' => '1') ) !!}
                    </div>
                   </div>
                    <div class="form-group col-md-6">
                    <div class="col-xs-4">
                          {!! Form::label('Avg Per Day Rate') !!}
                         {!! Form::number('avg_day_from', null , array('class' => 'form-control','min' => '1') ) !!}
                         
                    </div>
                     <div class="col-xs-1">  
                      <label>&nbsp;</label>  
                       to
                     </div> 
                     <div class="col-xs-4">
                         <label>&nbsp;</label>  
                          {!! Form::number('avg_day_to', null , array('class' => 'form-control','min' => '1') ) !!}
                    </div>
                   </div> 
                    <div class="form-group col-md-6">
                    <div class="col-xs-4">
                          {!! Form::label('Sleeps') !!}
                         {!! Form::number('sleeps_from', null , array('class' => 'form-control','min' => '1') ) !!}
                         
                    </div>
                     <div class="col-xs-1">  
                      <label>&nbsp;</label>  
                       to
                     </div> 
                     <div class="col-xs-4">
                         <label>&nbsp;</label>  
                          {!! Form::number('sleeps_to', null , array('class' => 'form-control','min' => '1') ) !!}
                    </div>
                   </div>
                     <div class="form-group col-md-6">
                    <div class="col-xs-4">
                          {!! Form::label('Bedrooms') !!}
                         {!! Form::number('bedrooms_to', null , array('class' => 'form-control','min' => '1') ) !!}
                         
                    </div>
                     <div class="col-xs-1">  
                      <label>&nbsp;</label>  
                       to
                     </div> 
                     <div class="col-xs-4">
                         <label>&nbsp;</label>  
                          {!! Form::number('bedrooms_from', null , array('class' => 'form-control','min' => '1') ) !!}
                    </div>
                   </div> 
                     <div class="form-group col-md-6">
                    <div class="col-xs-4">
                          {!! Form::label('Bathrooms') !!}
                         {!! Form::number('bathrooms_from', null , array('class' => 'form-control','min' => '1') ) !!}
                         
                    </div>
                     <div class="col-xs-1">  
                      <label>&nbsp;</label>  
                       to
                     </div> 
                     <div class="col-xs-4">
                         <label>&nbsp;</label>  
                          {!! Form::number('bathrooms_to', null , array('class' => 'form-control','min' => '1') ) !!}
                    </div>
                   </div> 
                    <div class="form-group col-md-6">
                        <label>{!! Form::checkbox('honour_loc', 1  ) !!}Honor Location Context</label>
                    </div>

                </div>
                <div class="tab-pane" id="tab_12">
                 @if(count($parents)>0)
                  <?php $i=0; ?>
                    @foreach($parents as $row)
                    <div class="col-md-3">
                    <h3 class="text-light-blue">{{ $row->name }}</h3>
                  
                    <?php
                    $amentities=Amenities::where("parent_id","=",$row->id)->get();  
                    ?>
                    @if(count($amentities)>0)
                        @foreach($amentities as $child)
                       
                        <div class="form-group">
                            <label>
                              <input type="checkbox" value="{{ $child->id }}" name="amentity[]" class="flat-red"/>
                              {{ $child->name }}
                            </label>
                        </div>
                       
                        @endforeach
                    @endif
                    </div>
                     <?php $i++; ?>
                     @if($i==4)
                     <div class="clearfix"></div>
                     @endif
                    @endforeach
                  @endif

                </div>
                <div class="tab-pane" id="tab_13">
            <table class="table">
              
             <tbody class="list_property" id="list_property">
             @foreach($properties as $row)
               <tr>
                   <td><label><input type="checkbox" class="checkbox" value="{{ $row->id }}" name="property_id[]"/>
                   {{ $row->internal_name }}</label></td>
               </tr>   
             @endforeach 
              </tbody>
          </table>      
                    
                </div>
                  
              <!-- /.tab-pane -->
            
        
            </div>

            </div>
            <div class="tab-pane" id="tab_3">
              
            </div>
             <div class="tab-pane" id="tab_4">
                 <div class="form-group col-md-5">
                    {!! Form::label('Grouping Key:') !!}
                    {!! Form::text('grouping_key', null , array('class' => 'form-control') ) !!}
                </div>

                <div class="form-group col-md-5">
                    {!! Form::label('Sort Index:') !!}
                    {!! Form::text('sort_index', null , array('class' => 'form-control') ) !!}

                </div>
            </div>
             <div class="tab-pane" id="tab_5">
                 <button type="button" class="btn btn-primary btn-flat" id="search">Run search</button>
                 <div id="load"></div>
                <div id="search-result"></div>
            </div>
            
            </div>
            <!-- /.tab-content -->
          </div>
          {!! Form::close() !!}
      
        </div>
        <!-- /.box-body -->
      </div>

      <!-- /.box -->
    </div>
</div>

<script>   
    initSample();
    $("body").on("click","#search",function(){ 

    var search = $('#frm_search').serialize();

    $("#search-result").html("");
    $('#load').html('<img style="img-center img-responsive" src="<?php echo url('front/images/loading.gif');?>" />');
    $.ajax({
        type: 'GET',
        data: search,
        url: '{{ url("admin/propertyfinder/test-search") }}',
        success: function(data) 
        {
            $("#load").html("");
            $("#search-result").html(data);
        }
    }); 
   
});
</script>

@endsection