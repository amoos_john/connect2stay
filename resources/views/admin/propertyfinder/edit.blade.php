@extends('admin/admin_template')

@section('content')
<?php
use App\Amenities;
?>
 <style>
.text-light-blue{
  background: #2c3b41;
padding: 8px;
margin-top: 20px;
  text-align: center;
  border-radius: 3px; 
}
.text-light-blue a,.text-light-blue i{
  color:#8aa4af;
}            
.image-box{
  padding-top: 20px;
  text-align: center;
}
#gallery{
  background: #fff;
}
.image-style
{margin-top: -22px;  border: 1px solid #ddd;
  border-radius: 4px;
  padding: 8px;
height: 212px;
}
.image-style img{
  width: 100%;
    max-height: 100%;
  border-radius: 4px;
}
.box-body .form-control{border-radius: 4px;}
.panel{
  border-radius: 0px;
  margin-bottom: 0px;
      border-bottom: 1px dashed #ddd;
}
.view-btn{

  background: #ddd;
    height: 40px;
  line-height: 40px;
  width: 100px;
  text-align: center;
  margin: 0 auto;
  display: block;
      z-index: 8;
    position: relative;
    border-radius: 4px;
      transition-timing-function: ease-in;

  /* Quick on the way out */
  transition: 0.2s;
}
.view-btn a{
  color:#000;
  display: block;
  transition-timing-function: ease-in;

  /* Quick on the way out */
  transition: 0.2s;
}
input[type=checkbox], input[type=radio]{
  width: 17px;
  height: 17px;
}
.view-btn:hover{background: #26ABE2;}
.view-btn a:hover{color: #fff;}
.label-style{
 padding-right: 0px;
  text-align: center;
  color:#555;

}
label{
 color:#26ABE2;
}
.label-style1{
  padding-right: 0px;

}
.label-style label{
  color:#26ABE2;
  font-size: 16px !important;
      display: block;
    margin-top: 14px;
}



    .nav-tabs-custom>.tab-content {
    padding: 0;
}
    .list_property {
   height: 400px;
    overflow-y: scroll;
    display: block;
    width: 100%;
}
.textarea{
    resize: none;
    height: 100px !important;
}
.box-body .nav-tabs-custom {
    padding: 0;
}
input.checkbox {
    display: inline-block !important;
}

        </style> 
<div class="row">
    <div class="col-md-12">
          
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
     @include('admin/commons/errors')
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        <div class="box-header with-border">
          <h3 class="box-title">Edit Property Finder</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::model($model, ['files' => true,'class' => 'form','id' => 'frm_search','url' => ['admin/propertyfinder/update', $model->id], 'method' => 'post']) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="{{ url('admin/propertyfinder') }}" class="btn btn-danger">Cancel</a>
           
            </div>
            <!-- text input -->
                   <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
           <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
              <li><a href="#tab_2" data-toggle="tab">Criteria</a></li>
              <li><a href="#tab_3" data-toggle="tab">Photos</a></li>
              <li><a href="#tab_4" data-toggle="tab">Advanced</a></li>
              <li><a href="#tab_5" data-toggle="tab">Test This Search</a></li>
            </ul>
            <div class="tab-content">
            <?php $page='admin.propertyfinder.form'?>
            @include($page)
            <div class="tab-pane" id="tab_2">
                 <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_11" data-toggle="tab" aria-expanded="true">General</a></li>
                <li class=""><a href="#tab_12" data-toggle="tab" aria-expanded="false">Amenities</a></li>
                <li class=""><a href="#tab_13" data-toggle="tab" aria-expanded="false">Specific Properties</a></li>

              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_11">
                    <div class="form-group col-sm-10">
                    {!! Form::label('Category:') !!}
                    {!! Form::select('category_id', $categories ,null, array('class' => 'form-control') ) !!}
   
                   </div>
                    
                    <div class="clearfix"></div>
                   <div class="form-group col-md-6">
                    <div class="col-xs-4">
                          {!! Form::label('Min Stay') !!}
                         {!! Form::number('min_stay_from', null , array('class' => 'form-control','min' => '1') ) !!}
                         
                    </div>
                     <div class="col-xs-1">  
                      <label>&nbsp;</label>  
                       to
                     </div> 
                     <div class="col-xs-4">
                         <label>&nbsp;</label>  
                          {!! Form::number('min_stay_to', null , array('class' => 'form-control','min' => '1') ) !!}
                    </div>
                   </div>
                    <div class="form-group col-md-6">
                    <div class="col-xs-4">
                          {!! Form::label('Avg Per Day Rate') !!}
                         {!! Form::number('avg_day_from', null , array('class' => 'form-control','min' => '1') ) !!}
                         
                    </div>
                     <div class="col-xs-1">  
                      <label>&nbsp;</label>  
                       to
                     </div> 
                     <div class="col-xs-4">
                         <label>&nbsp;</label>  
                          {!! Form::number('avg_day_to', null , array('class' => 'form-control','min' => '1') ) !!}
                    </div>
                   </div> 
                    <div class="form-group col-md-6">
                    <div class="col-xs-4">
                          {!! Form::label('Sleeps') !!}
                         {!! Form::number('sleeps_from', null , array('class' => 'form-control','min' => '1') ) !!}
                         
                    </div>
                     <div class="col-xs-1">  
                      <label>&nbsp;</label>  
                       to
                     </div> 
                     <div class="col-xs-4">
                         <label>&nbsp;</label>  
                          {!! Form::number('sleeps_to', null , array('class' => 'form-control','min' => '1') ) !!}
                    </div>
                   </div>
                     <div class="form-group col-md-6">
                    <div class="col-xs-4">
                          {!! Form::label('Bedrooms') !!}
                         {!! Form::number('bedrooms_to', null , array('class' => 'form-control','min' => '1') ) !!}
                         
                    </div>
                     <div class="col-xs-1">  
                      <label>&nbsp;</label>  
                       to
                     </div> 
                     <div class="col-xs-4">
                         <label>&nbsp;</label>  
                          {!! Form::number('bedrooms_from', null , array('class' => 'form-control','min' => '1') ) !!}
                    </div>
                   </div> 
                     <div class="form-group col-md-6">
                    <div class="col-xs-4">
                          {!! Form::label('Bathrooms') !!}
                         {!! Form::number('bathrooms_from', null , array('class' => 'form-control','min' => '1') ) !!}
                         
                    </div>
                     <div class="col-xs-1">  
                      <label>&nbsp;</label>  
                       to
                     </div> 
                     <div class="col-xs-4">
                         <label>&nbsp;</label>  
                          {!! Form::number('bathrooms_to', null , array('class' => 'form-control','min' => '1') ) !!}
                    </div>
                   </div> 
                    <div class="form-group col-md-6">
                        <label>{!! Form::checkbox('honour_loc', 1  ) !!}Honor Location Context</label>
                    </div>

                </div>
                <div class="tab-pane" id="tab_12">
                 @if(count($parents)>0)
                  <?php $i=0; 
                  $amentity= explode(",", $model->amentites);
                  
                  ?>
                    @foreach($parents as $row)
                    <div class="col-md-3">
                    <h3 class="text-light-blue">{{ $row->name }}</h3>
                  
                    <?php
                    $amentities=Amenities::where("parent_id","=",$row->id)->get();  
                    ?>
                    @if(count($amentities)>0)
                        @foreach($amentities as $child)
                        <?php $checked=''; ?>
                            @foreach($amentity as $amentity_id)
                                <?php
                                    if($amentity_id==$child->id)
                                    {
                                        $checked='checked';
                                    }
                                ?>
                        @endforeach
                        <div class="form-group">
                            <label>
                              <input type="checkbox" value="{{ $child->id }}" name="amentity[]" class="flat-red" <?php echo $checked; ?>/>
                              {{ $child->name }}
                            </label>
                        </div>
                       
                        @endforeach
                    @endif
                    </div>
                     <?php $i++; ?>
                     @if($i==4)
                     <div class="clearfix"></div>
                     @endif
                    @endforeach
                  @endif

                </div>
           <div class="tab-pane" id="tab_13">
            <table class="table">
              
             <tbody class="list_property" id="list_property">
             @foreach($properties as $row)
              <?php  
              $properties= explode(",", $model->properties);
              $checked=''; 
              ?>
                   @foreach($properties as $property_id)
                   <?php
                       if($property_id==$row->id)
                        {
                           $checked='checked';
                        }
                   ?>
              @endforeach
               <tr>
                   <td><label><input type="checkbox" class="checkbox" value="{{ $row->id }}" name="property_id[]" <?php echo $checked; ?>/>
                   {{ $row->internal_name }}</label></td>
               </tr>   
             @endforeach 
              </tbody>
          </table>      
                    
                </div>
                  
              <!-- /.tab-pane -->
            
        
            </div>

            </div>
           <div class="tab-pane" id="tab_3">
                 <div class="col-sm-12">
                <h4 class="text-light-blue"><i class="fa fa-image"></i> <a  onclick="window.open('<?php echo url('admin/addimage/propertyfinder')."/".$gallery_id; ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1000,height=600');">Add an Image</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-camera" aria-hidden="true"></i> <a onclick="window.open('<?php echo url('admin/addmultiple/propertyfinder')."/".$gallery_id; ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1000,height=600');">Add Multiple Images</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-times" aria-hidden="true"></i> <a data-href="{{ url('admin/deleteallimages/propertyfinder').'/'.$gallery_id }}" data-target="#confirm-delete"   data-toggle="modal">Delete All Images</a> 
                </h4>
                 <div id="gallery"></div>
              </div> 
            </div>
                
              <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_4">
                 <div class="form-group col-md-5">
                    {!! Form::label('Grouping Key:') !!}
                    {!! Form::text('grouping_key', null , array('class' => 'form-control') ) !!}
                </div>

                <div class="form-group col-md-5">
                    {!! Form::label('Sort Index:') !!}
                    {!! Form::text('sort_index', null , array('class' => 'form-control') ) !!}

                </div>
            </div>
             <div class="tab-pane" id="tab_5">
                 <button type="button" class="btn btn-primary btn-flat" id="search">Run search</button>
                 <div id="load"></div>
                <div id="search-result"></div>
                </div> 
            </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        
      </div>
       <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this image?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
      <!-- /.box -->
    </div>
</div>
<script>
     $('.datepicker').datepicker({
      autoclose: true
    });
    initSample();
    
loadGallery();
function loadGallery()
{
    var gallery_id='<?php echo ($gallery_id)?$gallery_id:''; ?>';
         $.ajax({
            type:'get',
            url:'<?php echo url('admin/property/loadgallery'); ?>',
            data:{
              gallery_id:gallery_id,
              loadgllery:1
            },
          success:function(html){
                   
                 $('#gallery').html(html);
                        
                },
                error: function(errormessage) {
                      //you would not show the real error to the user - this is just to see if everything is working
                    
                    alert("Error ");
                }
      });
}
$("body").on("click","#search",function(){ 

    var search = $('#frm_search').serialize();

    $("#search-result").html("");
    $('#load').html('<img style="img-center img-responsive" src="<?php echo url('front/images/loading.gif');?>" />');
    $.ajax({
        type: 'GET',
        data: search,
        url: '{{ url("admin/propertyfinder/test-search") }}',
        success: function(data) 
        {
            $("#load").html("");
            $("#search-result").html(data);
        }
    }); 
   
});
</script>
@endsection