<?php
$required="required";
?>

<div class="tab-pane active" id="tab_1">
    
<div class="form-group col-md-5">
    {!! Form::label('Name:*') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}
</div>

<div class="form-group col-md-5">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}
   
</div>

<div class="clearfix"></div>
  
<div class="form-group col-md-8">
   {!! Form::label('Description:') !!}
   {!! Form::textarea('description', null , array('class' => 'form-control','id'=>'editor') ) !!}
</div> 
<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Notes</h3>
   {!! Form::textarea('notes', null , array('class' => 'form-control textarea','row' => '20') ) !!}
</div>   

</div> 
              
<!-- /.tab-pane -->
         

