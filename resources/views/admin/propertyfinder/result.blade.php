@if(isset($properties))
<h3>Showing {{ count($properties) }} properties</h3>
<table class="table">
    <tbody class="list_property">
        @if(count($properties)>0)    
        @foreach($properties as $row)
        <tr>
            <td><label><a href="{{ url('rentals/allrentals/'.$row->id) }}" target="_blank" title="View on public website">{{ $row->internal_name }}</a></label></td>
        </tr>   
        @endforeach
        @else
        <tr>
            <td><label>Result not found!</label></td>
        </tr> 
        @endif
    </tbody>
</table>
@else
<h3>{{ $title }}</h3>
@endif