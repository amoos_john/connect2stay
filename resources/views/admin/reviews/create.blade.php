@extends('admin/admin_template')

@section('content')

<style>
    .list_property {
    height: 300px;
    overflow-y: scroll;
}
.textarea{
    resize: none;
    height: 100px !important;
}
</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
     @include('admin/commons/errors')     
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
	@endif
        <div class="box-header with-border">
          <h3 class="box-title">Add New Review</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          {!! Form::open(array( 'class' => 'form','url' => 'admin/reviews/insert', 'files' => true)) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ url('admin/reviews') }}" class="btn btn-danger">Cancel</a>

            </div>
            <?php $page='admin.reviews.form'?>
            @include($page)
          {!! Form::close() !!}
      
        </div>
        <!-- /.box-body -->
      </div>
 
      <!-- /.box -->
    </div>
</div>

<script>   
    initSample();
    initSample1();
</script>
@endsection