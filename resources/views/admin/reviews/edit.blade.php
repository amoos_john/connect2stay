@extends('admin/admin_template')

@section('content')

<div class="row">
    <div class="col-md-12">
          
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
     @include('admin/commons/errors')
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        <div class="box-header with-border">
          <h3 class="box-title">Edit Review</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['admin/reviews/update', $model->id], 'method' => 'post']) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="{{ url('admin/reviews') }}" class="btn btn-danger">Cancel</a>
           
            </div>
            <!-- text input -->

            <?php $page='admin.reviews.form'?>
            @include($page)
      
          <!-- nav-tabs-custom -->
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        
      </div>
     
      <!-- /.box -->
    </div>
</div>
<script>
     $('.datepicker').datepicker({
      autoclose: true
    });
    initSample();
    initSample1();

$('.starRating').mouseover(function() {
  $('label').removeClass('checked');
});
$('.starRating').mouseout(function() {
  $('.starRating>label.check').addClass('checked');
});
$('.starRating>label').click(function() {
  $('.starRating>label').removeClass('check');
});
</script>
@endsection