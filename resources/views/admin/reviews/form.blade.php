<?php
$required="required";
?> 
<link rel="stylesheet" href="{{ asset('front/css/star-rating.css') }}" rel="stylesheet" type="text/css"/>            
<script src="{{ asset('front/js/star-rating.js') }}" type="text/javascript"></script>
<div class="form-group col-md-7">
    {!! Form::label('Title:*') !!}
    {!! Form::text('title', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="clearfix"></div>
<div class="form-group col-md-5">

    {!! Form::label('Reviewed By:') !!}
    {!! Form::text('reviewed_by', null , array('class' => 'form-control') ) !!}
</div>

<div class="form-group col-md-5">

    {!! Form::label('Rating:*') !!}
    <?php
    $rating='';
    if(isset($model->ratings))
    {
            $rating=$model->ratings; 
    }
    ?>
    <div id="example" class='slider-example'>
        <input type="hidden" id="starRating" name="ratings" value="<?php echo $rating; ?>" class="rb-rating rating-input" required="" />     
   
    </div>
</div>
<div class="clearfix"></div>

<div class="form-group col-md-5">
    {!! Form::label('Related To:*') !!}
    {!! Form::select('related_to', $related_to,null , array('class' => 'form-control',$required) ) !!}
   
</div>

<div class="form-group col-md-5">
    {!! Form::label('Source:*') !!}
    {!! Form::select('source', $source,null , array('class' => 'form-control','readonly' => 'true',$required) ) !!}
   
</div>

<div class="clearfix"></div>

<div class="form-group col-md-5">
    {!! Form::label('Related To Value:*') !!}
    {!! Form::select('property_id', $properties,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="form-group col-md-5">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}
   
</div>

<div class="clearfix"></div>

    <div class="col-md-10">
    <div class="form-group">
       {!! Form::label('Comment:') !!}
       {!! Form::textarea('comment', null , array('class' => 'form-control','id'=>'editor') ) !!}
   </div>  
   <div class="form-group">
        {!! Form::label('Response:') !!}
        {!! Form::textarea('response', null , array('class' => 'form-control','id'=>'editor2') ) !!}
    </div> 
   
  </div>
<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Notes</h3>
   {!! Form::textarea('notes', null , array('class' => 'form-control textarea','row' => '20') ) !!}
</div>
<script>
$('.rb-rating').rating({
                'stars': '5',
                'min': '0',
                'max': '5',
                'step': '1',
                'size': 'xs'
            });  

</script>
            
<!-- /.tab-pane -->
         

