@extends('admin/admin_template')
@section('content')
<link rel="stylesheet" href="{{ asset('front/css/star-rating.css') }}" rel="stylesheet" type="text/css"/>            
<script src="{{ asset('front/js/star-rating.js') }}" type="text/javascript"></script>
<!-- Main row -->
<?php
if(isset($title) || isset($status_id))
{
    $querystringArray = [ 'responded'=>$responded_id,'property_id'=>$property_id,
    'source'=>$source,'title'=>$title,'status'=>$status_id];
    $link=str_replace("search/?","search?",$model->appends($querystringArray)->render()); 
}
else
{
     $link=$model->render(); 
}

                  
?>
<style>
    .time{
        color: #999;
    }
    td.text__in p {
    white-space: nowrap;
    width: 300px;
    overflow: hidden;
    text-overflow: ellipsis;
}
.box-body .row > div:nth-child(6) {
    width: 25%;
}

em {
    font-size: 14px;
}
.rating-sm {
    font-size: 1em;
}
</style>
<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Reviews</h3>
                   
                    <div class="box-tools pull-right">
                    <a class="btn btn-success" href="{{url('admin/reviews/create')}}">Add New Review</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   

                    </div>
                
                </div>
               
               {!! Form::open(array( 'class' => 'form','url' => 'admin/reviews/search', 'method' => 'get')) !!}

            
                <div class="box-body">
                    
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Responded') !!}
                                {!! Form::select('responded',$responded,(isset($responded_id))?$responded_id:'' ,['class' => 'form-control']) !!} 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                            {!! Form::label('Related To Value') !!}
                            {!! Form::select('property_id', $properties,(isset($property_id))?$property_id:'' , array('class' => 'form-control') ) !!}
                        </div>
                        </div>
                         <div class="col-sm-3">
                            <div class="form-group">
                            {!! Form::label('Source:') !!}
                            {!! Form::select('source', $source,(isset($source_id))?$source_id:''  , array('class' => 'form-control') ) !!}
                        </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('Title') !!}
                                {!! Form::text('title', (isset($title))?$title:'' , array('class' => 'form-control') ) !!}
                            </div>
                        </div>
                       <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Status') !!}
                                {!! Form::select('status',$status,(isset($status_id))?$status_id:'' ,['class' => 'form-control']) !!} 
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Search</button>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <a href="{{ url('admin/reviews') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
    <!-- PRODUCT LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>
            @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-check"></i> {!! session('success') !!}
            </div>
            @endif
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Booking ID</th>
                            <th>Added On</th>
                            <th>Title</th>
                            <th>Related To</th>
                            <th>Responded</th>
                            <th>Ranking</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($model as $row)
                        <?php
                           
                            $color = '';
                            $status = '';
                            if($row->status=='1')
                            {
                                $color = 'text-green text-bold';
                                $status = 'Active';
                            }
                            elseif($row->status=='0')
                            {
                                $color = 'text-red text-bold';
                                $status = 'Inactive';
                            }
                           
                            if (array_key_exists($row->source, $source)) {
                               $source_name = $source[$row->source];
                            }
                            $reviewed_by=($row->reviewed_by!='')?'<br/><em>Reviewer: '.$row->reviewed_by:'<br/><em>Reviewer: '.'N/A';
                            
                        ?>
                        <tr>
                            <td><?php echo $row->id; ?></td>
                            <td><?php echo $row->booking_id; ?></td>
                            <td><?php echo $row->created_at; ?></td>
                            <td><?php echo $row->title.'<br/><em>Source:'.$source_name.'</em>'; ?></td>
                            <td><?php echo $row->properties->internal_name.$reviewed_by;?></td>
                            <td><?php echo ($row->response!='')?'Yes':'No'; ?></td>
                            <td><input id="input-3" name="input-3" value="<?php echo $row->ratings; ?>" class="rating-loading" data-max="5" data-stars=5 data-step=0.1 data-size="sm">
                            </td>
                            <td class="{{ $color }}"><i class="fa fa-circle"></i>  <?php echo $status; ?></td>

                            <td>
  
                                <a class="btn btn-primary btn-sm" href="<?php echo url('admin/reviews/edit/'.$row->id); ?>" title="Edit"><i class="fa fa-pencil"></i> </a>                       
                                <a data-href="<?php echo url('admin/reviews/delete/'.$row->id); ?>" data-target="#confirm-delete"  class="btn btn-danger btn-sm"  data-toggle="modal" title="Delete"><i class="fa fa-trash"></i> </a>
                           </td>    
                            
                        </tr>
                        
                        @endforeach       
                    </tbody>
                </table>
                <?php echo $link; ?>
                 <div><?php echo "Showing ".$model->count()." of ".$model->total(); ?></div>
            </div>
        </div>
    
     <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this Review?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->	
<script>
$('.rating-loading').rating({displayOnly: true});

</script>
@endsection