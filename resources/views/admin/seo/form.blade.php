 <?php
$required="required";
?>    
<div class="form-group col-md-6">
    {!! Form::label('Title:*') !!}
    {!! Form::text('title', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="clearfix"></div>
<div class="form-group col-md-8">
    {!! Form::label('Language:') !!}
    {!! Form::select('language', $language,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="clearfix"></div>
@if($applies_to!='')
<div class="form-group col-md-8">
    {!! Form::label('Applies to:*') !!}
    {!! Form::select('applies_to', $applies_to,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="clearfix"></div>
@endif
<div class="form-group col-md-6">
    {!! Form::label('Meta Keywords') !!}
   {!! Form::textarea('meta_keyword', null , array('class' => 'form-control textarea','row' => '20') ) !!}
   
</div>
<div class="clearfix"></div>
<div class="form-group col-md-6">
    {!! Form::label('Meta Description') !!}
   {!! Form::textarea('meta_description', null , array('class' => 'form-control textarea','row' => '20') ) !!}
   
</div>
<div class="clearfix"></div>
<div class="form-group col-md-6">
    {!! Form::label('Primary Name:') !!}
    {!! Form::text('primary_name', null , array('class' => 'form-control') ) !!}
</div>
<div class="clearfix"></div>


<div class="form-group col-md-5">
{!! Form::hidden('type', $type) !!}
   {!! Form::label('Type:') !!}<br/>
   <label>{{ $type_name }}</label>
</div>
<div class="form-group col-md-5">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}
   
</div>

<div class="clearfix"></div>

<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Advanced</h3>
</div>
    <div class="form-group col-md-6">
    {!! Form::label('Friendly URL:*') !!}
    {!! Form::text('friendly_url', null , array('class' => 'form-control',$required) ) !!}
    </div>
    <div class="clearfix"></div>
    <div class="form-group col-md-6">
    {!! Form::label('Redirect Path (usually left blank)') !!}
    {!! Form::text('path', null , array('class' => 'form-control') ) !!}
    </div>
 

            
<!-- /.tab-pane -->
         

