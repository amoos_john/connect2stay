<?php
$required="required";
?>
@include('admin/commons/errors')
<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
</div>
<div class="col-md-12">
   
 <h3 class="text-light-blue">Booking Settings</h3> 
</div>
<div class="col-md-6">
    <table class="table table-responsive">
        <tbody>
            <tr>
                <td>Allow Booking Up To This # of Days In Advance:</td>
                <td>{!! Form::number('advance_days', null , array('class' => 'form-control','min' => '1') ) !!}</td>
            </tr>
            <tr>
                <td>Allow Bookings This # of Days From Today:</td>
                <td>{!! Form::number('from_days', null , array('class' => 'form-control','min' => '1') ) !!}</td>
            </tr>
            <tr>
                <td>Default Online Booking Mode:</td>
                <td>{!! Form::select('booking_mode', $booking_mode,null , array('class' => 'form-control') ) !!}</td>
            </tr>
            <tr>
                <td>Default Check -In Time:</td>
                <td>{!! Form::text('checkin_time', null , array('class' => 'form-control timepicker') ) !!}</td>
            </tr>
            <tr>
                <td>Default Check-Out Time:</td>
                <td>{!! Form::text('checkout_time', null , array('class' => 'form-control timepicker') ) !!}</td>
            </tr>
            <tr>
               <td>Set Default Booking's Hold Time:</td>
               <td>{!! Form::number('hold_time', null , array('class' => 'form-control','min' => '1') ) !!}</td>
            </tr>
            <tr>
              <td>Default Min Night Stay For New Units:</td>
              <td>{!! Form::number('min_stay', null , array('class' => 'form-control','min' => '1') ) !!}</td>
            </tr>
            <tr>
               <td>Per Person Pricing Includes Children:</td>
               <td>{!! Form::checkbox('children_include', 1  ) !!}
              </td>
            </tr>
            <tr>
               <td>Auto Cancel Failed Payment Booking:</td>
               <td>{!! Form::checkbox('cancel_payment', 1  ) !!}</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="col-md-12">  
 <h3 class="text-light-blue">Payment Method Settings</h3> 
</div>
<div class="col-md-6">
    <table class="table table-responsive">
        <tbody>
            <?php 
            $i=1; 
            $smethods=explode(",",$model->payment_method);
            ?>
            @foreach($payment_method as $key=>$method)
             @if($i%2)
                <tr>
              @endif
             <?php
             $checked='';
             foreach($smethods as $smethod)
             {
                 if($smethod==$key)
                 {
                     $checked='checked';
                 }
                     
             }
             ?>
             <td><label><input type="checkbox" value="{{ $key }}" name="payment_method[]" <?php echo $checked; ?> /> {{ $method }}</label></td>
            
            <?php $i++; ?>
              @if($i%2)
                </tr>
              @endif
            
            @endforeach
           
        </tbody>
    </table>
</div>
<div class="col-md-12">
   
 <h3 class="text-light-blue">Rate Display Settings</h3> 
</div>
<div class="col-md-5">
    <table class="">
        <tbody>
            <tr>
                <td><label>With Dates Supplied</label></td>
            </tr>
            <tr>
                <td><label>{!! Form::radio('date_supplied', 1  ) !!}Total Cost of Stay</label></td>
            </tr>
            <tr>
                <td><label>{!! Form::radio('date_supplied', 2  ) !!}Avg. Nightly Rate for Entire Stay</label></td>
            </tr>
            <tr>
                <td><label>{!! Form::radio('date_supplied', 3  ) !!} Rate for Lowest Applicable Length of Stay</label></td>
            </tr>
            <tr>
                <td><label>{!! Form::radio('date_supplied', 4  ) !!} Rate for Highest Applicable Length of Stay</label></td>
            </tr>
              <tr>
                <td><label>{!! Form::radio('date_supplied', 5  ) !!} Display Rate Range if Applicable</label></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="col-md-6">
    <table class="">
        
        <tbody>
            <tr>
                <td><label>No Dates Supplied</label></td>
            </tr>
            <tr>
                <td><label>{!! Form::radio('nodate_supplied', 1  ) !!} Lowest Available Rate based on Lowest Length Of Stay</label></td>
            </tr>
            <tr>
                <td><label>{!! Form::radio('nodate_supplied', 2 ) !!}  Lowest Available Rate based on Highest Length Of Stay</label></td>
            </tr>
          
        </tbody>
    </table>
</div>
<div class="col-md-12">
   
 <h3 class="text-light-blue">Rate Display</h3> 
</div>
<div class="col-md-6">
    <table class="table table-responsive">
        <tbody>
            <tr>
                <td>No dates supplied</td>
                <td>{!! Form::text('no_date_supplied', null , array('class' => 'form-control') ) !!}</td>
            </tr>
            <tr>
                <td>With dates supplied</td>
                <td>{!! Form::text('with_date_supplied', null , array('class' => 'form-control') ) !!}</td>
            </tr>
             <tr>
                <td>No rates avail text</td>
                <td>{!! Form::text('no_rate_avail', null , array('class' => 'form-control') ) !!}</td>
            </tr>
            <?php
            $services=explode(",",$model->consolidates_service_total);
            ?>
            <tr>
               <td>Consolidate Taxes into Service Total</td>
               <td><input type="checkbox" value="1" <?php echo (in_array(1,$services))?'checked':''; ?> name="consolidates_service_total[]" /></td>
            </tr>
            <tr>
               <td>Consolidate Fees into Service Total</td>
               <td><input type="checkbox" value="2" <?php echo (in_array(2,$services))?'checked':''; ?> name="consolidates_service_total[]" /></td>
            </tr>
             <tr>
               <td>Consolidate Deposits into Service Total</td>
               <td><input type="checkbox" value="3" <?php echo (in_array(3,$services))?'checked':''; ?> name="consolidates_service_total[]" /></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="col-md-12">
   
 <h3 class="text-light-blue">Rate Calculation Rules</h3> 
</div>
<div class="col-md-8">
    <table class="table table-responsive">
        <tbody>
            <tr>
                <td>Lock Rate in on Day of Check in</td>
                <td><label>{!! Form::radio('lock_rate', 1  ) !!} Use Start Rate For The Entire Length Of Stay</label><br/>
                <label>{!! Form::radio('lock_rate', 2  ) !!} Use Start Rate For Its Entire Length Of Stay</label><br/>
                <label>{!! Form::radio('lock_rate', 3  ) !!} Use Start Rate Until Rate Changes</label><br/>
                </td>
            </tr>
          
            <tr>
               <td>Prorate Rates Based on Highest Length of Stay:</td>
               <td>{!! Form::checkbox('length_of_stay', 1  ) !!}</td>
            </tr>
            <tr>
               <td>Rate Mode:</td>
               <td>{!! Form::select('rate_mode', $rate_mode,null , array('class' => 'form-control') ) !!}</td>
            </tr>
         
        </tbody>
    </table>
</div>
<div class="col-md-12">
   
 <h3 class="text-light-blue">3rd Party Owners</h3> 
</div>
<div class="col-md-4">
     <?php
            $party_owner=explode(",",$model->party_owner);
      ?>
    <table class="table table-responsive">
        <tbody>
            <tr>
               <td>Enable 3rd Party Owner Features:</td>
               <td><input type="checkbox" value="1" name="party_owner[]"  <?php echo (in_array(1,$party_owner))?'checked':''; ?>/></td>
            </tr>
            <tr>
               <td>Hide Renter Info in Owner Extranet:</td>
               <td><input type="checkbox" value="2" name="party_owner[]"  <?php echo (in_array(2,$party_owner))?'checked':''; ?>/></td>
            </tr>
             <tr>
               <td>Hide Statement Info from Owners:</td>
               <td><input type="checkbox" value="3" name="party_owner[]"  <?php echo (in_array(3,$party_owner))?'checked':''; ?>/></td>
            </tr>
         
        </tbody>
    </table>
</div>
<div class="col-md-12">
   
 <h3 class="text-light-blue">Rental Policy</h3> 
</div>

<div class="form-group col-md-10">
    {!! Form::label('Summary:') !!}
    {!! Form::text('title[]', $title , array('class' => 'form-control') ) !!}
    {!! Form::hidden('ids[]', $id1) !!}
</div>
<div class="form-group col-md-10">
  {!! Form::textarea('description[]', $description , array('class' => 'form-control','id'=>'editor') ) !!}

</div>
<div class="col-md-12">
   
 <h3 class="text-light-blue">Booking Disclaimer</h3> 
</div>

<div class="form-group col-md-10">
    {!! Form::label('Summary:') !!}
    {!! Form::text('title[]', $title2 , array('class' => 'form-control') ) !!}
    {!! Form::hidden('ids[]', $id2) !!}
</div>
<div class="form-group col-md-10">
  {!! Form::textarea('description[]', $description2 , array('class' => 'form-control','id'=>'editor2') ) !!}

</div>
<div class="col-md-12">
   
 <h3 class="text-light-blue">Calendar Default Settings</h3> 
</div>
<div class="col-md-8">
    <table class="table table-responsive">
        <tbody>
            <tr>
               <td>{!! Form::label('Num Days Ahead') !!}</td>
               <td>{!! Form::label('Num Days Behind') !!}</td>
               <td>{!! Form::label('Num Props') !!}</td>
               <td>{!! Form::label('Legend View') !!}</td>
            </tr>
            <tr>
               <td>{!! Form::text('day_ahead', null , array('class' => 'form-control') ) !!}</td>
               <td>{!! Form::text('day_behind', null , array('class' => 'form-control') ) !!}</td>
               <td>{!! Form::text('num_pros', null , array('class' => 'form-control') ) !!}</td>
               <td>{!! Form::select('legend_view', $legend_view,null , array('class' => 'form-control') ) !!}</td>
            </tr>
        </tbody>
    </table>
</div>
