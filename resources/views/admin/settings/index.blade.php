@extends('admin/admin_template')

@section('content')
<link rel="stylesheet" href="{{ asset('plugins/timepicker/jquery.timepicker.min.css') }}">
<script src="{{ asset('plugins/timepicker/jquery.timepicker.min.js') }}"></script>

<style>
.table>tbody>tr>td {
    padding: 2px;
}
.center-block
{
    margin: 0 auto !important;
    display: table;
}
</style>
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        <div class="box-header with-border">
          <h3 class="box-title">Settings</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['admin/settings/update', $model->id], 'method' => 'post']) !!}
            <?php $page='admin.settings.form';?>
            @include($page)
          {!! Form::close() !!}
          {!! Form::open(array( 'class' => 'form','url' => 'admin/settings/insert', 'files' => true)) !!}
        <div class="col-md-12">
   
         <h3 class="text-light-blue">Rate Setup</h3> 
         <div class="form-group">
            <button type="submit" class="btn btn-primary">Save Rate Settings</button>
        </div>
       </div>
        <div class="col-md-10">
        <table class="table table-responsive">
            <tbody>
                <tr>
                   <th>{!! Form::label('Length of Stay') !!}</th>
                   <th>{!! Form::label('Description') !!}</th>
                   <th>{!! Form::label('Singular Prompt') !!}</th>
                   <th>{!! Form::label('Plural Prompt') !!}</th>
                   <th>{!! Form::label('Display as avg. nightly') !!}</th>
                   <th>{!! Form::label('Enabled') !!}</th>
                </tr>
                @foreach($ratesettings as $rate)
                <tr>
                   <td>{!! Form::hidden('rate_id[]', $rate->id) !!}{!! Form::number('length_stay[]', $rate->length_stay , array('class' => 'form-control','min' => '1') ) !!}</td>
                   <td>{!! Form::text('description[]',  $rate->description , array('class' => 'form-control') ) !!}</td>
                   <td>{!! Form::text('s_prompt[]',  $rate->s_prompt , array('class' => 'form-control') ) !!}</td>
                   <td>{!! Form::text('p_prompt[]',  $rate->p_prompt , array('class' => 'form-control') ) !!}</td>
                   <td><input type="checkbox" value="1" name="avg_night[]" class="center-block" <?php echo ($rate->avg_night==1)?'checked':''; ?>/></td>
                   <td><input type="checkbox" value="1" name="status[]" class="center-block"  <?php echo ($rate->status==1)?'checked':''; ?>/></td>
                </tr>
                @endforeach
               <tr>
                   <td>{!! Form::hidden('rate_id[]', 0) !!}{!! Form::number('length_stay[]', null , array('class' => 'form-control','min' => '1') ) !!}</td>
                   <td>{!! Form::text('description[]',  null , array('class' => 'form-control') ) !!}</td>
                   <td>{!! Form::text('s_prompt[]', null , array('class' => 'form-control') ) !!}</td>
                   <td>{!! Form::text('p_prompt[]',  null , array('class' => 'form-control') ) !!}</td>
                   <td><input type="checkbox" value="1" name="avg_night[]" class="center-block"/></td>
                   <td><input type="checkbox" value="1" name="status[]" class="center-block"/></td>
                </tr>
            </tbody>
        </table>
    </div>  
          {!! Form::close() !!}
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
<script>
    initSample();
    initSample1();
    
(function($) {
    $(function() {
        $('input.timepicker').timepicker();
    });
})(jQuery);    
</script>
@endsection