@extends('admin/admin_template')

@section('content')
<?php
use App\Units;
?>
<style>
    .list_property {
    height: 300px;
    overflow-y: scroll;
}
.textarea{
    resize: none;
    height: 100px !important;
}

.text-light-blue{
  background: #2c3b41;
padding: 8px;
margin-top: 20px;
  text-align: center;
  border-radius: 3px; 
}
.text-light-blue a,.text-light-blue i{
  color:#8aa4af;
}            
.image-box{
  padding-top: 20px;
  text-align: center;
}
#gallery{
  background: #fff;
}
.image-style
{margin-top: -22px;  border: 1px solid #ddd;
  border-radius: 4px;
  padding: 8px;
height: 212px;
}
.image-style img{
  width: 100%;
    max-height: 100%;
  border-radius: 4px;
}
.box-body .form-control{border-radius: 4px;}
.panel{
  border-radius: 0px;
  margin-bottom: 0px;
      border-bottom: 1px dashed #ddd;
}
.view-btn{

  background: #ddd;
    height: 40px;
  line-height: 40px;
  width: 100px;
  text-align: center;
  margin: 0 auto;
  display: block;
      z-index: 8;
    position: relative;
    border-radius: 4px;
      transition-timing-function: ease-in;

  /* Quick on the way out */
  transition: 0.2s;
}
.view-btn a{
  color:#000;
  display: block;
  transition-timing-function: ease-in;

  /* Quick on the way out */
  transition: 0.2s;
}
input[type=checkbox], input[type=radio]{
  width: 17px;
  height: 17px;
}
.view-btn:hover{background: #26ABE2;}
.view-btn a:hover{color: #fff;}
.label-style{
 padding-right: 0px;
  text-align: center;
  color:#555;

}
label{
 color:#26ABE2;
}
.label-style1{
  padding-right: 0px;

}
.label-style label{
  color:#26ABE2;
  font-size: 16px !important;
      display: block;
    margin-top: 14px;
}



    .nav-tabs-custom>.tab-content {
    padding: 0;
}
    .list_property {
   height: 400px;
    overflow-y: scroll;
    display: block;
}
.textarea{
    resize: none;
    height: 100px !important;
}
.box-body .nav-tabs-custom {
    padding: 0;
}
input.checkbox {
    display: inline-block !important;
}

        </style> 
<div class="row">
    <div class="col-md-12">
      <!-- Horizontal Form -->
      
      <!-- /.box -->
      <!-- general form elements disabled -->
      <div class="box box-warning">
     @include('admin/commons/errors')
      @if (Session::has('success'))
		<div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!! session('success') !!}
        </div>
		@endif
        <div class="box-header with-border">
          <h3 class="box-title">Edit Special</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::model($model, ['files' => true,'class' => 'form','url' => ['admin/specials/update', $model->id], 'method' => 'post']) !!}
            <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="{{ url('admin/specials') }}" class="btn btn-danger">Cancel</a>
            @if($model->applicability==0)
            <a onclick="apply();" class="btn btn-default pull-right" title="Apply to Multiple Properties">Apply to Multiple Properties</a>
            @else
               <a class="btn btn-default pull-right" data-target="#multiple" data-toggle="modal" title="Apply to Multiple Properties">Apply to Multiple Properties</a>
            @endif
            </div>
            <!-- text input -->
                   <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
              <!--<li><a href="#tab_2" data-toggle="tab">Black Out Dates</a></li>-->
              <li><a href="#tab_3" data-toggle="tab">Description</a></li>
              <li><a href="#tab_4" data-toggle="tab">Photos</a></li>
              <li><a href="#tab_5" data-toggle="tab">Advance</a></li>
            </ul>
            <div class="tab-content">
            <?php $page='admin.specials.form'?>
            @include($page)
           
                
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        
      </div>
       <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this image?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
      <!-- /.box -->
    </div>
</div>
@include('admin/commons/multiple')
<script>
function apply()
{
    alert('This action is not applicable because this special applies to all Properties'); 
    return false;  
}
     $('.datepicker').datepicker({
      autoclose: true
    });
    initSample();
    initSample1();
    initSample2();
loadGallery();
function loadGallery()
{
var gallery_id='<?php echo ($gallery_id)?$gallery_id:''; ?>';
 $.ajax({
    type:'get',
    url:'<?php echo url('admin/property/loadgallery'); ?>',
    data:{
      gallery_id:gallery_id,
      loadgllery:1
    },
  success:function(html){

         $('#gallery').html(html);

        },
        error: function(errormessage) {
              //you would not show the real error to the user - this is just to see if everything is working

            alert("Error ");
        }
});
}

function display_order(image_id)
{
    
    var images = $('input[name="image_id[]"]').map(function(){ 
                    return this.value; 
     }).get();
     
    var order = $('select[name="display_order[]"]').map(function(){ 
                    return this.value; 
     }).get();
  
   var gallery_id='<?php echo ($gallery_id)?$gallery_id:''; ?>';
    
    
    $.ajax({
            type:'post',
            url:'<?php echo url('admin/property/insertorder'); ?>',
            data:{
              _token:'<?php echo csrf_token(); ?>',
              'image_id[]': images,
              'display_order[]':order,
               change_id:image_id,
               gallery_id:gallery_id
               
            },
          success:function(html){
                
                if(html==1)
                {
                    loadGallery();
                }
                 
                        
                },
                error: function(errormessage) {
                      //you would not show the real error to the user - this is just to see if everything is working
                    
                    alert("Error ");
                }
      });
    
    //document.getElementById("image_id").value = image_id;
    //document.getElementById("myform").submit();
}
</script>
@endsection