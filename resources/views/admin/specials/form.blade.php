<?php
$required="required";
?>

<div class="tab-pane active" id="tab_1">
    
<div class="form-group col-md-5">
    {!! Form::label('Name:*') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="clearfix"></div>
<div class="form-group col-md-5">

    {!! Form::label('Public Name:*') !!}
    {!! Form::text('public_name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="clearfix"></div>
{!! Form::hidden('type', $type) !!}
{!! Form::hidden('gallery_id', $gallery_id) !!}
<div class="form-group col-md-5">
    {!! Form::label('Status:*') !!}
    {!! Form::select('status', $status,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="form-group col-md-5">
    {!! Form::label('Applicability:') !!}
    {!! Form::select('applicability', $applicability,null , array('class' => 'form-control',$required) ) !!}
   
</div>
<div class="clearfix"></div>
<div class="form-group col-md-5">

    {!! Form::label('Coupon Code:*') !!}
    {!! Form::text('coupon_code', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group col-md-5">
 
    {!! Form::label('Effective:*') !!}
    {!! Form::text('effective', null , array('class' => 'datepicker form-control',$required) ) !!}
   
</div>
<div class="clearfix"></div>
<div class="form-group col-md-5">
 
    {!! Form::label('Expiration:*') !!}
    {!! Form::text('expiration', null , array('class' => 'datepicker form-control',$required) ) !!}
   
</div>
<div class="form-group col-md-5">
    {!! Form::label('Cutoff Date:*') !!}
    {!! Form::text('cutoff_date', null , array('class' => 'datepicker form-control',$required) ) !!}
   
</div>
<div class="clearfix"></div>
<div class="form-group col-md-5">
    {!! Form::label('Min Stay:') !!}
    {!! Form::number('min_stay', null , array('class' => 'form-control','min' => '1') ) !!}
</div>
<div class="form-group col-md-5">
    @if($type==1)    
    {!! Form::label('Percentage:') !!}
    @else
    {!! Form::label('Amount:') !!}
    @endif
    {!! Form::number('amount', null , array('class' => 'form-control','min' => '1') ) !!}
    @if($type==3)   
       {!! Form::label('Calc. Based On:') !!}
      {!! Form::select('cal_base', $cal_base,null , array('class' => 'form-control') ) !!}

    @endif
</div>
<div class="clearfix"></div>
<div class="form-group col-md-5">
    <h3 class="text-light-blue">Options</h3>
    <label>{!! Form::checkbox('show_site', 1  ) !!} Show on Site</label>
</div>
<div class="form-group col-md-5">
    <h3 class="text-light-blue">&nbsp;</h3>
    <label>{!! Form::checkbox('auto_apply', 1  ) !!} Auto Apply (if applicable)</label>  
</div>
<div class="clearfix"></div>

<div class="form-group col-sm-8">
    <h3 class="text-light-blue">Notes</h3>
   {!! Form::textarea('notes', null , array('class' => 'form-control textarea','row' => '20') ) !!}
</div>

</div>

<div class="tab-pane" id="tab_2">
                
         
</div>   
<div class="tab-pane" id="tab_3">
    <div class="col-md-8">
    <div class="form-group">
       {!! Form::label('Summary:') !!}
       {!! Form::textarea('summary', null , array('class' => 'form-control','id'=>'editor') ) !!}
   </div>  
   <div class="form-group">
        {!! Form::label('Description:') !!}
        {!! Form::textarea('description', null , array('class' => 'form-control','id'=>'editor2') ) !!}
    </div> 
    <div class="form-group">
        {!! Form::label('Terms:') !!}
        {!! Form::textarea('terms', null , array('class' => 'form-control','id'=>'editor3') ) !!}
    </div>
    </div>
</div>   
<div class="tab-pane" id="tab_4">
     <div class="col-sm-12">
                <h4 class="text-light-blue"><i class="fa fa-image"></i> <a  onclick="window.open('<?php echo url('admin/addimage/propertyfinder')."/".$gallery_id; ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1000,height=600');">Add an Image</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-camera" aria-hidden="true"></i> <a onclick="window.open('<?php echo url('admin/addmultiple/propertyfinder')."/".$gallery_id; ?>', '_blank', 'toolbar=no,scrollbars=yes,top=100,left=200,width=1000,height=600');">Add Multiple Images</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-times" aria-hidden="true"></i> <a data-href="{{ url('admin/deleteallimages/propertyfinder').'/'.$gallery_id }}" data-target="#confirm-delete"   data-toggle="modal">Delete All Images</a> 
                </h4>
                 <div id="gallery"></div>
     </div>           
</div> 
<div class="tab-pane" id="tab_5">
     <h3 class="text-light-blue">Taxable</h3> 
   <div class="form-group col-md-5">
    <label>{!! Form::checkbox('lower_taxes', 1  ) !!} Lowers taxes?</label>  
    </div>
</div> 
                
<!-- /.tab-pane -->
         

