@extends('admin/admin_template')
@section('content')
<!-- Main row -->
<?php
use App\Functions\Functions;
if(isset($types) || isset($status_id))
{
    $querystringArray = [ 'type'=>$types, 'status'=>$status_id];
    $link=str_replace("search/?","search?",$model->appends($querystringArray)->render()); 
}
else
{
     $link=$model->render(); 
}

                  
?>
<style>
    .time{
        color: #999;
    }
</style>
<div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Specials</h3>
                    <div class="box-tools pull-right">
                    <div class="btn-group">
                    <button type="button" class="btn btn-success">Add New Special</button>
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                    @foreach($type as $key=>$typ)  
                    @if($key!="")
                      <li><a href="{{url('admin/specials/create/'.$key)}}">{{ $typ }}</a></li>
                      @endif
                    @endforeach
                    </ul>
                  </div>
                   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
      
                </div>
                </div>
               {!! Form::open(array( 'class' => 'form','url' => 'admin/specials/search', 'method' => 'get')) !!}

            
                <div class="box-body">
                    
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Type') !!}
                                {!! Form::select('type',$type,(isset($types))?$types:'' ,['class' => 'form-control']) !!} 
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::label('Status') !!}
                                {!! Form::select('status',$status,(isset($status_id))?$status_id:'' ,['class' => 'form-control']) !!} 
                            </div>
                        </div>
                     
                        <div class='clearfix'></div>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Search</button>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                            <a href="{{ url('admin/specials') }}" class="btn btn-danger btn-block btn-flat">Clear Search</a>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
    <!-- PRODUCT LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>   
                </div>
            </div>
            @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-check"></i> {!! session('success') !!}
            </div>
            @endif
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Coupon Code</th>
                            <th>Effective</th>
                            <th>Expiration</th>
                            <th>Cutoff</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach ($model as $row)
                        <?php
                            $color = '';
                            $status = '';
                            if($row->status=='1')
                            {
                                $color = 'text-green text-bold';
                                $status = 'Active';
                            }
                            elseif($row->status=='0')
                            {
                                $color = 'text-red text-bold';
                                $status = 'Inactive';
                            }
                            if (array_key_exists($row->type, $type)) {
                               $type_name = $type[$row->type];
                            }
                            
                        ?>
                        <tr>
                            <td><?php echo $row->id; ?></td>
                            <td><?php echo $row->name; ?>
                            <td><?php echo $row->coupon_code; ?></td>
                            <td><?php echo ($row->effective!='0000-00-00')?Functions::showDate($row->effective):''; ?></td>
                            <td><?php echo ($row->expiration!='0000-00-00')?Functions::showDate($row->expiration):''; ?></td>
                            <td><?php echo ($row->cutoff_date!='0000-00-00')?Functions::showDate($row->cutoff_date):''; ?></td>
                            <td><?php echo $type_name; ?></td>
                            <td class="{{ $color }}"><i class="fa fa-circle"></i>  <?php echo $status; ?></td>
                            <td>
  
                                <a class="btn btn-primary btn-sm" href="<?php echo url('admin/specials/edit/'.$row->id); ?>" title="Edit"><i class="fa fa-pencil"></i> </a>                       
                                <a data-href="<?php echo url('admin/specials/delete/'.$row->id); ?>" data-target="#confirm-delete"  class="btn btn-danger btn-sm"  data-toggle="modal" title="Delete"><i class="fa fa-trash"></i> </a>

                            
                        </tr>
                        <?php $i++; ?>
                        @endforeach       
                    </tbody>
                </table>
                <?php echo $link; ?>
                 <div><?php echo "Showing ".$model->count()." of ".$model->total(); ?></div>
            </div>
        </div>
    
     <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>Are you sure to delete this Special?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->	

@endsection