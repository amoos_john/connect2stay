<?php
$required="required";
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::label('Name') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group">
    {!! Form::label('Email') !!}
    {!! Form::email('email', null , array('class' => 'form-control',$required) ) !!}
</div>
<div class="form-group">
    {!! Form::label('Password') !!}
    {!! Form::password('password' , array('class' => 'form-control') ) !!}
</div>
<div class="form-group">
    {!! Form::label('Confirm Password') !!}
    {!! Form::password('password_confirmation' , array('class' => 'form-control') ) !!}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="{{ url('admin/users') }}" class="btn btn-danger">Cancel</a>
</div>