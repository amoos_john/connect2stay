<!DOCTYPE html>
<html lang="en" class="broken-image-checker">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="<?php echo (isset($meta_keyword))?$meta_keyword:'Connect2stay'; ?>">
    <meta name="description" content="<?php echo (isset($meta_description))?$meta_description:'Connect2stay'; ?>">
	<!--iPhone from zooming form issue (maximum-scale=1, user-scalable=0)-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>{{ (isset($page_title))?$page_title:'Home' }} | {{Config::get('params.site_name')}}</title>

	<link rel="icon" type="image/png" href="{{ asset('front/images/favicon.png')}}">
	
    <!-- Bootstrap -->
        <link href="{{ asset('front/css/bootstrap.min.css')}}" rel="stylesheet">
	 <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Tangerine">

	<link rel="stylesheet" href="{{ asset('front/css/stylized.css')}}">
	<link rel="stylesheet" href="{{ asset('front/css/colorized.css')}}">
	<link rel="stylesheet" href="{{ asset('front/css/animate.css')}}">
	<link rel="stylesheet" href="{{ asset('front/css/slidenav.css')}}">
	<link rel="stylesheet" href="{{ asset('front/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('front/extralized/jquery-ui.css')}}">
	<link rel="stylesheet" href="{{ asset('front/css/swiper.min.css')}}">
	<link rel="stylesheet" href="{{ asset('front/style.css')}}">


	
	<!-- jQuery -->
	<!--[if (!IE)|(gt IE 8)]><!-->
	  <script src="{{ asset('front/js/jquery-2.2.4.min.js')}}"></script>
          
        <script src="{{ asset('front/extralized/jquery-ui.js')}}"></script>

	   
	 <script src="{{ asset('front/extralized/bootstrap-datepicker.js')}}"></script>
	<!--<![endif]-->


	<!--[if lte IE 8]>
	  <script src="js/jquery1.9.1.min.js"></script>
	<![endif]-->
	
	<!--browser selector-->
	<script src="{{ asset('front/js/css_browser_selector.js')}}" type="text/javascript"></script>

	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
		

  </head>
  <body class="transition nav-plusminus slide-navbar slide-navbar--right">
      
  @include('front/common/header')
  <main id="page-content" class="main-container">
  @yield('content')
  @include('front/common/footer')    
  <a href="" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>
  </main>	    
  
<!--Swiper Slider-->
<!--Bootstrap-->
    <script src="{{ asset('front/js/bootstrap.min.js')}}"></script>
	<!--./Bootstrap-->
	
	<!--Major Scripts-->
    <script src="{{ asset('front/js/viewportchecker.js')}}"></script>
    <script src="{{ asset('front/js/kodeized.js')}}"></script>
	<!--./Major Scripts-->
    <script src="{{ asset('front/js/swiper.jquery.min.js')}}"></script>
		<script>
var swiper1 = new Swiper('.s1', {
        pagination: '.swiper-pagination',
        slidesPerView: '4',
        centeredSlides: false,
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 15,
        autoplay: 2500,
        autoplayDisableOnInteraction: false,
        breakpoints: {
        1024: {	slidesPerView: 3, spaceBetween: 40 },
        768: { slidesPerView: 3, spaceBetween: 30 },
        640: { slidesPerView: 1, spaceBetween: 20 },
        320: { slidesPerView: 1, spaceBetween: 10 }
}
});


var swiper2 = new Swiper('.s2', {
        pagination: '.swiper-pagination',
        slidesPerView: '4',
        centeredSlides: false,
        paginationClickable: true,
        nextButton: '.swiper-button-next2',
        prevButton: '.swiper-button-prev2',
        spaceBetween: 15,
        autoplay: 2500,
        autoplayDisableOnInteraction: false,
        breakpoints: {
        1024: {	slidesPerView: 3, spaceBetween: 40 },
        768: { slidesPerView: 3, spaceBetween: 30 },
        640: { slidesPerView: 1, spaceBetween: 20 },
        320: { slidesPerView: 1, spaceBetween: 10 }
}
});
                                            
var swiper3 = new Swiper('.s4', {
pagination: '.swiper-pagination',
slidesPerView: '1',
centeredSlides: false,
paginationClickable: true,
nextButton: '.swiper-button-next2',
prevButton: '.swiper-button-prev2',
spaceBetween: 15,
autoplay: 2500,
autoplayDisableOnInteraction: false,
breakpoints: {
1024: { slidesPerView: 1, spaceBetween: 40 },
768: { slidesPerView: 1, spaceBetween: 30 },
640: { slidesPerView: 1, spaceBetween: 20 },
320: { slidesPerView: 1, spaceBetween: 10 }
}
});


          
                                       
					
		</script>
<!---<script>
    var swiper3 = new Swiper('.s3', {
              slidesPerView: '6',
              centeredSlides: false,
              nextButton: '.swiper_btn_next',
              prevButton: '.swiper_btn_prev',
              spaceBetween: 15,
              autoplay: 2500,
              width:4200,
              autoHeight :true,
              height:500,
              breakpoints: {
              1024: { slidesPerView: 3, spaceBetween: 40 },
              768: { slidesPerView: 3, spaceBetween: 30 },
              640: { slidesPerView: 1, spaceBetween: 20 },
              320: { slidesPerView: 1, spaceBetween: 10 }
            }
          });

 </script>-->
<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

// var slideIndex = 1;
// showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  //captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
   
	<!--./Swiper Slider -->
	




 <script>




$(document).ready(function() {


    var pageHeight = $(document).height(); 
    var footerHeight = 1400; 
    var targetHeight = pageHeight - footerHeight;
    var windowHeight = $(window).height();
    targetHeight = targetHeight - windowHeight;


    $(window).scroll(function(){ 
        //console.log(windowHeight);
        //console.log($(this).scrollTop());
        //console.log(targetHeight);
        if ( $(this).scrollTop() >= targetHeight ) { 
            //console.log('Target reached!');
            //alert('Target reached!');
            $('#need_help').fadeIn();
        } else { 
            $('#need_help').fadeOut();
            
        } 
    }); 
 jQuery('.form-control.active').attr('checked',true);
;
});


 
</script> 

</body>
</html>