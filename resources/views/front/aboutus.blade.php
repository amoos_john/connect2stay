@extends('front')

@section('content')

<section>
<div class="about-heading"> {{ $content[0]->title }} </div> <!-- abput jeadog end-->
<div class="about-img__image"><img src="{{ asset('front/images/quemsomos.png') }}" alt=""></div>
<div class="container">

<div class="col-md-6 col-sm-10 col-xs-10 about-us">

<?php echo $content[0]->body; ?>

</div>

</div><!--container end-->
</section>
<section class="product-area swiper_slide animated fadeInDown" data-wow-delay="0.3s" id="swiperSlider">
<div class="container">

<h2 style="color:#999999;font-family:sans-serif;">PARTNERS </h2>

<div class="dontfly s1">
        <div class="swiper-wrapper">
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-1.jpg')}}" alt=""></div>
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-2.jpg')}}" alt=""></div>
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-3.jpg')}}" alt=""></div>
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-4.jpg')}}" alt=""></div>
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-5.jpg')}}" alt=""></div>
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-6.jpg')}}" alt=""></div>
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-8.jpg')}}" alt=""></div>
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-9.jpg')}}" alt=""></div>
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-10.jpg')}}" alt=""></div>
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-11.jpg')}}" alt=""></div>
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-12.jpg')}}" alt=""></div>
                <div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-13.jpg')}}" alt=""></div>
        </div>

        <!-- Add Arrows -->
        <div class="swiper-button-next swiper_btn"></div>
        <div class="swiper-button-prev swiper_btn"></div>

</div>

</div>
</section>

@endsection