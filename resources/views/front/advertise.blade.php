﻿@extends('front')

@section('content')
<link rel="stylesheet" href="{{ asset('front/style-extra.css')}}">
    <div class="property-heading1_"> List your property and have great results </div>
 <section>
<div class="container">
<div class="property-list-btn"><a href="{{ url('register-your-property') }}" class="">Click here to list your property</a>
 </div>

</div>
<div class="property-style-bg">
  <div class="accordion-area accordion-plus ">
        
            
            <div class="panel-group animated fadeInUp " id="accordion">
        <div class="panel panel-default active property-style">
            
              <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse" class="collapsed">
              Benefit to list your property with us: </a>
              </h4>
            
            <div id="collapse" class="panel-collapse collapse  pb10">
              <div class="panel-body">The benefit of listing your property with Connect2stay is that you only have to worry about receiving the guests on the day of check in and checking them out, the rest Connect2stay will do the work for you. Connect2stay do not charge to list your property, the rate provided by you will respected and you will receive the payment for the daily rate that you told us. Beside of it your property it’s going to be displayed in the websites of our partners. Do you want more? Take a look at our FAQ page. 
                  <a href="{{ url('faq') }}">"FAQ page"</a>. </div>
            </div>
            </div>
              <div class="panel panel-default active property-style">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="collapsed">
              How to List your property. </a>
              </h4>
            
            <div id="collapse1" class="panel-collapse collapse  pb10">
              <div class="panel-body">Listing your property is easy, simply click the button above "Click here to list your property" and complete the registration of your property, the rest leave with us. </div>
            </div>
            </div>    
      <div class="panel panel-default active property-style">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed">
              Which service do you provide to my property? </a>
              </h4>
            
            <div id="collapse2" class="panel-collapse collapse  pb10">
              <div class="panel-body">Professional Photography<br><br>

Creation of a professional” Headline” for your property<br><br>

Professional property description.<br><br>

Direct marketing<br><br>

Customer support 24/7.<br><br>

Connection with multi-channel. </div>
            </div>
            </div> 
     <div class="panel panel-default active property-style">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed">
              Which type of property can I add? </a>
              </h4>
            
            <div id="collapse3" class="panel-collapse collapse  pb10">
              <div class="panel-body">With connect2stay you can list any type of property. Apartment, House, Chalets, lodging, Boat, only property where the guest can book the all unit. </div>
            </div>
            </div> 

          </div>
      
        
     
        </div>
</div>
</section>

  <section class="product-area" id="swiperSlider">
    <div class="container">
      <div class="col-sm-12  mt40  mb50">
      
    <div class="col-sm-9 pro-slide">
      
      <div class="swiper-container dontfly s4 property-slider ">
        <div class="swiper-wrapper ">
      <div class="swiper-slide pro-sty">
            <div class="col-sm-2"><img src="{{ asset('front/images/anonimo.jpg')}}" alt="... 123">
              <h3>Douglas F. S.</h3></div>
            <div class="col-sm-8"><p>I am listing my property with Connect2stay almost 4 months. I am very happy with the service and attention given by Connect2stay team. I received my third booking and the payment of the booking in advance.</p></div>

          </div>      <div class="swiper-slide  pro-sty">
            <div class="col-sm-2"><img src="{{ asset('front/images/anonimo.jpg')}}" alt="... 123">
              <h3>Raquel A.</h3></div>
            <div class="col-sm-8"><p>I have never rented my property before for a foreign and the first booking that I received was from a Belgium guest. I am really happy to see my property been visible and available to the whole world.</p></div>
 
          </div>      <div class="swiper-slide  pro-sty">
            <div class="col-sm-2"><img src="{{ asset('front/images/anonimo.jpg')}}" alt="... 123">
              <h3>Alessandra S.</h3></div>
            <div class="col-sm-8"><p>I added my apartment to list with Connect2stay and in two weeks I received two booking. Thank you Connect2stay.</p></div>

          </div>      
          
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination hidden"></div>
        
        <!-- Add Arrows -->
       
        
      </div></div>
       <div class="swiper-button-next swiper-button-next2 pro-next"><i class="fa fa-chevron-left"></i></div>
        <div class="swiper-button-prev swiper-button-prev2 pro-prev"><i class="fa fa-chevron-right"></i></div>
</div>
    </div>
 <div class="img-full">
<img src="{{ asset('front/images/anuncie.png')}}" width="100%">

 </div>
 </section>
<style>
.property-list-btn{
  width: 59%;
  display: block;
  margin: 0 auto;
  height: 64px;
  margin-top: 20px;
  margin-bottom: 20px;
  line-height: 64px;
  text-align: center;
      font-family: sans-serif;

}
.property-list-btn a{

    color: #c8b7c2 !important;
    background-color: #681e53;
    font-size: 28px;
    padding: 0px 30px;
     font-family: sans-serif;
 
}
.property-style{
    border-bottom: 1px solid #fff;
    background: #5fa9de;
        font-family: sans-serif;
}
.property-style .panel-title{
background: #5fa9de;
    color: #6a1951;
    padding: 40px 0px;
        font-family: sans-serif;
    text-align: left;
    width: 34%;
    display: block;
    margin: 0 auto;
    position: relative;
    padding-right: 30px;
   }
.property-style .panel-title a{
    background: #5fa9de;
       color: #6a1951;
       display: block;
           font-family: sans-serif;
       font-size: 24px;
       line-height: 33px;}

.accordion-plus .panel-title .panel-default:first-child a:after, .accordion-plus .panel-title a.collapsed:after {
 content: "\f067";
    font-family: FontAwesome;
}


.accordion-plus .panel-title a:after {
    content: "\f068";
    font-family: FontAwesome;
}
.property-style .panel-collapse{
   background: #5fa9de; 
       font-family: sans-serif;
}


.property-style-bg .accordion-plus .panel-title a:after {

    border: 0px;
    border-radius: 0px;
    font-weight: lighter;
    background:#5fa9de ;

    color:#fff;
        position: absolute;
    right: 0;

}
.property-style-bg .panel-body{

    color: #6a1951;
    color: #2c4275;
    padding-bottom: 1em;
    text-align: justify;
    width: 57%;
        font-family: sans-serif;
    margin: 0 auto;
    display: block;
    font-size: 22px;
    line-height: 27px;
}
.property-style-bg .panel-body a{
  color:#fff;
      font-family: sans-serif;
}
.property-slider{
  text-align: center;
      font-family: sans-serif;
}
.property-slider .swiper-slide{

}
.property-style{
  margin-top: 0px !important;
}
.pro-slide{
  float: none;
      font-family: sans-serif;
  display: block;
  margin: 0 auto;
}
.pro-next{
  left: 0px;
      font-family: sans-serif;
  right: auto;
  font-size: 30px;
  color:#000;
  text-shadow: 0 1px 2px rgba(0,0,0,.6);
  background: none;
}
 
.pro-prev{
  right: 0px;
  background: none;
  font-size: 30px;
      font-family: sans-serif;
  color:#000;
  text-shadow: 0 1px 2px rgba(0,0,0,.6);
  background: none;
  left: auto;
} 

.pro-sty h3{    color: #6b6b6b;
    font-weight: bold;
    font-size: 18px;
      font-family: sans-serif;}
.pro-sty p{
  color: #6b6b6b;
    text-align: left;
        font-family: sans-serif;
font-size: 20px;
line-height: 24px;}

.property-heading1_ {
    background: url(front/images/property-bg.png);
    height: 140px;
    line-height: 137px;
    text-align: center;
    margin-top: 0;
    font-size: 60px;
    color: #fff;
    font-weight: bold;
    font-family: 'Tangerine', serif;
    background-size: contain;
    margin-bottom: 5px;

}
.accordion-plus .panel-title .panel-default:first-child a:after, .accordion-plus .panel-title a.collapsed:after {
    content: "\f067";
    font-family: FontAwesome;
}

</style>
@endsection