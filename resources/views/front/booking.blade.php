@extends('front')

@section('content')
<?php

use App\Functions\Functions;

$currency = Config::get('params.currency');
$symbol = $currency["BRL"]["symbol"];
?>
<link href='{{ asset('front/style-extra.css') }}' rel='stylesheet' />
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<style>
    .modal{
        z-index: 9999;
    }
    .bg-col {
        background: rgba(0,0,0,0.1);
    }

    .strip_modal{
        z-index: 9999;
    }
    .strip{
        width: 100%;

    }
    .strip_modal .modal-body{
        padding: 13px;
        margin-bottom: 76px;
    }
    .strip_modal_close{
        top:0px !important;
    }
    .strip_modal .modal-content{
        box-shadow: 0px 0px 0px #fff;
        padding: 0;
    }
    .back-image
    {
        display: block;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        height: 400px;
    }
    .flag-img {
        float: left;
        width: 40px;
        margin-right: 10px;

    }
</style>
<script>
$(document).ready(function () {
    setTimeout(function () {
        $(".card-wrap").addClass("loaded");
    }, 3000);
});
</script>

<div id="myModalInfo" class="modal fade z10 bg-col" role="dialog">
    <div class="modal-dialog modal-area">
        <!-- Modal content-->
        <div class="modal-content style-modal no-shadow pt0">
            <div class="modal-header pb0">

                <h4 class="modal-title "><i class="fa fa-list" aria-hidden="true"></i>
                    Change Info</h4>
            </div>
            <div class="modal-body modal-style-body ">
                <div class="clear_both"></div>
                <form action="get" name="book_form" id="book_form"> 
                    <div class="input-group" style="width:100%">
                        <h2>Check-In</h2>
                        <input class="form-control" value="{{ $start_date }}" id="date6" name="start_date" placeholder="Check-In" type="text" required=""/>

                    </div>


                    <div class="input-group" style="width:100%">
                        <h2>Check-Out</h2>
                        <input class="form-control" value="{{ $end_date }}" id="date5" name="end_date" placeholder="Check-Out" type="text" required=""/>

                    </div>

                    <div class="clear_both">
                        <div class="col-lg-12 col-ms-12 col-sm-12 no-padding modal-style-bottom">


                            <div class="input-group col-lg-6 col-ms-6 col-sm-6">
                                <h2>Guests</h2>
                                <input type="text" class="form-control" value="{{ $guest }}" name="guest" placeholder="Guests" required=""/>

                            </div>


                            <div class="input-group col-lg-5 col-ms-5 col-sm-5" style="margin-left:20px;">

                                <h2>Children</h2>
                                <input type="text" class="form-control" value="{{ $children }}" name="children" placeholder="Children"/>

                            </div>



                        </div>
                    </div>
                </form>
            </div>
            <div class="clear_both"></div>
            <div class="modal-footer pt0">
                <button type="button" class="btn btn-default close-btn"  data-dismiss="modal" style="border:0px;">Close</button>
                <button type="button" id="updateinfo" class="btn btn-default btn-primary">Update</button>
            </div>
        </div>

    </div>
</div>
<section class="detail">
    <div class="">
        <div class="center">


            @if(count($gallery_images)>0)
<?php $i = 1; ?>
            @foreach($gallery_images as $image)    

            <div class="swiper-slide item-slider" ><img src="{{ asset('/'.$image->url)}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(<?php echo $i; ?>)"></div>


<?php $i++; ?>  
            @endforeach
            @endif

        </div>

        <!--<div class="swiper-button-next swiper_btn_next"></div>
         <div class="swiper-button-prev swiper_btn_prev"></div>
        -->
    </div>

    <div id="myModal" class="modal slider_detail">
        <span class="close cursor" onclick="closeModal()">&times;</span>
        <div class="modal-content">
            <div class="detail-slider"> 

                @if(count($gallery_images)>0)
                @foreach($gallery_images as $image)

                <div class="mySlides">
                    <div class="numbertext"></div>
                    <img src="{{ asset('/'.$image->url)}}" style="width:100%;">
                </div>
                @endforeach
                @endif

            </div>
            <div class="controls">
                <a class="mprev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="mnext" onclick="plusSlides(1)">&#10095;</a>
            </div>
            <div class="caption-container">
                <p id="caption"></p>
            </div>

            <div class="col-slider">
                @if(count($gallery_images)>0)
<?php $i = 1; ?>
                @foreach($gallery_images as $image)    
                <div class="column">
                    <img class="demo cursor" src="{{ asset('/'.$image->url)}}" style="width:100%" onclick="currentSlide(<?php echo $i; ?>)" alt="{{ $image->image_caption }}">

                </div>
<?php $i++; ?>  
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div><!--container end-->
</section>
<div class="container">

    {!! Form::open(array( 'class' => 'form','id' => 'myform','url' => 'booking/insert', 'files' => true)) !!}

    <section>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-area no-padding">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 form-sec-one">

                @include('admin/commons/errors')     
                <h1>1. GUEST INFORMATION </h1>

                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="first_name">First Name:*</label>
                    <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control" id="first_name" maxlength="50" required/>
                </div>
                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="last_name">Last Name:*</label>
                    <input type="text" name="last_name" value="{{ old('last_name') }}"  class="form-control" id="last_name" maxlength="50" required/>
                </div>


                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="email">Email:*</label>
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="email" maxlength="50" required/>
                </div>
                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="phone">Phone:*</label>
                    <input type="tel" name="phone" value="{{ old('phone') }}" class="form-control" id="phone" maxlength="50" required/>
                </div>


                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="hear_about">How Did You Hear About Us?:</label>
                    <select class="form-control" name="lead_source" id="lead_source">
                        <option value="">How Did You Hear About Us?</option>
                        @foreach($lead_source as $key=>$source)
                        @if($key!='')
                        <option value="{{ $key }}">{{ $source }}</option>
                        @endif
                        @endforeach

                    </select>  
                </div>
                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="request">Special Requests:</label>
                    <input type="text" name="special" class="form-control" id="special">
                </div>

                <div class="clear_both"> </div>
                <h1>2. Billing Address </h1>


                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="country">Counrty:</label>
                    <select name="country" class="form-control" id="country">
                        <option value="">Select Country</option>
                        @foreach($countries as $key=>$country)
                        <option value="{{ $key }}">{{ $country }}</option>
                        @endforeach

                    </select>
                </div>
                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="street1">Street Line 1:</label>
                    <input type="text" name="address" class="form-control" id="address" value="{{ old('address') }}" maxlength="150"/>
                </div>

                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="street2">Street Line 2:</label>
                    <input type="text" name="address2" class="form-control" id="address2" value="{{ old('address2') }}" maxlength="150"/>
                </div>
                <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="street1">City/Town:</label>
                    <input type="text" name="city" class="form-control" id="city" value="{{ old('city') }}" maxlength="30"/>
                </div>

                <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="hear_about">State/Province:</label>
                    <input type="text" name="state" class="form-control no-padding" id="state" maxlength="30" value="{{ old('state') }}" style="width:100%; display:inline-block; vertical-align:top;"></div>

                <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12 form_top">
                    <label for="hear_about">Postal Code:</label>
                    <input type="text" name="postal_code" class="form-control" id="postal_code" maxlength="20" value="{{ old('postal_code') }}" style="width:100%; display:inline-block; vertical-align:top; margin-left:4px">

                </div>
                <div class="clear_both"> </div>
                <h1>3. Payment Method*</h1>

                <div id="myRadioGroup">
                    <div class="social-connect">
                        <p for="input1">Paypal</p>
                        <input type="radio" name="payment_type" id="input1" value="1" <?php echo (old('payment_type') == 1) ? 'checked' : ''; ?> required/>
                        <div class="line-co"></div>
                        <p for="input2">Stripe</p>
                        <input type="radio" name="payment_type" id="input2" value="2" <?php echo (old('payment_type') == 2) ? 'checked' : ''; ?> data-toggle="modal" data-target="#myModalHorizontal_stripe"/>
                    </div>
                    <div id="Payment1" class="desc-img">
                        <img src="{{ asset('front/images/paypal_logo.jpg') }}" alt="">
                    </div>
                    <div id="Payment2" class="desc-img" style="display: none;">
                        <img src="{{ asset('front/images/social.png') }}" data-toggle="modal" data-target="#myModalHorizontal_stripe" alt="">

                    </div>
                </div>

                <div class="clear_both"> </div>

                <h1>4. Accept Terms and Conditions </h1>
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 form_top">

                    <input type="checkbox" class="form-control" id="terms" required style=" display:inline-block; vertical-align:top; margin-left:4px; margin-top:3px;">
                    <label for="terms" style="font-size:15px; font-weight:600;  display:initial; vertical-align:top; margin-left:4px; letter-spacing:1px;">I have read and accept the terms and conditions:
                        <a href="{{ url('rentalpolicy') }}" target="_blank" class="a-color">Termos e condições </a></label>


                </div>

                <div class="clear_both"></div>

                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding form_error">

                    <div id="message"></div>
                    <div class="book-btn"></div>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-sec-two">

                <div id="load" style="position: relative;"></div> 
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding form_error2" id="message-info">
                    <!-- Unfortunately, this property is not available for your selected period. Please try another property.
                    -->    
                </div>
                <div id="booking-info">
                    @include('front/bookinginfo')
                </div>

            </div>    

        </div>
        <!-- Modal -->
        <div class="modal fade strip_modal" id="myModalHorizontal_stripe" tabindex="-1" role="dialog" 
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header" style="border-bottom:0px solid #fff;">
                        <button type="button" class="close strip_modal_close" 
                                data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">

                        </h4>
                    </div>

                    <!-- Modal Body -->
                    <div class="modal-body">


                        <div class="container-fluid grid strip">

                            <div class="row pull-center">
                                <div class="col-md-12">

                                    <div class="well">

                                        <div class="row card">
                                        </div>

                                        <br/>

                                        <div class="row-fluid">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Credit Card Number </label>
                                                    <input type="text" name="card_no" value="{{ old('card_no') }}"  class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Expiration</label>

                                                    <input type="text" placeholder="MM/YY" name="ccExpiry"  value="{{ old('ccExpiry') }}" class="form-control" />

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row-fluid">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <input type="text" name="card_name" value="{{ old('card_name') }}" class="form-control" />
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">

                                                    <label>CVC </label>

                                                    <input type="text" name="cvcNumber" value="{{ old('cvcNumber') }}" class="form-control" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row ">

                                            <div class="col-md-12 text-right">
                                                <button type="button" data-dismiss="modal" class="btn btn-success">Submit</button>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>  

                </div>
            </div>
        </div>
    </section>
</div>
{!! Form::close() !!}		
</div><!--container  end-->			
<script>
    var id = '&id=<?php echo $id; ?>';
    getBooking(id);
    myform();
    $(document).ready(function () {
        $("input[name$='payment_type']").click(function () {
            var test = $(this).val();

            $("div.desc-img").hide();
            $("#Payment" + test).show();
        });

        $("body").on("click", "#updateinfo", function () {

            $('#myModalInfo').modal('toggle');


            $(".blockUI").css("display", "block");

            $.ajax({
                type: "GET",
                url: "{{ url('bookinginfo') }}",
                data: $("#book_form").serialize() + id,
                success: function (data) {

                    $(".blockUI").css("display", "none");
                    $('#booking-info').html(data);

                },
                error: function (errormessage) {

                    alert(errormessage);
                }
            });

            getBooking(id);

        });


    });
    function getBooking(id)
    {
        $.ajax({
            type: "GET",
            url: "{{ url('detail/bookval') }}",
            data: $("#book_form").serialize() + id,
            success: function (data) {
                if (data == '1')
                {
                    $('#message').html("");
                    $('#message-info').html("");
                    $('.book-btn').html('<button type="button" id="btn-book" class="btn btn-book" disabled>Reserve Now</button>');
                } else
                {
                    $('.book-btn').html("");
                    $('#message').delay(500).fadeIn();
                    $('#message').html(data);
                    $('#message-info').delay(500).fadeIn();
                    $('#message-info').html(data);
                }

            },
            error: function (errormessage) {
                alert("Error");


            }
        });
    }
    $(document).on('click', '#btn-book', function () {

        if ($("#first_name").val() == '') {
            alert("Please Fill out all required fields");
            return false;
        } else if ($("#last_name").val() == '')
        {
            alert("Please Fill out all required fields");
            return false;
        } else if ($("#email").val() == '')
        {
            alert("Please Fill out all required fields");
            return false;
        } else if ($("#phone").val() == '')
        {
            alert("Please Fill out all required fields");
            return false;
        } else if ($("#payment_type").val() == '')
        {
            alert("Please Fill out all required fields");
            return false;
        } else if ($("#terms").val() == '')
        {
            alert("Please Fill out all required fields");
            return false;
        } else
        {
            //noBack();

            $('.book-btn').html("");
            $('.book-btn').html('<img class="loading-img" src="<?php echo url('front/images/loading2.gif'); ?>" />');

            document.getElementById("myform").submit();
        }

    });


    function noBack() {
        window.history.forward();
    }

    function myform()
    {
        var empty = false;

        if ($("#first_name").val() == '') {
            empty = true;
        } else if ($("#last_name").val() == '')
        {
            empty = true;
        } else if ($("#email").val() == '')
        {
            empty = true;
        } else if ($("#phone").val() == '')
        {
            empty = true;
        } else if ($("#payment_type").val() == '')
        {
            empty = true;
        }



        if (empty) {
            $("#btn-book").attr('disabled', 'disabled');

        } else {
            $('#btn-book').removeAttr('disabled');

        }
    }
    $("body").load(function () {
        form();
    });

    $('#myform :input').keyup(function () {
        myform();
    });

</script>
<script>
    $(document).ready(function () {
        var array = [<?php echo $dates_not_avail ?>];
        var start_date = $('input[name="start_date"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        start_date.datepicker({
            format: 'yyyy-mm-dd',
            startDate: new Date(),
            container: container,
            todayHighlight: true,
            autoclose: true,
            active: true,
            datesDisabled: array

        })

        var end_date = $('input[name="end_date"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        end_date.datepicker({
            format: 'yyyy-mm-dd',
            startDate: new Date(),
            container: container,
            todayHighlight: true,
            autoclose: true,
            active: true,
            datesDisabled: array

        })
    })
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/card/1.3.1/js/card.min.js"></script>

<script>
    new Card({
        form: '.form',
        container: '.card',
        formSelectors: {
            numberInput: 'input[name=card_no]',
            expiryInput: 'input[name=ccExpiry]',
            cvcInput: 'input[name=cvvNumber]',
            nameInput: 'input[name=card_name]'
        },

        width: 390, // optional — default 350px
        formatting: true,

        placeholders: {
            card_no: '•••• •••• •••• ••••',
            card_name: 'Full Name',
            ccExpiry: '••/••',
            cvvNumber: '•••'
        }
    });

    $(document).ready(function () {
        $("input[type=tel]").keypress(function (event) {
            //if the letter is not digit then display error and don't type anything
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;
            if (charCode == 45)
            {
                return true;
            } else if (charCode > 31 && (charCode < 48 || charCode > 57))
            {
                return false;
            } else
            {
                return true;
            }


            /*if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
             return false;
             }*/
        });
        $("input[name=first_name],input[name=last_name],input[name=special],input[name=city],input[name=state]").keypress(function (event) {
            var inputValue = event.charCode;
            if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
                event.preventDefault();
            }
        });
        $('.center').slick({
            centerMode: true,
            centerPadding: '190px',
            slidesToShow: 1,
            nextArrow: '<i class="fa fa-chevron-circle-right fa-right" aria-hidden="true" ></i>',
            prevArrow: '<i class="fa fa-chevron-circle-left fa-left" aria-hidden="true"></i>',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    });
</script>

  <!--<script>
    $(function() {
  var expMonthAndYear = $('input[name=expiry]').val().split(" / ");
  Stripe.card.createToken({
              number: $('input[name=number]').val(),
              cvc: $('input[name=cvv]').val(),
              exp_month: expMonthAndYear[0],
              exp_year: expMonthAndYear[1]
            }, stripeResponseHandler);
});

var stripeResponseHandler = function(status, response) {
  if (response.error) { 
    // Show appropriate error to user
  } else {
    // Get the token ID:
    var token = response.id;
    // Save token mapping it to customer for all future payment activities
  }
}
  </script>-->
@endsection