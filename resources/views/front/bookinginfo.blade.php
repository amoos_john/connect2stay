<?php
use App\Functions\Functions;

$currency= Config::get('params.currency');
$symbol=$currency["BRL"]["symbol"];
?>
<style>
td.disabled.day {
    opacity: .2;
}

</style>
<div id="statement-details" class="modal fade z10 bg-col" role="dialog">
  <div class="modal-dialog modal-area">
    <!-- Modal content-->
    <div class="modal-content style-modal no-shadow pt0">
      <div class="modal-header pb0">
        <h4 class="modal-title m_left">Charges</h4>
      </div>
        <div class="modal-body modal-style-body">
           <div id="charges-modal" class="item row-fluid">
             <h5>Rent</h5>
             <p>{{ $nights }} Nights @ <?php echo $symbol.''.Functions::MoneyFormat($daily); ?> <small>{{ $checkin }}-{{ $checkout }}</small><span class="pull-right"><?php echo $symbol.''.Functions::MoneyFormat($rent); ?></span></p>
           </div>
            <hr>
            <div id="fees-modal" class="item row-fluid">
                                <h5>Fees</h5>
                @foreach($feestaxes as $feetax)
                
                <p>{{ $feetax->public_name }} 
                        <span class="pull-right"><?php echo $symbol.''.Functions::MoneyFormat($feetax->amount); ?> </span></p>
               @endforeach
            </div>
                    
     </div>
     
      <div class="clear_both"></div>
      <div class="modal-footer pt0">
        <button type="button" class="btn btn-danger close-btn"  data-dismiss="modal" style="border:0px;">Close</button>
      </div>
    </div>

  </div>
</div>

  <div class="clear_both"> </div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding">
<i class="fa fa-calendar" aria-hidden="true"></i>
<h3>Check-In</h3>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding blue-fonts--date">
{{ $checkin }}
<input type="hidden" name="checkin" value="{{ $start_date }}" />
  </div>

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding">
<i class="fa fa-calendar" aria-hidden="true"></i>
<h3>Check-Out</h3>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding blue-fonts--date">
{{ $checkout }}
<input type="hidden" name="checkout" value="{{ $end_date }}"/>
  </div>


 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding">
<i class="fa fa-users" aria-hidden="true"></i>

<h3>Guests</h3>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding blue-fonts--date">
{{ $guest }}
<input type="hidden" name="guest" value="{{ $guest }}"/>
  </div>

   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding">
<i class="fa fa-users" aria-hidden="true"></i>

<h3>Children</h3>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding blue-fonts--date">
{{ $children }}
<input type="hidden" name="children" value="{{ $children }}"/>
  </div>

<div class="btn btn-default info-btn"><a href="#myModalInfo" role="button" class="btn" data-toggle="modal">CHANGE INFO</a></div>
<div class="blank_div">

</div>


<p class="black-fonts mb0">Charges <small class="charges-detail"><a href="#statement-details" data-target="#statement-details" data-toggle="modal">(Details)</a></small></p>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding">

<h3>Rent</h3>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding blue-fonts--date">
<?php echo $symbol.''.Functions::MoneyFormat($rent); ?> 
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding ">

<h3>Taxes & Fees</h3>
  </div>

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 blue-fonts no-padding blue-fonts--date">
<?php echo $symbol.''.Functions::MoneyFormat($otherfees); ?> 
  </div>
<div class="clear_both"></div>
  <div class="blue-box col-sm-12 pb30">
  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 white-fonts p0">
<h1>Total</h1>
  </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 white-fonts p0 b-price">
<h1><?php echo $symbol.' '.Functions::MoneyFormat($total); ?> </h1>
<input type="hidden" name="property_id" value="{{ $id }}"/>
<input type="hidden" name="key" value="{{ $keys }}"/>
<input type="hidden" name="grand_total" value="{{ $total }}"/>
  </div>

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 white-fonts p0">
Amount Due Now
  </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 white-font p0 b-price">
<?php echo $symbol.' '.Functions::MoneyFormat($total); ?>


  </div>
</div>
<div class="clear_both"></div>

<div class="question_area">
<div class="question">
<h2>Have a Question?</h2>
</div>

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 black-fonts no-padding pull-left">
<p>Phone:</p>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 black-fonts no-padding black-fonts--date pull-right">
      <p class="mb20 text-left"><img src="{{ asset('front/images/brazil-flag.png') }}" class="flag-img" width="40px" height="40px"/> +55 11 94839 7818</p>
      <p class="mb20 text-left"><img src="{{ asset('front/images/netherlands-flag.jpg') }}" class="flag-img" width="40px" height="40px"/> +31 6218333 62</p>
  </div>
<div class="clear_both"> </div>

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 black-fonts no-padding pull-left">
<p>Email</p>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  no-padding black-fonts--date pull-right">
      <p><a href="mailto:info@connect2stay.com" class="black-fonts">info@connect2stay.com</a></p>
  </div>



</div><!--question_area end-->

<div class="no-padding ftr_img--text">
  <h4 class="black-fonts">We accept the following payment:</h4>
<img src="{{ asset('front/images/ps.png') }}" alt="Payment">
</div>



<div class="blockUI" style="display:none;">
	<div class="blockUI blockOverlay" style="z-index: 1000; border: none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; background-color: rgb(0, 0, 0); opacity: 0.6; cursor: wait; position: absolute;">
	</div>
	<div class="blockUI blockMsg blockElement" style="z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 30%; top: 152.5px; left: 145px; text-align: center; color: rgb(0, 0, 0); border: 3px solid rgb(170, 170, 170); background-color: rgb(255, 255, 255); cursor: wait;">
           
        </div>
</div>