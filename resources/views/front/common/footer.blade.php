<footer>
    <section class="ftr-area ftr--blind" id="footer">
        <div class="container">

            <div class="col-sm-12 ftr-area--top no-padding">
                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 no-padding">
                    <a href="#"><div class="ftr-logo "><img src="{{ asset('front/images/flogo.png')}}" alt="ftr logo" /></div></a>


                </div>

                <div class="col-lg-10 col-md-5 col-sm-12 col-xs-12 follow pull-right">
                    <ul>
                        <li><a href="https://www.facebook.com/connect2stay" target="_blank"><i class="fa fa-facebook"></i></a></li>

                        <li><a href="https://www.instagram.com/connect2stay/" target="_blank"><i class="fa fa-instagram"></i></a></li>

                    </ul>


                </div>



            </div>
            <div class="col-sm-12 ftr-area--top no-padding">
                <div class="ftr__box ftr__links col-sm-5 clrlist no-padding">

                    <ul>
                        <li><a href="{{ url('about-us') }}">About Connect2Stay</a></li>
                        <li><a href="{{ url('how-it-works') }}">How it works</a></li>
                        <li><a href="{{ url('advertise') }}">List your property</a></li>
                        <li><a href="{{ url('faq') }}">FAQ</a></li>
                        <li><a href="{{ url('contact') }}">Contact</a></li>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="ftr__box ftr__links col-sm-4  clrlist  no-padding">

                    <ul>
                        <li><a href="#"><img src="{{ asset('front/images/brazil-flag.png') }}"  width="40px" height="40px"/> +55 11 94839 7818</a></li>
                        <li><a href="#"><img src="{{ asset('front/images/netherlands-flag.jpg') }}" width="40px" height="40px"/> +31 6218333 62</a></li>

                    </ul>
                </div>
                <div class="ftr__box ftr__links col-sm-3  clrlist  no-padding ftr_img--text">
                    <img src="{{ asset('front/images/ps.png') }}" alt="Payment Method">
                </div>

            </div>

            <div class="clear_both"></div>
            <div class="copyright mt20">

                <div class="p-left pull-left ">© <?php echo date('Y'); ?> <a href="{{ url('/') }}">Connect2stay</a> All Rights Reserved.</div>
                <div class="p-right pull-right ">Designed & Developed by <a href="http://golpik.com/" target="_blank">GOLPIK</a></div>
            </div>


    </section>




</footer>