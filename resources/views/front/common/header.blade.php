<section class="hdr-area hdr-nav hdr--sticky cross-toggle bg-white mobile-section" data-navitemlimit="7">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation" id="slide-nav">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<a class="navbar-brand" href="./"><img src="{{ asset('front/images/logo.png')}}" alt="logo" class="broken-image"/></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="slidemenu">
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					  <ul class="nav navbar-nav navbar-main navbar-move">
						<li><a href="{{ url('about-us') }}">About Us</a></li>
						<li><a href="{{ url('advertise')}}">List Your Property</a></li>
						<li><a href="{{ url('admin/login') }}">Login</a></li> 
                                                     <!--<div style="width:20px;height:20px;display:inline-block;vertical-align:top;"> <img src="{{ asset('front/images/login.png')}}" alt=""> </div> -->
						
					  </ul>
					  
			

					</div><!-- /.navbar-collapse -->
				</div>
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</section>


<header class="header">
	<section class="hdr-area hdr-nav hdr--sticky cross-toggle bg-white" data-navitemlimit="7">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation" id="slide-nav">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header logo">
					<a class="navbar-brand" href="{{url('/')}}"><img src="{{ asset('front/images/logo.png')}}" alt="logo" class="broken-image"/></a>
				</div>





				<div class="form-group has-feedback pull-left search" style="display:none;">
				<a href="#" id="">						
							<label><i class="glyphicon glyphicon-search"></i></button>
							<label>Search</label>
						</a>
						</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="slidemenu" class="side_links pull-right">
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					  <ul class="nav navbar-nav navbar-main pull-right top_links top__links">
						<li><a href="{{url('about-us')}}">About Us</a></li>
						<li><a href="{{url('advertise')}}">List Your Property</a></li>
						<li><a href="{{url('admin/login')}}">Login</a></li> <!--<div> <img src="{{ asset('front/images/login.png')}}" alt=""> </div>-->
					  </ul>
					</div><!-- /.navbar-collapse -->
				</div>
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</section>
</header>