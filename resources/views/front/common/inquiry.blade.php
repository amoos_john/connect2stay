<!-- Modal -->
<div class="modal fade strip_modal" id="myModalHorizontal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close strip_modal_close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Inquiry Now
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <div id="messages"></div>
                <form  method="post" name="inquiry_frm" id="inquiry_frm">
 
                    <input type="text" name="full_name" class="form-control"
                           placeholder="Name*" required=""/>
                      <input type="email" name="email" class="form-control"
                       placeholder="Email*" required=""/>
                      <input type="tel" name="phone" class="form-control"
                       placeholder="Phone*" required=""/>
                      <input type="text" name="checkin"  class="form-control"
                       placeholder="Check In*" id="date9" required=""/>
                      <input type="text" name="checkout" class="form-control"
                       placeholder="Check Out*" id="date10" required=""/>
                      <input type="number" min="2" name="guests" class="form-control"
                       placeholder="Guests*" required/>
                      <input type="number" name="childrens" class="form-control"
                       placeholder="Children"/>
                      <select class="form-control"  name="source">
                        <option value="">How Did You Hear About US?</option>

                        <option>0.facebook.com</option>
                        <option>9Flats</option>
                        <option>AirBnB</option>
                        <option>AlphaHolidayLettings.com</option>
                        <option>Always on Vacation</option>
                        <option>Amazing Accom - Luxury Holiday Houses</option>
                        <option >Apartment Barcelona</option>
                        <option >Atraveo</option>
                        <option >Barcelona Point</option>
                        <option>Be Mate</option>
                        <option>bedandbreakfast.com</option>
                        <option>Bing</option>
                        <option>bookabach.co.nz</option><option>BOOKING.COM</option><option>booktmail.com</option><option>Casamundo</option><option>Cities Reference</option><option>City Getaway</option><option >Despegar.com</option><option>Divino Villas</option><option>E-Domizil</option><option>Emailed Link</option><option>Escalea.com</option><option>Flat4Day.com</option><option>Flipkey</option><option >Flyer</option><option >Friday Flats</option><option>Friend</option><option >Friendly Rentals</option><option >Furnished Rentals</option><option >Get Me An Apartment</option><option >google</option><option>google</option><option >google</option><option>google</option><option>GowithOh.com</option><option>Habitat Apartments</option><option 1360877">HitRental</option><option 1360840">Holiday Velvet</option><option>HolidayApartments.co.za</option><option>Holidu.es</option><option >homeaway.gr</option><option>homeaway.ie</option><option >homeaway.pl</option><option>homeaway.ru</option><option >homeaway.tr</option><option >homyspace.com</option><option >HouseTrip</option><option >I'm a previous customer</option><option>itravex.com</option><option>La Comunity</option><option >Locasun.fr</option><option>londonchoice.com</option><option >Loving Apartments</option><option>Magazine</option><option >Magic Event</option><option >Migoa</option><option 1360874">OK Apartment</option><option >Only Apartments </option><option>Other Website</option><option >Packlate</option><option>Private Homes</option><option 1360845">Rated Apartments</option><option 1360878">RedAwning</option><option >Rent The Sun</option><option>RentalsCombined</option><option>Rentxpress</option><option>Roomorama</option><option>Spain Holiday</option><option>stayz.com.au</option><option >Tourico Holidays</option><option>travelmob.com</option><option>Travelopo.com</option><option >TravelStaytion.com</option><option>VacAgent</option><option >vacationrentals.com</option><option >Vive Unique</option><option >VRguest.com</option><option >Way To Stay</option><option>WikkiWi.com</option><option>Wimdu</option><option>Yahoo</option><option>yesbookit.com</option><option>Zhubaijia.com</option><option>abritel.fr</option><option >aluguetemporada.com.br</option><option >fewo-direkt.de</option><option >HomeAway (Email Leads)</option><option>homeaway.at</option><option >homeaway.co.uk</option><option >homeaway.com</option><option>homeaway.com.ar</option><option >homeaway.dk</option><option>homeaway.es</option><option>homeaway.fi</option><option >homeaway.it</option><option >homeaway.nl</option><option >homeaway.pt</option><option >homeaway.se</option><option >homelidays.com</option><option >homelidays.es</option><option>homelidays.it</option><option >indicação</option><option >outro</option><option>ownersdirect.co.uk</option><option >vrbo.com</option>

                      </select>
                    <textarea name="comments" class="form-control textarea_" placeholder="Comments"></textarea>

                  <button type="button" id="inquiry_now" class="btn btn-default submit__btn">Submit</button>
           </form>
  
                
            </div>
            
              </div>  
      
        </div>
</div>