<script type="text/javascript">
        function SearchResult(search)
        {
            
            $("#search-result").html("");
            $('#load').html('<img style="img-center img-responsive" src="<?php echo url('front/images/loading2.gif');?>" />');
            //$("#search-result").html("<i class='fa fa-spinner fa-spin fa-4x'></i>");
            $.ajax({
                type: 'GET',
                data: search,
                url: '{{ url("result") }}',
                success: function(data) 
                {
                    $("#load").html("");
                    $("#search-result").html(data);
                }
            });   
        }
        function RSearchUrl(name,value,name2,value2) {
                var originalURL="<?php echo url("rentals/". ((Request::getQueryString() ? ('?' . Request::getQueryString()) : ''))); ?>";
                
                var url  = removeParam(name, originalURL);
                url  = removeParam(name2, url);
                removeParam('page', originalURL);
                
                var title = "Page 1";
                var surl="<?php echo (Request::getQueryString() ? ( Request::getQueryString()) : ''); ?>";
                
                var datasend = url+"&"+name+"="+value+"&"+name2+"="+value2;
               
                var search = surl+"&"+name+"="+value+"&"+name2+"="+value2;
		if (typeof (history.pushState) != "undefined") {
			var obj = { Title: title, Url: datasend };
			//window.history.pushState(obj, obj.Title, obj.Url);
                        SearchResult(search);
		} else {
			alert("Browser does not support HTML5.");
		}
	}
	function SearchUrl(name,value) {
                var originalURL="<?php echo url("rentals/". ((Request::getQueryString() ? ('?' . Request::getQueryString()) : ''))); ?>";
                
                if(name=='page')
                {
                     var originalURL="<?php echo (Request::getQueryString() ? ( Request::getQueryString()) : ''); ?>";   
                     var url  = removeParam(name, originalURL,value);
                }
                else
                {
                    var url  = removeParam(name, originalURL,value);
                }
                 
                var title = "Page 1";
                var surl="<?php echo (Request::getQueryString() ? ( Request::getQueryString()) : ''); ?>";
                
                var datasend = url+"&"+name+"="+value;
               
                var search = surl+"&"+name+"="+value;
		if (typeof (history.pushState) != "undefined") {
			var obj = { Title: title, Url: datasend };
			//window.history.pushState(obj, obj.Title, obj.Url);
                        SearchResult(search);
		} else {
			alert("Browser does not support HTML5.");
		}
	}
        
        function removeParam(key, sourceURL,value) {
         var rtn = sourceURL.split("?")[0],
             param,
             pvalue,
             params_arr = [],
             queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
         if (queryString !== "") {
             params_arr = queryString.split("&");
             for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                 param = params_arr[i].split("=")[0];
                 pvalue = params_arr[i].split("=")[1];
                 //console.log(pvalue);
                 if (param === key) {
                     params_arr.splice(i, 1);
                 }
             }
             
             rtn = rtn + "?" + params_arr.join("&");
         }
         return rtn;
     }
     
 function searchTags(id)
 {
     var originalURL="<?php echo (Request::getQueryString() ? ( Request::getQueryString()) : ''); ?>";
   
     var checkboxes = $('.all input[type="checkbox"]');
     var page = removeParam('page', originalURL);
     var url = removeParam('tags', originalURL,id)
     var search =  $("#search").serialize()+"&"+checkboxes.serialize() + location.hash;
     SearchResult(search);
     
     
 }
 function searchPagination(value)
 {
     var originalURL="<?php echo (Request::getQueryString() ? ( Request::getQueryString()) : ''); ?>";
   
     var url = removeParam('page', originalURL,value);
     var search =  url+"&page="+ value;
     SearchResult(search);
     
     
 }


    
</script>