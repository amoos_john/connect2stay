<script>
    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
    var markers = [];
    var a = document.getElementById("lat");
    function initMap() {
    var haightAshbury = {lat: <?php echo ($property[0]->latitude != '') ? $property[0]->latitude : '0'; ?>, lng: <?php echo ($property[0]->longitude != '') ? $property[0]->longitude : '0'; ?>};
    map = new google.maps.Map(document.getElementById('map'), {
    zoom: 30,
            center: haightAshbury,
    });
    // This event listener will call addMarker() when the map is clicked.


    // Adds a marker at the center of the map.
    addMarker(haightAshbury, '45.4311700', '-75.6911200');
    }

    // Adds a marker to the map and push to the array.
    function addMarker(location, lat, long) {
    clearMarkers();
    markers = [];
    var image = "{{ url('images/map.png')}}";
    var marker = new google.maps.Marker({
    position: location,
            map: map,
            icon: image

    });
    markers.push(marker);
    //var latitude_text_field = document.getElementById("lat");
    //latitude_text_field.value = lat;

    // var longitude_text_field = document.getElementById("long");
    //longitude_text_field.value = long;

    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
    }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
    setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
    setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
    clearMarkers();
    markers = [];
    }


</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpxQvcYyGxCSZWDof-C1qF6XT_YETCfJQ&libraries=places&callback=initMap"
></script>
<!--Swiper Slider-->

<!--FIXED DETAI LSECTION -->

<script>
    $(document).ready(function(){


    var start_date = $('#date9'); //our date input has the name "date"
    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    start_date.datepicker({

    format: 'yyyy-mm-dd',
            startDate: new Date(),
            container: container,
            todayHighlight: true,
            autoclose: true,
            active:true


    });
    var end_date = $('#date10'); //our date input has the name "date"
    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    end_date.datepicker({
    format: 'yyyy-mm-dd',
            startDate: new Date(),
            container: container,
            todayHighlight: true,
            autoclose: true,
            active:true

    });
    $('#date9').keydown(function(event) {
    return false;
    });
    $('#date10').keydown(function(event) {
    return false;
    });
    });
    function getBooking(id)
    {
    $.ajax({
    type: "GET",
            url: "{{ url('detail/bookval') }}",
            data: $("#book_form").serialize() + id,
            success: function(data){
            if (data == '1')
            {
            $('#message').html("");
            $('.book-btn').html('<button class="booknow" id="booknow" type="button">Book Now</button>');
            }
            else
            {
            $('.book-btn').html("");
            $('#message').delay(500).fadeIn();
            $('#message').html("<button class='btn btn-primary btn-detail'>" + data + "</button>");
            }

            },
            error: function(errormessage) {
            //you would not show the real error to the user - this is just to see if everything is working
            $('#message').delay(500).fadeIn();
            $('#message').html(errormessage);
            //$('#message').delay(8000).fadeOut(); 
            }
    });
    }

    var id = '&id=<?php echo $id; ?>';
    getBooking(id);
    $(document).ready(function () {
    $("body").on("click", "#update", function(){
    getBooking(id);
    });
    $("body").on("click", "#inquiry_now", function(){

    var id = '&id=' +<?php echo $id; ?>;
    var token = "&_token=<?php echo csrf_token() ?>";
    $.ajax({
    type: "POST",
            url: "{{ url('booking/inquiry') }}",
            data: $("#inquiry_frm").serialize() + token + id,
            success: function(data){
            if (data == '0')
            {
            $("#inquiry_frm").find("input[type=text],input[type=tel],input[type=email],input[type=number], textarea").val("");
            var successHtml = '<div class="alert alert-success alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a>';
            successHtml += "Thank you, your request has been submitted."; //showing only the first error.

            successHtml += '</div>';
            $('#messages').html(successHtml).show();
            }
            else
            {
            var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a><ul>';
            errorsHtml += data; //showing only the first error.

            errorsHtml += '</ul></div>';
            $('#messages').html(errorsHtml).show();
            }

            },
            error: function(errormessage) {
            //you would not show the real error to the user - this is just to see if everything is working
            alert(errormessage);
            }
    });
    });
    $("body").on("click", "#booknow", function(){

    $(".blockUI").css("display", "block");
    var id = '&id=' +<?php echo $id; ?>;
    var data = $("#book_form").serialize() + id;
    location.href = "{{ url('makebooking') }}?" + data;
    });
    $('.rating-loading').rating({displayOnly: true});
    $('.center').slick({
    centerMode: true,
            centerPadding: '190px',
            slidesToShow: 1,
            nextArrow: '<i class="fa fa-chevron-circle-right fa-right" aria-hidden="true" ></i>',
            prevArrow: '<i class="fa fa-chevron-circle-left fa-left" aria-hidden="true"></i>',
            responsive: [
            {
            breakpoint: 768,
                    settings: {
                    arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 2
                    }
            },
            {
            breakpoint: 480,
                    settings: {
                    arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                    }
            }
            ]
    });
    });
    $(document).ready(function() {

    $('#calendar').fullCalendar({

    header: {
    left: 'prev,next today',
            center: 'title',
            right: 'year,month'
    },
            defaultView: 'year',
            yearColumns: 3,
            selectable: true,
            selectHelper: true,
            select: function(start, end, cell) {
            var selectionStart = moment(start).format('YYYY-MM-DD');
            var today = moment().format('YYYY-MM-DD'); // passing moment nothing defaults to today

            if (selectionStart < today) {
            $(this).attr("tooltip", "Unavailable");
            $('#calendar').fullCalendar('unselect');
            }
            else
            {
            //var dataToFind = moment(start).format('YYYY-MM-DD');
            //closedate(selectionStart);    
            }
            },
            dayRender: function(date, cell) {
            var selectionStart = moment(date).format('YYYY-MM-DD');
            var today = moment().format('YYYY-MM-DD'); // passing moment nothing defaults to today

            if (selectionStart < today) {
            $(cell).attr("title", "Unavailable");
            $(cell).attr("data-toggle", "tooltip");
            }
            else
            {
            //$(cell).attr("title","Available");
            //$(cell).attr("data-toggle","tooltip");
            var dataToFind = moment(date).format('YYYY-MM-DD');
            $("td[data-date='" + dataToFind + "']").attr("title", "Available");
            $("td[data-date='" + dataToFind + "']").attr("data-toggle", "tooltip");
            }

            },
            eventLimit: true, // allow "more" link when too many events
            eventRender: function(event, element, view) {
            $(element).attr("title", "Unavailable");
            $(element).attr("data-toggle", "tooltip");
            var dataToFind = moment(event.start).format('YYYY-MM-DD');
            $("td[data-date='" + dataToFind + "']").addClass('opacity');
            $("td[data-date='" + dataToFind + "']").attr("title", "Unavailable");
            $(view).fullCalendar('unselect');
            //console.log(parentc);
            //$(parentc).addClass('opacity');             '  

            },
            eventClick: function(calEvent, jsEvent, view) {

            $(calEvent).fullCalendar('unselect');
            },
            events: [

                    @foreach($avails as $avail)

            {
            title : 'Unavailable',
                    start : '{{ $avail->date }}',
                    end : '{{ $avail->date }}',
            },
                    @endforeach

            ]

    });
    });




</script>