 <?php
 use App\Properties;     
?>
<style>
td.disabled.day {
    opacity: .2;
}
.clear-search {
    text-align: center;
    display: block;
    font-size: 12px;
}
</style>
<aside class="container  search_top">
<div class="search_inner">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_area">
<form method="get" name="search" id="search">
<input  type="hidden" id="price_min" name="price_min"/>
<input  type="hidden" id="price_max" name="price_max"/>
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 search_drop">
<h2>City</h2>
<div class="search_dropup">
   
    <select class="form-control" name="city" id="city"> 
        <option value="">City</option>
        <?php
        $cities=Properties::groupby("city")->where("status","=",1)->orderby("city","asc")->get();
        $selected="";
        $cval=(isset($city))?$city:'';
        ?>
       
        @foreach($cities as $row)
        <?php
        $selected=($cval==$row->city)?'selected':'';
        ?>
            <option value="{{ $row->city }}" <?php echo $selected; ?>>{{ $row->city }}</option>
        @endforeach
    </select>
  </div>
</div>

<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 search_drop">
<h2>Arrival Date</h2>

  <div class="input-group">
        
      <input class="form-control" id="start_date" class="" name="start_date" value="<?php echo (isset($start_date))?$start_date:''; ?>" placeholder="Arrival" type="text"/>
        <div class="input-group-addon" style="background:#fff; border:0px solid #fff;">
         <i class="fa fa-calendar">
         </i>
        </div>
       </div>



<script>
    $(document).ready(function(){
        var start_date=$('input[name="start_date"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        start_date.datepicker({
            format: 'yyyy-mm-dd',
            startDate: new Date(),
            container: container,
            todayHighlight: true,
            autoclose: true,
            active:true

        });

        var end_date=$('input[name="end_date"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        end_date.datepicker({
            format: 'yyyy-mm-dd',
            startDate: new Date(),
            container: container,
            todayHighlight: true,
            autoclose: true,
            active:true

        });
        start_date.keydown(function(event) { 
            return false;
        });   
        end_date.keydown(function(event) { 
            return false;
        });
    });
</script>

</div>

<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 search_drop">
<h2>Departure Date</h2>

  <div class="input-group">
        
        <input class="form-control" id="end_date" name="end_date" value="<?php echo (isset($end_date))?$end_date:''; ?>" placeholder="Departure" type="text"/>
        <div class="input-group-addon">
         <i class="fa fa-calendar">
         </i>
        </div>
       </div>

</div>

<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 search_drop ">
<h2>Guests</h2>
	  <div class="search_dropup">
  
   <select class="form-control" name="guest" id="guest"> 
    
       <option value="">Guest</option>
       
       <?php
       
       for($i=1;$i<=15;$i++)
       {
           $gval=(isset($guest))?$guest:'';
           $selected=($gval==$i)?'selected':'';
           
           if($i==15)
           echo '<option value="'.$i.'" '.$selected.'>'.$i.'+</option>';
           else
           echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
       }
       
       ?>
    </select>
  </div>

</div>

<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 search_drop">

    <button type="submit" onclick="ChangeUrl('Page1', '{{ url("rentals") }}');" class="btn btn-default search_btn">Search</button>	
    <a href="{{ url("rentals?") }}" class="clear-search">Clear Search</a>
</div>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        $("button").click(function(){
            $.ajax({
                type: 'GET',
                data: $("#search").serialize(),
                url: '{{ url("rentals") }}',
                success: function(data) 
                {
            
                    //$("p").text(data); 
                }
});
        
    });
});
</script>


<script type="text/javascript">
	function ChangeUrl(title, url) {
		var datasend = url+"?"+$("#search").serialize();
		if (typeof (history.pushState) != "undefined") {
			var obj = { Title: title, Url: datasend };
			history.pushState(obj, obj.Title, obj.Url);
		} else {
			alert("Browser does not support HTML5.");
		}
	}
</script>


</div>
</div><!--cotainer end-->
</aside>