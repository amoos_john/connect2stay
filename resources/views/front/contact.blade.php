@extends('front')

@section('content')
<link rel="stylesheet" href="{{ asset('front/style-extra.css')}}">
 <div class="property-heading3_"> Talk to us </div>
 <div class="property-heading__bg"> You got any questions? Have you checked our FAQ?
     <br><a href="{{ url('faq') }}">See FAQ</a> </div>

<section class="contact-form">
<div class="container">
 <div class="contact__form">
{!! Form::open(array( 'class' => 'form','url' => 'contact/send', 'files' => true)) !!}
@include('front/common/errors') 
    <input type="text" name="full_name" class="form-control" value="{{ old('full_name') }}" placeholder="First Name and Last Name*" required="">
<input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="E-mail*" required="">
<input type="text"  name="phone" class="form-control" value="{{ old('phone') }}" placeholder="Phone" required="">
<textarea class="form-control" name="message" placeholder="Message" required>{{ old('message') }}</textarea>

<button type="submit" class="btn contact__form--btn-style">to send</button>
{!! Form::close() !!}
</div>

</div>
</section>
@endsection