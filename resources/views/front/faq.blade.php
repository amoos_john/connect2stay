@extends('front')

@section('content')
<link rel="stylesheet" href="{{ asset('front/style-extra.css')}}">
 <div class="property-heading2_"> FAQ </div>
 <section class="bg__color--white pb80">
<div class="container">


<div class="property-style-bg_faq">
	<h4>Travelers</h4>
  <div class="accordion-area accordion-plus ">
          
            <div class="panel-group animated fadeInUp " id="accordion">
  
              <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="collapsed">
              How to List your property. </a>
              </h4>
            
            <div id="collapse1" class="panel-collapse collapse  pb10">
              <div class="panel-body">All you have to do is enter your travel dates, number of persons, preferred bedroom type and the location into the search bar to find a suitable holiday home.

 </div>
            </div>
            </div>    
   
              <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed">
             How can I book? </a>
              </h4>
            
            <div id="collapse2" class="panel-collapse collapse  pb10">
              <div class="panel-body">You can request to book the property by clicking the “Contact the Owner” or you can use “Instant Book”, to confirm your stay directly. The accommodation provider will respond within 24 hours to your inquiry.

 </div>
            </div>
            </div>    
   

  
              <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed">
             What does ‘Instant Book’ mean? </a>
              </h4>
            
            <div id="collapse3" class="panel-collapse collapse  pb10">
              <div class="panel-body">
Instant book means you will get instant confirmation on your booking and you do not need to wait for the host to approve your booking. Instant book is only made available for hosts with live availability.
 </div>
            </div>
            </div>  


          <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed">
             When do I receive the confirmation of my booking? </a>
              </h4>
            
            <div id="collapse4" class="panel-collapse collapse  pb10">
              <div class="panel-body">
After finalizing the booking, you receive a confirmation of the booking/rental agreement, which serves as a proof of the agreement. Please keep these details safe!
 </div>
            </div>
            </div>  

         <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="collapsed">
           Is there someone present in the holiday home upon arrival and departure?</a>
              </h4>
            
            <div id="collapse5" class="panel-collapse collapse  pb10">
              <div class="panel-body">

The property owner/manager will bring the key of the accommodation and meet you at the reserved property.
 </div>
            </div>
            </div>  

         <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="collapsed">
           Are pets allowed?</a>
              </h4>
            
            <div id="collapse6" class="panel-collapse collapse  pb10">
              <div class="panel-body">
Pets are allowed, if this is agreed during the reservations process.
 </div>
            </div>
            </div>  
     

         <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse7" class="collapsed">
           What should I do if I encounter any problem during my stay?</a>
              </h4>
            
            <div id="collapse7" class="panel-collapse collapse  pb10">
              <div class="panel-body">

If you become aware of any discrepancies relating to the accommodation, we kindly ask you to notify the accommodation immediately, but in any way not later than 48 hours after arrival. If the problem is not solved after 48 hours, or if you are unable to contact the accommodation provider, you are kindly requested to report any issue to Connect2Stay.
 </div>
            </div>
            </div>  


         <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse8" class="collapsed">
           Can I bring extra person during my stay?</a>
              </h4>
            
            <div id="collapse8" class="panel-collapse collapse  pb10">
              <div class="panel-body">

The holiday property must be occupied only by persons specified in the rental agreement. In the event of overcrowding, the owner/property manager is entitled to claim an additional reasonable fee for the period of overcrowding.
 </div>
            </div>
            </div>  

       <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse9" class="collapsed">
           How do you ensure that the properties exist?</a>
              </h4>
            
            <div id="collapse9" class="panel-collapse collapse  pb10">
              <div class="panel-body">

Our professional team visits every property before we list them on our platform.
 </div>
            </div>
            </div>  



       <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse10" class="collapsed">
           How do I cancel a reservation?</a>
              </h4>
            
            <div id="collapse10" class="panel-collapse collapse  pb10">
              <div class="panel-body">

If you wish to review, adjust or cancel your reservations, please revert to the confirmation email and follow the instructions therein.


 </div>
            </div>
            </div>



                   <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse11" class="collapsed">
           When will I receive the exact directions and address of my holiday home?</a>
              </h4>
            
            <div id="collapse11" class="panel-collapse collapse  pb10">
              <div class="panel-body">
They will be sent out at least 10 days prior to your arrival, once the full balance has been received.




 </div>
            </div>
            </div>  
  


                   <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse12" class="collapsed">
          How can I see how much it will cost?</a>
              </h4>
            
            <div id="collapse12" class="panel-collapse collapse  pb10">
              <div class="panel-body">
Once you have entered the dates of your stay, the available accommodation types are listed with the rates clearly displayed next to them. You might see that the same accommodation type has a different rate based on how many people are staying, whether or not breakfast is included, and whether or not the reservation can be cancelled.




 </div>
            </div>
            </div>  
  

                   <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse13" class="collapsed">
          What does the price include?</a>
              </h4>
            
            <div id="collapse13" class="panel-collapse collapse  pb10">
              <div class="panel-body">
All the facilities listed under the property type are included in the price.




 </div>
            </div>
            </div>  
  

                   <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse14" class="collapsed">
         Can I pay by credit card?</a>
              </h4>
            
            <div id="collapse14" class="panel-collapse collapse  pb10">
              <div class="panel-body">

Yes, you can pay via credit card or bank transfer.



 </div>
            </div>
            </div>  
  


                   <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse15" class="collapsed">
      I am entering my credit card details, when will I pay?</a>
              </h4>
            
            <div id="collapse15" class="panel-collapse collapse  pb10">
              <div class="panel-body">

YThe payment process for your stay varies based on the individual property. You can check how you will be charged for your stay in the ‘Accommodation Policies’ and in ‘Conditions’ at your booking confirmation.



 </div>
            </div>
            </div>  



                   <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse16" class="collapsed">
      I am entering my credit card details, when will I pay?</a>
              </h4>
            
            <div id="collapse16" class="panel-collapse collapse  pb10">
              <div class="panel-body">

YThe payment process for your stay varies based on the individual property. You can check how you will be charged for your stay in the ‘Accommodation Policies’ and in ‘Conditions’ at your booking confirmation.



 </div>
            </div>
            </div>  


            <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse17" class="collapsed">
     Is it safe to pay via your website?</a>
              </h4>
            
            <div id="collapse17" class="panel-collapse collapse  pb10">
              <div class="panel-body">

Payments through Connect2Stay are safely processed via our secure online payment provider. The payment will be processed from your credit/debit card or bank account to the bank account of the accommodation provider through a reliable third party payment processor.



 </div>
            </div>
            </div>  



            <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse19" class="collapsed">
     Is it safe to pay via your website?</a>
              </h4>
            
            <div id="collapse19" class="panel-collapse collapse  pb10">
              <div class="panel-body">

Payments through Connect2Stay are safely processed via our secure online payment provider. The payment will be processed from your credit/debit card or bank account to the bank account of the accommodation provider through a reliable third party payment processor.



 </div>
            </div>
            </div>  

       <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse20" class="collapsed">
     Can you confirm when you have received payment from me?</a>
              </h4>
            
            <div id="collapse20" class="panel-collapse collapse  pb10">
              <div class="panel-body">

We will send you an automated email as soon as we have received your payments.



 </div>
            </div>
            </div>

       <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse21" class="collapsed">
    Can you confirm when you have received payment from me?</a>
              </h4>
            
            <div id="collapse21" class="panel-collapse collapse  pb10">
              <div class="panel-body">





 </div>
            </div>
            </div>





       <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse22" class="collapsed">
    Can you confirm when you have received payment from me?</a>
              </h4>
            
            <div id="collapse22" class="panel-collapse collapse  pb10">
              <div class="panel-body">





 </div>
            </div>
            </div>



       <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse23" class="collapsed">
    How do I pay the security deposit?</a>
              </h4>
            
            <div id="collapse23" class="panel-collapse collapse  pb10">
              <div class="panel-body">
The security deposit need to be paid direct to the property owner at the check-in-date and it’s going to be refunded at the check-out-date after property inspection.




 </div>
            </div>
            </div>


        </div>
</div></div>
</section>
<div class="clear_both"></div>
	 <section class="bg__color--grey">
<div class="container">


<div class="property-style-bg_faq">
	<h4>Owner</h4>
  <div class="accordion-area accordion-plus ">
  
            <div class="panel-group animated fadeInUp " id="accordion">
  
              <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_1" class="collapsed">
              How can I list my property? </a>
              </h4>
            
            <div id="collapse_1" class="panel-collapse collapse  pb10">
              <div class="panel-body">At the top right side of our website you will find the option “List your property” once you click there you can fill in the registration form to list your property.

 </div>
            </div>
            </div>    
   
              <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_2" class="collapsed">
            How do I close the date when the property is not available? </a>
              </h4>
            
            <div id="collapse_2" class="panel-collapse collapse  pb10">
              <div class="panel-body">You can close the date of your property on our “extranet” using your login account that it’s going to be provided to you.

 </div>
            </div>
            </div>    
   

  
              <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_3" class="collapsed">
            Why should I advertise with Connect2Stay? </a>
              </h4>
            
            <div id="collapse_3" class="panel-collapse collapse  pb10">
              <div class="panel-body">
Connect2stay is connected with several vacation rental website where your chances to receive a booking is much higher.
 </div>
            </div>
            </div>  


               <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_4" class="collapsed">
           Which service do you provide to my property that I am listing on Connect2Stay? </a>
              </h4>
            
            <div id="collapse_4" class="panel-collapse collapse  pb10">
              <div class="panel-body">

We will create a property description, property headline, calendar for the rates and availability, map, professional photos and guest reviews.
 </div>
            </div>
            </div>  

               <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_5" class="collapsed">
          Who will charge the guest: Connect2Stay or myself? </a>
              </h4>
            
            <div id="collapse_5" class="panel-collapse collapse  pb10">
              <div class="panel-body">

Connect2Stay will charge the guest and will transfer the amount to you via bank transfer or credit card.
 </div>
            </div>
            </div>  


                <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_6" class="collapsed">
          How much do I have to pay to list my property on your website? </a>
              </h4>
            
            <div id="collapse_6" class="panel-collapse collapse  pb10">
              <div class="panel-body">


Connect2Stay will add its commission for its intermediary services on top of the rate made available by the Partner in the Accommodation Information.
 </div>
            </div>
            </div>  


                <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_7" class="collapsed">
          When will Connect2Stay pay the amount of the reservation? </a>
              </h4>
            
            <div id="collapse_7" class="panel-collapse collapse  pb10">
              <div class="panel-body">
The guest should pay the total amount of the reservations 14 days before arrival; Connect2Stay will pay the property owner 10 days before check in date.
 </div>
            </div>
            </div>  



                <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_8" class="collapsed">
          Where can I see the reservation that was made for my property? </a>
              </h4>
            
            <div id="collapse_8" class="panel-collapse collapse  pb10">
              <div class="panel-body">
Connect2Stay will provide you with a login account to our “extranet” where you can see all the reservations made for your property.
 </div>
            </div>
            </div>  

         <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_9" class="collapsed">
          How do I know that there is a reservation for my property?</a>
              </h4>
            
            <div id="collapse_9" class="panel-collapse collapse  pb10">
              <div class="panel-body">
Every time when a reservation is made for your property you will receive a booking confirmation from us.
 </div>
            </div>
            </div>  

         <div class="panel panel-default active property-style_faq">
        <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_10" class="collapsed">
          What happens when the guest cancels a reservation?</a>
              </h4>
            
            <div id="collapse_10" class="panel-collapse collapse  pb10">
              <div class="panel-body">
You will receive a cancellation email from us, and we will make sure that our calendar and those of the partners are updated.
 </div>
            </div>
            </div>  



  
</div>

   
        </div>
</div></div>
</section>
@endsection