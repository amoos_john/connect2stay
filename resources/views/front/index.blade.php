@extends('front')

@section('content')
@include('front/common/search')
	<section class="slider-area hover-ctrl fadeft " >
		<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
		
		  <ol class="carousel-indicators thumbs">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner top_slider">


			<div class="item active img-cntr">
			  <img src="{{ url('front/images/slide1.jpg')}}" alt="...">
			
			</div>

			<div class="item img-cntr">
			 <img src="{{ url('front/images/slide2.jpg')}}" alt="...">
			
			</div>
			

			<div class="item img-cntr">
			 <img src="{{ url('front/images/slide3.jpg')}}" alt="...">
			
			</div>

				<div class="item img-cntr">
			  <img src="{{ url('front/images/slide4.jpg')}}" alt="...">
			
			</div>

			<div class="item img-cntr">
			 <img src="{{ url('front/images/slide5.jpg')}}" alt="...">
			
			</div>
			

			<div class="item img-cntr">
			 <img src="{{ url('front/images/slide6.jpg')}}" alt="...">
			
			</div>
			
			

		

		  </div>

		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		  
		  
		</div>

		
	</section>
	
	
	

	<section class="">
		<div class="">
			
		<div class="col-sm-12 list_area no-padding">

		<div class="col-sm-6 list_text">
			
			<h1 style="color:#9c7990;">let your property <br>work for you</h1>
			<div class="btn btn-default list_property col-lg-8 no-padding">
                            <a href="{{ url('advertise') }}">
				List your Property
				</a>
		</div>
			<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

<div class="col-lg-12 container-fluid list_box">
	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 list_image">
<img src="{{ url('front/images/noimage.jpg')}}" alt="" class="image-responsive">
	</div><!--list_image end--> 

	<div class="col-lg-6 col-md-3 col-sm-12 col-xs-12 list_info">
		<h1>Property Name</h1>
		<h3>City</h3>
		<div class="row property_img">
			<div class="col-lg-12 col-md-13 col-sm-12 col-xs-12">	  
		 <ul class="row">
          <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>

     </ul> 
		</div>
	</div><!--propert_img end-->
	<span> $12000 </span>
	</div><!--list_image end--> 

	</div><!--container-fluid end-->

<div class="col-lg-12 container-fluid list_box">
	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 list_image">
<img src="{{ url('front/images/noimage.jpg')}}" alt="" class="image-responsive">
	</div><!--list_image end--> 

	<div class="col-lg-6 col-md-3 col-sm-12 col-xs-12 list_info">
		<h1>Property Name</h1>
		<h3>City</h3>
		<div class="row property_img">
			<div class="col-lg-12 col-md-13 col-sm-12 col-xs-12">	  
		 <ul class="row">
          <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>

     </ul> 
		</div>
	</div><!--propert_img end-->
	<span> $12000 </span>
	</div><!--list_image end--> 

	</div><!--container-fluid end-->

<div class="col-lg-12 container-fluid list_box">
	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 list_image">
<img src="{{ url('front/images/noimage.jpg')}}" alt="" class="image-responsive">
	</div><!--list_image end--> 

	<div class="col-lg-6 col-md-3 col-sm-12 col-xs-12 list_info">
		<h1>Property Name</h1>
		<h3>City</h3>
		<div class="row property_img">
			<div class="col-lg-12 col-md-13 col-sm-12 col-xs-12">	  
		 <ul class="row">
          <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
          	<a href=""><img src="{{ url('front/images/noimage.jpg')}}" class="img-responsive" alt=""></a>
          </li>

     </ul> 
		</div>
	</div><!--propert_img end-->
	<span> $12000 </span>
	</div><!--list_image end--> 

	</div><!--container-fluid end-->


</div><!--container-fluid end-->

    </div>
  </div>
</div>
	
		<div class="col-sm-6 banner_area animated zoomIn no-padding">
			
			<img src="{{ url('front/images/banner.png')}}" alt="" class="img-responsive">
		</div>
		</div>
	</nav>
	</section>
	
	<section class="destination">
		<div class="container dest">
			<h1>Popular Destinations</h1>
			<h3>Find the Perfect Place </h3>

<div class="text-center no-padding mm">
<a href="{{ url('rentals/sao-paulo') }}" class="b_box">
  <div class="col-sm-4 col-xs-12 container-fluid box_img  animated fadeInLeft" data-wow-delay="0.3s">
    <div class="caption_image">

          <p>São Paulo</p>
        </div>
        <img src="{{ asset('front/images/box1.png')}}"  class="img-responsive  ftr-logo" alt="Lights" >

         
  </div>
</a>


<a href="{{ url('rentals/rio-de-janeiro') }}" class="b_box">
  <div class="col-sm-4 col-xs-12 container-fluid box_img  animated fadeInLeft" data-wow-delay="0.3s">
    <div class="caption_image">
          <p>Rio de Janeiro</p>
        </div>
        <img src="{{ asset('front/images/box2.png')}}"  class="img-responsive ftr-logo" alt="Lights" >
      

         
  </div>
</a>

<a href="{{ url('rentals/fortaleza') }}"  class="b_box">
  <div class="col-sm-4 col-xs-12 container-fluid box_img  animated fadeInLeft" data-wow-delay="0.3s">
    <div class="caption_image">

          <p>Fortaleza</p>
        </div>
        <img src="{{ asset('front/images/box3.png')}}"  class="img-responsive ftr-logo" alt="Lights" >
      

         
  </div>
</a>

	</div>
			
	</div>
	</section>

	<section class="product-area swiper_slide animated fadeInDown" data-wow-delay="0.3s" id="swiperSlider">
		<div class="container">
			
			<h2 style="color:#999999;font-family:sans-serif;">PARTNERS </h2>
			
			<div class="dontfly s1">
				<div class="swiper-wrapper">
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-1.jpg')}}" alt=""></div>
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-2.jpg')}}" alt=""></div>
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-3.jpg')}}" alt=""></div>
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-4.jpg')}}" alt=""></div>
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-5.jpg')}}" alt=""></div>
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-6.jpg')}}" alt=""></div>
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-8.jpg')}}" alt=""></div>
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-9.jpg')}}" alt=""></div>
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-10.jpg')}}" alt=""></div>
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-11.jpg')}}" alt=""></div>
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-12.jpg')}}" alt=""></div>
					<div class="swiper-slide animated zoomIn"><img src="{{ asset('front/images/p-13.jpg')}}" alt=""></div>
				</div>

				<!-- Add Arrows -->
				<div class="swiper-button-next swiper_btn"></div>
				<div class="swiper-button-prev swiper_btn"></div>

			</div>
			
		</div>
	</section>
@endsection