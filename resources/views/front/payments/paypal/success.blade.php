@extends('front')

@section('content')

<link rel="stylesheet" href="{{ asset('front/style-extra.css')}}">
<div class="property-heading5_"> {{ $content->title }} </div>
<section>
<div class="container">
<div class="text-container col-sm-12 pr80 pl80 text-center">
    
    <img class="img-responsive" src="{{ asset('front/images/thankyou.png') }}">
    <?php echo $content->body ;?>

</div>
</div>

</section>
				
@endsection