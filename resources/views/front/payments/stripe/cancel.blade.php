@extends('front')

@section('content')


<div class="container">
	<section>
	<div class="container-fluid">
		<div class="row">
<div class="destina col-sm-9 bg-white">
	<h3>{{ $content->title }}</h3>

   @if (Session::has('error'))
    <div class="alert alert-danger">
        <strong>Whoops! Something went wrong!</strong>
    	 {!! session('error') !!}
    </div>
@endif
</div>
</div>
</div>

       
</section>
</div>
				
@endsection