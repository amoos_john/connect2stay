@extends('front')

@section('content')
<link rel="stylesheet" href="{{ asset('front/style-extra.css')}}">
 <div class="property-heading4_"> Privacy Policy </div>
<section>
<div class="container">
<div class="text-container pl80 pr80 mt20 mb50">

<h3>Privacy Policy</h3>
<p>We've provided this Privacy Policy, which sets forth our terms regarding the collection, use and protection of the personal information of those using the Connect2Stay website. Personal information means information that can be linked to a specific individual, such as name, address, ip address, telephone number, e-mail address and credit card number. We encourage you to review our Privacy Policy, and become familiar with it, but you should know that we do not sell or rent our customers' personal information to any third parties. Please note that we review our Privacy Policy from time to time, and we may make periodic changes to the policy in connection with that review. Therefore, you may wish to bookmark this page and/or periodically review this page to make sure that you have the current version.</p><br><br>

<h3>This Privacy Policy explains the following:</h3>
<p>What personal information does Connect2Stay gather from me and how is this When browsing our site, you are not required to provide any personal information unless and until you choose to make a purchase or sign up for one of our e-mail newsletters or other services as described below. We do not knowingly permit the site to be used by any person under the age of eighteen and we do not knowingly collect any personal information from children (meaning those younger than thirteen years of age).</p><br><br>

<h3>Making a Purchase</h3>
<p>In order to purchase travel and related services through our site, you must provide us with certain personal information such as your name, your credit card number and expiration date, your credit card billing address, your telephone number, your e-mail address and the name or names of the person(s) traveling.</p><br><br>

<h3>Member Registration</h3>
<p>If you choose to become a registered member of www.connect2stay.com, you must provide your name, e-mail address, a unique login name and password. Other information may be collected on a voluntary basis. As a www.connect2stay.com member you will also occasionally receive updates from us about fare sales in your area, special offers, new Connect2Stay services, and other noteworthy items. However, you may choose at any time to no longer receive these types of e-mail messages. By supplying a phone number, it is implied that you are giving us consent to contact you regardless if the phone number is listed in any "Do not call" lists.</p><br><br>

<h3>Member Profile</h3>
<p>As a www.connect2stay.com member, you can choose to complete your online profile by providing us with travel preferences. This information is primarily used to assist us in finding the most suitable travel deal for you. With your login and password, you can at anytime update or remove supplied information.</p><br><br>

<h3>Automatic Logging of Session Data</h3>
<p>We automatically log generic information about your computer's connection to the Internet, which we call "session data". Session data consists of things such as IP address, operating system and type of browser software being used and the activities conducted by the user while on our site. An IP address is a number that lets computers attached to the Internet, such as our web servers, know where to send data back to the user, such as the pages of the site the user wishes to view. We collect session data because it helps us analyze such things as what items visitors are likely to click on most, the way visitors are clicking through the site, how many visitors are surfing to various pages on the site, how long visitors to the site are staying and how often they are visiting. It also helps us diagnose problems with our servers and lets us better administer our systems. It is possible to determine from an IP address a visitor's Internet Service Provider (ISP) and the approximate geographic location of his or her point of connectivity. We may also use some session data, such as the pages you visited on our site, to send you e-mail unless you had previously opted out of receiving such messages.</p><br><br>

<h3>Cookies</h3>
<p>"Cookies" are small pieces of information that are stored by your browser on your computer's hard drive. There are many myths circulating about cookies, but you should know that cookies are only read by the server that placed them, and are unable to do such things as run programs on your computer, plant viruses or harvest your personal information. The use of cookies is very common on the Internet and www.connect2stay.com's use of cookies is similar to that of such sites as Expedia and Travelocity, as well as Google, CNN.com and other reputable online companies.First and foremost, you can rest assured that no personal information about you is gathered or stored in the cookies placed by the www.connect2stay.com website and, as a result, none can be passed on to any third parties.Cookies allow us to serve you better and more efficiently, and to personalize your experience at our site. A cookie may also be placed by our advertising server. Such cookies are used only for purposes of tracking the effectiveness of advertising served by us on our site and no personal information about you is gathered from you by the use of these cookies, nor is this information shared with any third parties. Similarly, a cookie may be placed by our third-party advertising companies. These companies may use aggregated statistics about your visits to this and other web sites in order to provide advertisements about travel-related goods and services that you may be interested in. The information they collect does not include your personal information.The third-party advertising companies may also employ technology that is used to measure the effectiveness of ads. Any such information is anonymous. They may use this anonymous information about your visits to this and other sites in order to provide advertisements about goods and services of potential interest to you. No personal information about you is collected during this process. The information is anonymous and does not link online actions to an identifiable person. Most web browsers automatically accept cookies. Of course, by changing the options on your web browser or using certain software programs, you can control how and whether cookies will be accepted by your browser. Connect2Stay supports your right to block any unwanted Internet activity, especially that of unscrupulous websites. However, blocking www.connect2stay.com cookies may disable certain features on our site and may make it impossible to purchase or use certain services available on the site. Please note that it is possible to block cookie activity from certain web sites while permitting cookies from sites you trust, like www.connect2stay.com</p><br><br>

<h3>Other</h3>
<p>From time to time we may add or enhance services available on the site. To the extent these services are provided and used by you, we will use the information you provide to facilitate the service requested. For example, if you email us with a question, we will use your email address, phone number, name, nature of the question, etc. to respond to your question. We may also store such information to assist us in making the site the better and easier to use. Thank you for using www.connect2stay.com!
</div>

</div>


</section>
@endsection