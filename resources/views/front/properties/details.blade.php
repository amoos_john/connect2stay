@extends('front')

@section('content')
<section class="detail">
	   <div class="swiper-container swiper-slider">
        <div class="swiper-wrapper swiper-wrapper-slider ">
            <div class="swiper-slide item-slider" ><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(1)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(2)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(3)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(4)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(4)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(4)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(4)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(5)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(4)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(4)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(6)" ></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(4)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(7)" ></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(4)"></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(8)" ></div>
            <div class="swiper-slide item-slider"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(4)"></div>
            <div class="swiper-slide"><img src="{{ url('front/images/banner.png')}}"  class="img-responsive " alt="Lights" onclick="openModal();currentSlide(9)" ></div>

        </div>


        		<div class="swiper-button-next swiper_btn_next"></div>
				<div class="swiper-button-prev swiper_btn_prev"></div>
 
    </div>

<div id="myModal" class="modal slider_detail">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <div class="mySlides">
      <div class="numbertext"></div>
      <img src="{{ url('front/images/slide1.jpg')}}" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext"></div>
      <img src="{{ url('front/images/slide2.jpg')}}" style="width:100%">
    </div>

    <div class="mySlides">
      <div class="numbertext"></div>
      <img src="{{ url('front/images/slide3.jpg')}}" style="width:100%">
    </div>
    
    <div class="mySlides">
      <div class="numbertext"></div>
      <img src="{{ url('front/images/slide4.jpg')}}" style="width:100%">
    </div>
    
    <a class="mprev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="mnext" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>


    <div class="column">
      <img class="demo cursor" src="{{ url('front/images/slide5.jpg')}}" style="width:100%" onclick="currentSlide(1)" alt="Slide 5">
    </div>
    <div class="column">
      <img class="demo cursor" src="{{ url('front/images/slide6.jpg')}}" style="width:100%" onclick="currentSlide(2)" alt="Slide 6">
    </div>
    <div class="column">
      <img class="demo cursor" src="{{ url('front/images/slide1.jpg')}}" style="width:100%" onclick="currentSlide(3)" alt="Slide 1">
    </div>
    <div class="column">
      <img class="demo cursor" src="{{ url('front/images/slide2.jpg')}}" style="width:100%" onclick="currentSlide(4)" alt="Slide 2">
    </div>
  </div>
</div>




</section>

	<section>
    <div class="container">
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 detail-section">


<div class="heading pull-left"><h1>Name Of House</h1></div>
<div class="property_inc pull-right">

  <div class="inc inline-block container-fluid">
  <span><img src="{{ url('front/images/d_1.png')}}" alt=""></span>
  <p>Bathrooms</p>
  </div>
  <div class="inc inline-block container-fluid">
  <span><img src="{{ url('front/images/d_2.png')}}" alt=""></span>
  <p>Sleeps</p>
  </div>
  <div class="inc inline-block container-fluid">
  <span><img src="{{ url('front/images/d_3.png')}}" alt=""></span>
  <p>Bed rooms</p>
  </div>
</div>

<p class="pull-left">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec
quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec
pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a,
venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.
Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.
Phasellus viverra nulla ut metus varius laoreet.
</p>










<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 review pull-left no-padding">
<span class="inline-block"><h1>Review</h1></span>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<div class="area_search inline-block pull-right">
        <div class="input-group">
                <div class="input-group-btn search-panel">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                      <span id="search_concept">Filter by</span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#contains">Contains</a></li>
                      <li><a href="#its_equal">It's equal</a></li>
                      <li><a href="#greather_than">Greather than ></a></li>
                      <li><a href="#less_than">Less than < </a></li>
                      <li class="divider"></li>
                      <li><a href="#all">Anything</a></li>
                    </ul>
                </div>
                <input type="hidden" name="search_param" value="all" id="search_param">         
                <input type="text" class="form-control search_bar" name="x" placeholder="Search term...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </div>
</div>
<div class="clear_both"></div>
<div class="col-lg-6 container-fluid inline-block no-padding">
<span class="inline-block"><h1>Accuracy</h1></span>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
</div>
<div class="col-lg-6 container-fluid inline-block">
<span class="inline-block"><h1>Location</h1></span>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
</div>
<div class="col-lg-6 container-fluid inline-block no-padding">
<span class="inline-block"><h1>Communication</h1></span>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
</div>
<div class="col-lg-6 container-fluid inline-block">
<span class="inline-block"><h1>Check in</h1></span>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
</div>
<div class="col-lg-6 container-fluid inline-block no-padding">
<span class="inline-block"><h1>Cleanness</h1></span>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
</div>
<div class="col-lg-6 container-fluid inline-block">
<span class="inline-block"><h1>Value</h1></span>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
<i class="fa fa-star star" aria-hidden="true"></i>
</div>
<div class="clear_both"></div>

<div id="exTab3" class="tab-panel--area no-padding"> 
<ul  class="nav nav-pills">
      <li class="active">
        <a  href="#1b" data-toggle="tab">All Reviews</a>
      </li>
      <li><a href="#2b" data-toggle="tab">Highest Reviews</a>
      </li>
      <li><a href="#3b" data-toggle="tab">Lowest Highest</a>
      </li>
      
    </ul>

      <div class="tab-content clearfix">
        <div class="tab-pane active" id="1b">
          <span><img src="{{ url('front/images/noimage.jpg')}}" alt="" class="img-thumbnail inline-block" width="34" height="36"></span>
          <h3>Name First</h3>
          <h4>Date: 04-Dec-2017</h4>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
        </div>
        <div class="tab-pane" id="2b">
                    <span><img src="{{ url('front/images/noimage.jpg')}}" alt="" class="img-thumbnail inline-block" width="34" height="36"></span>
          <h3>Name Second</h3>
          <h4>Date: 04-Dec-2017</h4>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
        </div>
        <div class="tab-pane" id="3b">
              <span><img src="{{ url('front/images/noimage.jpg')}}" alt="" class="img-thumbnail inline-block" width="34" height="36"></span>
          <h3>Name Third</h3>
          <h4>Date: 04-Dec-2017</h4>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
        </div>

      </div>
  </div> <!--tab--panel--area end-->

  <div class="rules">
<div class="col-lg-3"><h1> House Rules</h1></div>
<div class="col-lg-9">
  <ul>
<li>No Drugs
 </li>
<li>Illegal activities are not allowed.
 </li>
<li> Keep volume low after 22:00
</li>
<li> Do not bring guests without permission.
</li>
<li> Please be nice to our neighbours! They are lovely and we don’t want to upset them.

</li>
<li> Now, relax and enjoy your stay!

</li>
</ul>
</div>

  </div><!--rules end-->


</div><!--detail section end-->


<div class="col-lg-4 col-md-4 col-sm-12 detail-section">
<div class="col-lg-9 col-md-9 col-sm-12">
<button class="btn btn-primary btn-detail">
Please specify your dates of stay.
</button>
</div>



<div class="ppt-reservar">


  </div>


  <div class="col-lg-6 col-md-6 col-sm-12 checkin">
<h2>Check In</h2>
<form>
  <div class="input-group">
        
        <input class="form-control form_checkin" id="date3" name="date" placeholder="Check-In" type="text"/>
        <div class="input-group-addon">
         <i class="fa fa-calendar">
         </i>
        </div>
       </div>

</form>
<script>
  $(document).ready(function(){
    var date_input=$('#date3'); //our date input has the name "date"
    var container=$('.bootstrap-iso form').length>0 ? $('').parent() : "body";
    date_input.datepicker({
      format: 'dd/mm/yyyy',
      container: container,
      todayHighlight: true,
      autoclose: true,
      active:true

    })
  })

   

</script>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 checkout">
<h2>Check Out</h2>
<form>
  <div class="input-group">
        
        <input class="form-control form_checkout" id="date4" name="date" placeholder="Check-Out" type="text"/>
        <div class="input-group-addon">
         <i class="fa fa-calendar">
         </i>
        </div>
       </div>

</form>

  </div>

  <div class="col-lg-6 col-md-6 col-sm-12 checkin">
<h2>Guests</h2>
<form>
  <div class="input-group">
        
        <input class="form-control form_guest" id="date3" name="checkin" placeholder="Guests" type="text"/>

       </div>

</form>

  </div>


  <div class="col-lg-6 col-md-6 col-sm-12 checkout">
<h2>Children</h2>
<form>
  <div class="input-group">
        
        <input class="form-control form_children"  name="children" placeholder="Children" type="text"/>
        
       </div>

</form>

  </div>
<div class="container-fluid">
  <button class="btn btnupdate">Update</button>
  <button class="btn btnupdate" style="margin-left:12px;">Inquire Now</button>
</div>
</div><!--detail section end-->


</div><!--container end-->

	</section>
  <div class="clear_both"></div>




	<section class="col-lg-12 no-padding">
<div class="container">
  <div class="rules rules2">
    
<div class="col-lg-12"><h1>Amenitites</h1></div>


<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12"><ul><li> <strong>Activities</strong></li></ul></div>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
  <ul>
<li>Cinema
 </li>
<li> Cycling
 </li>
<li> Hiking
</li>
<li> Nightlife
</li>
<li> Shopping

</li>
<li> Sightseeing


</li>
</ul>
</div>



<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12"><ul><li><strong>Bedrooms</strong></li></ul></div>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
  <ul>
<li>Cinema
 </li>
<li> Cycling
 </li>
<li> Hiking
</li>
<li> Nightlife
</li>
<li> Shopping

</li>
<li> Sightseeing


</li>
</ul>
</div>



<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12"><ul><li><strong>Communications</strong></li></ul></div>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
  <ul>
<li>Cinema
 </li>
<li> Cycling
 </li>
<li> Hiking
</li>
<li> Nightlife
</li>
<li> Shopping

</li>
<li> Sightseeing


</li>
</ul>
</div>



<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12"><ul><li><strong>Entertainment</strong></li></ul></div>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
  <ul>
<li>Cinema
 </li>
<li> Cycling
 </li>
<li> Hiking
</li>
<li> Nightlife
</li>
<li> Shopping

</li>
<li> Sightseeing


</li>
</ul>
</div>


<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12"><ul><li><strong>Kitchen Equipment</strong></li></ul></div>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
  <ul>
<li>Cinema
 </li>
<li> Cycling
 </li>
<li> Hiking
</li>
<li> Nightlife
</li>
<li> Shopping

</li>
<li> Sightseeing


</li>
</ul>
</div>


<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12"><ul><li><strong>pool and Spa</strong></li></ul></div>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
  <ul>
<li>Cinema
 </li>
<li> Cycling
 </li>
<li> Hiking
</li>
<li> Nightlife
</li>
<li> Shopping

</li>
<li> Sightseeing


</li>
</ul>
</div>

<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12"><ul><li><strong>Suitability</strong></li></ul></div>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
  <ul>
<li>Cinema
 </li>
<li> Cycling
 </li>
<li> Hiking
</li>
<li> Nightlife
</li>
<li> Shopping

</li>
<li> Sightseeing


</li>
</ul>
</div>


<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12"><ul><li><strong>View and Location</strong></li></ul></div>
<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
  <ul>
<li>Cinema
 </li>
<li> Cycling
 </li>
<li> Hiking
</li>
<li> Nightlife
</li>
<li> Shopping

</li>
<li> Sightseeing


</li>
</ul>
</div>



  </div><!--rules end-->

<div class="clear_both"> </div>

  <div class="rules rules2">
    
<div class="col-lg-12"><h1>Rates & Availability</h1></div>


<div class="calender-area">

<div id="calender"></div>
<script>


  $( function() {
    $( "#calender" ).datepicker({
      numberOfMonths: 6,
      showButtonPanel: true,


    });
  } );




  </script>


</div> <!--calender-->
  </div><!--rules end-->

<div class="rules rules2">
    
<div class="col-lg-12"><h1>Attractions</h1></div>

<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 map">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3936.261281236687!2d-40.47271738521062!3d-9.398452693265096!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7737021d6c4b0c7%3A0x2520dc2447179744!2sCondom%C3%ADnio+Summerville!5e0!3m2!1sen!2s!4v1491899130880" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 map-desc">
<div class="side-bar-map">




</div> <!--map-->
</div><!--map desc end-->
  </div><!--rules end-->
</div><!--container end-->
  </section>

				

	
	
</main>
    <!--Swiper Slider-->

<!--FIXED DETAI LSECTION -->

<div class="col-lg-4 col-md-3 col-sm-12 detail-section2" id="need_help" style="display:none">
<div class="col-lg-9 col-md-9 col-sm-12">
<!-- <button class="btn btn-primary btn-detail" style>
Please specify your dates of stay.
</button> -->
</div>





  <div class="col-lg-6 col-md-6 col-sm-12 checkin">
<h2>Check In</h2>
<form>
  <div class="input-group">
        
        <input class="form-control form_checkin" id="date3" name="date" placeholder="Check-In" type="text"/>
        <div class="input-group-addon">
         <i class="fa fa-calendar">
         </i>
        </div>
       </div>

</form>
<script>
  $(document).ready(function(){
    var date_input=$('#date3'); //our date input has the name "date"
    var container=$('.bootstrap-iso form').length>0 ? $('').parent() : "body";
    date_input.datepicker({
      format: 'dd/mm/yyyy',
      container: container,
      todayHighlight: true,
      autoclose: true,
      active:true

    })
  })
</script>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 checkout">
<h2>Check Out</h2>
<form>
  <div class="input-group">
        
        <input class="form-control form_checkout" id="date4" name="date" placeholder="Check-Out" type="text"/>
        <div class="input-group-addon">
         <i class="fa fa-calendar">
         </i>
        </div>
       </div>

</form>
<script>
  $(document).ready(function(){
    var date_input=$('#date4'); //our date input has the name "date"
    var container=$('.bootstrap-iso form').length>0 ? $('').parent() : "body";
    date_input.datepicker({
      format: 'dd/mm/yyyy',
      container: container,
      todayHighlight: true,
      autoclose: true,
      active:true

    })
  })
</script>
  </div>

  <div class="col-lg-6 col-md-6 col-sm-12 checkin">
<h2>Guests</h2>
<form>
  <div class="input-group">
        
        <input class="form-control form_guest" id="date3" name="checkin" placeholder="Guests" type="text"/>

       </div>

</form>

  </div>


  <div class="col-lg-6 col-md-6 col-sm-12 checkout">
<h2>Children</h2>
<form>
  <div class="input-group">
        
        <input class="form-control form_children"  name="children" placeholder="Children" type="text"/>
        
       </div>

</form>

  </div>
<div class="container-fluid">
  <button class="btn btnupdate">Update</button>
  <button class="btn btnupdate" style="margin-left:12px;">Inquire Now</button>
</div>
</div><!--detail section2 end-->

	
@endsection