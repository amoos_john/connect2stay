@extends('front')

@section('content')
@include('front/common/search')
<div class="purple-border destination-bg" style="background:url('front/images/destinos.jpg')">
    <h1>Your top Destination</h1> 
</div>
<div class="container">
<section>
<div class="container-fluid">
<div class="row">
<div class="destina col-sm-9 bg-white">
	<h3>{{ $content[0]->title }}</h3>
<div class="des-img ">
    <img src="{{ url('uploads/content/'.$content[0]->image) }}">
</div>
        
<?php echo $content[0]->body; ?>        
<a href="{{ url('rentals/'.$content[0]->url.'-properties') }}" class="dest-btn">see the properties in {{ $content[0]->title }}</a> 
</div>
</div>
    
</div>

       
</section>
</div>
@endsection