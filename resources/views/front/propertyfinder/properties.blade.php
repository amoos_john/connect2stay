@extends('front')
@section('content')
<?php
use App\Properties;
use App\Functions\Functions;

$currency = Config::get('params.currency');
$symbol = $currency["BRL"]["symbol"];
?>
<style>
    .properties h3{

        color: #727272 !important;
        font-size: 18px;
        margin-top: 5px;
        position: static;
        width: 100%;
        padding-bottom: 0;
        font-weight: lighter;
        font-family: 'Aleo'
    }
    .about-img__image{
        height: 500px;
        overflow: hidden;
    }
    .about-img__image img{
        width: 100%;
        height: 100%;
    }
</style>

<div class="about-heading"> {{ $model->name }}</div> <!-- abput jeadog end-->
@if(isset($image))
<div class="container">
    <div class="about-img__image"><img src="{{ asset('/'.$image->url) }}" alt=""></div>
</div>
@endif


<div class="container">

    <section>



        <div class="" >

            <div class="mt60">

                <div class="properties col-sm-9 bg-white ">

                <?php echo $model->description; ?>    

                    <div class="clear_both"></div>

                    @if(isset($properties) && count($properties)>0)  

                    @foreach($properties as $row)
                    <?php
                    $rates = Properties::getRates($row->id);
                    ?>
                    @if(count($rates)>0)
                    <div class="col-lg-12 container-fluid list_box">

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 list_image">
                            <a href="{{ url('rentals/allrentals/'.$row->id) }}">
                                <img src="{{ asset('/'.$row->url) }}" alt="{{ $row->image_caption }}" class="image-responsive"  > 
                            </a>
                        </div><!--list_image end--> 

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 list_info">
                            <h1>{{ $row->public_headline }}</h1>
                            <h3>{{ $row->city }}</h3>
                            <div class=" property_img">
                                <div class="col-lg-12 col-md-13 col-sm-12 col-xs-12 prop">   
                                    <ul class="no-padding">
                                        <li class="">
                                            <span><img src="{{ asset('front/images/d_2.png')}}" class="img-responsive" alt="" style="width:50px;"></span>
                                            <span>{{ $row->sleeps }}</span>
                                        </li>            
                                        <li class="">
                                            <span><img src="{{ asset('front/images/d_1.png')}}" class="img-responsive" alt="" style="width:50px;"></span>
                                            <span>{{ $row->bathrooms }}</span>
                                        </li>
                                        <li class="">
                                            <span><img src="{{ asset('front/images/d_3.png')}}" class="img-responsive" alt="" style="width:50px;"></span>
                                            <span>{{ $row->bedrooms }}</span>
                                        </li>

                                    </ul> 
                                </div>
                            </div><!--propert_img end-->
                            <span> From: {{ (count($rates)>0)?$symbol.$rates[0]->daily:'' }}/Night </span>
                        </div><!--list_image end--> 

                    </div><!--list box end-->
                    @endif
                    @endforeach
                    @endif





                </div></div>

        </div>



    </section>

</div>

@endsection