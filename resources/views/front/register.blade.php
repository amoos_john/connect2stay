
@extends('front')

@section('content')
<link rel="stylesheet" href="{{ asset('front/style-extra.css')}}">

<style>
    span {
    color: #f00;
    font-size: 18px;
    text-align: center;
    }
    .list_box:hover{
background:#e1e1e1;}
.tab-list--btn {
    margin-top: 0px;
    border: 0;
    color: #c8b7c2;
    font-size: 19px;
    font-weight: bold;
}
</style>
<?php
$required = 'required';

?>
<div class="property-heading1_"> List your property and have great results </div>
<div class="container">

<div id="exTab2" class="list_tab">	
<ul class="nav nav-tabs list-ul">
    <li id="dat" class="active" >
    <a href="#data" data-toggle="tab">Personal Data</a>
    </li>
    <li id="add" style="pointer-events: none;"><a href="#address" data-toggle="tab">Address</a>
    </li>
    <li id="det" style="pointer-events: none;"><a href="#details" data-toggle="tab">Details</a>
    </li>
    <li id="fac" style="pointer-events: none;"><a href="#faclities" data-toggle="tab">Facilities</a>
    </li>
</ul>
{!! Form::open(array( 'class' => 'form','url' => 'register/send', 'files' => true)) !!}
@include('front/common/errors')  
<div class="tab-content ">
<div class="tab-pane fade in active" id="data">
          
 <div class="form-group form_list">
    <input type="text" name="full_name" class="form-control" id="full_name" placeholder="First name and last name*" <?php echo $required;?> ><span role="alert" id="nameError"></span>
  </div>
   <div class="form-group form_list">
    <input type="email" name="email" class="form-control" id="email" placeholder="E-mail*" <?php echo $required;?> ><span id="emailError" role="alert"></span>
  </div>
   <div class="form-group form_list">
    <input type="tel" name="phone" class="form-control" id="phone" placeholder="Phone*" <?php echo $required;?> ><span id="phoneError" role="alert"></span>
  </div>
 
<div class="tab-list-btn"><a id="next1" data-toggle="tab"> Next </a> </div>

</div>
<div class="tab-pane fade" id="address">

<div class="f-tab">
  <div class="form-group form_list faddress">
    <input type="text" name="address" class="form-control" id="addresss" placeholder="Address*" <?php echo $required;?> ><span id="addressError" role="alert"></span>
  </div>
   <div class="form-group form_list fapt">
    <input type="text" name="apt" class="form-control" id="apt" placeholder="Apt. sala (opicional)">
  </div>
   <div class="form-group form_list fcity">
    <input type="text" name="city" class="form-control" id="city" placeholder="City*" <?php echo $required;?> ><span id="cityError" role="alert"></span>
  </div>
 <div class="form-group form_list fstate">
    <input type="text" name="state" class="form-control" id="state" placeholder="State*" <?php echo $required;?> ><span id="stateError" role="alert"></span>
  </div>
  <div class="form-group form_list fzcode">
    <input type="text" name="zipcode" class="form-control" id="zipcode" placeholder="Zip Code*" <?php echo $required;?> ><span id="zipcodeError" role="alert"></span>
  </div>
  <div class="form-group form_list fcountry">
      <select name="country" class="form-control" id="country">
	<option value="">Country</option>
        @foreach($countries as $key=>$country)
            <option value="{{ $key }}">{{ $country }}</option>
        @endforeach
		
    </select>
  </div>

</div><!--ftab end-->

<div  class="tab-list-btn"><a id="next2" data-toggle="tab"> Next </a> </div>
</div>
    
<div class="tab-pane fade" id="details">

  <div class="col-lg-9 col-md-12 col-sm-12  col-xs-12 detail_ ">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-detail ">
          <div class="form-group form_list">
    <input type="text" name="category" class="form-control" id="category" placeholder="House, Apartment, Bedroom*"  <?php echo $required;?> ><span id="categoryError" role="alert"></span>
  </div>
   <div class="form-group form_list">
    <input type="number" min="1" name="stay" class="form-control" id="stay" placeholder="How many people can stay?*"  <?php echo $required;?> ><span id="stayError" role="alert"></span>
  </div>
   <div class="form-group form_list">
    <input type="number" min="1" name="rooms" class="form-control" id="rooms" placeholder="How many rooms?*"  <?php echo $required;?> ><span id="roomsError" role="alert"></span>
  </div>
 <div class="form-group form_list">
     <input type="number" min="1" name="baths" class="form-control" id="baths" placeholder="How many bathrooms?*"  <?php echo $required;?> ><span id="bathsError" role="alert"></span>
  </div>

<p style="  font-family: 'Roboto', sans-serif;
    color: #929292;
    font-size: 18px; margin-bottom:0px; margin-top:20px;">Shared Areas</p>

  <div class="form-group form_list list_check">
      <input type="checkbox" name="areas[]" value="Porch" class="form-control" placeholder="Zip Code">
                            <label for="inlineCheckbox2"> Porch </label>

  </div>
<div class="form-group form_list list_check">
    <input type="checkbox" name="areas[]" value="Terrace"  class="form-control"  placeholder="Zip Code">
                            <label for="inlineCheckbox2"> Terrace </label>

  </div>

  <div class="form-group form_list list_check">
    <input type="checkbox" name="areas[]" value="Pool"  class="form-control"  placeholder="Zip Code">
                            <label for="inlineCheckbox2"> Pool</label>

  </div>

    <div class="form-group form_list list_check">
    <input type="checkbox" name="areas[]" value="Parking"  class="form-control" placeholder="Zip Code">
                            <label for="inlineCheckbox2"> Parking</label>

  </div>

<div class="tab-list-btn"><a id="next3" data-toggle="tab"> Next </a> </div>


</div><!--f-detail end-->


      				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 f-detail f-detail2">
         
<div style="border:solid 2px #bebebe; border-radius:5px;padding:1em;background-color:#ededed" class="col-sm-12">
<p class="text-left"><img src="{{ asset('front/images/importante.jpg') }}" width="54" height="50"></p>
<p class="text-left" style=" font-size:1.2em; margin:0;color:#8c8c8c;font-weight:bold;">The house</p>
<p class="text-left" style=" margin:0;">The guests will rent the entire property.</p>
<p class="text-left" style="margin:0;  font-size:1.2em; color:#8c8c8c;font-weight:bold;">Bedrooms</p>
<p class="text-left" style=" margin:0;">The number of bedrooms where the guest will sleep.</p>
<p class="text-left" style=" font-size:1.2em;margin:0; color:#8c8c8c;font-weight:bold;">Number of guests</p>
<p class="text-left" style="margin:0;">The number and type of beds will determine how many guest can stay at your property.</p>
</div>
</div><!--f-detail2 end-->


</div>
				</div>



<div class="tab-pane fade" id="faclities">

 <div class="col-lg-9 col-md-12 col-sm-12  col-xs-12 detail_ no-padding">
   <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 f-detail f-facilties no-padding">
 

    <div class="form-group form_list list_check">


<p style="margin:0;    font-family: 'Roboto', sans-serif;
    color: #929292; font-size:16px;">Living room</p>
<p></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/tv.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap TV"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="living_rooms[]" value="TV">&nbsp;<span class="wpcf7-list-item-label">TV</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/canaldesatelite.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap cableTV"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="living_rooms[]" value="cable TV">&nbsp;<span class="wpcf7-list-item-label">cable TV</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/internetwifi.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Wi-Fi"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="living_rooms[]" value="Wi-Fi">&nbsp;<span class="wpcf7-list-item-label">Wi-Fi</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/arcondicionado.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Airconditioning"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="living_rooms[]" value="Air conditioning">&nbsp;<span class="wpcf7-list-item-label">Air conditioning</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/lareira.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Fireplace"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="living_rooms[]" value="Fireplace">&nbsp;<span class="wpcf7-list-item-label">Fireplace</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/sofa.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap couch"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="living_rooms[]" value="Couch">&nbsp;<span class="wpcf7-list-item-label">Couch</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/ferrodepassarroupa.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Electric"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="living_rooms[]" value="Electric iron">&nbsp;<span class="wpcf7-list-item-label">Electric iron</span></span></span></span>
</p><p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/ventilador.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap fun"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="living_rooms[]" value="Fun">&nbsp;<span class="wpcf7-list-item-label">Fun</span></span></span></span></p>
<p></p>
<p><br>   </p>
<p style="margin:0;  font-family: 'Roboto', sans-serif;
    color: #929292; font-size:16px;">Kitchen</p>
<p></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/microondas.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Microwave"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="kitchen[]" value="Microwave">&nbsp;<span class="wpcf7-list-item-label">Microwave</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/liquidificador.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Blender"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="kitchen[]" value="Blender">&nbsp;<span class="wpcf7-list-item-label">Blender</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/cafeteira.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Coffeemachine"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="kitchen[]" value="Coffee machine">&nbsp;<span class="wpcf7-list-item-label">Coffee machine</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/torradeira.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap toaster"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="kitchen[]" value="Toaster">&nbsp;<span class="wpcf7-list-item-label">Toaster</span></span></span></span></p>
<p style="margin:0;"> <img style="margin-right:10px" src="{{ asset('front/icones/maquinadelavarlouca.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Dishwasher"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="kitchen[]" value="Dishwasher">&nbsp;<span class="wpcf7-list-item-label">Dishwasher</span></span></span></span></p>
<p style="margin:0;"><img style="margin-left:10px;" src="{{ asset('front/icones/utensiliosdecozinha.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Kitchenutensils"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="kitchen[]" value="Kitchen utensils">&nbsp;<span class="wpcf7-list-item-label">Kitchen utensils</span></span></span></span></p>
<p><br></p>
<p style="margin:0;  font-family: 'Roboto', sans-serif;
        color: #929292; font-size:16px;">Bedroom</p>
<p></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/camadecasal.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Doublebedl"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="bedroom[]" value="Double bed">&nbsp;<span class="wpcf7-list-item-label">Double bed</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/camadesolteiro.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Singlebed"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="bedroom[]" value="Single bed">&nbsp;<span class="wpcf7-list-item-label">Single bed</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/beliche.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Bunkbed"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="bedroom[]" value="Bunk bed">&nbsp;<span class="wpcf7-list-item-label">Bunk bed</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/guardaroupa.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap wardrobe"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="bedroom[]" value="Wardrobe">&nbsp;<span class="wpcf7-list-item-label">Wardrobe</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/jogodecama.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap beddingset"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="bedroom[]" value="Bedding set">&nbsp;<span class="wpcf7-list-item-label">Bedding set</span></span></span></span></p>
<p><br></p>
<p style="margin:0;  font-family: 'Roboto', sans-serif;
    color: #929292; font-size:16px;">WC</p>
<p></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/toalha.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap bathtowel"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="wc[]" value="Bath towel">&nbsp;<span class="wpcf7-list-item-label">Bath towel</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/papelhigienico.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap toiletpaper"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="wc[]" value="Toilet paper">&nbsp;<span class="wpcf7-list-item-label">Toilet paper</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/xampu.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Shampoo"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="wc[]" value="Shampoo">&nbsp;<span class="wpcf7-list-item-label">Shampoo</span></span></span></span></p>
<p style="margin:0;"> <img style="margin-right:10px" src="{{ asset('front/icones/secadordecabelo.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap Hair"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="wc[]" value="Hair dryer">&nbsp;<span class="wpcf7-list-item-label">Hair dryer</span></span></span></span></p>
<p><br></p>
<p style="margin:0;  font-family: 'Roboto', sans-serif;
    color: #929292; font-size:16px;">Others:</p>
<p></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/aceitaanimais.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap petsathome"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="others[]" value="Pets at home">&nbsp;<span class="wpcf7-list-item-label">Pets at home</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/garagem.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap garage"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="others[]" value="Garage">&nbsp;<span class="wpcf7-list-item-label">Garage</span></span></span></span></p>
<p style="margin:0;"><img style="margin-right:10px" src="{{ asset('front/icones/churrasqueira.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap barbecuegrill"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="others[]" value="Barbecue grill">&nbsp;<span class="wpcf7-list-item-label">Barbecue grill</span></span></span></span></p>
<p style="margin:0;"> <img style="margin-right:10px" src="{{ asset('front/icones/piscina.png') }}" width="30" height="30"><span class="wpcf7-form-control-wrap pool"><span class="wpcf7-form-control wpcf7-checkbox"><span class="wpcf7-list-item first last"><input type="checkbox" name="others[]" value="Pool">&nbsp;<span class="wpcf7-list-item-label">Pool</span></span></span></span></p>
<p><br><br>
<br></p>





<div class="form-group form_list list_check">
    <input type="checkbox" name="terms" class="form-control" id="terms" <?php echo $required;?> ><span id="termError" role="alert"></span>
    <label for="inlineCheckbox2"><a style="color:#6b6b6b !important; text-decoration:underline !important;" href="{{ url('front/partnershipagreement.pdf') }}" target="_blank">I have read and agree to the Partnership Agreement</a></label>
</div>
  </div>


  

</div><!--f-detail end-->


      				<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 f-detail f-detail2 f-fac2 no-padding">
         
<div style="border:solid 2px #bebebe; border-radius:5px;padding:1em;background-color:#ededed" class="col-sm-12">
<p><img src="{{ asset('front/images/importante.jpg') }}" width="54" height="50"></p>
<p class="text-left" style=" margin:0;">Add only the bathrooms that the guest can use.</p>
<p>Do not include laundry room or areas that is not part of the property.</p>
<p>If your neighbors accept, you can add a swimming pool, Jacuzzi or other common areas that need to be shared amongst other residents.</p>
<p>By adding as many amenities that you have in your property helps the guests to feel at home.</p>
<p>Some hosts offer a coffee or tea. That is not mandatory but sometimes these small details give a special touch helping the guests to feel at home.
</p>
</div>
</div><!--f-detail2 end-->

<div class="clear_both"> </div>
<div class="tab-list-btn tab-list--btn"><button class="tab-list-btn tab-list--btn" type="submit" id="send"> Send </button> </div>

</div>
				</div>

</div>
{!! Form::close() !!}
  </div>

</div><!--container end-->
<script>
    var Errorimg = 'The field is required.';
    $('#next1').click(function () {
        if ($("#full_name").val().length < '1') {
            jQuery('#nameError').html(Errorimg).show();
        }
        if ($("#email").val().length < '1') {
            jQuery('#emailError').html(Errorimg).show();
        }
        if ($("#phone").val().length < '1') {
            jQuery('#phoneError').html(Errorimg).show();
        }
        else
        {
            jQuery('a#next1').fadeIn("slow").attr("href", "#address");
            jQuery('li#add').fadeIn("fast").removeAttr("style");
            jQuery('li#dat').removeClass('active');
            jQuery('li#add').addClass('active');
        }
        
        
    });
    $('#next2').click(function () {
        if ($("#addresss").val().length < '1') {
            jQuery('#addressError').html(Errorimg).show();
        }
        if ($("#city").val().length < '1') {
            jQuery('#cityError').html(Errorimg).show();
        }
        if ($("#state").val().length < '1') {
            jQuery('#stateError').html(Errorimg).show();
        }
        if ($("#zipcode").val().length < '1') {
            jQuery('#zipcodeError').html(Errorimg).show();
        }
        else
        {
            jQuery('a#next2').fadeIn("slow").attr("href", "#details");
            jQuery('li#det').fadeIn("fast").removeAttr("style");
            jQuery('li#add').removeClass('active');
            jQuery('li#det').addClass('active');
        }

    });
    
    $('#next3').click(function () {
        if ($("#category").val().length < '1') {
            jQuery('#categoryError').html(Errorimg).show();
        }
        if ($("#stay").val().length < '1') {
            jQuery('#stayError').html(Errorimg).show();
        }
        if ($("#rooms").val().length < '1') {
            jQuery('#roomsError').html(Errorimg).show();
        }
        if ($("#baths").val().length < '1') {
            jQuery('#bathsError').html(Errorimg).show();
        }
        else
        {
            jQuery('a#next3').fadeIn("slow").attr("href", "#faclities");
            jQuery('li#fac').fadeIn("fast").removeAttr("style");
            jQuery('li#det').removeClass('active');
            jQuery('li#fac').addClass('active');
        }

    });
     $('#send').click(function () {
        if ($("#termError").val().length < '1') {
            jQuery('#termError').html(Errorimg).show();
        }
       

    });

</script>
@endsection