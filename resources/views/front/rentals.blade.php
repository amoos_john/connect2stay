@extends('front')

@section('content')
<?php
use App\Properties;
use App\PropertyAmentity;
use App\Functions\Functions;

$currency= Config::get('params.currency');
$symbol=$currency["BRL"]["symbol"];
?>
@include('front/common/search')
<div class="purple-border"> </div>
<div class="container">

<section>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 list-range">
    <!--<p>Choose a location to enable filtering by price</p>-->
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
<span class="pull-left">Price per night
</span>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
<span class="pull-right" id="amount"><?php echo $symbol;?>120 – <?php echo $symbol;?>1240+</span>
</div>
<div id="slider-range" class="price-range"></div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding margin">
<span class="pull-left"><?php echo $symbol;?>120
</span>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding margin">
<span class="pull-right"><?php echo $symbol;?>1240+</span>
</div>

<div class="all">
@if(count($parents)>0)
@foreach($parents as $row)
@if($row->parent_id==0)
<?php $i=0; ?>
<div class="listing">
    <h3>{{ $row->name }}</h3>
@endif
@if(count($row->child)>0)
      @foreach($row->child as $child)              
      <div class="form-group listing_">  
      <input type="checkbox" onChange="searchTags('<?php echo $child->id;?>')" value="{{ $child->id }}" name="tags[]" class="form-control" id="{{ $child->id }}" />
      <label for="{{ $child->id }}"> {{ $child->name }} <span>({{ PropertyAmentity::countAmenities($child->id,$start_date) }})</span></label>
      </div> 
        <?php $i++; 
        if($i==5)
        {
           break; 
        }
        ?>             
@endforeach
  
 @endif
@if($row->parent_id==0)
</div><!--listing end-->   
 @endif

 @endforeach
 @endif
</div>

</div><!--list-range end-->

<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 list_main">

<!--<h2>No dates entered</h2>
<p>Enter your dates to see available homes and the most accurate prices.</p>
-->

<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 no-padding sort">
	<h4>Sort By:</h4>
<div class="search_dropup recom">

<select class="form-control" onchange="SearchUrl('sort',this.value);" name="sort" id="sort"> 
    <option value="asc">Recommended</option>
    <option value="desc">Recently Added</option>
    <!--<option value="sleep">Sleep(Most)</option>-->

</select>
</div>

</div>

<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12  no-padding sort rsort">

	<h4>Result per page:</h4>
    <div class="search_dropup result" >
       <select class="form-control" onchange="SearchUrl('results_per_page',this.value);" name="results_per_page" id="results_per_page"> 
       
        <option value="10">10</option>
        <option value="20">20</option>
        <option value="30">30</option>        
       </select>
 
    </div>
</div>
<div id="load"></div>
    <div id="search-result">   
        @include('front.result')
    </div>
</div>        
</section>
</div>
<script>
     $( function() {
         
    $( "#slider-range" ).slider({
            range: true,
            min: 120,
            max: 1240,
            values: [120, 1240 ],
            slide: function( event, ui ) {
             $("#amount").text("R$"+ui.values[0] + " - R$" + ui.values[1]);

            },
             stop: function( event, ui ) {
             $("#amount").text(ui.values[0] + "-" + ui.values[1]);
             //SearchUrl("price_min",ui.values[0]);
             $( "#price_min" ).val(ui.values[0]);
             $( "#price_max" ).val(ui.values[1]);
             $( "#amount" ).text( "R$" + $( "#slider-range" ).slider( "values", 0 ) +
    " - R$" + $( "#slider-range" ).slider( "values", 1 ) );  
             RSearchUrl("price_min",ui.values[0],"price_max",ui.values[1]);
             
            }

    });

  

    } );


$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        var url = $(this).attr('href'); 
        var getval = url.split('=');
        var value=getval[1];
        $("html, body").animate({ scrollTop: 0 }, "slow");
        searchPagination(value);
        return false;

    });

});

</script>
@include('front/common/javascript')
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});

</script>
@endsection