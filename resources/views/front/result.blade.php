<?php

use App\Properties;
use App\Functions\Functions;

$currency = Config::get('params.currency');
$symbol = $currency["BRL"]["symbol"];
?>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"> 
    <h3 class='title'> Showing <?php echo $properties->total(); ?> properties </h3>
</div>

@if(count($properties)>0)  

@foreach($properties as $row)
<?php
$rates = Properties::getRates($row->id, $start_date);
?>
@if(count($rates)>0)
<div class="col-lg-12 container-fluid list_box">

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 list_image">
        <a href="{{ url('rentals/allrentals/'.$row->id) }}">
            <img src="{{ asset('/'.$row->url) }}" alt="{{ $row->image_caption }}" class="image-responsive"  > 
        </a>
    </div><!--list_image end--> 

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 list_info">
        <h1>{{ $row->public_headline }}</h1>
        <h3>{{ $row->city }}</h3>
        <div class=" property_img">
            <div class="col-lg-12 col-md-13 col-sm-12 col-xs-12 prop">	  
                <ul class="no-padding">
                    <li class="">
                        <span><img src="{{ asset('front/images/d_2.png')}}" data-toggle="tooltip" title="Sleeps" data-placement="bottom" class="img-responsive" alt="" style="width:50px;"></span>
                        <span>{{ $row->sleeps }}</span>
                    </li>            
                    <li class="">
                        <span><img src="{{ asset('front/images/d_1.png')}}" data-toggle="tooltip" title="Bathrooms" data-placement="bottom" class="img-responsive" alt="" style="width:50px;"></span>
                        <span>{{ $row->bathrooms }}</span>
                    </li>
                    <li class="">
                        <span><img src="{{ asset('front/images/d_3.png')}}"  data-toggle="tooltip" title="Bedrooms" data-placement="bottom" class="img-responsive" alt="" style="width:50px;"></span>
                        <span>{{ $row->bedrooms }}</span>
                    </li>

                </ul> 
            </div>
        </div><!--propert_img end-->
        <span> From: {{ (count($rates)>0)?$symbol.$rates[0]->daily:'' }}/Night </span>
    </div><!--list_image end--> 

</div><!--list box end-->
@endif
@endforeach
@else
<div class="col-lg-12 container-fluid">
    <h1>No Results Found</h1>
    <p>Sorry,  there are no exact matches for this search, If you would like to further refine your search or modify some details, 
        we invite you to use the filters on the top and left side of the page.</p>
</div>
@endif
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  no-padding  rsort">
    <?php echo $properties->render(); ?>
</div>
@include('front/common/javascript')