@extends('front')

@section('content')
<link rel="stylesheet" href="{{ asset('front/style-extra.css')}}">
<link rel="stylesheet" href="{{ asset('front/css/star-rating.css') }}" rel="stylesheet" type="text/css"/>            
<script src="{{ asset('front/js/star-rating.js') }}" type="text/javascript"></script>
<div class="property-heading5_"> {{ $page_title }} </div>
<section>
    <div class="container">
        <div class="contact__form">
            @if(count($model)>0)
            <div id='review_messages'></div>
            <form  method="post" name="reviews_frm" id="reviews_frm">
                <div class="form-group">
                    {!! Form::label('Title:*') !!}
                    {!! Form::text('title', null , array('class' => 'form-control','id' => 'title','required') ) !!}
                </div>
                <div class="clear_both"></div>
                <div class="form-group">

                    {!! Form::label('Name:*') !!}
                    {!! Form::text('reviewed_by', null , array('class' => 'form-control','id' => 'reviewed_by','required') ) !!}
                </div>
                <div class="form-group">

                    {!! Form::label('Rating:*') !!}
                    <div id="example" class='slider-example'>
                        <input type="hidden" id="starRating" name="ratings"  class="rb-rating rating-input" required="" />     

                    </div>    
                </div>
                <div class="clear_both"></div>

                <div class="form-group">
                    {!! Form::label('Comment:*') !!}
                    {!! Form::textarea('comment', null , array('class' => 'form-control textarea','id'=>'comment','required') ) !!}
                </div>  

                <div class="form-group">
                    <button type="button" id="reviews" class="btn btn-default submit__btn">Send</button>
                </div>
            </form>
            <script>
    $(document).ready(function () {
        $("body").on("click", "#reviews", function () {

            if ($("#title").val() == '') {
                alert("Please Fill out all required fields");
                return false;
            } else if ($("#reviewed_by").val() == '')
            {
                alert("Please Fill out all required fields");
                return false;
            } else if ($("#comment").val() == '')
            {
                alert("Please Fill out all required fields");
                return false;
            } else
            {
                var id = '&id=' +<?php echo $token; ?>;
                var token = "&_token=<?php echo csrf_token() ?>";

                $.ajax({
                    type: "POST",
                    url: "{{ url('property/reviews') }}",
                    data: $("#reviews_frm").serialize() + token + id,
                    success: function (data) {
                        if (data == '0')
                        {
                            $("#reviews_frm").find("input[type=text], textarea").val("");
                            var successHtml = '<div class="alert alert-success alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a>';
                            successHtml += "Thank you, your reviews has been submitted."; //showing only the first error.

                            successHtml += '</div>';
                            $('#review_messages').html(successHtml).show();
                            setTimeout(function(){ window.location='<?php echo url('/') ?>'; }, 1000)

                        } else
                        {
                            $("#reviews_frm").find("input[type=text], textarea").val("");
                            var errorsHtml = '<div class="alert alert-danger alert-dismissable"><a href="#" class="pull-right" data-dismiss="alert" aria-label="close">&times;</a>';

                            errorsHtml += data; //showing only the first error.

                            errorsHtml += '</div>';
                            $('#review_messages').html(errorsHtml).show();
                        }

                    },
                    error: function (errormessage) {
                        //you would not show the real error to the user - this is just to see if everything is working
                        alert(errormessage);

                    }
                });

            }


        });
        
        $('.rb-rating').rating({
                'stars': '5',
                'min': '0',
                'max': '5',
                'step': '1',
                'size': 'xs'
            });
    });
</script>
            @else
            <h3 class="title text-center">Token not found!</h3>
            @endif
        </div>
    </div>

</section>
@endsection