@extends('front')

@section('content')
<link rel="stylesheet" href="{{ asset('front/style-extra.css')}}">
 <div class="property-heading5_"> How it works </div>
<section>
<div class="container">
<div class="text-container col-sm-12 pr80 pl80">

<div class="pr40 pl40">
      <h3>Information for Our Guests</h3>
        <br>
<div class="row">
<div class="col-sm-1" style="padding-right:1em;"><p class="iconecomofunciona"><img src="{{ asset('front/images/gadgets.png') }}" id="iconegadgets" width="100%"></p></div>
<div class="col-sm-10">
<p id="gadget" class="text-justify textopagina">WELCOME TO OUR ONLINE PLATFORM!<br>
We are here to provide you with a quality short-term stay with excellent personalized customer service. Our goal is to ensure we connect the right home to the right guest for your ultimate experience.</p><br>
</div>
</div> 
<div class="row">
<div class="col-sm-1" style="padding-right:1em;"><p class="iconecomofunciona"><img src="{{ asset('front/images/placeholder.png') }}" id="iconeplaceholder" width="100%"></p></div>
<div class="col-sm-10">
<p id="placeholder" class="text-justify textopagina">YOUR VACATION – ALL MAPPED OUT!<br>
One thing that travel shoppers like about our website is that it is easy to find the right vacation rental and amenities for their needs. We know that people are not just looking for a place to stay, but an entire vacation experience, and that is why all of our listings are mapped in relation to all the activities, attractions and fun that will meet their personalized needs.
Do we know the area? You bet we do! If you are looking for the inside info on the best attractions and leisure activities, the best restaurants, nightlife and shopping venues, the best of everything – you have come to the right place. Our staff is well prepared to provide you with personalized recommendations and service to ensure a holiday that matches your needs and dreams. We are here to help you experience the perfect vacation. Our goal is to provide customized service leading to greater customer satisfaction which will leave you wanting to vacation with us again and again.</p><br>
 </div>
 </div>
 <div class="row">
<div class="col-sm-1" style="padding-right:1em;"><p class="iconecomofunciona"><img src="{{ asset('front/images/hand.png') }}" id="iconehand" width="100%"></p></div>
<div class="col-sm-10">
<p id="hand" class="text-justify textopagina">BOOKING YOUR HOLIDAY HOME<br>
Have you found a holiday home that meets your expectations? The next step is to send us an inquiry with your travel dates, and we will get back to you with a confirmation in 24 hours.
If you prefer you can also select one of our accommodations where you will instantly get a real-time confirmation of your booking.</p><br>
</div>
</div>
  <div class="row">
<div class="col-sm-1"></div>
<div class="col-sm-10">

<p class="text-justify textopagina">THANK YOU FOR VISITING US!!<br>
We look forward to provide you with your perfect holiday stay and help you in the best possible way.</p>
</div></div>
<br><br>
       </div>
</div>
</div>


</section>
@endsection